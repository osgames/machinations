�
 TMAIN 0S  TPF0TMainMainLeftTopmActiveControlbtExitBorderIconsbiSystemMenu
biMinimize BorderStylebsSingleCaptionPoint-to-Point PathfindingClientHeight/ClientWidthColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterVisible		OnDestroyFormDestroyPixelsPerInch`
TextHeight TLabelLabel1LeftTop Width� HeightCaption1. Create obstaclesFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLabel2LeftTopWidthdHeightCaptionNumber of obstacles:  TLabelLabel3Left� TopWidth@HeightCaptionAverage size:  TLabelLabel4LeftTopHWidth� HeightCaption"2. Position origin and destinationFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLabel5LeftTop`Width�Height!AutoSizeCaption�Position the origin by clicking the right mouse button at the desired location.  Position the destination by clicking the left mouse button.WordWrap	  TLabelLabel6LeftTop� Width� HeightCaption3. Find shortest pathFont.CharsetANSI_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameArial
Font.StylefsBold 
ParentFont  TLabelLabel7LeftTop2Width+HeightCaption
Unit size:  TPanelPanel1Left Top� WidthHeight�AlignalBottom
BevelOuter	bvLoweredTabOrder  TImageimWorldLeftITopWidth�Height�OnMouseDownimWorldMouseDown   TEditedCountLeftpTopWidthAHeightTabOrderText10  TEditedSizeLeftTopWidthAHeightTabOrderText100  TButtonbtCreateObstaclesLeftXTopWidth3HeightCaptionGo!TabOrderOnClickbtCreateObstaclesClick  	TCheckBox
cbVerticesLeft�Top~WidthYHeightCaptionDraw VerticesTabOrderOnClickcbClick  TButton
btFindPathLeft� Top� Width3HeightCaptionGo!TabOrderOnClickbtFindPathClick  TButtonbtExitLeft@Top� WidthKHeightCaptionExitTabOrderOnClickbtExitClick  	TGroupBox	GroupBox2Left�TopWidth� HeightuCaptionResultsTabOrder TLabellaPathFoundLeftTopWidthHeight  TLabellaSetupTimeLeftvTop$WidthHeight	AlignmenttaRightJustify  TLabellaAStarTimeLeftvTop4WidthHeight	AlignmenttaRightJustify  TLabel
laVerticesLeftvTopDWidthHeight	AlignmenttaRightJustify  TLabelLabel10LeftTop$Width9HeightCaptionSetup Time:  TLabelLabel11LeftTop4Width(HeightCaptionA* Time:  TLabellaEdgesLeftvTopTWidthHeight	AlignmenttaRightJustify  TLabelLabel12LeftTopDWidth)HeightCaption	Vertices:  TLabelLabel13LeftTopTWidth!HeightCaptionEdges:  TLabellaNodesLeftvTopdWidthHeight	AlignmenttaRightJustify  TLabelLabel8LeftTopdWidth"HeightCaptionNodes:   	TCheckBoxcbEdgesLeft�Top� WidthYHeightCaption
Draw EdgesTabOrderOnClickcbClick  TEdit
edUnitSizeLeftpTop0WidthAHeightTabOrder	Text0   