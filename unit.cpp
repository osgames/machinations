/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* UNIT.CPP:     Manages unit types, units, and unit explosions.             *
*               Draws and controls units.                                   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "unit.h"
#include "message.h"
#include "client02.h"
#include "oglwrap.h"
#include "font.h"
#include "behavior.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif
#pragma package(smart_init)

static vector<tUnitType *>  unitTypes;              //Unit type container
static eUnitSize parseSize(const char *fieldName, tStream& s) throw(tString);

/****************************************************************************
                              Command Table
 ****************************************************************************/
// FORMAT:
//               {Name,                 Selection Type,     UI Cmd, Immed,  Repeat, Benev}
//                                                                              (PARAM_UNIT only)
tCommandType commandTable[MAX_COMMANDS]={
    tCommandType("none",                PARAM_NONE,         false,  false,  false,  false),

    tCommandType("move",                PARAM_POSITION,     false,  false,  false,  false),
    tCommandType("attack",              PARAM_UNIT,         false,  false,  false,  false),
    tCommandType("follow",              PARAM_UNIT,         false,  false,  false,  true),
    tCommandType("patrol",              PARAM_POSITION,     false,  false,  true,   false),
    tCommandType("stop",                PARAM_NONE,         false,  true,   false,  false),

    tCommandType("wander",              PARAM_NONE,         false,  true,   false,  false),
    tCommandType("maneuver",            PARAM_NONE,         false,  true,   false,  false),
    tCommandType("hold_position",       PARAM_NONE,         false,  true,   false,  false),
    tCommandType("evade",               PARAM_NONE,         false,  true,   false,  false),
    tCommandType("disband",             PARAM_NONE,         true,   true,   false,  false),

    tCommandType("delete",              PARAM_NONE,         false,  true,   false,  false),
    tCommandType("self_destruct",       PARAM_NONE,         false,  true,   false,  false),

    tCommandType("cancel_production",   PARAM_NONE,         false,  true,   false,  false),
    tCommandType("fire_at_will",        PARAM_NONE,         false,  true,   false,  false),
    tCommandType("return_fire",         PARAM_NONE,         false,  true,   false,  false),
    tCommandType("hold_fire",           PARAM_NONE,         false,  true,   false,  false),

    tCommandType("auto_repair",         PARAM_NONE,         false,  true,   false,  false),
    tCommandType("manual_repair",       PARAM_NONE,         false,  true,   false,  false),

    tCommandType("repair",              PARAM_UNIT,         false,  false,  false,  true ),
    tCommandType("disassemble",         PARAM_UNIT,         false,  false,  false,  true ),
    tCommandType("divide",              PARAM_NONE,         false,  true,   false,  false),
    tCommandType("cancel_divide",       PARAM_NONE,         false,  true,   false,  false),

    tCommandType("collection_on",       PARAM_NONE,         false,  true,   false,  false),
    tCommandType("collection_off",      PARAM_NONE,         false,  true,   false,  false),
    tCommandType("production_on",       PARAM_NONE,         false,  true,   false,  false),
    tCommandType("production_off",      PARAM_NONE,         false,  true,   false,  false),
    tCommandType("detection_on",        PARAM_NONE,         false,  true,   false,  false),
    tCommandType("detection_off",       PARAM_NONE,         false,  true,   false,  false)

	/* The remaining entries are initialized to zero */
};

//Number of commands defined in the current world:
int                  commandCount=PREDEFINED_COMMANDS;

/****************************************************************************/
// COMMAND
/****************************************************************************/
tCommand::tCommand(const tCommandMsg& msg) : command(msg.command),
    paramType(PARAM_NONE), pos(msg.pos), targets(msg.targets),
    targetCount(msg.targetCount), quantity(msg.quantity), repeat(msg.repeat)
{
    if(targets)
    {
        targets=mwNew long[targetCount];
        memcpy(targets,msg.targets,targetCount*sizeof(long));
    }
    if(msg.type==MSG_COMMAND_POS)
        paramType=PARAM_POSITION;
    else if(msg.type==MSG_COMMAND_UNIT)
        paramType=PARAM_UNIT;
    else if(msg.type==MSG_COMMAND_PRODUCTION)
        paramType=PARAM_PRODUCTION;
}
/****************************************************************************/
tCommand::tCommand(const tCommand& x) : targets(NULL)
{
    operator=(x);
}
/****************************************************************************/
tCommand::tCommand(eCommand _command, eParamType _paramType) : command(_command),
    paramType(_paramType), targets(NULL), targetCount(0)
{
}
/****************************************************************************/
tCommand::tCommand(eCommand _command, eParamType _paramType, tVector2D _pos) :
    command(_command), paramType(_paramType), pos(_pos), targets(NULL), targetCount(0)
{
}
/****************************************************************************/
tCommand::tCommand(eCommand _command, eParamType _paramType, long target) :
    command(_command), paramType(_paramType), targetCount(1)
{
    targets = mwNew long[1];
    *targets = target;
}
/****************************************************************************/
tCommand::tCommand(eCommand _command, eParamType _paramType, int _quantity,
    bool _repeat) : command(_command), paramType(_paramType), targets(NULL),
    targetCount(0), quantity(_quantity), repeat(_repeat)
{
}
/****************************************************************************/
tCommand& tCommand::operator=(const tCommand& x)
{
    if(targets)
        mwDelete[] targets;

    memcpy(this,&x,sizeof(*this));

    if(targets)
    {
        targets=mwNew long[targetCount];
        memcpy(targets,x.targets,targetCount*sizeof(long));
    }
    return *this;
}
/****************************************************************************/
tCommand::~tCommand()
{
    if(targets)
        mwDelete[] targets;
}
/****************************************************************************/
// UNIT TYPE
/****************************************************************************/
//Defaults for unit type if not specified in unit type file:
tUnitType::tUnitType(tStream& s) throw(tString): ID(0), modelType(NULL), HP(0),
    size(SIZE_MEDIUM), canMove(false), vMax(1), aMax(1), isBuilding(false),
    domain(DOMAIN_GROUND), turnRate(0), buildRate(0), onBirthProc(NULL), onDeathProc(NULL),
	onConceptionProc(NULL), elevationArg(0,BOUNDARY_RANGE,-10,10),
	modelOpacityArg(1,BOUNDARY_RANGE,0,1), alwaysVisibleArg(true,BOUNDARY_NONE) 
{
    int i;
    float buildTime=10;

    try
    {
        for(i=0;i<MAX_RESOURCES;i++)
            costs[i]=0;
        for(i=0;i<MAX_PLAYERS;i++)
            repairable[i]=false;
        for(i=0;i<MAX_ARMOR_CLASSES;i++)
            armor[i]=0;
        buildHotKey[0]=0;
        buildHotKey[1]=0;

		name=parseString("unit type name", s);
		name=STRTOLOWER(name);
		if(unitTypeLookup(name, /*exact=*/true)!=NULL)
			throw tString("A unit by the name of '")+name+"' already exists";

		if(s.beginBlock())
		{
			while(!s.endBlock())
			{
				tString str=parseString("tag",s);
				s.matchOptionalToken("=");

				if(strPrefix(str,"caption")) //Preserve capitalization in caption
					caption=parseString("unit type caption", s);
				else if(strPrefix(str,"model_type"))
					modelType=parseModelType("model type name", s);
				else if(strPrefix(str,"hitpoints")||strPrefix(str,"hp"))
					buildRate=HP=parseFloatMin("hit points",0,s);
				else if(strPrefix(str,"size"))
					size=parseSize("unit size",s);
				else if(strPrefix(str,"can_move"))
					canMove=parseBool("can move",s);
				else if(strPrefix(str,"max_velocity"))
				{
					if(canMove==false)
						throw tString("Please set 'can_move' to true before you define the maximum velocity");
					vMax=parseFloatMin("maximum velocity", .1, s);
				}
				else if(strPrefix(str,"max_acceleration"))
				{
					if(canMove==false)
						throw tString("Please set 'can_move' to true before you define the maximum acceleration");
					aMax=parseFloatMin("maximum acceleration", .1, s);
				}
				else if(strPrefix(str,"building"))
					isBuilding=parseBool("is building", s);
				else if(strPrefix(str,"domain"))
					domain=parseDomain("domain", s);
				else if(strPrefix(str,"elevation"))
				{
					validateModelType(modelType);
					if(strPrefix(s.peekAhead(), "seafloor"))
					{
						parseString("seafloor",s); //Discard the token
						elevationArg.setConstant(SEA_FLOOR_DEPTH);
					}
					else
						elevationArg.load(modelType,s);
				}
				else if(strPrefix(str,"model_opacity"))
				{
					validateModelType(modelType);
					modelOpacityArg.load(modelType,s);
				}
				else if(strPrefix(str,"always_visible"))
				{
					validateModelType(modelType);
					alwaysVisibleArg.load(modelType,s);
				}
				else if(strPrefix(str,"turn_rate"))
					turnRate=parseFloatRange("turn rate",0,100,s);
				else if(strPrefix(str,"cost"))
					parseResourceBlock(costs,s);
				else if(strPrefix(str,"build_time"))
				{
					//buildTime cannot be zero since it will cause subsequent divide by zero errors
					buildTime=parseFloatMin("build time",.1,s);
				}
				else if(strPrefix(str,"armor"))
				{
					i=parseArmorClass("armor class name",s);
					armor[i]=parseFloatMin("armor strength",0,s);
				}
				else if(strPrefix(str,"behavior"))
				{
					tBehaviorType *st = parseBehaviorTypeBlock(this,s);
					if(st)
						behaviorTypes.push_back(st);
				}
				else if(strPrefix(str,"on_birth"))
				{
					validateModelType(modelType);
					onBirthProc=modelType->parseProcedureType("procedure name",s);
				}
				else if(strPrefix(str,"on_death"))
				{
					validateModelType(modelType);
					onDeathProc=modelType->parseProcedureType("procedure name",s);
				}
				else if(strPrefix(str,"on_conception"))
				{
					validateModelType(modelType);
					onConceptionProc=modelType->parseProcedureType("procedure name",s);
				}
				else if(strPrefix(str,"ability_switch"))
					parseAbilitySwitch(s);
				else if(strPrefix(str,"build_description"))
					buildDesc=parseString("build description",s);
				else if(strPrefix(str,"build_hotkey"))
				{
					buildHotKey[0]=parseHotKey("hot key #1",s);
					if(s.hasOptionalParameter())
					{
						s.matchOptionalToken(",");
						buildHotKey[1]=parseHotKey("hot key #2",s);
						if(s.hasOptionalParameter())
							throw tString("Currently, only two hot keys are supported");
					}
				}
				else
					throw tString("Unrecognized tag '")+str+"'";

				s.endStatement();
			}
		}

        if(modelType==NULL)
            throw tString("You need to associate this unit type with a model type!");
		if(caption=="")
			caption=name;

        //Do this at the end so that the player can define costs, hitpoints, and
        //build time in any order.
        buildRate/=buildTime;
        for(i=0;i<MAX_RESOURCES;i++)
            costs[i]/=buildTime;
    }
    catch(const tString& s)
    {
        destroy();
        throw s;
    }
}
void tUnitType::destroy()
{
    for(int i=0;i<behaviorTypes.size();i++)
        if(behaviorTypes[i])
            mwDelete behaviorTypes[i];
}
void tUnitType::parseAbilitySwitch(tStream& s) throw(tString)
{
    tAbilitySwitchType as;
    as.ability=parseAbility("ability name",s);

    s.matchOptionalToken("=");

    validateModelType(modelType);
    as.argumentType.load(modelType,s);

    abilitySwitchTypes.push_back(as);
}
/****************************************************************************/
// UNIT
/****************************************************************************/
//Update position, animation, etc. (called before each frame)
void tUnit::tick(tWorldTime t)
{
    int count;
    tTileInfo tileInfo;

    float dt=t-t0;

    if(state<STATE_SELECTABLE)
        return;

	model->tick(t);

    flashOn=false;
    if(flashing)
    {
        count=(int)((currentTime-flashTimer)*5);
        if(count>=5)
            flashing=false;
        else if(count%2==0)
            flashOn=true;
    }

	if(tractorBeamParent)
	{
		//This is a reasonable approximation of the unit's velocity if the tractor
		//beam origin doesn't move very much.  We must calculate the velocity in
		//this fashion since tractorBeamParent->velocity is not synchronized.
		velocity=tractorBeamParent->v0 + tractorBeamParent->acceleration *
			(t-tractorBeamParent->t0);
	
		if(tractorBeamBasis)
		{
			tractorBeamParent->locateReferenceFrame(tractorBeamBasis,position3D,
													forward3D,side3D,up3D);
			forward=to2D(forward3D).normalize();
		}
		else if(tractorBeamOrigin)
		{
			//We must 'tick' the models before the units in order for the position below
			//to remain synchronized.
			position3D=tractorBeamParent->locatePoint(tractorBeamOrigin);
			forward=f0;
		}
		position3D+=tractorBeamOffset;
		position=to2D(position3D);
	}
	else
	{
		velocity=v0+acceleration*dt;
		position=p0+(v0+acceleration/2*dt)*dt;
	}

    speed=velocity.length();
	moving=(speed>0.001); //fudge factor

	if(tractorBeamParent==NULL)
	{
		if(moving) 
			forward=velocity.normalize();
		else
			forward=f0;
		distance=d0+speed*dt;
	}
    
	if(!(tractorBeamParent && tractorBeamBasis))
	{
		if(terrain)
		{
			tileInfo=terrain->getTileInfo(position,getDomain(),getElevation());
			position3D.x=position.x;
			position3D.y=position.y;
			position3D.z=tileInfo.height;
			up3D=tileInfo.normal; //ensure that up3D never becomes 0
			//up3D.z*=2;
			up3D=up3D.normalize();
			side3D=up3D.cross(to3D(forward)).normalize();
			forward3D=side3D.cross(up3D);
		}
		else
		{
			//Default routine in case terrain does not exist:
			position3D=to3D(position);
			forward3D=to3D(forward);
	
			side3D=kHat3D;
			side3D=side3D.cross(forward3D).normalize();
			up3D=forward3D.cross(side3D).normalize();
		}
	}

    HP=h0+deltaHP*dt;
    clampRange(HP,0.0f,getMaxHP());
}
/****************************************************************************/
tVector2D tUnit::interceptPoint(tVector2D p, float range)
{
    tVector2D offset=p-position;
    float length=offset.length();

    if(length<range)
        return zeroVector2D;

    return offset.normalize()*getMaxVelocity();
}
/****************************************************************************/
tVector2D tUnit::seekPoint(tVector2D p, float range)
{
    tVector2D offset=p-position;
    float length=offset.length();

    if(length<range)
        return zeroVector2D;

    tVector2D unitOffset=offset.normalize();

    if(length-range>getMaxVelocity())
        return unitOffset*getMaxVelocity();
    else
        return unitOffset*(length-range);

}
/****************************************************************************/
tVector2D tUnit::avoidPoint(tVector2D p, float range)
{
    tVector2D offset=position-p;
    float length=offset.length();

    if(length>range)
        return zeroVector2D;

    tVector2D unitOffset=offset.normalize();

    if(range-length>getMaxVelocity())
        return unitOffset*getMaxVelocity();
    else
        return unitOffset*(range-length);
}
/****************************************************************************/
tVector2D tUnit::fleePoint(tVector2D p)
{
    tVector2D offset=position-p;
    tVector2D desired=offset.normalize()*getMaxVelocity();
    return desired;
}
/****************************************************************************/
tVector2D tUnit::brake()
{
    return zeroVector2D;
}
/****************************************************************************/
tVector2D tUnit::pursueUnit(tUnit *u, float range, bool overshoot)
{
#ifdef DEBUG
    if(getMaxVelocity()<=0)
    {
        bug("tUnit::pursueUnit: invalid maxVelocity");
        return zeroVector2D;
    }
#endif
    tVector2D offset=u->position-position;
    tVector2D ohat=offset.normalize();
    float scalar=(ohat.dot(u->forward)+.5)/2;
    float time=offset.length()/getMaxVelocity()*scalar;
    if(overshoot)
        return interceptPoint(u->position+u->velocity*time, range);
    else
        return seekPoint(u->position+u->velocity*time, range);
}
/****************************************************************************/
tVector2D tUnit::avoidUnit(tUnit *u, float range)
{
    return avoidPoint(u->position+u->velocity/2, (range*2+u->velocity.length())/2);
}
/****************************************************************************/
tVector2D tUnit::evadeUnit(tUnit *u)
{
    return fleePoint(u->position);
}
/****************************************************************************/
tVector2D tUnit::wander(float dt)
{
    float rate=0.1 * dt;
    wanderSide += ((rand()%2049)/1024.0 - 1) * rate;
    clampRange(wanderSide, -1.0f, 1.0f);
    tVector2D desired=getSide() * wanderSide + forward;
    return desired.normalize()*getMaxVelocity();
}
/****************************************************************************/
void tUnit::intervalTick(tWorldTime t)
//Update orders, movement, etc. (called at each game interval)
//This function MUST be synchronized on all computers
//Precondition: all units must be ticked at time t
//this function must be called simultaneously (with respect to game time) on all computers!!
{
    tVector2D desired=zeroVector2D;
    bool separationBehavior=true;
    tUnit *u=NULL;
    tVector2D v;
    float d, range;
    bool overshoot;
    int i;
    ePriority priority,curPriority=PRIORITY_NONE;

#ifdef DEBUG
    if(gameInterval<=0)
    {
        bug("tUnit::intervalTick: invalid gameInterval");
        return;
    }
    if(gameSpeed<=0)
    {
        bug("tUnit::intervalTick: invalid gameSpeed");
        return;
    }
#endif

    if(state<STATE_SELECTABLE)
        return;

	model->intervalTick(t);
    for(i=0;i<behaviors.size();i++)
        behaviors[i]->intervalTick(t);
    if(curCommand==COMMAND_STOP && commands.size()>0) //There are more commands waiting to be processed.
        loadNextCommand(t);

    if(movementMode==COMMAND_HOLD_POSITION || !isAbilityEnabled(ABILITY_MOVE))
    {
        desired = brake();
        separationBehavior=false;
        isAnchored=false;
    }
    else if(attacker && movementMode==COMMAND_EVADE)
    {
        range=MAX(getAttackRange()*.9,attacker->getAttackRange()*1.1); //10% cushion on your range and attacker's range
        desired = avoidUnit(attacker, range);
        isAnchored=false;
    }
    else
    {
        for(i=0;i<behaviors.size();i++)
        {
            priority=behaviors[i]->getLeader(u,d,overshoot);
            if(priority>curPriority)
            {
                curPriority=priority;
                desired=pursueUnit(u,d,overshoot);
                isAnchored=false;
            }
            priority=behaviors[i]->getDestination(v,d,overshoot);
            if(priority>curPriority)
            {
                curPriority=priority;
                if(overshoot)
                    desired=interceptPoint(v,d);
                else
                    desired=seekPoint(v,d);
                isAnchored=false;
            }
        }

        if(curPriority==PRIORITY_NONE || curPriority==PRIORITY_SECONDARY && movementMode!=COMMAND_WANDER)
        {
            if(isAnchored)
                desired = seekPoint(origin, 0);
            else
            {
                desired = brake();
                if(moving)
                    separationBehavior=false;
                else
                {
                    origin=position;
                    isAnchored=true;
                }
            }
        }
    }

    //desired = collisionAvoidance(this, desired);

    if(separationBehavior)
        desired += separation(this, 10, 0.707);

    //watch out for the edge of the screen!
    //keep unit within rectangle [r,w-r]x[r,h-r] where r is the unit's radius
    tVector2D p=position+desired;
    makeValid(p,model->getMaxRadius());
    desired=p-position;

    /*                        Velocity                        Velocity
     *                 Boundary .|   Boundary           Boundary |  Boundary
     *                     \   . |     /                    \    |    /
     *                      \ .  |    /        turnRate -->  \___|___/
     *  Adjusted Desired --> *   |   /                        \  |  /
     *                      . \__|__/     Adjusted Desired --> *.|./
     *                     .   \ | / <-- turnRate             . \|/ .
     *                  Accel   \|/                           .  *--. <-- 0.01
     *                   .      /                               .|.
     *                  .     /                                  |
     *                 .    /                                  Accel
     *                . Desired                                  |
     *               .  /                                        |
     *              . /                                          |
     *             ./                                         Desired
     *
     * If the user specifies a non-zero turnRate, the unit will revolve at a
     * fixed rate according to this algorithm.  If the unit is not moving, it
     * will receive a small impulse in its forward direction that lasts one game
     * interval.  If the unit is moving, I calculate two boundaries that flank
     * the velocity vector at an angle determined by turnRate.  I then calculate
     * the expected acceleration (by subracting velocity from desired) and find
     * the point where the acceleration crosses one of the boundaries.  The
     * distance between the center at this point is length of the adjusted
     * desired vector.  I multiply this distance by the appropriate boundary
     * vector to find the adjusted desired vector.  NOTE: The length of this
     * vector must be at least 0.01 (see second diagram).
     */
    if(getTurnRate()>0 && desired.length()>0.01)
    {
        if(speed<0.001)
            desired=forward*0.01;
        else
        {
            //Adjust the turn rate using the same formula as acceleration:
            float turnRate=getTurnRate()*gameInterval*gameSpeed/1000.0;

            tVector2D dn=desired.normalize();
            float cosAngle=forward.dot(dn);
            float cosTurnRate=cos(turnRate);
            float sinTurnRate=sin(turnRate);

            if(cosAngle<cosTurnRate) //outside of valid turning range
            {
                tVector2D w;
                float sinAngle=forward.cross(dn);
                if(sinAngle>0) //acceleration is CCW of velocity vector
                {
                    w.x=cosTurnRate*forward.x-sinTurnRate*forward.y; //rotate forward CCW by TURN_RATE
                    w.y=sinTurnRate*forward.x+cosTurnRate*forward.y;
                }
                else //acceleration is CW of velocity vector
                {
                    w.x=cosTurnRate*forward.x+sinTurnRate*forward.y; //rotate forward CW by TURN_RATE
                    w.y=-sinTurnRate*forward.x+cosTurnRate*forward.y;
                }

                //Solve the following simultaneous equations:
                //Vx + Ax*t = Wx*s  =>  -Wx*s + Ax*t + Vx = 0
                //Vy + Ay*t = Wy*s  =>  -Wy*s + Ay*t + Vy = 0
                float a=-w.x, b=desired.x-velocity.x, c=velocity.x;
                float d=-w.y, e=desired.y-velocity.y, f=velocity.y;
                float denominator=a*e-b*d;
                if(denominator!=0)
                {
                    float s=(b*f-c*e)/denominator;
                    if(s<0.01)
                        s=0.01;
                    desired=w*s;
                }
            }
        }
    }
    else if(getTurnRate()>0)
        desired=zeroVector2D;

    /* Calculate steering */
    tVector2D steering = desired - velocity;

    acceleration=steering/gameInterval/gameSpeed*1000.0;

    //Assume that vehicle has thrusters on front, back, and sides--each of these thrusters can
    //propel the vehicle at a maximum acceleration of getMaxAccel()
    //Also assume that vehicle has ideal brakes which can theoretically stop the vehicle instantly

    /*                          Acceleration Domain:
     *
     *                            .   ' | '   .          ---
     *                        .         |         .       |
     *                                  |
     *                      '        Forward        ' Max Accel.
     *                                  |
     *                     '                         '    |
     *                     ----Left---- # ---Right----   ---
     *                     .                         .    |
     *                     .            |            .  Speed
     *                     .          Back           .    |
     *                     .............|.............   ---
     *                     '            |            '    |
     *                                  |
     *                      .           |           . Max Accel.
     *                                  |
     *                        '         |         '       |
     *                            '   . | .   '          ---
     *
     *                     |<Max Accel.>|<Max Accel.>|
     */

    tVector2D vn=velocity.normalize();
    tVector2D vb=vn.perpendicularCW();

    float yc=acceleration.dot(vn);
    if(yc>=0)   //Acceleration vector pointing forward
    {
        if(acceleration.length()>getMaxAccel())
            acceleration=acceleration.normalize()*getMaxAccel();
    }
    else if(yc<=-speed) //Acceleration vector pointing backward
    {
        tVector2D a=acceleration+velocity;
        if(a.length()>getMaxAccel())
            a=a.normalize()*getMaxAccel();
        acceleration=a-velocity;
    }
    else    //Acceleration vector pointing to the side
    {
        float xc=acceleration.cross(vn);
        clampRange(xc, -getMaxAccel(), getMaxAccel());
        acceleration=vn*yc+vb*xc;
    }

    if(fieldOfView>0)
    {
        int x1=(int)p0.x, y1=(int)p0.y;
        int x2=(int)position.x, y2=(int)position.y;
        if(x1!=x2 || y1!=y2)
        {
            concealCircle(owner,x1,y1,(int)fieldOfView);
            revealCircle(owner,x2,y2,(int)fieldOfView);
        }
    }
    transferValues(t); //store initial values for position, velocity, etc. and update t0
    calculateHash();

    int switchCount=type->abilitySwitchTypes.size();
    bool b;
    for(i=0;i<switchCount;i++)
    {
        tAbilitySwitchType& ast=type->abilitySwitchTypes[i];
        bool& as=abilitySwitches[i];
        b=ast.argumentType.evaluate(model);
        if(as && !b)
            disableAbility(ast.ability);
        else if(!as && b)
            enableAbility(ast.ability);
        as=b;
    }
}
/****************************************************************************/
void tUnit::processCommand(const tCommand& c, tWorldTime t)
//Process a message received from engine
//precondition: map must be ticked at time message->executiontime
{
    if(state<STATE_SELECTABLE) //destroyed units can't receive message--they are frozen
        return;

#ifdef DEBUG
    if(c.command<0 || c.command>=commandCount)
    {
        bug("tUnit::processCommand: invalid command identifier");
        return;
    }
#endif
    if(!canExecute(c.command))
        return;

    if(commandTable[c.command].immediate) //generate event immediately
    {
        onCommand(c, t);
        return;
    }

    commands.push_back(c);
    if(curCommand==COMMAND_STOP) //We aren't executing any commands right now.
        loadNextCommand(t);
}
/****************************************************************************/
//Draw unit's visibility circle
void tUnit::preDraw()
{
    int count;

    if(state < STATE_SELECTABLE)
        return;

    //This routine must go in tick and draw!
    flashOn=false;
    if(flashing)
    {
        count=(int)((currentTime-flashTimer)*5);
        if(count>=5)
            flashing=false;
        else if(count%2==0)
            flashOn=true;
    }

    glPushMatrix();
    glTranslatef(position3D.x,position3D.y,position3D.z);
    //Translate this vector with forward3D, side3D, and up3D
    GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
        side3D.x,side3D.y,side3D.z,0,
        up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
    glMultMatrixf(orientation);

    if(getElevation()==0 && (selected && !flashOn || !selected && flashOn))
    {
        glColor3ubv((const unsigned char *) &allianceColors[getAlliance(owner)]);
        gluDisk(globalQuadricObject, model->getSelRadius(), model->getSelRadius()+.1, 20, 1);
    }
    if(fieldOfView>0)
    {
        glEnable(GL_BLEND);
        glDepthMask(false);

        glColor4f(1,1,1,.2);
        //mglFuzzyDisk(MAX(fieldOfView-3,0),fieldOfView,30);
        gluDisk(globalQuadricObject,0,fieldOfView,30,1);
        glDepthMask(true);
        glDisable(GL_BLEND);
    }
    glPopMatrix();
}
/****************************************************************************/
//Draw unit's model and selection circle
void tUnit::draw()
{
    float mp;
    int flags=0;

    if(state < STATE_SELECTABLE)
        return;

    glEnable(GL_LIGHTING);
    glPushMatrix();
    glTranslatef(position3D.x,position3D.y,position3D.z);
    //Translate this vector with forward3D, side3D, and up3D
    GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
        side3D.x,side3D.y,side3D.z,0,
        up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
    glMultMatrixf(orientation);
    if(getElevation()>0 && (selected && !flashOn || !selected && flashOn))
    {
        glColor3ubv((const unsigned char *) &allianceColors[getAlliance(owner)]);
        gluDisk(globalQuadricObject, model->getSelRadius(), model->getSelRadius()+.1, 20, 1);
    }

    mp=getModelProgress();

    if(state==STATE_ALIVE)
        flags|=DRAW_TEXTURE|DRAW_MODEL;
    if(state==STATE_SELECTABLE)
        flags|=DRAW_GREY_WIREFRAME;
    else if(!built)
        flags|=DRAW_GREEN_WIREFRAME;

    if(getModelOpacity()==1)
    {
        glColor3f(1,1,1);
        model->draw(mp, flags);
    }

    glPopMatrix();
    glDisable(GL_LIGHTING);
}
/****************************************************************************/
void tUnit::drawPickShell(bool convexShell)
{
    int flags;

    if(state < STATE_SELECTABLE)
        return;

    glPushMatrix();
    glTranslatef(position3D.x,position3D.y,position3D.z);
    //Translate this vector with forward3D, side3D, and up3D
    GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
        side3D.x,side3D.y,side3D.z,0,
        up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
    glMultMatrixf(orientation);

    if(convexShell)
        flags=DRAW_CONVEX_SHELL;
    else
        flags=DRAW_MODEL;

    model->draw(1, flags);

    glPopMatrix();
}
/****************************************************************************/
//Engine draws overlays after all units have been drawn
void tUnit::postDraw()
{
    int i;
    float mp,mo;

    if(state < STATE_SELECTABLE)
        return;

    mp=getModelProgress();
    mo=0.5+getModelOpacity()/2;

    if(mo<1)
    {
        glEnable(GL_LIGHTING);
        glPushMatrix();
        glTranslatef(position3D.x,position3D.y,position3D.z);
        //Translate this vector with forward3D, side3D, and up3D
        GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
            side3D.x,side3D.y,side3D.z,0,
            up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
        glMultMatrixf(orientation);

        glEnable(GL_BLEND);
        glDepthMask(false);

        glColor4f(1,1,1,mo);

		int flags=0;
		if(state==STATE_ALIVE)
			flags|=DRAW_TEXTURE|DRAW_MODEL;
		if(state==STATE_SELECTABLE)
			flags|=DRAW_GREY_WIREFRAME;
		else if(!built)
			flags|=DRAW_GREEN_WIREFRAME;
        model->draw(mp, flags);

        glDepthMask(true);
        glDisable(GL_BLEND);

        glPopMatrix();
        glDisable(GL_LIGHTING);
    }

    glColor3f(1,1,1);
    for(i=0;i<behaviors.size();i++)
        behaviors[i]->postDraw();

    if(shockwaveTimer>0)
    {
        float dt=(currentTime-shockwaveTimer)*2;
        float radius=dt*15;
        float opacity=(1-dt)*2;
        clampRange(opacity,0.0f,1.0f);
        
        glPushMatrix();
        glTranslatef(position3D.x,position3D.y,position3D.z);
        //Translate this vector with forward3D, side3D, and up3D
        GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
            side3D.x,side3D.y,side3D.z,0,
            up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
        glMultMatrixf(orientation);

        glEnable(GL_BLEND);
        glDisable(GL_DEPTH_TEST);
        glDepthMask(false);
        glBlendFunc(GL_DST_COLOR, GL_ONE);
        glColor4f(opacity,opacity,opacity,opacity);
        if(shockwaveSkin)
        {
            glEnable(GL_TEXTURE_2D);
            bindSkin(shockwaveSkin);
        }
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);
            glVertex2f(-radius,-radius);
            glTexCoord2f(1,0);
            glVertex2f(radius,-radius);
            glTexCoord2f(1,1);
            glVertex2f(radius,radius);
            glTexCoord2f(0,1);
            glVertex2f(-radius,radius);

            glTexCoord2f(0,0);
            glVertex2f(-radius,-radius);
            glTexCoord2f(1,0);
            glVertex2f(radius,-radius);
            glTexCoord2f(1,1);
            glVertex2f(radius,radius);
            glTexCoord2f(0,1);
            glVertex2f(-radius,radius);
        glEnd();
        glDisable(GL_TEXTURE_2D);

        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glDepthMask(true);
        glEnable(GL_DEPTH_TEST);
        glDisable(GL_BLEND);

        glPopMatrix();

        if(dt>1)
            shockwaveTimer=-1;
    }
}
/****************************************************************************/
void tUnit::drawOverlay()
{
    int x1,y1,x2,y2,x3,y3;
    int px,py;
    float ratio;
    char buf[100];

    float cosr=cos(-mapRotation)*model->getSelRadius();
    float sinr=sin(-mapRotation)*model->getSelRadius();
    if(selected && state==STATE_ALIVE && getMaxHP()>0)
    {
        tVector3D p1,p2,p3;
        p1=p2=p3=position3D+up3D*model->getMaxZ();
        p1.x-=cosr;     p1.y-=sinr;
        p2.x+=cosr;     p2.y+=sinr;
        p3.x+=-sinr;    p3.y+=cosr;
        vector3DToPixel(p1,x1,y1);
        vector3DToPixel(p2,x2,y2);
        vector3DToPixel(p3,x3,y3);

        y1=y3;
        y2=y1+6;

        glBegin(GL_QUADS);
            glColor3f(.5,.5,.5);
            glVertex2i(x1,y1);
            glVertex2i(x2,y1);
            glVertex2i(x2,y2);
            glVertex2i(x1,y2);

            glColor3f(0,0,0);
            glVertex2i(x1+1,y1+1);
            glVertex2i(x2-1,y1+1);
            glVertex2i(x2-1,y2-1);
            glVertex2i(x1+1,y2-1);

            ratio=HP/getMaxHP();
            clampRange(ratio,0.0f,1.0f);
            x2=x1+(int)((x2-x1)*ratio);
            if(ratio<.5)
                glColor3f(1,ratio*2,0);
            else
                glColor3f(2-ratio*2,1,0);
            glVertex2i(x1,y1);
            glVertex2i(x2,y1);
            glVertex2i(x2,y2);
            glVertex2i(x1,y2);
        glEnd();
    }
    if(group>=0)
    {
        vector3DToPixel(position3D,px,py);
        snprintf(buf,sizeof(buf),"%d",group);
        px-=stringWidth(buf,smallFont)/2;
        glColor3f(.5,.5,.5);
        drawString(buf,px+1,py,smallFont);
        glColor3f(1,1,1);
        drawString(buf,px,py+1,smallFont);
    }
    if(!built)
    {
        tVector3D p3=position3D;
        p3.x+=-sinr;
        p3.y+=cosr;

        vector3DToPixel(p3,px,py);
        snprintf(buf, sizeof(buf), "Future %s", CHARPTR(type->caption));
        px-=stringWidth(buf,smallFont)/2;
        py-=15;
        glColor3f(.5,.5,.5);
        drawString(buf,px+1,py,smallFont);
        glColor3f(1,1,1);
        drawString(buf,px,py+1,smallFont);
    }
}
/****************************************************************************/
void tUnit::drawShadow()
{
    float mp,alpha;

    if(state < STATE_SELECTABLE)
        return;

    //Flat units laying on the ground don't have shadows.
    if(getElevation()<=0 && model->getMaxZ()==0)
        return;

    if(getModelOpacity()==0)
        return;

    mp=getModelProgress();

    alpha=getModelOpacity();
    if(alpha<1)
        glEnable(GL_BLEND);
    glColor4f(0,0,0,alpha); //translucent black

    glPushMatrix();
    glTranslatef(position3D.x,position3D.y,position3D.z);

    //Translate this vector with forward3D, side3D, and up3D
    GLfloat orientation[16]={forward3D.x,forward3D.y,forward3D.z,0,
        side3D.x,side3D.y,side3D.z,0,
        up3D.x,up3D.y,up3D.z,0, 0,0,0,1};
    glMultMatrixf(orientation);

    //Setup clip plane:
    double ground[4]={0,0,1,0};
    glClipPlane(GL_CLIP_PLANE0,ground);
    model->draw(mp, DRAW_SHADOW);

    glPopMatrix();
    glDisable(GL_BLEND);
}
/****************************************************************************/
void tUnit::drawStats()
{
    int px,py;
    char buf[100];
    vector3DToPixel(position3D,px,py);
    snprintf(buf, sizeof(buf), "%d (%ld)",ID,(long)HP);
    px-=stringWidth(buf,smallFont)/2;
    py+=20;
    glColor3f(.5,.5,.5);
    drawString(buf,px+1,py,smallFont);
    glColor3f(1,1,1);
    drawString(buf,px,py+1,smallFont);
}
/****************************************************************************/
tUnit::tUnit(long _ID, int _owner, tUnitType *_type, const tVector2D& _position,
    const tVector2D& _forward, tWorldTime t) throw(int) : ID(_ID), owner(_owner), type(_type),
	position(_position), position3D(to3D(_position)), velocity(zeroVector2D), p0(_position),
	v0(zeroVector2D), f0(iHat2D), acceleration(zeroVector2D), speed(0), forward(_forward),
	forward3D(to3D(_forward)), side3D(to3D(_forward.perpendicularCCW())),
    up3D(kHat3D), selected(false), flashing(false), flashOn(false),
    attacker(NULL), curCommand(COMMAND_STOP), commandCounter(0), isAnchored(false),
    commandIndex(0), syncPos(zeroVector2D), syncHP(0), hashValue(0),
    nextNeighbor(NULL), deltaHP(0), group(-1), state(STATE_SELECTABLE),h0(0),HP(0), t0(t),
    fieldOfView(0), moving(false), origin(_position), movementMode(COMMAND_MANEUVER), wanderSide(0),
    distance(0), d0(0), shockwaveTimer(-1), model(NULL), abilitySwitches(NULL),
	tractorBeamParent(NULL), tractorBeamOrigin(NULL), tractorBeamBasis(NULL),
	tractorBeamOffset(zeroVector3D)
{
    int i;
    tBehavior *s;

#ifdef DEBUG
    //Throw an arbitrary exception if the constructor contains an invalid owner
    //and/or type.  Any function that creates a unit via mwNew tUnit shoulc catch
    //these exceptions using catch(...)
    if(owner<0 || owner>=MAX_PLAYERS)
    {
        bug("tUnit::tUnit: invalid player ID");
        throw -1;
    }
    if(MEMORY_VIOLATION(type))
        throw -1;
#endif
    model=mwNew tModel(type->modelType,this,t);

    for(i=0;i<MAX_ABILITIES;i++)
        abilityCounters[i]=1;
    if(!type->canMove)
        disableAbility(ABILITY_MOVE);

    if(getMaxHP()>0)
    {
        built=false;
        for(i=0;i<MAX_ABILITIES;i++)
            disableAbility((eAbility)i);
    }
    else
        built=true;

    calculateHash();

    //Instantiate a new behavior for each behavior type that belong to the unit's type:
    for(i=0;i<type->behaviorTypes.size();i++)
    {
        s=type->behaviorTypes[i]->createBehavior(this);
        if(s)
            behaviors.push_back(s);
    }

    //Instantiate a new movement behavior for the unit if it can move:
    //Movement has the LEAST priority.
    if(type->canMove)
    {
        s=mwNew tMovementBehavior(this);
        if(s)
            behaviors.push_back(s);
    }

    int switchCount=type->abilitySwitchTypes.size();
    abilitySwitches=mwNew bool[switchCount];
    for(i=0;i<switchCount;i++)
        abilitySwitches[i]=true;
}
/****************************************************************************/
tUnit::~tUnit()
{
    int i;
    if(model)
        mwDelete model;
    for(i=0;i<behaviors.size();i++)
        if(behaviors[i])
            mwDelete behaviors[i];
    behaviors.clear();
    if(abilitySwitches)
        mwDelete[] abilitySwitches;
}
/****************************************************************************/
//clear unit's orders
void tUnit::clearOrders(tWorldTime t)
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onClearOrders(t);

    attacker=NULL;
    isAnchored=false;
    commandIndex=0;
    commands.clear();
}
/****************************************************************************/
void tUnit::onBirth(tWorldTime t)
//precondition: unit is alive but not selected
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onBirth(t);
    if(type->onBirthProc)
        model->runProcedure(type->onBirthProc,t);
}
/****************************************************************************/
void tUnit::onDeath(tWorldTime t)
//precondition: unit is alive but not selected
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onDeath(t);
    if(type->onDeathProc)
        model->runProcedure(type->onDeathProc,t);
}
/****************************************************************************/
void tUnit::onTransfer(int newOwner, tWorldTime t)
//precondition: unit is alive but not selected
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onTransfer(newOwner,t);
}
/****************************************************************************/
void tUnit::onConception(tWorldTime t)
//precondition: unit is alive and not selected
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onConception(t);
    if(type->onConceptionProc)
        model->runProcedure(type->onConceptionProc,t);
}
/****************************************************************************/
void tUnit::destroy(tWorldTime t)
//precondition: unit is not selected
{
    if(state==STATE_ALIVE)
        onDeath(t);

    //Clear the unit's orders for good measure.  In many cases, we don't need to
    //reset the unit's state variables, but it simplifies the code.
    clearOrders(t);
    setVelocity(zeroVector2D,t);

    flashing=false;
    //Setting the state to STATE_DESTROYED causes the engine to delete this object!
    state=STATE_DESTROYED;
    group=-1;
}
/****************************************************************************/
void tUnit::onUnitDestroyed(tUnit *u, tWorldTime t)
//precondition: unit still exists
{
    int i;
    if(attacker==u)
        attacker=NULL;
    if(tractorBeamParent==u)
        disengageTractorBeam();

    for(i=0;i<behaviors.size();i++)
        behaviors[i]->onUnitDestroyed(u,t);
    if(curCommand==COMMAND_STOP && commands.size()>0) //There are more commands waiting to be processed.
        loadNextCommand(t);
}
/****************************************************************************/
void tUnit::damage(tUnit *u, float amt, int armorClass, tWorldTime t)
//If a unit runs out of hit points, wait till next interval to kill unit
//This is necessary for sychronization--namely, if the computer freezes for a couple of seconds
{
#ifdef DEBUG
    if(armorClass<0 || armorClass>=MAX_ARMOR_CLASSES)
    {
        bug("tUnit::damage: invalid armor class");
        return;
    }
#endif
    if(!isVulnerable())
        return;

    //Calculate the effective damage inflicted to the unit after the armor deduction.
    float damage=amt-getArmor(armorClass);
    if(damage>0) //No damage if the effective amount is less than zero!
    {
        setHP(HP-damage,t);

        //Attacker is still alive
        if(u && attacker!=u && (attacker==NULL ||
            UNIT_DISTANCE(this,u)-u->getAttackRange() < UNIT_DISTANCE(this,attacker)-attacker->getAttackRange()))
        {
            attacker=u;
            //reel in the anchor and let's get outta here!
            if(movementMode==COMMAND_EVADE)
                isAnchored=false;
        }
        for(int i=0;i<behaviors.size();i++)
            behaviors[i]->onDamage(u,amt,t);
    }
}
/****************************************************************************/
void tUnit::adjustHealRate(tUnit *u, float amt, tWorldTime t)
{
    deltaHP+=amt;
}
/****************************************************************************/
void tUnit::transferValues(tWorldTime t)
{
    p0=position;
    v0=velocity;
    f0=forward;
    h0=HP;
    d0=distance;
    t0=t;
}
/****************************************************************************/
void tUnit::setPosition(tVector2D pos, tWorldTime t)
{
#ifdef LOG
	if(tractorBeamParent)
	{
		log("tUnit::setPosition: function failed since a tractor beam is controlling the unit");
		return;
	}
#endif
	
    setVelocity(zeroVector2D,t);
    if(position==pos)
        return;
    position=pos;
    if(fieldOfView>0)
    {
        int x1=(int)p0.x, y1=(int)p0.y;
        int x2=(int)position.x, y2=(int)position.y;
        if(x1!=x2 || y1!=y2)
        {
            concealCircle(owner,x1,y1,(int)fieldOfView);
            revealCircle(owner,x2,y2,(int)fieldOfView);
        }
    }
    transferValues(t);
    calculateHash();
}
/****************************************************************************/
void tUnit::setVelocity(tVector2D vel, tWorldTime t)
{
#ifdef LOG
	if(tractorBeamParent)
	{
		log("tUnit::setPosition: function failed since a tractor beam is controlling the unit");
		return;
	}
#endif
    
	acceleration=zeroVector2D;
    velocity=vel;
    speed=velocity.length();
	moving=(speed>0.001); //fudge factor

    if(moving)
		forward=velocity.normalize();
    
	transferValues(t);
}
/****************************************************************************/
void tUnit::setForward(tVector2D fwd, tWorldTime t)
{
#ifdef LOG
	if(tractorBeamParent && tractorBeamBasis)
	{
		log("tUnit::setPosition: function failed since a tractor beam is controlling the unit");
		return;
	}
#endif
    forward=fwd.normalize();
    transferValues(t);
}
/****************************************************************************/
void tUnit::setHP(float _HP, tWorldTime t)
{
    HP=_HP;
    clampRange(HP,0.0f,getMaxHP());
    transferValues(t);
    updateState(t);
}
/****************************************************************************/
void tUnit::kill(tWorldTime t)
{
    setHP(0,t);
}
/****************************************************************************/
void tUnit::setOwner(int _owner, tWorldTime t)
//precondition: unit is not selected
{
    if(owner==_owner)
        return;
    bool s=selected;
    float oldFieldOfView=fieldOfView;
    if(s)
        deselect();
    if(state==STATE_ALIVE)
        onTransfer(_owner, t);
    setFieldOfView(0);
    owner=_owner;
    setFieldOfView(oldFieldOfView);
    calculateHash();
    if(s)
        select();
}
/****************************************************************************/
void tUnit::loadNextCommand(tWorldTime t)
{
#ifdef DEBUG
    if(commands.size()==0)
    {
        bug("tUnit::loadNextCommand: command queue is empty");
        return;
    }
    if(commandIndex<0 || commandIndex>=commands.size())
    {
        bug("tUnit::loadNextCommand: invalid commandIndex");
        return;
    }
    if(commands[commandIndex].command<0 || commands[commandIndex].command>=commandCount)
    {
        bug("tUnit::loadNextCommand: invalid command");
        return;
    }
#endif
    onCommand(commands[commandIndex], t);
    if(commandTable[commands[commandIndex].command].repeat)
        commandIndex++;
    else
        commands.erase(commands.begin()+commandIndex);
    if(commandIndex>=commands.size())
        commandIndex=0;
}
/****************************************************************************/
//Flash the unit's selection circle for three seconds
void tUnit::flash()
{
    if(!flashing)
    {
        flashTimer=currentTime;
        flashing = true;
    }
}
/****************************************************************************/
void tUnit::onCommand(const tCommand& c, tWorldTime t)
{
    int i, j;
    tUnit *unit;
    eCommand action;
    vector<tUnit *> targets;

    //Find next target in array:
    if(c.paramType==PARAM_UNIT)
    {
        for(i=0;i<c.targetCount;i++)
        {
            unit=unitLookup(c.targets[i]);
            if(unit && unit!=this)
            {
                action=c.command;
                if(action==COMMAND_NONE)
                    action=getDefaultCommand(unit);
                //Some units (such as buildings) may not have a default command for unit right-click.
                if(action!=COMMAND_NONE)
                    for(j=0;j<behaviors.size();j++)
                        behaviors[j]->onCommand(action, unit, t);
            }
        }
    }
    else

    action=c.command;
    if(action==COMMAND_NONE)
        action=getDefaultCommand();
    //Some units (such as buildings) may not have a default command for position right-click.
    if(action!=COMMAND_NONE)
        switch(action)
        {
            case COMMAND_STOP:
                clearOrders(t);
                break;
            case COMMAND_WANDER:
            case COMMAND_MANEUVER:
            case COMMAND_HOLD_POSITION:
            case COMMAND_EVADE:
                setMovementMode(action, t);
                break;
            case COMMAND_DELETE:
                if(!built && state==STATE_SELECTABLE)
                    destroyUnit(this, t);
                break;
            case COMMAND_SELF_DESTRUCT:
                if(state==STATE_ALIVE)
                    kill(t);
                break;
            default:
                if(c.paramType==PARAM_POSITION)
                    for(i=0;i<behaviors.size();i++)
                        behaviors[i]->onCommand(action,c.pos,t);
                else if(c.paramType==PARAM_PRODUCTION)
                    for(i=0;i<behaviors.size();i++)
                        behaviors[i]->onCommand(action,c.quantity,c.repeat,t);
                else
                    for(i=0;i<behaviors.size();i++)
                        behaviors[i]->onCommand(action,t);
        }
}
/****************************************************************************/
//Select unit
void tUnit::select()
{
    int i;
    selected = true;
    if(areCommandsVisible())
    {
        for(i=1;i<commandCount;i++)
            if(canExecute((eCommand)i))
                enableCommand((eCommand)i);

        for(i=0;i<behaviors.size();i++)
            behaviors[i]->onSelect();

        highlightCommand(curCommand);
        if(type->canMove)
            highlightCommand(movementMode);
    }
}
/****************************************************************************/
//Deselect unit
void tUnit::deselect()
{
    int i;
    if(areCommandsVisible())
    {
        resetCommand(curCommand);
        if(type->canMove)
            resetCommand(movementMode);

        for(i=0;i<behaviors.size();i++)
            behaviors[i]->onDeselect();

        for(i=1;i<commandCount;i++)
            if(canExecute((eCommand)i))
                disableCommand((eCommand)i);
    }
    selected = false;
}
/****************************************************************************/
void tUnit::setCurCommand(eCommand command, tWorldTime t)
{
    if(commandCounter==0)
    {
        if(areCommandsVisible())
            resetCommand(curCommand);
        curCommand=command;
        if(areCommandsVisible())
            highlightCommand(curCommand);
        commandCounter++;
    }
    else if(command==curCommand)
        commandCounter++;
    else
        bug("tUnit::setCurCommand: unit is already processing another command");
}
/****************************************************************************/
void tUnit::resetCurCommand(tWorldTime t)
{
    if(commandCounter>1)
        commandCounter--;
    else if(commandCounter==1)
    {
        if(areCommandsVisible())
            resetCommand(curCommand);
        curCommand=COMMAND_STOP;
        if(areCommandsVisible())
            highlightCommand(curCommand);
        commandCounter--;
    }
    else
        bug("tUnit::setCurCommand: unit isn't processing any commands");
}
/****************************************************************************/
void tUnit::setMovementMode(eCommand _movementMode, tWorldTime t)
{
    if(_movementMode==movementMode || !type->canMove)
        return;
    if(areCommandsVisible())
        resetCommand(movementMode);
    movementMode=_movementMode;
    if(areCommandsVisible())
        highlightCommand(movementMode);
}
/****************************************************************************/
void tUnit::dumpCommands()
{
    int i,j;
    char buf[BUFFER_SIZE];
    tBufferIterator ptr(buf, sizeof(buf));
#ifdef DEBUG
    if(curCommand<0 || curCommand>=commandCount)
    {
        bug("tUnit::dumpCommands: invalid command");
        return;
    }
#endif

    try
    {
        mySprintf(ptr, "-Commands for Unit %ld-  Current Command: %s", ID, CHARPTR(commandTable[curCommand].name));
        for(i=0;i<commands.size();i++)
        {
            switch(commands[i].paramType)
            {
                case PARAM_NONE:
                    mySprintf(ptr, "\n    %cCommand: %s",i==commandIndex?'>':' ',
                        CHARPTR(commandTable[commands[i].command].name));
                    break;
                case PARAM_POSITION:
                    mySprintf(ptr, "\n    %cCommand: %s  Position: (%f,%f)",
                        i==commandIndex?'>':' ',
                        CHARPTR(commandTable[commands[i].command].name),
                        commands[i].pos.x,
                        commands[i].pos.y);
                    break;
                case PARAM_UNIT:
                    mySprintf(ptr, "\n    %cCommand: %s  Object:",
                        i==commandIndex?'>':' ',
                        CHARPTR(commandTable[commands[i].command].name));
                    for(j=0;j<commands[i].targetCount;j++)
                        mySprintf(ptr, " %ld", commands[i].targets[j]);
                    break;
            }
        }
        if(commandIndex==commands.size())
            mySprintf(ptr, "\n    >");
    }
    catch(int x)
    {
        bug("tUnit::dumpCommands: buffer overflow");
    }

    log(buf);

    for(i=0;i<behaviors.size();i++)
        behaviors[i]->dumpCommands();
}
/****************************************************************************/
void tUnit::calculateHash()
{
    hashValue = (HASH_FUNC(position.y) * hashWidth + HASH_FUNC(position.x))
        * MAX_PLAYERS + owner;
    clampRange(hashValue,0l,(long)(hashSize-1));
}
/****************************************************************************/
void tUnit::getRepairPoints(tVector3D& a, tVector3D& b)
{
    model->getRepairPoints(HP/getMaxHP(),a,b);
    a=pointToWorldCoords(a);
    b=pointToWorldCoords(b);
}
/****************************************************************************/
bool tUnit::repelNearbyUnits()
{
    //Prevent separation behavior around non-existant buildings.
    if(!built && state==STATE_SELECTABLE)
        return false;

    return (model->getMaxZ()!=0);
}
/****************************************************************************/
float tUnit::getModelProgress()
{
    //Built:
    if(built || getMaxHP()<=0)
        return 1;

    //Construction:
    return HP/getMaxHP();
}
/****************************************************************************/
eCommand tUnit::getDefaultCommand()
{
    eCommand command;

    for(int i=0;i<behaviors.size();i++)
    {
        command=behaviors[i]->getDefaultCommand();
        if(command!=COMMAND_NONE)
            return command;
    }

    return COMMAND_NONE;
}
/****************************************************************************/
eCommand tUnit::getDefaultCommand(tUnit *u)
{
    eCommand command;

    for(int i=0;i<behaviors.size();i++)
    {
        command=behaviors[i]->getDefaultCommand(u);
        if(command!=COMMAND_NONE)
            return command;
    }

    return COMMAND_NONE;
}
/****************************************************************************/
bool tUnit::areCommandsVisible()
{
    return selected && canControl(this);
}
/****************************************************************************/
bool tUnit::canExecute(eCommand command)
{
    switch(command)
    {
    //Basic commands:
        case COMMAND_NONE:              return true;
        case COMMAND_STOP:              return true;

    //Movement modes:
        case COMMAND_WANDER:            return type->canMove;
        case COMMAND_MANEUVER:          return type->canMove;
        case COMMAND_HOLD_POSITION:     return type->canMove;
        case COMMAND_EVADE:             return type->canMove;

    //Extended commands:
        case COMMAND_DELETE:            return (!built && state==STATE_SELECTABLE);
        case COMMAND_SELF_DESTRUCT:     return (state==STATE_ALIVE);
        case COMMAND_DISBAND:           return false;

        default:
            for(int i=0;i<behaviors.size();i++)
                if(behaviors[i]->canExecute(command))
                    return true;
            return false;
    }
}
/****************************************************************************/
void tUnit::updateState(tWorldTime t)
{
    bool s=selected;
    if(state<STATE_SELECTABLE)
        return;
    if(HP==0 && state==STATE_ALIVE)
    {
        //If the unit isn't built, it doesn't make very much sense to create
        //an explosion because there's nothing there!  All of the polys have
        //been blown away.  NOTE: getMaxRadius is an arbitrary choice.
        if(built)
            createUnitExplosion(mwNew tUnitExplosion(position3D, up3D, model->getMaxRadius(), model->getMaxZ(), t));

        if(isBuilding() && getMaxHP()>0)
        {
            if(s)
                deselect();
            setBuilt(false,t);
            //This if-statement could be removed and the game would still work fine.
            //It merely eliminates an extra state transition to STATE_SELECTED and
            //back to STATE_ALIVE when the unit's hitpoints rise above 0.
            if(deltaHP<=0)
                state=STATE_SELECTABLE;
            if(s)
                select();
        }
        else
            destroyUnit(this,t);
    }
    else if(!built)
    {
        if(HP>0 && state<STATE_ALIVE)
        {
            if(s)
                deselect();
            state=STATE_ALIVE;
            onConception(t);
            if(s)
                select();
        }
        //A unit can never become built as long as a unit is depleting its hitpoints.
        if(HP==getMaxHP() && deltaHP>=0)
            setBuilt(true,t);
    }
}
/****************************************************************************/
void tUnit::setFieldOfView(float _fieldOfView)
{
    if(_fieldOfView!=fieldOfView)
    {
        if(fieldOfView>0)
            concealCircle(owner,(int)p0.x,(int)p0.y,(int)fieldOfView);
        fieldOfView=_fieldOfView;
        if(fieldOfView>0)
            revealCircle(owner,(int)p0.x,(int)p0.y,(int)fieldOfView);
    }
}
/****************************************************************************/
void tUnit::onDepositDepleted(tDeposit *d, tWorldTime t)
{
    for(int i=0;i<behaviors.size();i++)
        behaviors[i]->onDepositDepleted(d,t);
}
/****************************************************************************/
bool tUnit::isFiringAt(tUnit *u)
{
    for(int i=0;i<behaviors.size();i++)
        if(behaviors[i]->isFiringAt(u))
            return true;
    return false;
}
/****************************************************************************/
float tUnit::getAttackRange()
{
    float range=0,f;
    for(int i=0;i<behaviors.size();i++)
    {
        f=behaviors[i]->getAttackRange();
        if(f>range)
            range=f;
    }
    return range;
}
/****************************************************************************/
bool tUnit::isAbilityEnabled(eAbility a)
{
#ifdef DEBUG
    if(a<0 || a>=MAX_ABILITIES)
    {
        bug("tUnit::isAbilityEnabled: invalid ability");
        return false;
    }
#endif
    return abilityCounters[a]>0;
}
/****************************************************************************/
void tUnit::disableAbility(eAbility a)
{
#ifdef DEBUG
    if(a<0 || a>=MAX_ABILITIES)
    {
        bug("tUnit::disableAbility: invalid ability");
        return;
    }
#endif
    abilityCounters[a]--;
}
/****************************************************************************/
void tUnit::enableAbility(eAbility a)
{
#ifdef DEBUG
    if(a<0 || a>=MAX_ABILITIES)
    {
        bug("tUnit::enableAbility: invalid ability");
        return;
    }
#endif
    abilityCounters[a]++;
}
/****************************************************************************/
void tUnit::setBuilt(bool _built, tWorldTime t)
{
    int i;
    bool s=selected;
    if(!built&&_built)
    {
        if(s)
            deselect();
        built=true;
        for(i=0;i<MAX_ABILITIES;i++)
            enableAbility((eAbility)i);
        onBirth(t);
        if(s)
            select();
    }
    else if(built&&!_built && getMaxHP()>0)
    {
        if(s)
            deselect();
        onDeath(t);
        built=false;
        for(i=0;i<MAX_ABILITIES;i++)
            disableAbility((eAbility)i);
        if(s)
            select();
    }
}
/****************************************************************************/
//Original author: Craig Reynolds
bool tUnit::inBoidNeighborhood (const tVector2D& queryLocation,
                         const float minDistance,
                         const float maxDistance,
                         const float cosMaxAngle)
{
    const tVector2D offset=queryLocation-position;
    const float distance = offset.length();
    tVector2D unitOffset = offset / distance;
    const float forwardness = forward.dot(unitOffset);

    return (distance < minDistance) || ((distance < maxDistance) && (forwardness > cosMaxAngle));
}
/****************************************************************************/
void tUnit::spawnShockwave()
{
    shockwaveTimer=currentTime;
}
/****************************************************************************/
void tUnit::locateReferenceFrame(tReferenceFrameType *rft,
                                 tVector3D& p,
                                 tVector3D& f,
                                 tVector3D& s,
                                 tVector3D& u)
{
    model->locateReferenceFrame(rft,p,f,s,u);
    p=pointToWorldCoords(p);
    f=vectorToWorldCoords(f);
    s=vectorToWorldCoords(s);
    u=vectorToWorldCoords(u);
}
/****************************************************************************/
void tUnit::engageTractorBeam(tUnit *_parent, tModelVectorType *_origin,
							  const tVector3D& _offset)
{
#ifdef DEBUG
    if(_origin==NULL)
    {
        bug("tUnit::engageTractorBeam: origin is NULL");
        return;
    }
#endif
	tractorBeamParent=_parent;
	tractorBeamOrigin=_origin;
	tractorBeamBasis=NULL;
	tractorBeamOffset=_offset;
}
/****************************************************************************/
void tUnit::engageTractorBeam(tUnit *_parent, tReferenceFrameType *_basis,
							  const tVector3D& _offset)
{
#ifdef DEBUG
    if(_basis==NULL)
    {
        bug("tUnit::engageTractorBeam: basis is NULL");
        return;
    }
#endif
	tractorBeamParent=_parent;
	tractorBeamOrigin=NULL;
	tractorBeamBasis=_basis;
	tractorBeamOffset=_offset;
}
/****************************************************************************/
void tUnit::disengageTractorBeam()
{
	tractorBeamParent=NULL;
	tractorBeamOrigin=NULL;
	tractorBeamBasis=NULL;
	tractorBeamOffset=zeroVector3D;
}
/****************************************************************************/
// EXPLOSION
/****************************************************************************/
tUnitExplosion::tUnitExplosion(const tVector3D& _position3D, const tVector3D& up3D,
    float _radius, float _height, tWorldTime t) : position3D(_position3D),
    radius(_radius), height(_height), anim(animTypeLookup("Explosion", /*exact=*/true)), t0(t), dt(0)
{
    center=up3D; //convert up3D to a non-const vector first
    center=position3D+center*(height/2);
    radius3D=MAX(radius,height/2);
    anim.start(t);

    //Assign a new sound source to the unit.
    soundSource = sndNewSource(soundTypeLookup("Explosion",/*exact=*/true), position3D, t);
}

void tUnitExplosion::draw()
{
    float r=dt*30;
    float opacity=(1-dt*2)*2;
    clampRange(opacity,0.0f,1.0f);

    glPushMatrix();
    glTranslatef(position3D.x,position3D.y,position3D.z);

    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glDepthMask(false);
    glBlendFunc(GL_DST_COLOR, GL_ONE);
    glColor4f(opacity,opacity,opacity,opacity);

    if(shockwaveSkin)
    {
        glEnable(GL_TEXTURE_2D);
        bindSkin(shockwaveSkin);
    }
    glBegin(GL_QUADS);
        glTexCoord2f(0,0);
        glVertex2f(-r,-r);
        glTexCoord2f(1,0);
        glVertex2f(r,-r);
        glTexCoord2f(1,1);
        glVertex2f(r,r);
        glTexCoord2f(0,1);
        glVertex2f(-r,r);

        glTexCoord2f(0,0);
        glVertex2f(-r,-r);
        glTexCoord2f(1,0);
        glVertex2f(r,-r);
        glTexCoord2f(1,1);
        glVertex2f(r,r);
        glTexCoord2f(0,1);
        glVertex2f(-r,r);
    glEnd();
    glDisable(GL_TEXTURE_2D);

    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glDepthMask(true);
    glEnable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);

    glPopMatrix();

    glPushMatrix();
    glTranslatef(center.x,center.y,center.z);
    glRotatef(mapRotation*180/M_PI,0,0,-1);
    glRotatef(mapTilt*180/M_PI,1,0,0);
    glTranslatef(0,0,radius);
    glScalef(radius3D*0.06,radius3D*0.06,1);
    glColor3f(1,1,1);
    anim.draw(0,0);
    glPopMatrix();

    //Destroy the sound source if we can:
    if(soundSource>0 && sndIsFinished(soundSource))
    {
        sndDeleteSource(soundSource);
        soundSource=0;
    }
}
tUnitExplosion::~tUnitExplosion()
{
    if(soundSource>0)
        sndDeleteSource(soundSource);
}
bool tUnitExplosion::isFinished()
{
    if(soundSource>0 && !sndIsFinished(soundSource))
        return false;
    return anim.isFinished();
}
/****************************************************************************/
tUnitType *parseUnitTypeBlock(tStream& s) throw(tString)
{
    tUnitType *ut = mwNew tUnitType(s);
    if(ut)
    {
        ut->ID=unitTypes.size()+1; //First unit type has ID of 1
        unitTypes.push_back(ut);
    }
    return ut;
}
/****************************************************************************/
//deallocate memory used by unit types
void destroyUnitTypes()
{
    for(int i=0; i<unitTypes.size(); i++)
        mwDelete unitTypes[i];
    unitTypes.clear();
}
/****************************************************************************/
//lookup unit type with name (NULL if not found)
tUnitType *unitTypeLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<unitTypes.size(); i++)
        if(STREQUAL(unitTypes[i]->name,n))
            return unitTypes[i];
    if(exact)
        return NULL;
    for(i=0; i<unitTypes.size(); i++)
        if(STRPREFIX(n,unitTypes[i]->name)) //STRPREFIX is case-insensitive
            return unitTypes[i];
    return NULL;
}
/****************************************************************************/
//lookup unit type with ID (NULL if not found)
tUnitType *unitTypeLookup(int ID)
{
    for(int i=0; i<unitTypes.size(); i++)
        if(unitTypes[i]->ID==ID)
            return unitTypes[i];
    return NULL;
}
/****************************************************************************/
tUnitType *parseUnitType(const char *fieldName, tStream& s) throw(tString)
{
    tUnitType *ut;
    tString str=parseString(fieldName, s);
    ut=unitTypeLookup(str,/*exact=*/false);
    if(ut==NULL)
        throw tString("Unrecognized unit type '")+str+"'";
    return ut;
}
/****************************************************************************/
eUnitSize parseSize(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    if(strPrefix(str,"tiny"))
        return SIZE_TINY;
    if(strPrefix(str,"small"))
        return SIZE_SMALL;
    if(strPrefix(str,"medium"))
        return SIZE_MEDIUM;
    if(strPrefix(str,"large"))
        return SIZE_LARGE;
    if(strPrefix(str,"huge"))
        return SIZE_HUGE;

    throw tString("Unrecognized unit size '")+str+"'";
}
/****************************************************************************/
eCommand commandLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0;i<commandCount;i++)
        if(STREQUAL(commandTable[i].name,n))
            return (eCommand)i;
    if(exact)
        return COMMAND_NONE;
    for(i=0;i<commandCount;i++)
        if(STRPREFIX(n,commandTable[i].name))
            return (eCommand)i;
    return COMMAND_NONE;
}
/****************************************************************************/
eCommand parseCommand(const char *fieldName, tStream& s) throw(tString)
{
    eCommand c;
    tString str=parseString(fieldName, s);
    c=commandLookup(str,/*exact=*/false);
    if(c==COMMAND_NONE)
        throw tString("Unrecognized command '")+str+"'";
    return c;
}
/****************************************************************************/
eCommand parseImmediateCommand(const char *fieldName, tStream& s) throw(tString)
{
	eCommand c=parseCommand(fieldName,s);
	if(commandTable[c].paramType!=PARAM_NONE)
		throw tString("Please set this command's parameter type to none");
	if(commandTable[c].immediate==false)
		throw tString("Please set this command's immediate flag to true");
	return c;
}
/****************************************************************************/
//Called by engine during each interval tick.
//Determine whether each player has sufficient resources to build each type of unit.
void updateUnitTypes()
{
    int i,j,k;
    for(i=0;i<unitTypes.size();i++)
        for(j=0;j<MAX_PLAYERS;j++)
        {
            unitTypes[i]->repairable[j]=true;
            for(k=0;k<MAX_RESOURCES;k++)
                if(unitTypes[i]->costs[k]>0 && !isResourceAvailable(j,k))
                {
                    unitTypes[i]->repairable[j]=false;
                    break;
                }
        }
}
/****************************************************************************/
void lookupUnitTypes() throw(tString)
{
    for(int i=0;i<unitTypes.size();i++)
        for(int j=0;j<unitTypes[i]->behaviorTypes.size();j++)
            unitTypes[i]->behaviorTypes[j]->lookupUnitTypes();
}
/****************************************************************************/
eAbility parseAbility(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName,s);
    if(strPrefix(str,"move"))
        return ABILITY_MOVE;
    else if(strPrefix(str,"attack"))
        return ABILITY_ATTACK;
    else if(strPrefix(str,"repair"))
        return ABILITY_REPAIR;
    else if(strPrefix(str,"produce"))
        return ABILITY_PRODUCE;

    throw tString("Unrecognized ability '")+str+"'";
}
/****************************************************************************/
eParamType parseParamType(const char *fieldName, tStream& s) throw(tString)
{
	tString str=parseString(fieldName,s);
	if(strPrefix(str,"none"))
		return PARAM_NONE;
	if(strPrefix(str,"position"))
		return PARAM_POSITION;
	if(strPrefix(str,"unit"))
		return PARAM_UNIT;
	if(strPrefix(str,"construction"))
		return PARAM_CONSTRUCTION;
	if(strPrefix(str,"production"))
		return PARAM_PRODUCTION;

	throw tString("Unrecognized parameter type '")+str+"'";
}
/****************************************************************************/
void parseCommandBlock(tStream& s) throw(tString)
{
	tString str=parseIdentifier("command name",s);
	eCommand index=commandLookup(str,/*exact=*/true);
	if(index==COMMAND_NONE)
	{
		if(commandCount==MAX_COMMANDS)
			throw tString("Sorry, you can only define 256 commands including the standard commands");
		index=(eCommand)commandCount++;

		commandTable[index].name=str; //parseIdentifier converts the string to lowercase
	    //Immediate defaults to true since most custom commands will be immediate
	    commandTable[index].immediate=true;
	}
	else if(index>=PREDEFINED_COMMANDS)
		throw tString("A command by the name of '")+str+"' already exists";

	tCommandType& c=commandTable[index];

	if(s.beginBlock())
	{
		while(!s.endBlock())
		{
			str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"caption"))
				c.caption=parseString("caption",s);
			else if(strPrefix(str,"parameter_type") || strPrefix(str,"param_type"))
			{
				eParamType _paramType=parseParamType("parameter type",s);
				//The user is customizing a standard command:
				if(index<PREDEFINED_COMMANDS && _paramType!=c.paramType)
					throw tString("You are not allowed to change a predefined command's parameter type");
				c.paramType=_paramType;
			}
			else if(strPrefix(str,"immediate"))
			{
				bool _immediate=parseBool("immediate?",s);
				if(index<PREDEFINED_COMMANDS && _immediate!=c.immediate)
					throw tString("You are not allowed to change a predefined command's immediate flag");
				c.immediate=_immediate;
			}
			else if(strPrefix(str,"benevolent"))
			{
				bool _benevolent=parseBool("benevolent?",s);
				if(index<PREDEFINED_COMMANDS && _benevolent!=c.benevolent)
					throw tString("You are not allowed to change a predefined command's benevolent flag");
				c.benevolent=_benevolent;
			}
			else if(strPrefix(str,"hot_key") || strPrefix(str,"hotkey"))
			{
				c.hotKey[0]=parseHotKey("hot key #1",s);
				if(s.hasOptionalParameter())
				{
					s.matchOptionalToken(",");
					c.hotKey[1]=parseHotKey("hot key #2",s);
					if(s.hasOptionalParameter())
						throw tString("Currently, only two hot keys are supported");
				}
			}
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.matchOptionalToken(";");
		}
	}
}
/****************************************************************************/
eCommand createCommand(const tString& name, const tString& caption, eParamType paramType,
    bool immediate, bool benevolent, tKey hotKey[2], int unitID) throw(tString)
{
    if(commandCount==MAX_COMMANDS)
        throw tString("Sorry, you can only define 256 commands including the standard commands");

    int index=commandCount++;
    tCommandType& c=commandTable[index];
    c.name=name;
    c.caption=caption;
    c.paramType=paramType;
    c.immediate=immediate;
    c.benevolent=benevolent;
    c.hotKey[0]=hotKey[0];
    c.hotKey[1]=hotKey[1];
    c.unitID=unitID;

    return (eCommand)index;
}
/****************************************************************************/
eCommand createCommand(const tString& name, const tString& caption, eParamType paramType,
    bool immediate, bool benevolent) throw(tString)
{
    if(commandCount==MAX_COMMANDS)
        throw tString("Sorry, you can only define 256 commands including the standard commands");

    int index=commandCount++;
    tCommandType& c=commandTable[index];
    c.name=name;
    c.caption=caption;
    c.paramType=paramType;
    c.immediate=immediate;
    c.benevolent=benevolent;
    c.hotKey[0]=0;
    c.hotKey[1]=0;
    c.unitID=0;

    return (eCommand)index;
}
/****************************************************************************/
eCommand commandLookup(tUnitType *ut, eParamType paramType)
{
	#ifdef DEBUG
		if(MEMORY_VIOLATION(ut))
			return COMMAND_NONE;
	#endif
    for(int i=0;i<commandCount;i++)
        if(commandTable[i].unitID==ut->ID && commandTable[i].paramType==paramType)
			return (eCommand)i;
	return COMMAND_NONE;
}
/****************************************************************************/
void resetCommandTable()
{
	int i;
	for(i=0;i<MAX_COMMANDS;i++)
	{
		tCommandType& c=commandTable[i];
		c.caption="";
		c.hotKey[0]=0;
		c.hotKey[1]=0;
		c.unitID=0; //first unit has id of 1
	}
    for(i=PREDEFINED_COMMANDS;i<MAX_COMMANDS;i++)
	{
		tCommandType& c=commandTable[i];
		c.name="";
		c.paramType=PARAM_NONE;
		c.immediate=false;
		c.benevolent=false;
	}
	commandCount=PREDEFINED_COMMANDS;
}

