/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef mathexprH
#define mathexprH

#include "model.h"

struct tExpression
{
	virtual float evaluate()=0;
    virtual bool isConstant() { return false; }
};

#ifdef SGI_AUTO_PTR

/*
 * Copyright (c) 1997
 * Silicon Graphics Computer Systems, Inc.
 *
 * Permission to use, copy, modify, distribute and sell this software
 * and its documentation for any purpose is hereby granted without fee,
 * provided that the above copyright notice appear in all copies and
 * that both that copyright notice and this permission notice appear
 * in supporting documentation.  Silicon Graphics makes no
 * representations about the suitability of this software for any
 * purpose.  It is provided "as is" without express or implied warranty.
 *
 */

#ifndef __SGI_STL_MEMORY
#define __SGI_STL_MEMORY

// #include <stl_algobase.h>
// #include <stl_alloc.h>
// #include <stl_construct.h>
// #include <stl_tempbuf.h>
// #include <stl_uninitialized.h>
// #include <stl_raw_storage_iter.h>

#ifndef __STL_NOTHROW
#define __STL_NOTHROW
#endif

//__STL_BEGIN_NAMESPACE

template <class _Tp> class my_auto_ptr {
private:
  _Tp* _M_ptr;

public:
  typedef _Tp element_type;
  explicit my_auto_ptr(_Tp* __p = 0) __STL_NOTHROW : _M_ptr(__p) {}
  my_auto_ptr(my_auto_ptr& __a) __STL_NOTHROW : _M_ptr(__a.release()) {}
  template <class _Tp1> my_auto_ptr(my_auto_ptr<_Tp1>& __a) __STL_NOTHROW
    : _M_ptr(__a.release()) {}
  my_auto_ptr& operator=(my_auto_ptr& __a) __STL_NOTHROW {
    if (&__a != this) {
      delete _M_ptr;
      _M_ptr = __a.release();
    }
    return *this;
  }
  template <class _Tp1>
  my_auto_ptr& operator=(my_auto_ptr<_Tp1>& __a) __STL_NOTHROW {
    if (__a.get() != this->get()) {
      delete _M_ptr;
      _M_ptr = __a.release();
    }
    return *this;
  }
  ~my_auto_ptr() __STL_NOTHROW { delete _M_ptr; }

  _Tp& operator*() const __STL_NOTHROW {
    return *_M_ptr;
  }
  _Tp* operator->() const __STL_NOTHROW {
    return _M_ptr;
  }
  _Tp* get() const __STL_NOTHROW {
    return _M_ptr;
  }
  _Tp* release() __STL_NOTHROW {
    _Tp* __tmp = _M_ptr;
    _M_ptr = 0;
    return __tmp;
  }
  void reset(_Tp* __p = 0) __STL_NOTHROW {
    delete _M_ptr;
    _M_ptr = __p;
  }

  // According to the C++ standard, these conversions are required.  Most
  // present-day compilers, however, do not enforce that requirement---and, 
  // in fact, most present-day compilers do not support the language 
  // features that these conversions rely on.

//#ifdef __SGI_STL_USE_AUTO_PTR_CONVERSIONS

private:
  template<class _Tp1> struct my_auto_ptr_ref {
    _Tp1* _M_ptr;
    my_auto_ptr_ref(_Tp1* __p) : _M_ptr(__p) {}
  };

public:
  my_auto_ptr(my_auto_ptr_ref<_Tp> __ref) __STL_NOTHROW
    : _M_ptr(__ref._M_ptr) {}
  template <class _Tp1> operator my_auto_ptr_ref<_Tp1>() __STL_NOTHROW 
    { return my_auto_ptr_ref<_Tp>(this->release()); }
  template <class _Tp1> operator my_auto_ptr<_Tp1>() __STL_NOTHROW
    { return my_auto_ptr<_Tp1>(this->release()); }

//#endif /* __SGI_STL_USE_AUTO_PTR_CONVERSIONS */
};

//__STL_END_NAMESPACE

#endif /* __SGI_STL_MEMORY */

typedef my_auto_ptr<tExpression> pExpression;
#else
typedef auto_ptr<tExpression> pExpression;
#endif // SGI_AUTO_PTR

struct tValue : tExpression
{
	tArgumentType<float> argumentType;
	float evaluate();
	tValue(eVariableAttribute attribute, tVariableType *vt);
	tValue(eUnitVariable uv);
	tValue(tFunctionType *ft);
};

struct tConstant : tExpression
{
	float constant;
	float evaluate() { return constant; }
	tConstant(float _constant) : constant(_constant) {}
	bool isConstant() { return true; }
};

struct tNegate : tExpression
{
	pExpression exp;
	static float evaluate(float a) { return -a; }
    float evaluate() { return evaluate(exp->evaluate()); }
	tNegate(pExpression _exp) : exp(_exp) {}
};

struct tNot : tExpression
{
	pExpression exp;
	static float evaluate(float a)
    {
        if(a==0)
            return 1;
        return 0;
    }
    float evaluate() { return evaluate(exp->evaluate()); }
	tNot(pExpression _exp) : exp(_exp) {}
};

struct tExponent : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(b==2)	return a*a;
		if(b==3) 	return a*a*a;
		return pow(a,b);
	}
	float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tExponent(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tMultiply : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		return a*b;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tMultiply(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tDivide : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		//Watch out for division-by-zero:
		if(b==0)
			return 0;
		return a/b;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tDivide(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tModulus : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		//Watch out for division-by-zero:
		if(b==0)
			return 0;
		return fmod(a,b);
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tModulus(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tAdd : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		return a+b;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tAdd(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tSubtract : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		return a-b;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tSubtract(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tLessThan : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a<b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tLessThan(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tLessThanOrEqual : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a<=b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tLessThanOrEqual(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tGreaterThan : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a>b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tGreaterThan(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tGreaterThanOrEqual : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a>=b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tGreaterThanOrEqual(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tEqual : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a==b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tEqual(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tNotEqual : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a!=b)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tNotEqual(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tAnd : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a!=0 && b!=0)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tAnd(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tOr : tExpression
{
	pExpression exp1;
	pExpression exp2;
	static float evaluate(float a, float b)
	{
		if(a!=0 || b!=0)
			return 1;
		return 0;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate()); }
	tOr(pExpression _exp1, pExpression _exp2) : exp1(_exp1), exp2(_exp2) {}
};

struct tIfThenElse : tExpression
{
	pExpression exp1;
	pExpression exp2;
	pExpression exp3;
	static float evaluate(float a, float b, float c)
	{
		if(a!=0)
			return b;
		return c;
	}
    float evaluate() { return evaluate(exp1->evaluate(),exp2->evaluate(),exp3->evaluate()); }
	tIfThenElse(pExpression _exp1, pExpression _exp2, pExpression _exp3) :
		exp1(_exp1), exp2(_exp2), exp3(_exp3) {}
};

struct tSquareRoot : tExpression
{
	pExpression exp;
	static float evaluate(float a)
	{
		if(a<0)
			return 0;
		return sqrt(a);
	}
    float evaluate() { return evaluate(exp->evaluate()); }
	tSquareRoot(pExpression _exp) : exp(_exp) {}
};

struct tSine : tExpression
{
	pExpression exp;
	static float evaluate(float a)
	{
		return sin(a);
	}
    float evaluate() { return evaluate(exp->evaluate()); }
	tSine(pExpression _exp) : exp(_exp) {}
};

struct tCosine : tExpression
{
	pExpression exp;
	static float evaluate(float a)
	{
		return cos(a);
	}
    float evaluate() { return evaluate(exp->evaluate()); }
	tCosine(pExpression _exp) : exp(_exp) {}
};

extern tExpression *parseExpression(const char *s, tModelType *modelType) throw(tString);
extern float evaluateExpression(tExpression* exp, tModel *_context);
/****************************************************************************/
#endif

