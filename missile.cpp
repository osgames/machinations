/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MISSILE.CPP:  Draws and moves different forms of missiles.                *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "missile.h"
#include "engine.h"
#include "memwatch.h"
#include "oglwrap.h"
#pragma package(smart_init)

static vector<tMissileType *> missileTypes;
static eMissileStyle parseStyle(const char *fieldName, tStream& s) throw(tString);

/****************************************************************************/
tMissileType::tMissileType(tStream& s) throw(tString) : style(MISSILE_LASER),
    red(0), green(1), blue(0), firePower(10), velocity(30), soundType(NULL), length(5),
    width(2), armorClass(0) //Defaults to a green laser
{
	name=parseString("missile type name", s);
	name=STRTOLOWER(name);
	if(missileTypeLookup(name, /*exact=*/true)!=NULL)
		throw tString("A missile by the name of '")+name+"' already exists";

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"style"))
				style=parseStyle("style",s);
			else if(strPrefix(str,"color"))
			{
				red=parseFloatRange("red component",0,1,s);
				green=parseFloatRange("green component",0,1,s);
				blue=parseFloatRange("blue component",0,1,s);
			}
			else if(strPrefix(str,"fire_power")||strPrefix(str,"damage"))
				firePower=parseFloatMin("firepower",0,s);
			else if(strPrefix(str,"velocity"))
				velocity=parseFloatMin("velocity",1,s);
			else if(strPrefix(str,"sound_name"))
				soundType=parseSoundType("sound name",s);
			else if(strPrefix(str,"length"))
				length=parseFloatMin("length",0,s);
			else if(strPrefix(str,"width"))
				width=parseFloatMin("laser width",0,s);
			else if(strPrefix(str,"radius"))
				width=parseFloatMin("cannonball radius",0,s);
			else if(strPrefix(str,"armor_class"))
				armorClass=parseArmorClass("armor class name",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
}
/****************************************************************************/
tMissile::tMissile(tUnit *_source, const tVector3D& origin, tUnit *_target,
    tMissileType *_type, tWorldTime _startTime) throw(int) :
    source(_source), target(_target), startTime(_startTime), initTime(_startTime),
    expl(NULL), finished(false), randOffset(zeroVector3D), soundSource(0),
    sourcePos(zeroVector3D), targetPos(zeroVector3D), type(_type), laserVisible(true)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(source))
        throw -1;
    if(MEMORY_VIOLATION(target))
        throw -1;
    if(MEMORY_VIOLATION(type))
        throw -1;
    if(type->velocity<=0) //Zero velocity will cause divide-by-zero error
    {
        bug("tMissile::tMissile: velocity <= 0");
        throw -1;
    }
#endif
    //Source's relative position from target; z-component is zero
    tVector2D offset2D=source->getPosition()-target->getPosition();
    //endTime doesn't change--must be synchronized!
    endTime=startTime+offset2D.length()/type->velocity;

    initPos=headPos=tailPos=sourcePos=origin;
    targetPos=target->getPosition3D();

    //Adjust the offset so that it is perpendicular to the unit's up vector
    tVector3D offset3D=sourcePos-targetPos;
    offset3D-=target->getUp3D()*offset3D.dot(target->getUp3D());
    offset3D=offset3D.normalize();

    tVector3D offsetPerp=target->getUp3D();
    offsetPerp=offsetPerp.cross(offset3D);

    //These statements will not affect synchronization:
    randOffset = offset3D * (target->getModel()->getSelRadius()+0.1);
        //0.1 fudge factor so that animation will clear inclined vertical plane
    randOffset += offsetPerp * ((rand()%512-256)/1024.0) * target->getModel()->getSelRadius();
    randOffset += target->getUp3D() * ((rand()%512+256)/1024.0) * target->getModel()->getMaxZ();

    calcTargetPos();

    //Assign a new sound source to the missile:
    soundSource=sndNewSource(type->soundType,headPos,_startTime);
}
/****************************************************************************/
void tMissile::calcTargetPos()
//precondition: target != NULL
{
    //use algebra to calculate the trajectory of the missile
    //coordinates of missile origin
    float x1=sourcePos.x;
    float y1=sourcePos.y;

    //coordinates of target origin
    targetPos=target->getPosition3D();
    float x2=targetPos.x;
    float y2=targetPos.y;

    //velocity of missile
    float v=type->velocity;

    //movement of target
    tVector2D q=target->getVelocity();
    float m=q.x;
    float n=q.y;

    //solve the equations for u and t:
    //x1 + vtcos u = x2 + mt
    //y1 + vtsin u = y2 + nt
    double f=v*(y2-y1);
    double g=v*(x2-x1);
    double h=(y2-y1)*m-(x2-x1)*n;

    if(x1!=x2)
    {
        //solve the quadratic equation for x
        //(f^2+g^2)x^2 - 2fhx + h^2-g^2 = 0
        double a=f*f+g*g;
        double b=-2*f*h;
        double c=h*h-g*g;
        double d=b*b-4*a*c;
        if(d>0&&a!=0)
        {
            float cosu=(-b-sqrt(d))/2/a;
            if((x2-x1)/(v*cosu-m)<0)
                cosu=(-b+sqrt(d))/2/a;
            float t=(x2-x1)/(v*cosu-m);
            targetPos.x=x2+m*t;
            targetPos.y=y2+n*t;
        }
    }
    else if(y1!=y2)
    {
        //solve the quadratic equation for x
        //(f^2+g^2)x^2 + 2ghx + h^2-f^2 = 0
        double a=f*f+g*g;
        double b=2*g*h;
        double c=h*h-f*f;
        double d=b*b-4*a*c;
        if(d>0&&a!=0)
        {
            float sinu=(-b-sqrt(d))/2/a;
            if((y2-y1)/(v*sinu-n)<0)
                sinu=(-b+sqrt(d))/2/a;
            float t=(y2-y1)/(v*sinu-n);
            targetPos.x=x2+m*t;
            targetPos.y=y2+n*t;
        }
    }

    targetPos+=randOffset;
}
/****************************************************************************/
//Called at each game interval
void tMissile::intervalTick(tWorldTime t)
{
    if(target && t<endTime)
    {
        sourcePos=headPos;
        startTime=t;
        calcTargetPos();
    }
    else if(t>=endTime && expl==NULL)
    {
        expl=mwNew tAnim(animTypeLookup("Explosion",/*exact=*/true));
        expl->start(t);
        if(target)
            target->damage(source, type->firePower, type->armorClass, t);
    }
    if(soundSource>0)
        sndPositionSource(soundSource,headPos);
}
/****************************************************************************/
//Called before each frame
void tMissile::tick(tWorldTime t)
{
    if(finished)
        return;

    if(t<startTime)
    {
        headPos=tailPos=sourcePos;
        return;
    }

    tVector3D offset=(targetPos-sourcePos)/(endTime-startTime);
    float dt=type->length/type->velocity;

    if(t<endTime)
        headPos=sourcePos+offset*(t-startTime);
    else
        headPos=targetPos;

    if(t<initTime+dt)
        tailPos=initPos;
    else if(t<endTime+dt)
        tailPos=sourcePos+offset*(t-startTime-dt);
    else
    {
        tailPos=targetPos;
        laserVisible=false;
    }

    if(expl)
    {
        expl->tick(t);
        if(expl->isFinished())
            finished=true;
    }

    //Destroy the sound source if we can
    if(soundSource>0 && sndIsFinished(soundSource))
    {
        sndDeleteSource(soundSource);
        soundSource=0;
    }
}
/****************************************************************************/
//Draw a segment between A and B
void tMissile::draw()
{
    if(finished)
        return;
    if(expl)
    {
        glPushMatrix();
        glTranslatef(targetPos.x, targetPos.y, targetPos.z);
        glRotatef(mapRotation*180/M_PI,0,0,-1);
        glRotatef(mapTilt*180/M_PI,1,0,0);
        glScalef(.01,.01,1);
        glColor3f(1,1,1);
        expl->draw(0,0);
        glPopMatrix();
    }
    if(laserVisible)
    {
        switch(type->style)
        {
            case MISSILE_CANNON:
                glColor3f(type->red,type->green,type->blue);
                glPointSize(type->width);
                glEnable(GL_POINT_SMOOTH);
                glEnable(GL_BLEND);
                glBegin(GL_POINTS);
                    glVertex3fv((float *) &headPos);
                glEnd();
                glDisable(GL_BLEND);
                glDisable(GL_POINT_SMOOTH);
                glPointSize(1);
                break;
            case MISSILE_LASER:
                glEnable(GL_BLEND);
                tVector3D c=(headPos+tailPos)/2;
                glLineWidth(type->width);
                glBegin(GL_LINES);
                    glColor4f(type->red,type->green,type->blue,0);
                    glVertex3fv((float *) &headPos);
                    glColor4f(type->red,type->green,type->blue,1);
                    glVertex3fv((float *) &c);

                    glVertex3fv((float *) &c);
                    glColor4f(type->red,type->green,type->blue,0);
                    glVertex3fv((float *) &tailPos);
                glEnd();
                glLineWidth(1);
                glDisable(GL_BLEND);
                break;
        }
    }
}
/****************************************************************************/
tMissile::~tMissile()
{
    if(expl)
        mwDelete expl;
    if(soundSource>0)
        sndDeleteSource(soundSource);
}
/****************************************************************************/
void tMissile::onUnitDestroyed(tUnit *u, tWorldTime t)
{
    if(u==source)
        source=NULL;
    if(u==target)
        target=NULL;
}
/****************************************************************************/
void parseMissileTypeBlock(tStream& s) throw(tString)
{
    tMissileType *mt = mwNew tMissileType(s);
    if(mt)
        missileTypes.push_back(mt);
}
/****************************************************************************/
//deallocate memory used by missile types
void destroyMissileTypes()
{
    for(int i=0; i<missileTypes.size(); i++)
        mwDelete missileTypes[i];
    missileTypes.clear();
}
/****************************************************************************/
//lookup missile type with name (NULL if not found)
tMissileType *missileTypeLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0; i<missileTypes.size(); i++)
        if(STREQUAL(missileTypes[i]->name,n))
            return missileTypes[i];
    if(exact)
        return NULL;
    for(i=0; i<missileTypes.size(); i++)
        if(STRPREFIX(n,missileTypes[i]->name)) //STRPREFIX is case-insensitive
            return missileTypes[i];
    return NULL;
}
/****************************************************************************/
tMissileType *parseMissileType(const char *fieldName, tStream& s) throw(tString)
{
    tMissileType *mt;
    tString str=parseString(fieldName, s);
    mt=missileTypeLookup(str,/*exact=*/false);
    if(mt==NULL)
        throw tString("Unrecognized missile type '")+str+"'";
    return mt;
}
/****************************************************************************/
eMissileStyle parseStyle(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName,s);
    if(strPrefix(str,"laser"))
        return MISSILE_LASER;
    else if(strPrefix(str,"cannon"))
        return MISSILE_CANNON;

    throw tString("Unrecognized missile style '")+str+"'";
}
