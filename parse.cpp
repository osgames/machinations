/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* PARSE.CPP:   Defines a generic class for parsing strings and data         *
*              files.                                                       *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "parse.h"
#include "memwatch.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)
/****************************************************************************/
tKeyTableEntry keyTable[KEY_TABLE_SIZE]={
	{"ESC",			GLFW_KEY_ESC},
	{"F1",			GLFW_KEY_F1},
	{"F2",			GLFW_KEY_F2},
	{"F3",			GLFW_KEY_F3},
	{"F4",			GLFW_KEY_F4},
	{"F5",			GLFW_KEY_F5},
	{"F6",			GLFW_KEY_F6},
	{"F7",			GLFW_KEY_F7},
	{"F8",			GLFW_KEY_F8},
	{"F9",			GLFW_KEY_F9},
	{"F10",			GLFW_KEY_F10},
	{"F11",			GLFW_KEY_F11},
	{"F12",			GLFW_KEY_F12},
	{"F13",			GLFW_KEY_F13},
	{"F14",			GLFW_KEY_F14},
	{"F15",			GLFW_KEY_F15},
	{"F16",			GLFW_KEY_F16},
	{"F17",			GLFW_KEY_F17},
	{"F18",			GLFW_KEY_F18},
	{"F19",			GLFW_KEY_F19},
	{"F20",			GLFW_KEY_F20},
	{"F21",			GLFW_KEY_F21},
	{"F22",			GLFW_KEY_F22},
	{"F23",			GLFW_KEY_F23},
	{"F24",			GLFW_KEY_F24},
	{"F25",			GLFW_KEY_F25},
	{"UP",			GLFW_KEY_UP},
	{"DOWN",		GLFW_KEY_DOWN},
	{"LEFT",		GLFW_KEY_LEFT},
	{"RIGHT",		GLFW_KEY_RIGHT},
	{"LSHIFT",		GLFW_KEY_LSHIFT},
	{"RSHIFT",		GLFW_KEY_RSHIFT},
	{"LCTRL",		GLFW_KEY_LCTRL},
	{"RCTRL",		GLFW_KEY_RCTRL},
	{"LALT",		GLFW_KEY_LALT},
	{"RALT",		GLFW_KEY_RALT},
	{"TAB",			GLFW_KEY_TAB},
	{"ENTER",		GLFW_KEY_ENTER},
	{"BACKSPACE",	GLFW_KEY_BACKSPACE},
	{"INSERT",		GLFW_KEY_INSERT},
	{"DEL",			GLFW_KEY_DEL},
	{"PAGEUP",		GLFW_KEY_PAGEUP},
	{"PAGEDOWN",	GLFW_KEY_PAGEDOWN},
	{"HOME",		GLFW_KEY_HOME},
	{"END",			GLFW_KEY_END},
	{"KP_0",		GLFW_KEY_KP_0},
	{"KP_1",		GLFW_KEY_KP_1},
	{"KP_2",		GLFW_KEY_KP_2},
	{"KP_3",		GLFW_KEY_KP_3},
	{"KP_4",		GLFW_KEY_KP_4},
	{"KP_5",		GLFW_KEY_KP_5},
	{"KP_6",		GLFW_KEY_KP_6},
	{"KP_7",		GLFW_KEY_KP_7},
	{"KP_8",		GLFW_KEY_KP_8},
	{"KP_9",		GLFW_KEY_KP_9},
	{"KP_DIVIDE",	GLFW_KEY_KP_DIVIDE},
	{"KP_MULTIPLY",	GLFW_KEY_KP_MULTIPLY},
	{"KP_SUBTRACT",	GLFW_KEY_KP_SUBTRACT},
	{"KP_ADD",		GLFW_KEY_KP_ADD},
	{"KP_DECIMAL",	GLFW_KEY_KP_DECIMAL},
	{"KP_EQUAL",	GLFW_KEY_KP_EQUAL},
	{"KP_ENTER",	GLFW_KEY_KP_ENTER}
};

tStringBuffer::tStringBuffer(const tString& s) : pointer(NULL)
{
    pointer=mwNew char[STRLEN(s)+1];
    strcpy(pointer,CHARPTR(s));
}
tStringBuffer::~tStringBuffer()
{
    if(pointer)
        mwDelete[] pointer;
}

tStream::tStream() : file(NULL), stringStart(NULL), stringPointer(NULL),
    commentLevel(0), nextChar(0), nextSymbol(0),
    curLineNumber(1), nextLineNumber(1), curEndOfLine(true), nextEndOfLine(true),
    commentStart(0), curEndOfStream(false), nextEndOfStream(false)
{
}

void tStream::load(FILE *_file) throw(tString)
{
    file=_file;

    getNextChar();
    getNextSymbol();
    getNextToken();
}

void tStream::load(const char *string) throw(tString)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(string))
        return;
#endif
    //Reset the string pointer to the beginning of the line.
    stringStart=strdup(string);
    stringPointer=stringStart;

    getNextChar();
    getNextSymbol();
    getNextToken();
}

tStream::~tStream()
{
    if(stringStart)
        free(stringStart);
}

char tStream::getNextChar()
{
    char curChar=nextChar;

    if(file)
    {
        int ch=fgetc(file);
        if(ch<0)
            ch=0;
        nextChar=(char)ch;
    }
    else if(stringPointer)
    {
        nextChar=*stringPointer;
        if(nextChar)
            stringPointer++;
    }

    return curChar;
}

int tStream::getNextSymbol()
{
    int curSymbol=nextSymbol;

    nextSymbol=getNextChar();
    if(nextSymbol=='\\')
    {
        if(tolower(nextChar)=='n')
        {
            getNextChar();
            nextSymbol=256+(int)'\n';
        }
        else if(isdigit(nextChar))
            nextSymbol=256+(int)(getNextChar()-'0');
        else
            nextSymbol=256+getNextChar();
    }
    else if(nextSymbol=='\n')
    {
        if(nextChar=='\r')
            getNextChar();
        nextSymbol=PARSE_NEW_LINE;
    }
    else if(nextSymbol=='\r')
    {
        if(nextChar=='\n')
            getNextChar();
        nextSymbol=PARSE_NEW_LINE;
    }

    return curSymbol;
}

bool tStream::isWhiteSpace(int sym)
{
    if(sym==PARSE_NEW_LINE)
        return true;
    char ch=(char)sym;
    return (strchr(" \t\n\r",ch)!=NULL);
}
bool tStream::isPunctuation(char ch)
{
    return (ch=='(' || ch==')' || ch=='=' || ch=='{' || ch=='}' ||
        ch=='[' || ch==']' || ch=='<' || ch=='>' || ch==';' ||
        ch==',');
}

const tString& tStream::getNextToken() throw(tString)
{
    bool lineComment=false;
    bool breakOnNextNonWhiteSpace=false;
    bool foundNonWhiteSpace=false;

    int sym;

    curToken=nextToken;
    curLineNumber=nextLineNumber;
    curEndOfLine=nextEndOfLine;
    curEndOfStream=nextEndOfStream;
    nextToken="";

    while(true)
    {
        switch(nextSymbol)
        {
            case '\0':
                if(commentLevel>0)
                {
                    char buf[1000];
                    snprintf(buf,sizeof(buf),"Unterminated comment beginning on line %ld",commentStart);
                    throw tString(buf);
                }
                if(!foundNonWhiteSpace)
                    curEndOfStream=true;
                nextEndOfLine=true;
                nextEndOfStream=true;
                goto break2;
            case PARSE_NEW_LINE:
                if(foundNonWhiteSpace)
                {
                    nextEndOfLine=true;
                    goto break2;
                }
                getNextSymbol();
                nextLineNumber++;
                lineComment=false;
                continue;
            case '/':
                if(!lineComment && nextChar=='*')
                {
                    getNextSymbol();
                    getNextSymbol();
                    if(commentLevel==0)
                        commentStart=nextLineNumber;
                    commentLevel++;
                    continue;
                }
                else if(commentLevel==0 && nextChar=='/')
                {
                    getNextSymbol();
                    getNextSymbol();
                    lineComment=true;
                    continue;
                }
                break;
            case '*':
                if(!lineComment && nextChar=='/')
                {
                    getNextSymbol();
                    getNextSymbol();
                    commentLevel--;
                    if(commentLevel<0)
                        throw tString("Unexpected */");
                    continue;
                }
                break;
            case '#':
                if(commentLevel==0 && nextSymbol=='#')
                {
					getNextSymbol();
                    lineComment=true;
                    continue;
                }
                break;
            default:
                if(commentLevel==0 && !lineComment)
                {
                    if(isWhiteSpace(nextSymbol))
                    {
                        getNextSymbol();
                        if(foundNonWhiteSpace)
                            breakOnNextNonWhiteSpace=true;
                        continue;
                    }
                }
                break;
        }

        if(commentLevel==0 && !lineComment)
        {
            if(breakOnNextNonWhiteSpace || foundNonWhiteSpace &&
                (isPunctuation(nextSymbol) || nextSymbol=='-' || nextSymbol=='+'))
            {
                nextEndOfLine=false;
                goto break2;
            }

            sym=getNextSymbol();
			if(sym=='~')
			{
				sym=getNextSymbol();
				if(sym==PARSE_NEW_LINE)
                {
					sym=getNextSymbol();
                    nextLineNumber++;
                }
                while(sym!='~' && sym!='\0')
                {
					if(!(sym==PARSE_NEW_LINE && nextSymbol=='~'))
						nextToken+=(char)sym;
                    sym=getNextSymbol();
                    if(sym==PARSE_NEW_LINE)
                        nextLineNumber++;
                }
                if(sym!='~')
                    throw tString("Unterminated string");
                foundNonWhiteSpace=true;
			}
            else if(sym=='\"')
            {
                sym=getNextSymbol();
                while(sym!='\"' && sym!='\0' && sym!=PARSE_NEW_LINE)
                {
                    nextToken+=(char)sym;
                    sym=getNextSymbol();
                }
                if(sym!='\"')
                    throw tString("Unterminated string");
                foundNonWhiteSpace=true;
            }
            else
            {
                nextToken+=(char)sym;
                if(isPunctuation(sym))
                    breakOnNextNonWhiteSpace=true;
                foundNonWhiteSpace=true;
            }
        }
        else
            getNextSymbol();
    }
break2: ;

    return curToken;
}

bool tStream::beginBlock()
{
    eBraceStyle brace;

    if(nextToken=="(")
        brace=BRACE_PARENTHESIS;
    else if(nextToken=="[")
        brace=BRACE_SQUARE_BRACKET;
    else if(nextToken=="{")
        brace=BRACE_CURLY_BRACE;
    else if(nextToken=="<")
        brace=BRACE_ANGLE_BRACKET;
    else
        return false;

    getNextToken();
    braces.push(brace);
    return true;
}

bool tStream::endBlock() throw(tString)
{
    eBraceStyle brace;

    if(nextToken==")")
        brace=BRACE_PARENTHESIS;
    else if(nextToken=="]")
        brace=BRACE_SQUARE_BRACKET;
    else if(nextToken=="}")
        brace=BRACE_CURLY_BRACE;
    else if(nextToken==">")
        brace=BRACE_ANGLE_BRACKET;
    else
        return false;

    if(brace!=braces.top())
        throw tString("Brace styles do not match");
    braces.pop();
    getNextToken();
    return true;
}

const tString& tStream::parseToken(const char *fieldName) throw(tString)
{
    if(curEndOfStream)
        throw tString("Missing '")+fieldName+"'";
    return getNextToken();
}
void tStream::matchToken(const char *keyword) throw(tString)
{
    tString str=getNextToken();
    if(str=="")
        throw tString("Missing keyword or symbol '")+keyword+"'";
    if(!strPrefix(str,keyword))
        throw tString("Expected keyword or symbol '")+keyword+"'";
}

bool tStream::matchOptionalToken(const char *keyword) throw(tString)
{
	if(strPrefix(nextToken,keyword))
	{
		getNextToken();
		return true;
	}
	return false;
}

void tStream::endStatement() throw(tString)
{
	if(matchOptionalToken(";"))
		return;
    if(nextToken==")" || nextToken=="]" || nextToken=="}" || nextToken==">")
        return;
	if(curEndOfLine)
		return;
	throw tString("Unexpected token at end of statement: '")+nextToken+"'";
}

bool tStream::doesBlockHaveName()
{
    if(curEndOfLine)
        return false;
    if(nextToken=="(" || nextToken=="[" || nextToken=="{" || nextToken=="<")
        return false;
	if(nextToken==";")
		return false;
    return true;
}

bool tStream::hasOptionalParameter()
{
    if(curEndOfLine)
        return false;
    if(nextToken==")" || nextToken=="]" || nextToken=="}" || nextToken==">")
        return false;
	if(nextToken==";")
		return false;
	return true;
}
tString tStream::parseErrorMessage(const char *filename, const tString& message)
{
    char buf[1000];
    snprintf(buf,sizeof(buf),"Error in '%s' line #%ld:\n",filename,curLineNumber);
    return tString(buf) + message;
}

const tString& parseString(const char *fieldName, tStream& s) throw(tString)
{
    return s.parseToken(fieldName);
}

void matchString(const char *keyword, tStream& s) throw(tString)
{
    s.matchToken(keyword);
}

tString parseIdentifier(const char *fieldName, tStream& s) throw(tString)
{
    const tString& str=s.parseToken(fieldName);
    tStringBuffer buf(str);
    char *ptr=buf.begin();

    if(isdigit(*ptr))
        throw tString("Identifiers must begin with a letter or an underscore");
    while(*ptr)
    {
        if(isalnum(*ptr) || *ptr=='_')
            *ptr=tolower(*ptr);
        else
            throw tString("Identifiers can only contains letters, numbers, and underscores");
        ptr++;
    }
    return tString(buf.begin());
}

int parseInt(const char *fieldName, tStream& s) throw(tString)
{
    int i;
    const char *str=CHARPTR(s.parseToken(fieldName));
    if(sscanf(str, "%d", &i)<1)
        throw tString("Invalid '")+fieldName+"'";
    return i;
}

unsigned int parseUnsigned(const char *fieldName, tStream& s) throw(tString)
{
    unsigned int u;
    const char *str=CHARPTR(s.parseToken(fieldName));
    if(sscanf(str, "%u", &u)<1)
        throw tString("Invalid '")+fieldName+"'";
    return u;
}

long parseLong(const char *fieldName, tStream& s) throw(tString)
{
    long l;
    const char *str=CHARPTR(s.parseToken(fieldName));
    if(sscanf(str, "%ld", &l)<1)
        throw tString("Invalid '")+fieldName+"'";
    return l;
}

float parseFloat(const char *fieldName, tStream& s) throw(tString)
{
    float f;
    const tString& token=s.parseToken(fieldName);
    if(strPrefix(token,"true") || strPrefix(token,"yes"))
        return 1;
    if(strPrefix(token,"false") || strPrefix(token,"no"))
        return 0;

    tStringBuffer sb(token);
    char *str=sb.begin();
    strToLower(str);
    char *ptr;

    ptr=strstr(str,"pi");
    if(ptr)
    {
        char ch;
        float f,val=M_PI;

        if(ptr>str)
        {
            if(sscanf(str, "%f", &f)<1)
                throw tString("Please enter angles in the form ##PI/## (e.g. 1.5PI, PI/2)");
            val*=f;
        }

        int retval=sscanf(ptr,"pi%c%f",&ch,&f);
        if(retval<=0)
            return val;
        else if(retval==1)
            throw tString("Please enter angles in the form ##PI/## (e.g. 1.5PI, PI/2)");

        if(ch=='/')
            val/=f;
        else if(ch=='*')
            val*=f;
        else
            throw tString("Please enter angles in the form ##PI/## (e.g. 1.5PI, PI/2)");
        return val;
    }

    ptr=strstr(str,"sqrt");
    if(ptr)
    {
        char ch;
        float base,f,val=1;

        if(ptr>str)
        {
            if(sscanf(str, "%f", &f)<1)
                throw tString("Please enter radicals in the form ##sqrt(##)/## (e.g. 4sqrt(5), sqrt(2)/2)");
            val=f;
        }

        int retval=sscanf(ptr,"sqrt(%f)%c%f",&base,&ch,&f);
        if(retval==0)
            retval=sscanf(ptr,"sqrt%f%c%f",&base,&ch,&f);

        if(retval<=0)
            throw tString("Please enter radicals in the form ##sqrt(##)/## (e.g. 4sqrt(5), sqrt(2)/2)");
        if(base<0)
            throw tString("Imaginary number are not allowed");
        val*=sqrt(base);

        if(retval==1)
            return val;
        if(retval==2)
            throw tString("Please enter radicals in the form ##sqrt(##)/## (e.g. 4sqrt(5), sqrt(2)/2)");

        if(ch=='/')
            val/=f;
        else if(ch=='*')
            val*=f;
        else
            throw tString("Please enter radicals in the form ##sqrt(##)/## (e.g. 4sqrt(5), sqrt(2)/2)");
        return val;
    }

    if(sscanf(str, "%f", &f)<1)
        throw tString("Invalid '")+fieldName+"'";
    return f;
}

int parseIntRange(const char *fieldName, int min, int max, tStream& s) throw(tString)
{
    char buf[1000];
    int i=parseInt(fieldName,s);
    if(i<min || i>max)
    {
        snprintf(buf,sizeof(buf),"'%s' must be between %d and %d",fieldName,min,max);
        throw tString(buf);
    }
    return i;
}

long parseLongRange(const char *fieldName, long min, long max, tStream& s) throw(tString)
{
    char buf[1000];
    long l=parseLong(fieldName,s);
    if(l<min || l>max)
    {
        snprintf(buf,sizeof(buf),"'%s' must be between %ld and %ld",fieldName,min,max);
        throw tString(buf);
    }
    return l;
}

float parseFloatRange(const char *fieldName, float min, float max, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f<min || f>max)
    {
        snprintf(buf,sizeof(buf),"'%s' must be between %5.3f and %5.3f",fieldName,min,max);
        throw tString(buf);
    }
    return f;
}

int parseIntMin(const char *fieldName, int min, tStream& s) throw(tString)
{
    char buf[1000];
    int i=parseInt(fieldName,s);
    if(i<min)
    {
        snprintf(buf,sizeof(buf),"Minimum value for '%s' is %d",fieldName,min);
        throw tString(buf);
    }
    return i;
}

long parseLongMin(const char *fieldName, long min, tStream& s) throw(tString)
{
    char buf[1000];
    long l=parseLong(fieldName,s);
    if(l<min)
    {
        snprintf(buf,sizeof(buf),"Minimum value for '%s' is %ld",fieldName,min);
        throw tString(buf);
    }
    return l;
}

float parseFloatMin(const char *fieldName, float min, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f<min)
    {
        snprintf(buf,sizeof(buf),"Minimum value for '%s' is %5.3f",fieldName,min);
        throw tString(buf);
    }
    return f;
}

int parseIntMax(const char *fieldName, int max, tStream& s) throw(tString)
{
    char buf[1000];
    int i=parseInt(fieldName,s);
    if(i>max)
    {
        snprintf(buf,sizeof(buf),"Maximum value for '%s' is %d",fieldName,max);
        throw tString(buf);
    }
    return i;
}

long parseLongMax(const char *fieldName, long max, tStream& s) throw(tString)
{
    char buf[1000];
    long l=parseLong(fieldName,s);
    if(l>max)
    {
        snprintf(buf,sizeof(buf),"Maximum value for '%s' is %ld",fieldName,max);
        throw tString(buf);
    }
    return l;
}

float parseFloatMax(const char *fieldName, float max, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f>max)
    {
        snprintf(buf,sizeof(buf),"Maximum value for '%s' is %5.3f",fieldName,max);
        throw tString(buf);
    }
    return f;
}

bool parseBool(const char *fieldName, tStream& s) throw(tString)
{
    const char *str=CHARPTR(s.parseToken(fieldName));
    if(strPrefix(str,"yes") || strPrefix(str,"true"))
        return true;
    else if(strPrefix(str,"no") || strPrefix(str,"false"))
        return false;
    else
        throw tString("Invalid '")+fieldName+"'";
}

tVector2D parseVector2D(const char *fieldName, tStream& s) throw(tString)
{
    tVector2D v;
    if(s.atEndOfStream())
        throw tString("Missing x-coordinate of '")+fieldName+"'";
    v.x=parseFloat(fieldName,s);
    if(s.atEndOfStream())
        throw tString("Missing y-coordinate of '")+fieldName+"'";
    v.y=parseFloat(fieldName,s);
    return v;
}

tVector3D parseVector3D(const char *fieldName, tStream& s) throw(tString)
{
    tVector3D v;
    if(s.atEndOfStream())
        throw tString("Missing x-coordinate of '")+fieldName+"'";
    v.x=parseFloat(fieldName,s);
    if(s.atEndOfStream())
        throw tString("Missing y-coordinate of '")+fieldName+"'";
    v.y=parseFloat(fieldName,s);
    if(s.atEndOfStream())
        throw tString("Missing z-coordinate of '")+fieldName+"'";
    v.z=parseFloat(fieldName,s);
    return v;
}
tKey parseHotKey(const char *fieldName, tStream& s) throw(tString)
{
    int i;
    tString str=STRTOUPPER(s.parseToken(fieldName));
    for(i=0;i<KEY_TABLE_SIZE;i++)
        if(str==keyTable[i].name)
            return keyTable[i].value;

    if(STRLEN(str)==1)
    {
        char ch=str[0];
        if(ch>='A' && ch<='Z')
            return ch;
        if(ch>='0' && ch<='9')
            return ch;
    }

    throw tString("Unrecognized key '")+str+"'";
}
float parseAngle(const char *fieldName, tStream& s) throw(tString)
{
    return parseFloat(fieldName,s);
}
float parseAngleRange(const char *fieldName, float min, float max, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f<min*M_PI || f>max*M_PI)
    {
        snprintf(buf,sizeof(buf),"'%s' must be between %4.2fPI and %4.2fPI",fieldName,min,max);
        throw tString(buf);
    }
    return f;
}
float parseAngleMin(const char *fieldName, float min, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f<min*M_PI)
    {
        snprintf(buf,sizeof(buf),"Minimum value for '%s' is %4.2fPI",fieldName,min);
        throw tString(buf);
    }
    return f;
}
float parseAngleMax(const char *fieldName, float max, tStream& s) throw(tString)
{
    char buf[1000];
    float f=parseFloat(fieldName,s);
    if(f>max*M_PI)
    {
        snprintf(buf,sizeof(buf),"Maximum value for '%s' is %4.2fPI",fieldName,max);
        throw tString(buf);
    }
    return f;
}

bool lookupVariable(const char *name, const char *filename, tString& value)
{
    FILE *fp=fopen(filename,"r");
    tStream stream;
    if(fp==NULL)
        return false;

    try
    {
        stream.load(fp);
        while(!stream.atEndOfStream())
        {
            const tString& str=parseString("tag",stream);
            if(strPrefix(str,name))
            {
                value=parseString("value",stream);
                fclose(fp);
                return true;
            }
            while(!stream.atEndOfLine())
                parseString("",stream);
        }
    }
    catch(const tString& message)
    {
        tString error=stream.parseErrorMessage(filename,message);
        bug(CHARPTR(error));
    }

    fclose(fp);
    value="";
    return false;
}
