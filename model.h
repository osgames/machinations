/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef modelH
#define modelH

//Includes:
#include "parse.h"
#include "texture.h"
#include "memwatch.h"

//Defines:
#define MODEL_PATH      "models/"

enum eVariableAttribute { VARIABLE_NONE, VARIABLE_POSITION, VARIABLE_TARGET_POSITION,
    VARIABLE_VELOCITY, VARIABLE_TARGET_VELOCITY, VARIABLE_ACCELERATION };
enum eInstructionAction { INSTRUCTION_NONE, INSTRUCTION_ASSIGN, INSTRUCTION_ADD,
    INSTRUCTION_SUBTRACT, INSTRUCTION_ACTIVATE, INSTRUCTION_DEACTIVATE, INSTRUCTION_WAIT,
    INSTRUCTION_SHOW, INSTRUCTION_HIDE };
enum eTransformKind { TRANSFORM_NONE, TRANSFORM_TRANSLATE, TRANSFORM_ROTATE,
    TRANSFORM_SCALE };
enum eUnitVariable { UNITVAR_NONE, UNITVAR_HEALTH, UNITVAR_DISTANCE, UNITVAR_SPEED };
enum eArgumentBoundary { BOUNDARY_NONE, BOUNDARY_MIN, BOUNDARY_MAX, BOUNDARY_RANGE };
enum eColorComponent { COLOR_NONE, COLOR_RED, COLOR_GREEN, COLOR_BLUE, COLOR_ALPHA, COLOR_RGB, COLOR_RGBA };

/****************************************************************************
                                TYPE CLASSES
 ****************************************************************************/

struct tChannel
{
    tString name;
    eVariableAttribute attribute;
    float value;
    bool active;

    tChannel() : attribute(VARIABLE_NONE), value(0), active(false) {}
};

struct tModelType;

struct tVariableType
{
    int ID;
    tString name;
    float initialX;
    float minX;
    float maxX;
    float initialVelocity;
    float minVelocity;
    float maxVelocity;
    float initialAcceleration;
    float minAcceleration;
    float maxAcceleration;
    float circumference;
    float center;
    bool xConstrained;
	bool velocityConstrained;
	bool accelerationConstrained;
    vector<tChannel> channels;

    float getInitialAttribute(eVariableAttribute attr);
    void parseChannelBlock(tStream& s) throw(tString);
    int channelLookup(const tString& n, bool exact);
    tVariableType(int _ID, tModelType *owner, tStream& s) throw(tString);
};

class tVariable
{
    private:
    tVariableType *type;

    float x0;
    float t0;
    float v0;
    float x;
    float velocity;
    float acceleration;
	bool hasTargetX;
	float targetX;
	bool hasTargetVelocity;
	float targetVelocity;
    float t1;
    float x1;
    float v1;

    int curChannel;
    int channelCount;
    tChannel *channels;

	void setupVariables(eVariableAttribute attribute, float value);
	void transferValues(tWorldTime t);
	void calcVelocity();
	void calcAcceleration();

    public:
    float getX() { return x; }
    float getVelocity() { return velocity; }
    float getAcceleration() { return acceleration; }
    float getAttribute(eVariableAttribute attr);

    void tick(tWorldTime t);
    float getExpectedX();
    void intervalTick(tWorldTime t);
    void addToChannel(int index, float value, tWorldTime t);
    void assignToChannel(int index, float value, tWorldTime t);
    void activateChannel(int index, tWorldTime t);
    void deactivateChannel(int index, tWorldTime t);
	void assignToAttribute(eVariableAttribute attribute, float value, tWorldTime t);
	void addToAttribute(eVariableAttribute attribute, float value, tWorldTime t);
    tVariable(tVariableType *_type, tWorldTime t) throw(int);
    ~tVariable();
};

struct tReferenceFrameType;
class tModel;

struct tInstructionType
{
    eInstructionAction action;
	eVariableAttribute attribute;
    int channelIndex;
    tVariableType *variableType;
    float value;
    tReferenceFrameType *referenceFrameType;

    tInstructionType() : action(INSTRUCTION_NONE), attribute(VARIABLE_NONE), channelIndex(0),
        variableType(NULL), value(0), referenceFrameType(NULL) {}
};

struct tProcedureType
{
    tString name;
    bool runExclusive;
    float executionTime; //Duration that procedure takes to execute
    list<tInstructionType> instructionTypes;
    void parseInstructionType(tModelType *owner, tStream& s) throw(tString);

	tProcedureType(tModelType *owner, tStream& s) throw(tString);
};

class tProcedure
{
    private:
    tProcedureType *type;
    tModel *owner;
    list<tInstructionType>::iterator ip;
    tWorldTime t0;

    bool executeInstruction(const tInstructionType& it, float t); //Returns true if finished

    public:
	tProcedureType *getType() { return type; }
    void tick(tWorldTime t);
    bool isFinished();
    tProcedure(tProcedureType *_type, tModel *_owner, tWorldTime t) throw(int);
};

struct tVertexTransformType;

struct tJoint
{
    int height;
	int channelIndex;
    tVariableType *variableType;
    tReferenceFrameType *referenceFrameType;
	tVertexTransformType *vertexTransformType;
    bool approximate;
    tJoint() : height(-1), channelIndex(-1), variableType(NULL), referenceFrameType(NULL),
		approximate(true) {}

    bool operator<(const tJoint& j) const { return (height<j.height); } //for sorting joints 
};

struct tModelVectorType;

struct tAimProcedureType
{
    tString name;
    tModelVectorType *missileOriginType;
    tModelVectorType *missileDirectionType;
    tReferenceFrameType *topReferenceFrameType;
    list<tJoint> joints;

    void parseJoint(tModelType *owner, tStream& s) throw(tString);
    void run(tModel *model, const tVector3D& target, tWorldTime t); //target must be in model's coordinates
                                                                    //(e.g. multiplied by inverse of unit's matrix).
    void stop(tModel *model, tWorldTime t);

    tAimProcedureType(tModelType *owner, tStream& s) throw(tString);
};

struct tModelVectorType
{
    tString name;
    tVector3D v;
    tReferenceFrameType *parent;

    tModelVectorType(tReferenceFrameType *_parent, tModelType *owner, tStream& s) throw(tString);
};

struct tTriangle
{
    unsigned short indices[3];
};

//This class encapsulates one unique model component type.  It contains a unique string
//constant, mesh data, skin data, and a function for drawing the component type.
class tComponentType
{
    iDisplayList    list;
    tString         meshFilename;       //Mesh model
    tString         skinFilename;       //File containing the model's skin
    tSkin*          skin;               //Model's skin

    int             vertexCount;
    tVector3D*      vertices;
    tVector3D*      normals;
    tVector2D*      texCoords;

    int             triangleCount;
    tTriangle*      triangles;
    float           minX;
    float           maxX;
    float           minY;
    float           maxY;
    float           minZ;
    float           maxZ;

    float           vertexMatrix[16];   //Matrix containing simple translations, rotations, and scales.
	float			textureMatrix[16];	//This matrix lets the user orient the model properly without
										//having to go through the model conversion process.
    bool            oldBlending;
    bool            oldAlphaTesting;
    int             polyBase;

    void            loadMesh() throw(tString);
    void            drawMesh(int polyIndex);
    void            renderMesh(int polyIndex);
    void            destroyMesh();
    void            refreshMesh();

    void            destroy();

    public:
    tComponentType(int _polyBase, tStream& s) throw(tString);
    ~tComponentType() { destroy(); }

    void            draw(int polyIndex, int flags);
    void            setupFastDraw();
    void            fastDraw();
    void            cleanupFastDraw();
    void            refresh();

    bool            getRepairPoints(int polyIndex, tVector3D& a, tVector3D& b);
    int             getPolyCount() { return triangleCount; }
    int             getVertexCount() { return vertexCount; }
    const tVector3D&getVertex(int i) { return vertices[i]; }
    float           getMinX() { return minX; }
    float           getMaxX() { return maxX; }
    float           getMinY() { return minY; }
    float           getMaxY() { return maxY; }
    float           getMinZ() { return minZ; }
    float           getMaxZ() { return maxZ; }
};

struct tFunctionType;
class tFunction;

template<class T>
struct tArgumentType
{
    bool loaded;

    tVariableType *variableType;
    eVariableAttribute attribute;
    eUnitVariable unitVariable;
    tFunctionType *functionType;
    T constant;
    eArgumentBoundary boundary;
    T min;
    T max;
    T initialValue;
    T convertFloat(float f)
    {
        T t=(T)f;
        if(boundary==BOUNDARY_MIN || boundary==BOUNDARY_RANGE)
            if(t<min)
                t=min;
        if(boundary==BOUNDARY_MAX || boundary==BOUNDARY_RANGE)
            if(t>max)
                t=max;
        return t;
    }

    T evaluate(tModel *model)
    {
        tVariable *v;
        tFunction *f;
        float val;

        if(model==NULL)
            return initialValue;

        if(variableType)
        {
            v=model->findVariable(variableType);
            if(v)
                val=v->getAttribute(attribute);
        }
        else if(functionType)
        {
            f=model->findFunction(functionType);
            if(f)
                val=f->getValue();
        }
        else if(unitVariable!=UNITVAR_NONE)
            val=model->getUnitVariable(unitVariable);
        else
            return constant;

        return convertFloat(val);
    }
    void load(tModelType *owner, tStream& s) throw(tString)
    {
#ifdef DEBUG
        if(MEMORY_VIOLATION(owner))
            throw tString("Unexpected error occurred");
#endif
        if(loaded)
            throw tString("You may specify only one variable, function, or constant");

        tString lowerCase=STRTOLOWER(s.peekAhead());
		bool startsWithAttribute = (lowerCase=="position" || lowerCase=="rate" || lowerCase=="velocity" ||
            lowerCase=="acceleration");

		if(startsWithAttribute || strPrefix(s.peekAhead(),"define_variable") || strPrefix(s.peekAhead(),"defvariable"))
		{
			if(startsWithAttribute)
			{
				attribute=parseVariableAttribute("variable attribute",s);
				s.matchToken("of");
			}
			else
                attribute=VARIABLE_POSITION;

            if(strPrefix(s.peekAhead(),"define_variable") || strPrefix(s.peekAhead(),"defvariable"))
            {
                s.matchToken("define_variable");
                variableType=owner->parseVariableTypeBlock(s);
            }
            else
                variableType=owner->parseVariableType("variable name",s);
		}
		else if(strPrefix(s.peekAhead(),"define_function") || strPrefix(s.peekAhead(),"deffunction"))
		{
			parseString("define_function",s);
			functionType=owner->parseFunctionTypeBlock(s);
		}
        else
        {
            bool found=false;

            if(!found)
            {
                variableType=owner->variableTypeLookup(s.peekAhead(),/*exact=*/true);
                if(variableType)
                {
                    parseString("variable name",s);
                    attribute=VARIABLE_POSITION;
                    found=true;
                }
            }
            if(!found)
            {
                functionType=owner->functionTypeLookup(s.peekAhead(),/*exact=*/true);
                if(functionType)
                {
                    parseString("function name",s);
                    found=true;
                }
            }
            if(!found)
            {
                unitVariable=unitVariableLookup(s.peekAhead(),/*exact=*/false);
                if(unitVariable!=UNITVAR_NONE)
                {
                    parseString("unit variable name",s);
                    found=true;
                }
            }
            if(!found)
            {
                switch(boundary)
                {
                    case BOUNDARY_NONE:
                        constant=parse<T>("constant value",s);
                        break;
                    case BOUNDARY_MIN:
                        constant=parseMin<T>("constant value",min,s);
                        break;
                    case BOUNDARY_MAX:
                        constant=parseMax<T>("constant value",max,s);
                        break;
                    case BOUNDARY_RANGE:
                        constant=parseRange<T>("constant value",min,max,s);
                        break;
                }
            }
        }

        if(variableType)
            initialValue=convertFloat(variableType->getInitialAttribute(attribute));
        else if(functionType)
            initialValue=convertFloat(functionType->initialValue);
        else if(unitVariable!=UNITVAR_NONE)
            initialValue=convertFloat(owner->getUnitVariable(unitVariable));
        else
            initialValue=constant;

        loaded=true;
        return;
    }
    void setConstant(T _constant) throw(tString)
    {
        if(loaded)
            throw tString("You may specify only one variable, function, or constant");
        initialValue=constant=_constant;
        loaded=true;
    }
    void setVariableType(eVariableAttribute attr, tVariableType *vt) throw(tString)
    {
        if(loaded)
            throw tString("You may specify only one variable, function, or constant");
		attribute=attr;
		variableType=vt;
        initialValue=convertFloat(variableType->getInitialAttribute(attribute));
        loaded=true;
    }
	void setUnitVariable(eUnitVariable uv, tModelType *owner) throw(tString)
	{
        if(loaded)
            throw tString("You may specify only one variable, function, or constant");
		unitVariable=uv;
		initialValue=convertFloat(owner->getUnitVariable(unitVariable));
		loaded=true;
	}
	void setFunctionType(tFunctionType *ft) throw(tString)
	{
        if(loaded)
            throw tString("You may specify only one variable, function, or constant");
		functionType=ft;
		initialValue=convertFloat(functionType->initialValue);
		loaded=true;
	}
    tArgumentType(T initial, eArgumentBoundary _boundary)  :
        variableType(NULL), attribute(VARIABLE_NONE), unitVariable(UNITVAR_NONE),
        functionType(NULL), loaded(false), boundary(_boundary), constant(initial),
        initialValue(initial)
    {
        if(boundary!=BOUNDARY_NONE)
        {
            bug("tArgumentType::tArgumentType: invalid boundary");
            boundary=BOUNDARY_NONE;
        }
    }
    tArgumentType(T initial, eArgumentBoundary _boundary, T x) :
        variableType(NULL), attribute(VARIABLE_NONE), unitVariable(UNITVAR_NONE),
            functionType(NULL), loaded(false), boundary(_boundary), constant(initial),
        initialValue(initial)
    {
        if(boundary==BOUNDARY_MIN)
            min=x;
        else if(boundary==BOUNDARY_MAX)
            max=x;
        else
        {
            bug("tArgumentType::tArgumentType: invalid boundary");
            boundary=BOUNDARY_NONE;
        }
    }
    tArgumentType(T initial, eArgumentBoundary _boundary, T x, T y) :
        variableType(NULL), attribute(VARIABLE_NONE), unitVariable(UNITVAR_NONE),
        functionType(NULL), loaded(false), boundary(_boundary), constant(initial),
        initialValue(initial)
    {
        if(boundary==BOUNDARY_RANGE)
        {
            min=x;
            max=y;
        }
        else
        {
            bug("tArgumentType::tArgumentType: invalid boundary");
            boundary=BOUNDARY_NONE;
        }
    }
};

struct tExpression;

struct tFunctionType
{
    int ID;
    tString name;
    tExpression *expression;
    vector<tMapping> mappings;
    float initialValue;

    void destroy();

    float evaluate(tModel *model);
    tFunctionType(int _ID, tModelType *owner, tStream& s) throw(tString);
    ~tFunctionType() { destroy(); }
};

class tFunction
{
    private:
    tFunctionType *type;
    tModel *owner;
    float value;

    public:
    void tick(tWorldTime t);
    float getValue() { return value; }
    tFunction(tFunctionType *_type, tModel *_owner) throw(int);
};

struct tVertexTransformType
{
	float domainMin;
	float domainMax;
	float scale;
	float bias;

	bool hasStaticMatrix1;
	float staticMatrix1[16];
	bool hasStaticMatrix2;
	float staticMatrix2[16];

	eTransformKind transformKind;
    tVector3D axis;

    void multGLMatrix(float value);
	tVertexTransformType(tModelType *modelType, tArgumentType<float> *at, tStream& s) throw(tString);
	void parseDynamicTransform(eTransformKind kind, float& rangeMin, float& rangeMax,
        tModelType *modelType, tArgumentType<float> *at, tStream& s) throw(tString);
};

struct tTextureTransformType
{
	float domainMin;
	float domainMax;
	float scale;
	float bias;
	
	bool hasStaticMatrix1;
	float staticMatrix1[16];
	bool hasStaticMatrix2;
	float staticMatrix2[16];

	eTransformKind transformKind;
    tVector2D axis;

    void multGLMatrix(float value);
    tTextureTransformType(tModelType *modelType, tArgumentType<float> *at, bool *affectsChildren, tStream& s) throw(tString);
	void parseDynamicTransform(eTransformKind kind, float& rangeMin, float& rangeMax,
        tModelType *modelType, tArgumentType<float> *at, tStream& s) throw(tString);
};

struct tColorTransformType
{
    bool loaded;
    eColorComponent colorComponent;
    vector<tMapping> redMappings;
    vector<tMapping> blueMappings;
    vector<tMapping> greenMappings;
    vector<tMapping> alphaMappings;
    bool hasRedComponent;
    bool hasGreenComponent;
    bool hasBlueComponent;
    bool hasAlphaComponent;
	bool affectChildren;

    void multGLMatrix(float value, bool RGBMask);
    void load(tModelType *modelType, tArgumentType<float> *argumentType, bool *affectsChildren, tStream& s)
        throw(tString);
    tColorTransformType() : loaded(false), colorComponent(COLOR_NONE),
        hasRedComponent(false), hasGreenComponent(false),
        hasBlueComponent(false), hasAlphaComponent(false) {}
};

struct tReferenceFrameType
{
    int ID;
    tString name;
    tReferenceFrameType *parent;
	tArgumentType<float> vertexArgumentType;
    list<tVertexTransformType *> vertexTransformTypes;
    tArgumentType<float> textureArgumentType;
	list<tTextureTransformType *> textureTransformTypes;
	bool textureTransformAffectsChildren;
	tArgumentType<float> colorArgumentType;
    tColorTransformType colorTransformType;
	bool colorTransformAffectsChildren;

    list<tComponentType *> componentTypes;
    list<tModelVectorType *> modelVectorTypes;
    list<tReferenceFrameType *> referenceFrameTypes;

    float maxRadius;
    float selRadius;
    float radius3D;
    float maxZ;

    int polyCount;
    float GLVertexMatrix[16];
    bool initiallyVisible;

    void destroy();
    void parseVertexTransform(bool isPiecewise, tModelType *owner, tStream& s) throw(tString);
    void parseTextureTransform(bool isPiecewise, tModelType *owner, tStream& s) throw(tString);
	void parsePiecewiseVertexTransform(tModelType *owner, tStream& s) throw(tString);
	void parsePiecewiseTextureTransform(tModelType *owner, tStream& s) throw(tString);

    void setID(int _ID) { ID=_ID; }
    int getPolyCount() { return polyCount; }
    void initializeMatrices();
    void refresh();
	void draw(int flags);
    void multGLVertexMatrix(tModel *m);
	void multGLTextureMatrix(tModel *m);
    void multGLColorMatrix(tModel *m, bool RGBMask);

    tReferenceFrameType(tReferenceFrameType *_parent, tModelType *owner, tStream& s) throw(tString);
    ~tReferenceFrameType() { destroy(); }
};

class tReferenceFrame
{
    private:
    tModel *owner;
    tReferenceFrameType *type;
    tReferenceFrame *parent;
    list<tReferenceFrame *> referenceFrames;
    bool visible;

    int polyBase;

    float GLVertexMatrix[16];

    public:
    bool isVisible() { return visible; }
    int getPolyBase() { return polyBase; }
	int getPolyCount() { return type->polyCount; }
    tReferenceFrameType* getType() { return type; }
    tReferenceFrame* getParent() { return parent; }
    const float *getGLVertexMatrix() { return GLVertexMatrix; }

    void onShow(tWorldTime t) { visible=true; }
    void onHide(tWorldTime t) { visible=false; }
    void updatePolyBase(int& index);

    void getRepairPoints(int polyIndex, tVector3D& a, tVector3D& b);
    void updateMatrices();
    void draw(int polyIndex, int flags);
	void load() throw(int);
    tReferenceFrame(tReferenceFrameType *_type, tModel *_owner) :
		type(_type), owner(_owner), polyBase(0), parent(NULL) {}
};
		   
struct tUnitType;

struct tModelType
{
    tString name;

    list<tVariableType *>       variableTypes;
    list<tFunctionType *>       functionTypes;
    list<tProcedureType *>      procedureTypes;
    list<tReferenceFrameType *> referenceFrameTypes;
    list<tModelVectorType *>    modelVectorTypes;
    list<tAimProcedureType *>   aimProcedureTypes;
    tReferenceFrameType*        ancestor;
    tComponentType*             primaryComponentType;

    float                   initialMaxRadius;
    float                   initialSelRadius;
    float                   initialRadius3D;
    float                   initialMaxZ;

    tArgumentType<float>    maxRadiusArg;       //Minimum radius of a cylinder which will enclose unit
    tArgumentType<float>    selRadiusArg;       //Radius of the unit's selection circle
    tArgumentType<float>    maxZArg;            //Minimum height of a cylinder which will enclose unit

    int                     nextVariableID;
    int                     nextFunctionID;
    int                     nextReferenceFrameID;

    float                   getUnitVariable(eUnitVariable var);
    float                   getMaxRadius();
    float                   getSelRadius();
    float                   getRadius3D() { return initialRadius3D; }
    float                   getMaxZ();

    tVariableType*          parseVariableTypeBlock(tStream& s);
    tFunctionType*          parseFunctionTypeBlock(tStream& s);
    tProcedureType*         parseProcedureTypeBlock(tStream& s);
    tReferenceFrameType*    parseReferenceFrameTypeBlock(tReferenceFrameType *parent, tStream& s);
    tModelVectorType*       parseModelVectorTypeBlock(tReferenceFrameType *parent, tStream& s);
    tAimProcedureType*      parseAimProcedureTypeBlock(tStream& s);

    tVariableType*          variableTypeLookup(const tString& n, bool exact);
    tFunctionType*          functionTypeLookup(const tString& n, bool exact);
    tProcedureType*         procedureTypeLookup(const tString& n, bool exact);
    tReferenceFrameType*    referenceFrameTypeLookup(const tString& n, bool exact);
    tModelVectorType*       modelVectorTypeLookup(const tString& n, bool exact);
    tAimProcedureType*      aimProcedureTypeLookup(const tString& n, bool exact);
    tReferenceFrameType*    referenceFrameTypeLookup(tVariableType *vt, int& instances);

    tVariableType*          parseVariableType(const char *fieldName, tStream& s);
    tFunctionType*          parseFunctionType(const char *fieldName, tStream& s);
    tProcedureType*         parseProcedureType(const char *fieldName, tStream& s);
    tReferenceFrameType*    parseReferenceFrameType(const char *fieldName, tStream& s);
    tModelVectorType*       parseModelVectorType(const char *fieldName, tStream& s);
    tAimProcedureType*      parseAimProcedureType(const char *fieldName, tStream& s);
    
    void refresh();
    void draw(int flags);

    void setupFastDraw() { if(primaryComponentType) primaryComponentType->setupFastDraw(); }
    void fastDraw() { if(primaryComponentType) primaryComponentType->fastDraw(); }
    void cleanupFastDraw() { if(primaryComponentType) primaryComponentType->cleanupFastDraw(); }

    void destroy();
    tModelType(tStream& s) throw(tString);
    ~tModelType() { destroy(); }
};

class tUnit;

class tModel
{
    private:
    tModelType *type;
    tUnit *parent;
    tReferenceFrame *ancestor;

    vector<tVariable *> variables; //This needs to be a vector for random access
    vector<tFunction *> functions; //This needs to be a vector for random access
    list<tProcedure *> procedures; //Unordered list of running procedures
    vector<tReferenceFrame *> referenceFrames; //This needs to be a vector for random access

    float defaultMaxRadius;
    float defaultSelRadius;
    float defaultRadius3D;
    float defaultMaxZ;
    int polyCount;

    void updateDimensions();
	void updateMatrices();
    void destroy();

    public:
    tVariable *findVariable(tVariableType *vt);
    tFunction *findFunction(tFunctionType *ft);
    tReferenceFrame *findReferenceFrame(tReferenceFrameType *rft);
    void showReferenceFrame(tReferenceFrameType *rft, tWorldTime t);
    void hideReferenceFrame(tReferenceFrameType *rft, tWorldTime t);
    float getUnitVariable(eUnitVariable var);

    float getMaxRadius();
    float getSelRadius();
    float getRadius3D() { return defaultRadius3D; }
    float getMaxZ();

    void tick(tWorldTime t);
	void intervalTick(tWorldTime t);
    void draw(float progress, int flags);

    void runProcedure(tProcedureType *pt, tWorldTime t);
    tVector3D locatePoint(tModelVectorType *mvt);
    void locateReferenceFrame(tReferenceFrameType *rft,
                              tVector3D& position,
                              tVector3D& forward,
                              tVector3D& side,
                              tVector3D& up);
    tVector3D locateUnitVector(tModelVectorType *mvt);
    void getRepairPoints(float progress, tVector3D& a, tVector3D& b);

    tModel(tModelType *_type, tUnit *_parent, tWorldTime t) throw(int);
    ~tModel() { destroy(); }
};

//Externals:
extern void         parseModelTypeBlock(tStream& s) throw(tString);
extern void         refreshModelTypes();
extern void         destroyModelTypes();

extern tModelType*  modelTypeLookup(const tString& name, bool exact);
extern tModelType*  parseModelType(const char *fieldName, tStream& s) throw(tString);
extern eInstructionAction parseInstructionAction(const char *fieldName, tStream& s) throw(tString);
extern eVariableAttribute variableAttributeLookup(const tString& str, bool exact);
extern eVariableAttribute parseVariableAttribute(const char *fieldName, tStream& s) throw(tString);
extern eTransformKind parseTransformKind(const char *fieldName, tStream& s) throw(tString);
extern eUnitVariable unitVariableLookup(const tString& name, bool exact);
extern eUnitVariable parseUnitVariable(const char *fieldName, tStream& s) throw(tString);
extern eColorComponent parseColorComponent(const char *fieldName, tStream& s) throw(tString);
extern void validateModelType(tModelType *mt) throw(tString);
/****************************************************************************/
#endif
