/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef client02H
#define client02H
/****************************************************************************/
//Headers
#include "client01.h"
#include "engine.h"

//Defines
#define MAP_PATH                    "maps/"        //Directory where maps are stored
#define WORLD_PATH                  "worlds/"      //Directory where worlds are stored
#define HELP_PATH                   "data/help/"   //Directory where help files are stored

//Window tags
#define NO_MENU             0
#define MAIN_MENU           1
#define OPTIONS_MENU        2
#define CONNECT_MENU        3
#define SETUP_GAME_MENU     4
#define GAME_WINDOW         5
#define CONNECT_MESSAGE     6
#define GAME_MENU           7
#define MINIMAP_WINDOW      8
#define COMMAND_PANEL       9
#define ALLIANCE_MENU       10
#define GAME_OPTIONS_MENU   11
#define RECONNECT_MENU      12
#define MAP_MENU            13
#define TIMING_MENU         14
#define CHAT_WINDOW         15
#define PROGRESS_BAR_WINDOW 16

//Button tags
#define NO_BUTTON           0
#define OK_BUTTON           1
#define CANCEL_BUTTON       2

#define COMMAND_SIZE        32

#define MAX_HISTORY         10

enum eChoices { CHOICES_OK, CHOICES_CANCEL };

enum ePanDevice { PAN_NONE, PAN_MOUSE, PAN_KEYBOARD };

enum eRecipient { RECIPIENT_NONE, RECIPIENT_ALL, RECIPIENT_ALLIES, RECIPIENT_ENEMIES,
    RECIPIENT_PLAYER };

enum eSpin { SPIN_NONE, SPIN_CCW, SPIN_CW };
enum eZoom { ZOOM_NONE, ZOOM_IN, ZOOM_OUT };
enum eTilt { TILT_NONE, TILT_UP, TILT_DOWN };

//Standard message box
class tMessageBox : public tStdDialogBox
{
    private:
    tButton *def;
    void (* event)(int);
    void onClick(tControl *sender);
    void onHide();

    public:
    tMessageBox(const tStringList& message, const tString& title, eChoices choices, void (* _event)(int)=NULL);
};

//Static rectangle
class tStaticRect : public tStatic
{
    private:
    int margin;

    public:
    void drawControl();
    tStaticRect(int x1,int y1,int x2,int y2) : tStatic(x1,y1,x2,y2), margin(2) {}
};

//Edit which stores last line entered and supports an ENTER event handler
class tMyEdit : public tEdit
{
    private:
    tEvent event;
    tStringList history;
    int historyIndex;

    protected:
    void onKeyDown(tKey key, char ch);

    public:
    tMyEdit(int controlX, int controlY, int width, char *value,
        int fontIndex, bool readOnly, tEvent _event) : tEdit(controlX,
        controlY, width, value, fontIndex, readOnly), event(_event) { historyIndex=0; }
};

//Combo box for assigning players (calls setplayer)
class tMyComboBox : public tStdComboBox
{
    private:
    int index;

    protected:
    void onChange();

    public:
    tMyComboBox(int _index, int controlX, int controlY, int width, bool canFocus,
        const tString& choices, int choice, int dropDownHeight,
        bool dropDownVertScrollBar, bool dropDownHorScrollBar, int fontIndex) :
        tStdComboBox(controlX,controlY,width,canFocus,choices,choice,dropDownHeight,
        dropDownVertScrollBar, dropDownHorScrollBar, fontIndex), index(_index) {}
};

//Command buttons
class tCommandButton : public tButton
{
    private:
    bool active;
    bool highlight;
    int repeat;
    int quantity;
    tTexture *texture;
    bool leftPressed;
    bool rightPressed;

    protected:
    void drawControl();
    void onMouseDown(int x, int y, eMouseButton button);
    void onExecute();

    public:
    void setActive(bool _active) { active=_active; }
    void setHighlight(bool _highlight) { highlight=_highlight; }
    void setRepeat(int _repeat) { repeat=_repeat; }
    void setQuantity(int _quantity) { quantity=_quantity; }
    void reset();
    void drawHint();

    tCommandButton(int controlX, int controlY, int _tag);
    ~tCommandButton();
};

class tProgressBar : public tStdDialogBox
{
    private:
    float progress;
    
    protected:
    void drawWindow();

    public:
    void setProgress(float _progress) { progress=_progress; }
    tProgressBar(const tString& message);
};

class t3DWindow : public tWindow
{
    private:
    float matrix[16];
    
    protected:
    bool objectActivated(int x, int y);
    void draw();
    void onMouseDown(int x, int y, eMouseButton button);
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onSelect(int x, int y);

    public:
    void setMatrix(float _matrix[]);
    void onSetPosition(int x, int y) { bug("t3DWindow::onSetPosition: this control does not support onSetPosition"); }
    bool getMousePointer(int x, int y, ePointer& p, int& tag);
    t3DWindow(int width, int height, bool modal, bool background, bool destroyOnClose);
};

class t3DButton : public tStdButton
{
    protected:
    void drawControl();

    public:
    t3DButton(int controlX, int controlY, int width, int height, const tString& caption,
        int fontIndex, bool canFocus, tEvent event) : tStdButton(controlX,controlY,
        width,height,caption,fontIndex,canFocus,event) {}
};

//main menu form
class tMainMenu : public t3DWindow
{
    private:
    int cx, cy;
    tTime startTime;
    tEdit *edUserName;
    void onHostGame(tControl *sender);
    void onConnect(tControl *sender);
    void onSinglePlayer(tControl *sender);
    void onOptions(tControl *sender);
    void onExit(tControl *sender);
    bool getUserName();

    protected:
    void drawWindow();

    public:
    tMainMenu();
};

//options game form
class tOptionsMenu : public tWindow
{
    private:
    tEdit *edPort;
    tStdComboBox *cbIPAddress;
    tStdButton *btOK, *btCancel;
    tStdRadio *rbTCPIP, *rbUDP;
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender);

    protected:
    void drawWindow();
    
    public:
    tOptionsMenu();
};

//connect form
class tConnectMenu : public tWindow
{
    private:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender);

    protected:
    void drawWindow();

    public:
    tEdit *edIPAddress, *edPort;
    tStdButton *btOK, *btCancel;
    tConnectMenu();
};

//setup game form
class tSetupMenu : public tWindow
{
    private:
    tStdLabel *laIPAddress, *laPort, *laMapName, *laWorldName;
    tStdLabel *laNetInterval, *laGameInterval, *laLatency, *laGameMessage, *laGameSpeed;
    tStdButton *btStartGame, *btDrop, *btDisconnect, *btChooseMap, *btSetTiming;
    tEdit *edInput;
    tStdTextBox *meOutput;
    tStdListBox *lbUsers;
    tStdLabel *laPlayers[MAX_PLAYERS];
    tStdComboBox *cbPlayers[MAX_PLAYERS];

    void onChooseMapClick(tControl *sender);
    void onSetTimingClick(tControl *sender);
    void onStartGameClick(tControl *sender);
    void onDropClick(tControl *sender);
    void onSendText(tControl *sender);
    void onDisconnectClick(tControl *sender);

    protected:
    void drawWindow();

    public:
    void onShowMessage(const tString& str, eTextMessage type);
    void onAddUser(tUser *user, bool initial);
    void onDropUser(tUser *user);
    void onUpdateLag(tUser *user);
    void onSetTiming();
    void onChooseFiles();
    void onResetPlayers();
    void onSetPlayer(int index, ePlayer type, tUser *user);
    void onReconnect();
    void onStartGame();
    void onEndGame();
    void onGameWaiting();
    void onAbortGame();
    void onUnloadMap();
    void onUnloadWorld();
    void onLoadMap();
    void clearMessages() { if(meOutput) meOutput->clear(); }
    tSetupMenu();
};

//choose map form
class tMapMenu : public tStdDialogBox
{
    private:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender);

    public:
    tEdit *edMapFilename, *edWorldFilename;
    tStdButton *btOK, *btCancel;
    tMapMenu();
};

//reconnect form
class tReconnectMenu : public tStdDialogBox
{
    private:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender);
    tEdit *edPort;
    tStdComboBox *cbIPAddress;
    tStdButton *btOK, *btCancel;
    tStdRadio *rbTCPIP;

    public:
    tReconnectMenu();
};

//set timing form
class tTimingMenu : public tStdDialogBox
{
    private:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender);

    public:
    tEdit *edLatency, *edNetInterval, *edGameInterval, *edGameSpeed;
    tStdButton *btOK, *btCancel;
    tTimingMenu();
};

//game menu
class tGameMenu : public tStdDialogBox
{
    private:
    void onAllianceClick(tControl *sender);
    void onMissionClick(tControl *sender) {}
    void onPauseClick(tControl *sender);
    void onLoadClick(tControl *sender) {}
    void onSaveClick(tControl *sender) {}
    void onOptionsClick(tControl *sender);
    void onEndClick(tControl *sender);
    void onDisconnectClick(tControl *sender);
    void onReturnClick(tControl *sender) { close(); }
    void onReconnectClick(tControl *sender);
    void onTimingClick(tControl *sender);
    void onRestartClick(tControl *sender);

    public:
    tStdButton *btAlliance, *btMission, *btPause, *btLoad, *btSave, *btOptions;
    tStdButton *btEnd, *btDisconnect, *btReturn, *btReconnect, *btTiming, *btRestart;
    tGameMenu();
};

class tAllianceMenu : public tStdDialogBox
{
    private:
    tStdButton *btOK, *btCancel;
    tStdRadio *radios[MAX_PLAYERS][3];
    tStdToggle *toggles[MAX_PLAYERS][3];

    public:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender) { close(); }
    tAllianceMenu();
};

class tGameOptionsMenu : public tStdDialogBox
{
    private:
    tStdButton *btOK, *btCancel;
    tStdListBox *lbResolution;
    tStdToggle *cbFullScreen;
    tStdScrollBar *sbPanRate;
    tStdScrollBar *sbInterval;
    tStdToggle *cbAutoInterval;
    tStdScrollBar *sbShadeMap, *sbAlphaMap, *sbWaterMap;

    public:
    void onOKClick(tControl *sender);
    void onCancelClick(tControl *sender) { close(); }
    void onIntervalChange(tControl *sender);
    tGameOptionsMenu();
};

//minimap
class tMinimap : public tStdDialogBox
{
    private:
    int originX;
    int originY;
    bool targetted;

    protected:
    void onSetSize(int w, int h) { tStdDialogBox::onSetSize(w,h); update(); }
    void onMouseDown(int x, int y, eMouseButton button);
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onKeyDown(tKey key, char ch);
    void onKeyUp(tKey virtualKey, char ch);
    void onMouseWheel(int pos, eMouseWheel direction);
    void onDestroy();
    void onMinimize();

    public:
    void drawWindow();
    void update();
    bool getMousePointer(int x, int y, ePointer& p, int& tag);
    tMinimap();
};

//command panel
class tCommandPanel : public tStdDialogBox
{
    private:
    tCommandButton *customButtons[5];
    tCommandButton *buildButtons[5];

    protected:
    void onDestroy();
    void onKeyDown(tKey key, char ch);
    void onKeyUp(tKey virtualKey, char ch);
    void onMouseWheel(int pos, eMouseWheel direction);
    void onMinimize();

    public:
    void enableCommand(eCommand command);
    void disableCommand(eCommand command);
    void enableBuildOption(eCommand command);
    void disableBuildOption(eCommand command);
    void highlightCommand(eCommand command);
    void resetCommand(eCommand command);
    void activateCommand(eCommand command);
    void deactivateCommand(eCommand command);
    void setCommandQuantity(eCommand command, int quantity);
    void setCommandRepeat(eCommand command, int repeat);
    void reset();
    bool doesCommandExist(eCommand command);
    tCommandPanel();
};

//the chat window allows players to talk in a separate IRC-style window.
class tChatWindow : public tStdDialogBox
{
    private:
    tStdTextBox *meOutput;
    
    protected:
    void onDestroy();
    void onKeyDown(tKey key, char ch);
    void onKeyUp(tKey virtualKey, char ch);
    void onMouseWheel(int pos, eMouseWheel direction);
    void onMinimize();
    void onHide();
        
    public:
    void onShowMessage(const tString& str, eTextMessage type);
    tChatWindow();
    void clearMessages() { if(meOutput) meOutput->clear(); }
};

//the background spans the entire screen and handles events for controlling the view
//such as mouse panning, keyboard panning, and shortcut keys
class tGameWindow : public tWindow
{
    private:
    tVector2D anchor;        //used to calculate mapOffset
    tTime startTime;         //time at which scroll began
    eDir panDir;             //direction which user is currently panning
    ePanDevice panDevice;
    int selectionX;          //location where selection began
    int selectionY;          //"
    tVector3D selectionOrigin; //Map coordinates where the selection began
    bool selectingLeft;      //are you currently dragging the mouse with left button depressed to select group of units
    bool selectingRight;     //are you currently dragging the mouse with right button depressed to select group of units
    tKey firstHotKey;        //The first keystroke in a two-key shortcut

    //Used for spinning, zooming, and tilting:
    eSpin           spinDir;
    eZoom           zoomDir;
    eTilt           tiltDir;
    tVector3D       viewCenter;
    float           mapRotationAnchor;
    float           mapScaleAnchor;
    float           mapTiltAnchor;
    bool            orientingMapWithMouse;
    int             mouseWheelAnchorX;
    int             mouseWheelAnchorY;

    tStringList textMessages;
    tTime nextLine;
    tMyEdit *edInput;
    tStdLabel *laRecipient;
    eRecipient recipientType;
    tUser *recipient;
    tString replyTo;
    void onEnterText(tControl *sender);
    float textDuration(const tString& str);

    void panView();
    void adjustMousePan(int x, int y);

    //events
    protected:
    void onMouseDown(int x, int y, eMouseButton button);
    void onMouseMove(int x, int y);
    void onMouseUp(int x, int y, eMouseButton button);
    void onSelect(int x, int y);
    void onDeselect();
    void onDefocus();
    void poll();
    void onHide();
    void onDestroy();
    void onSetSize(int w, int h);

    public:
    //These three events must be public so that the minimap, command panel, and
    //chat window can dispatch events to the game window.
    void onKeyDown(tKey key, char ch);
    void onKeyUp(tKey key, char ch);
    void onMouseWheel(int pos,eMouseWheel direction);

    void drawWindow();
    void onShowMessage(const tString& str, eTextMessage type);
    void onDropUser(tUser *user);
    bool getMousePointer(int x, int y, ePointer& p, int& tag);
    void clearMessages() { textMessages.clear(); }
    tGameWindow();
};

/****************************************************************************/
extern void enableCommand(eCommand command);
extern void disableCommand(eCommand command);
extern void highlightCommand(eCommand command);
extern void resetCommand(eCommand command);
extern void adjustCommandQuantity(eCommand command, int amt);
extern void adjustCommandRepeat(eCommand command, int amt);

extern void setPanRate(float _panRate);
extern void setSpinRate(float _spinRate);
extern void setZoomRate(float _zoomRate);
extern void setTiltRate(float _tiltRate);
extern void setMouseZoomRate(float _mouseZoomRate);

extern void displayProgressBar(const tString& message);
extern void updateProgressBar(float progress);
extern void closeProgressBar();
/****************************************************************************/
#endif
