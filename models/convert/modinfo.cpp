/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* MODINFO.CPP:    Displays information about a model.                       *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <condefs.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include <string.h>
#include <ctype.h>
#include <vector.h>
#pragma hdrstop
//---------------------------------------------------------------------------
#pragma argsused

const long machModelCode='M'+('A'<<8)+('C'<<16)+('H'<<24);

struct tVector3D
{
    float x;
    float y;
    float z;
    float radius()
    {
        return sqrt(x*x+y*y);
    }
};

void addExtension(char *filename, const char *ext)
{
    char *ptr=filename;
    char *start=filename;
    while(*ptr!='\0')
    {
        if(*ptr=='/' || *ptr=='\\')
            start=ptr+1;
        ptr++;
    }

    ptr=strchr(start, '.');
    if(ptr==NULL)
        strcat(filename, ext);
}

void main(int argc, char* argv[])
{
    char filename[300];
    int i, vc, tc;
    FILE *fp;
    long l;
    float minx=1000000,miny=1000000,minz=1000000;
    float maxx=-1000000,maxy=-1000000,maxz=-1000000;
    float radius=0;
    tVector3D v3;

    if(argc!=2)
    {
        printf("Usage: modinfo <filename>\n");
        printf("    Displays information about one Machinations .mod model.\n");
        return;
    }

    strcpy(filename, argv[1]);
    addExtension(filename, ".mod");

    fp=fopen(filename, "rb");
    if(fp==NULL)
    {
        printf("Failed to open '%s'.  Please correct the filename and try again.\n", filename);
        return;
    }

    fread(&l, sizeof(l), 1, fp);
    if(l!=machModelCode)
    {
        printf("'%s' is not a valid Machinations model.  Aborting.\n", filename);
        fclose(fp);
        return;
    }

    fread(&l, sizeof(l), 1, fp);
    vc=(int)l;
    fread(&l, sizeof(l), 1, fp);
    tc=(int)l;

    for(i=0;i<vc;i++)
    {
        if(fread(&v3, sizeof(v3), 1, fp)<1)
        {
            printf("'%s' is corrupt.  Aborting.\n", filename);
            fclose(fp);
            return;
        }

        if(v3.x<minx)            minx=v3.x;
        if(v3.x>maxx)            maxx=v3.x;
        if(v3.y<miny)            miny=v3.y;
        if(v3.y>maxy)            maxy=v3.y;
        if(v3.z<minz)            minz=v3.z;
        if(v3.z>maxz)            maxz=v3.z;
        if(v3.radius()>radius)   radius=v3.radius();
    }

    printf("Info on '%s'\n", filename);
    printf("    Vertices: %7d    Triangles: %7d\n", vc, tc);
    printf("    Center X: %7.3f    Center Y:  %7.3f    Center Z: %7.3f\n",
        (maxx+minx)/2, (maxy+miny)/2, (maxz+minz)/2);
    printf("    Size X:   %7.3f    Size Y:    %7.3f    Size Z:   %7.3f\n", maxx-minx, maxy-miny, maxz-minz);
    printf("    Radius:   %7.3f    Max Z:     %7.3f\n", radius, maxz);
}


