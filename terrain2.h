/****************************************************************************
* Machinations copyright (C) 2001-2003 by John Carter and Jon Sargeant      *
****************************************************************************/
#ifndef	machinations_terrain_h
#define machinations_terrain_h

//Headers:
#include "parse.h"
#include "mtrand.h"
#include "cache.h"

//Defines:

//Location of terrain models and textures:
#define TERRAIN_PATH            "terrain/"

//Arbitrarily small elevation constant that glues a unit to the sea floor:
#define SEA_FLOOR_DEPTH         -1000

//The maximum number of strata that can be defined
#define MAX_STRATA              256

//The maximum number of buffers that can be defined
#define MAX_BUFFERS             8

//The number of units between each control point in both directions
#define CONTROL_SPACING         1

//The maximum elevation of a unit above the ground is 10
//This rule is necessary for efficient clipping (see orientMap in engine.cpp)
#define MAX_UNIT_ELEVATION      10

#define MESH_STACK_SIZE         100

//eDomain defines a unit's domain and movement on different types of terrain.
//Since air and hover units do not have contact with the ground, they move at
//maximum velocity over any terrain.  Ground and sea units share a narrow strip
//of shallow water.  This facilitates the construction of sea buildings and
//the transport of ground units. (same as Total Annihilation).  Sea units and
//amphibious units bob up and down.
enum eDomain
{
    DOMAIN_ANY,         //The unit can go anywhere.  They remain upright regardless of the incline beneath them.
    DOMAIN_AIR,         //Air units fly far above the ground and can generally fly over any terrain.
                            //Air units remain upright regardless of the incline beneath them.
    DOMAIN_HOVER,       //Hover units fly a small distance above the ground.  They can traverse any
                            //terrain except steep cliffs.
    DOMAIN_AMPHIBIOUS,  //Amphibious units can "walk" on land and "swim in water".  They combine
                            //the movement of ground and sea units.
    DOMAIN_GROUND,      //Ground units "walk" on land.  Although they can wade through knee-deep
                            //water, they cannot traverse lakes.
    DOMAIN_SEA,         //Sea units can only move in water.  They float on the water's surface,
                            //bobbing up and down.
    DOMAIN_NONE,        //The unit can't go anywhere
};

enum eWaveformSource { WAVEFORM_NONE, WAVEFORM_RANDOM, WAVEFORM_ELEVATION, WAVEFORM_STRATUM_TYPE,
    WAVEFORM_BUFFER, WAVEFORM_DECORATION_DENSITY};

enum eSplineSurfaceTarget { SPLINE_SURFACE_NONE, SPLINE_SURFACE_ELEVATION, SPLINE_SURFACE_STRATUM_TYPE,
    SPLINE_SURFACE_BUFFER, SPLINE_SURFACE_DECORATION_DENSITY};

enum eTrimOrientation { TRIM_NONE, TRIM_A, TRIM_B, TRIM_C, TRIM_AB, TRIM_BC, TRIM_CA };

//Typedefs:
typedef void (* tTexCoordFunc)(float a, float tx, float ty, float tx2, float ty2);

//Structures:
struct tTileInfo
{
    float			height;
    float           stratumIndex;
    tVector3D       normal;
    bool            underwater;
};

//Think front slashes and back slashes...
#define ORIENT_FRONT 0
#define ORIENT_BACK 1

struct tControlPoint
{
    float height;
    float smoothHeight;
    float stratumIndex;
    bool underwater : 1;
    bool tileUnderwater : 1;
    unsigned char regionUnderwater;
};

struct tStratum
{
    tString         name;
    iTextureIndex   texture;
    iTextureIndex   alphaMap;
    tString         filename;
    float           scaleX;
    float           scaleY;
    bool            blend;

    vector<unsigned int> solidTriangles;
    vector<unsigned int> translucentTriangles;
    vector<unsigned int> trimTriangles;
    
    tStratum() {}
};

struct tDecoration
{
    tVector3D       position;
    tVector3D       forward;
    tVector3D       side;
    tVector3D       up;
};

class tModelType;
struct tDecorationType
{
    tString         name;
    tModelType*     modelType;
    float*          densityData;
    vector<tDecoration> decorations;
};

struct tShorePoint
{
    tVector2D       v;
    tVector2D       gradient;
    tWorldTime      startTime; //-1 means wave does not exist here
    tVector2D       pos;
    float           length;
    float           alpha;
    tShorePoint(const tVector2D& _v, const tVector2D& _gradient) :
        v(_v), gradient(_gradient), startTime(-1), pos(zeroVector2D), length(0), alpha(0) {}
    tShorePoint() : v(zeroVector2D), gradient(zeroVector2D), startTime(-1),
        pos(zeroVector2D), length(0), alpha(0) {}
};

struct tWaveform
{
    eWaveformSource source;
    int sourceIndex;
    float wavelength;   //WAVEFORM_RANDOM only
    float amplitude;
    float bias;
    float *data; //Temporary structure for storing random waveform data.
    vector<tMapping> mappings;
};

struct tSplineSurface
{
    vector<tWaveform> waveforms;
    eSplineSurfaceTarget target;
    int targetIndex;
    bool influenceFlightPath;
};

struct tMeshVertex
{
    tVector3D point;
    tVector3D normal;
    unsigned char stratumIndex;
    float alpha;
};

struct tMeshTriangleEx
{
    unsigned char stratum;
    eTrimOrientation orientation : 8;
    int vi;
    int vj;
    tMeshTriangleEx *next;

    void setNext(tMeshTriangleEx *_next)
    {
        next=_next;
    }
    tMeshTriangleEx(unsigned char _stratum, eTrimOrientation _orientation,
        int _vi, int _vj) : stratum(_stratum),
        orientation(_orientation), vi(_vi), vj(_vj), next(NULL) {}
    tMeshTriangleEx() : stratum(0), orientation(TRIM_NONE), vi(0), vj(0), next(NULL) {}
};

/*  A
 *  |.
 *  |  .  H
 *  1    .
 *  |      .
 *  B---2----C
 */

struct tMeshTriangle
{
    int id;
    tMeshTriangle *leg1;
    tMeshTriangle *leg2;
    tMeshTriangle *hypotenuse;
    tMeshTriangle *child1;
    tMeshTriangle *child2;
    int va;
    int vb;
    int vc;
    tMeshTriangleEx *ex;
    unsigned char minStratumIndex;
    unsigned char maxStratumIndex;
    bool isEdge : 8;

    void setLeg1(tMeshTriangle *_leg1)
    {
        leg1=_leg1;
        if(child1)
            child1->setHypotenuse(_leg1);
    }
    void setLeg2(tMeshTriangle *_leg2)
    {
        leg2=_leg2;
        if(child2)
            child2->setHypotenuse(_leg2);
    }
    void setHypotenuse(tMeshTriangle *_hypotenuse)
    {
        hypotenuse=_hypotenuse;
    }
    void addEx(tMeshTriangleEx *_ex)
    {
        _ex->setNext(ex);
        ex=_ex;
    }
    tMeshTriangle(int _id, tMeshTriangle *_leg1, tMeshTriangle *_leg2, tMeshTriangle *_hypotenuse,
        int _va, int _vb, int _vc) :
        id(_id), leg1(_leg1), leg2(_leg2), hypotenuse(_hypotenuse), child1(NULL), child2(NULL),
        va(_va), vb(_vb), vc(_vc), minStratumIndex(0), maxStratumIndex(0), ex(NULL),
        isEdge(false) {}
    tMeshTriangle() :
        id(0), leg1(NULL), leg2(NULL), hypotenuse(NULL), child1(NULL), child2(NULL),
        va(0), vb(0), vc(0), minStratumIndex(0), maxStratumIndex(0), ex(NULL),
        isEdge(false) {}
};

struct tMesh
{
    tMeshTriangle *ancestor1;
    tMeshTriangle *ancestor2;
    tMesh() : ancestor1(NULL), ancestor2(NULL) {}
};

struct tTriangleNode
{
    tMeshTriangle *triangle;
    int depth;
    bool isFullyVisible; //true if the triangle is entirely visible
};

class tTerrain
{
private:
    float           mapWidth;
    float           mapHeight;

	int	        	controlSizeX;
    int             controlSizeY;
    float           controlScaleX;
    float           controlScaleY;

    vector<tStratum> strata;

    tString         waterFilename;      // Water texture filename
    tString         waveFilename;       // Wave texture filename

    float           waterScaleX;        // Scale of water (4 is default)
    float           waterScaleY;        // Scale of water (4 is default)
    float           waveScaleX;         // Scale of waves (2 is default)
    float           waveScaleY;         // Scale of waves (1 is default)
    float           waveLength;
    float           waveDuration;
    float           waveFrequency;
    float           waveRadius;

    float           minElevation;       // Lowest control point on the map
    float           maxElevation;       // Highest control point on the map
    float           averageElevation;   // The mean of the control points' elevation

    //float           percentWater;

    //Main arrays:
    tControlPoint*          controlPoints;
    vector<tMeshVertex>     meshVertices;
    vector<tMeshTriangle *> meshTriangles;
    vector<tMeshTriangle *> meshEdges;
    tMesh                   mesh;
    tMeshTriangle*          meshTriangleBlock;

    iTextureIndex   waterTexture;
    iTextureIndex   minimapTexture;
    iTextureIndex   shadeTexture;
    iTextureIndex   waveTexture;
    iTextureIndex   waterMapTexture;

    //Temporary variables for saving boundaries for postDraw
    int             interval;               //Temporary variable for holding the tile interval

    vector<tDecorationType> decorationTypes;
    vector<tShorePoint> shorePoints;
    vector<tSplineSurface> splineSurfaces;

    MTRand_int32    randomNumber;
    int             seed;
    bool            intervalFixed;

    int             internalShadeSize;
    int             internalAlphaSize;
    int             internalWaterSize;

    float           minPoolDepth;

    iTextureIndex   stratumTexture3D;

    float           minSeaElevation;
    float           maxSeaElevation;
    float           seaLevel;

    float           shadeMapScale;
    float           shadeMapBias;
    float           shadeMapMin;
    float           shadeMapMax;

    float           seaAlphaScale;
    float           seaAlphaBias;
    float           seaAlphaMax;
    float           seaAlphaMin;

    float           shadowTextureMinX;
    float           shadowTextureMaxX;
    float           shadowTextureMinY;
    float           shadowTextureMaxY;

    int             nextMeshTriangleID;

	void	        destroy();

    float           getBase(int x, int y, eDomain domain, float verticalOffset);
    float           getAlpha(int x, int y);

    void            drawNormals();
    void            applyWater(unsigned char *data, int width, int height, int bpp);

    void            loadRandomControlPoints();
    void            loadRandomDecorations();

    void            addShorePoint(const tVector2D& v);

    tControlPoint&  getControlPoint(int x, int y) { return controlPoints[(y+1)*(controlSizeX+2)+(x+1)]; }

    void            refreshShadeMap(int size);
    void            refreshAlphaMaps(int size);
    void            refreshWaterMap(int size);

    void            parseStratumBlock(tStream& s) throw(tString);
    void            parseSplineSurfaceBlock(tStream& s) throw(tString);
    tWaveform       parseWaveformBlock(tStream& s) throw(tString);
    void            parseDecorationTypeBlock(tStream& s) throw(tString);

    void            buildMesh();
    int             addMeshVertex(const tVector2D& v);
    bool            needsDivision(tMeshTriangle *tri);
    void            divideTriangle(tMeshTriangle *cur);
    void            connectTriangles(tMeshTriangle *prev, tMeshTriangle *cur);

    void            compileMesh(bool calcShadowTextureExtents);
    void            drawMesh(bool genColors, bool genNormals, int index);

    void            computeTransparency(tMeshTriangle *tri);
    unsigned char   calculateRelativePosition(const tVector3D& v);
    void            addTrim(tMeshTriangle *tri);
    void            completeTrim(tMeshTriangle *tri);
    int             interpolateMeshVertex(int a, int b, int stratum);
    int             duplicateMeshVertex(int a, int b, int x);
    void            compressVector(const tVector3D& v, char out[3]);
    void            scanAlphaMap(tMeshTriangle *tri, bool& isOpaque, bool& isTransparent, int stratum);
    //unsigned char   getAlphaMapPoint(int x, int y, int stratum) { ... }

    float           parseMappingValue(const char *fieldName, tStream& s) throw(tString);
    int             parseDecorationType(const char *fieldName, tStream& s) throw(tString);
    int             stratumLookup(const tString& name, bool exact);
    int             decorationTypeLookup(const tString& name, bool exact);
    void            checkBoundaryPoint(const tVector3D& v);

    void            addSplineSurfaceToCacheHeader(const tSplineSurface& ss, tCache& cache);
    void            addWaveformToCacheHeader(const tWaveform& w, tCache& cache);
    void            addMappingToCacheHeader(const tMapping& m, tCache& cache);
    void            addControlPointVariablesToCacheHeader(tCache& cache);

    void            writeMeshToFile(FILE *fp);
    void            readMeshFromFile(FILE *fp);

public:
	tTerrain(float width, float height, tStream& s) throw(tString);
	~tTerrain();

	tTileInfo	    getTileInfo(tVector2D position, eDomain domain, float verticalOffset);
    tVector3D       pixelToTerrain3D(int x, int y, eDomain domain, float elevation);
    tVector3D       pixelToTerrain3D(int x, int y)
                        { return pixelToTerrain3D(x,y,DOMAIN_NONE,0); }
    tVector2D       pixelToTerrain2D(int x, int y, eDomain domain, float elevation)
                        { return to2D(pixelToTerrain3D(x,y,domain,elevation)); }
    tVector2D       pixelToTerrain2D(int x, int y)
                        { return to2D(pixelToTerrain3D(x,y,DOMAIN_NONE,0)); }

    void            tick(tWorldTime t);
    void            intervalTick(tWorldTime t);
	void	        draw();
	void	        postDraw();
	void	        drawMinimap();
    void            drawDecorations();
    void            drawDecorationShadows();
    void            refresh();

    float           getMinElevation() { return minElevation; }
    float           getMaxElevation() { return maxElevation; }
    float           getAverageElevation() { return averageElevation; }

    void            onSetViewDimensions(); //The engine calls this function in response to setViewDimensions()
    void            onZoomMap(); //The engine calls this function in response to zoomMap()
    void            onTiltMap(); //The engine calls this function in response to tiltMap()
    void            onOrientMap(); //The engine calls this function in response to orientMap()

    bool            isIntervalFixed() { return intervalFixed; }
    int             getInterval() { return interval; }
    void            setInterval(int _interval);
    void            resetInterval();

    void            onSetShadeMapSize(int size);
    void            onSetAlphaMapSize(int size);
    void            onSetWaterMapSize(int size);

    float           getShadowTextureMinX() { return shadowTextureMinX; }
    float           getShadowTextureMaxX() { return shadowTextureMaxX; }
    float           getShadowTextureMinY() { return shadowTextureMinY; }
    float           getShadowTextureMaxY() { return shadowTextureMaxY; }
    void            calcVisibleArea(bool calcShadowTextureExtents);
};

extern eDomain      parseDomain(const char *fieldName, tStream& s) throw(tString);
extern eWaveformSource parseWaveformSource(const char *fieldName, tStream& s) throw(tString);
extern eSplineSurfaceTarget parseSplineSurfaceTarget(const char *fieldName, tStream& s) throw(tString);
/****************************************************************************/
#endif // machinations_terrain_h
