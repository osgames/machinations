/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef messageH
#define messageH

//Headers
#include "memwatch.h"
#include "engine.h"

/****************************************************************************/
/*
 * These macros faciliate reading from and writing to a stream in packageMessage
 * and unpackageMessage.  Although one could implement them as function, I prefer
 * macros since I can "pass" structures directly.
*/
/*
 * WRITE_TYPE writes an object to the stream.  For instance, WRITE_TYPE(int, foobar)
 * writes an integer called foobar.  The macro will attempt to convert the variable
 * to the specified type.  If the conversion fails, the macro will produce a compiler
 * error.  If there is insufficient buffer space to write the type, the function
 * will abort.
 */
#define WRITE_TYPE(type,val) \
    { \
        if(ptr-buffer+sizeof(type)>length) \
            return 0; \
        *(type*)ptr=(type)val; \
        ptr+=sizeof(type); \
    }

/*
 * READ_TYPE attempts to read an object from the stream.  If the object is
 * incomplete (e.g. the stream does not fully contain it), the function will
 * abort.  Otherwise, the macro will pull the object from the stream and store
 * it in the specified variable.  If the object is not compatible with the
 * variable, the macro will produce a compiler error.
 */
#define READ_TYPE(type,val) \
    { \
        if(ptr-buffer+sizeof(type)>length) \
            return 0; \
        val=*(type*)ptr; \
        ptr+=sizeof(type); \
    }

/*
 * WRITE_STRING writes a null-terminated character array to the stream including
 * the null-terminator.  You must provide a character pointer (char *) or
 * constant character pointer (const char *).  Any other data type will produce
 * a compiler error.  If there is insufficient buffer space to write the type,
 * the function will abort.
 */
#define WRITE_STRING(val) \
    { \
        if(ptr-buffer+strlen(val)+1>length) \
            return 0; \
        strcpy(ptr, val); \
        ptr+=strlen(val)+1; \
    }

/*
 * READ_STRING attempts to read a null-terminated character array from the stream.
 * If the macro does not encounter a null-terminator, it will conclude that the
 * string is incomplete and abort.  Otherwise, it will store a pointer to the
 * beginning of the string in the specified variable.  You must provide a
 * tString container to store the string in.  Any other data type will produce
 * a compiler error.
 */
#define READ_STRING(val) \
    { \
        const char *_start=ptr; \
        while(ptr-buffer<length && *ptr!='\0') \
            ptr++; \
        if(ptr-buffer>=length) \
            return 0; \
        val=_start; \
        ptr++; \
    }

/*
 * WRITE_MEMORY writes a block of memory to the stream.  It will write 'len'
 * bytes of memory beginning at 'val'.  'val' must be a character pointer (char *)
 * or constant character pointer (const char *).  'len' can be any integer data
 * type.  Any other data type will produce a compiler error.  If there is
 * insufficient buffer space to write the type, the function will abort.
 */
#define WRITE_MEMORY(val,len) \
    { \
        if(ptr-buffer+len>length) \
            return 0; \
        memcpy(ptr, val, len); \
        ptr+=len; \
    }

/*
 * READ_MEMORY attempts to read a block of memory from the stream.  If the block
 * is incomplete (e.g. the stream does not fully contain the block), the function
 * will abort.  Otherwise, it will copy the data to the array referenced by val.
 * You must ensure that the array is large enough to hold the block.
 */
#define READ_MEMORY(val,len) \
    { \
        if(ptr-buffer+len>length) \
            return 0; \
        memcpy(val,ptr,len); \
        ptr+=len; \
    }

enum eMessage {
    MSG_NONE,               //0
    MSG_RESEND,             //1 UDP only
    MSG_ACK_PACKETS,        //2 UDP only
    MSG_PACKET_RESENT,      //3 UDP only
    MSG_REFUSE,             //4
    MSG_HOST_ADDRESS,       //5
    MSG_USER_DATA,          //6 (Do not change this value for compatibility)
    MSG_ASSIGN,             //7
    MSG_CONNECT_USER,       //8
    MSG_ADD_USER,           //9
    MSG_ID,                 //10
    MSG_DROP_USER,          //11
    MSG_DISCONNECT,         //12
    MSG_TEXT,               //13
    MSG_PING,               //14
    MSG_PONG,               //15
    MSG_START_GAME,         //16
    MSG_END_GAME,           //17
    MSG_END_INTERVAL,       //18
    MSG_SET_TIMING,         //19
    MSG_SET_TIMING_IN_GAME, //20
    MSG_TIMING_DIVIDER,     //21
    MSG_FILE_TRANSFER,      //22
    MSG_ABORT_UPLOAD,       //23
    MSG_ABORT_DOWNLOAD,     //24
    MSG_FILE_PACKET,        //25
    MSG_PAUSE_GAME,         //26
    MSG_RESUME_GAME,        //27
    MSG_RESET_PLAYERS,      //28
    MSG_CHOOSE_FILES,       //29
    MSG_ACK_FILES,          //30
    MSG_SET_PLAYER,         //31
    MSG_SET_ALLIANCE,       //32
    MSG_SET_RESOURCE,       //33
    MSG_SYNC_QUERY,         //34
    MSG_SYNC,               //35

    MSG_COMMAND,            //36
    MSG_COMMAND_POS,        //37
    MSG_COMMAND_UNIT,       //38
    MSG_COMMAND_PRODUCTION, //39

    MSG_UNIT_PLAN,          //40
    MSG_UNIT_CREATE,        //41
    MSG_UNIT_DESTROY,       //42
    MSG_UNIT_SET_OWNER,     //43
    MSG_UNIT_SET_POS,       //44
    MSG_UNIT_SET_VELOCITY,  //45
    MSG_UNIT_SET_FORWARD,   //46
    MSG_UNIT_SET_HP,        //47

    MSG_DEPOSIT_CREATE,     //48
    MSG_DEPOSIT_DESTROY,    //49
    MSG_DEPOSIT_SET,        //50
};

class tMessage
{
    public:
    eMessage    type;           //Message type
    tGameTime   executionTime;  //Time when the message will execute
    short       playerID;       //Used in game messages only -- indicates who sent message

/***************************************************************************\
packageMessage

"Package" message and write it to a stream.  The generic implementation
just writes one character: the type.  Descendents will override this function
to write additional values.

Inputs:
    buffer      Output stream
    length      Length of the output stream
Outputs:
    Returns the number of bytes written to the output stream or 0 if there
        is insufficient space in the buffer to package the message.
\***************************************************************************/
    virtual int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        return (int) (ptr - buffer);
    };

/***************************************************************************\
unpackageMessage

Read a message from a stream and "unpackage" it.  The generic implementation
just reads one character: the type.  Descendents will override this function
to read additional values and store them in their respective member variables.

Inputs:
    buffer      Input stream
    length      Length of the input stream
Outputs:
    Returns the number of bytes read from the input stream or 0 if the message
        is incomplete.
\***************************************************************************/
    virtual int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        return (int) (ptr - buffer);
    };

    static tMessage *getMessage(const char *buffer, int length, int& bytesRead);
    static tMessage *duplicateMessage(tMessage *message);
    static void dumpMessage(tMessage *message);

    //Constructors
    tMessage() {}
    tMessage(eMessage _type) : type(_type) {}

    //We must declare the destructor as virtual
    virtual ~tMessage() {}
};

/*******************
 Connection Messages
 *******************/
class tRefuseMsg : public tMessage
{
    public:
    eRefuse code;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(eRefuse, code)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(eRefuse, code)
        return (int) (ptr - buffer);
    }
    tRefuseMsg() : tMessage(MSG_REFUSE) {}
    tRefuseMsg(eRefuse _code) : tMessage(MSG_REFUSE), code(_code) {}
};

class tHostAddressMsg : public tMessage
{
    public:
    long IPAddress;
    short port;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(long, IPAddress)
        WRITE_TYPE(short, port)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(long, IPAddress)
        READ_TYPE(short, port)
        return (int) (ptr - buffer);
    }
    tHostAddressMsg() : tMessage(MSG_HOST_ADDRESS) {}
    tHostAddressMsg(long _IPAddress, short _port)
         : tMessage(MSG_HOST_ADDRESS), IPAddress(_IPAddress), port(_port) {}
};

class tUserDataMsg : public tMessage
{
    public:
    short version;
    tString name;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, version)
        WRITE_STRING(CHARPTR(name))
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, version)
        READ_STRING(name)
        return (int) (ptr - buffer);
    }
    tUserDataMsg() : tMessage(MSG_USER_DATA) {}
    tUserDataMsg(short _version, tString& _name) : tMessage(MSG_USER_DATA), version(_version), name(_name) {}
};

class tAssignMsg : public tMessage
{
    public:
    short ID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    tAssignMsg() : tMessage(MSG_ASSIGN) {}
    tAssignMsg(short _ID) : tMessage(MSG_ASSIGN), ID(_ID) {}
};

class tConnectUserMsg : public tMessage
{
    public:
    tUser user;
    long IPAddress;
    short port;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, user.ID)
        WRITE_TYPE(long, IPAddress)
        WRITE_TYPE(short, port)
        WRITE_STRING(CHARPTR(user.name))
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        short uID;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, uID)
        user.ID=(int)uID;
        READ_TYPE(long, IPAddress)
        READ_TYPE(short, port)
        READ_STRING(user.name)
        return (int) (ptr - buffer);
    }
    tConnectUserMsg() : tMessage(MSG_CONNECT_USER) {}
    tConnectUserMsg(tUser *_user, long _IPAddress, short _port) : tMessage(MSG_CONNECT_USER),
        user(*_user), IPAddress(_IPAddress), port(_port) {}
};

class tAddUserMsg : public tMessage
{
    public:
    tUser user;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, user.ID)
        WRITE_STRING(CHARPTR(user.name))
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        short uID;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, uID)
        user.ID=(int)uID;
        READ_STRING(user.name)
        return (int) (ptr - buffer);
    }
    tAddUserMsg() : tMessage(MSG_ADD_USER) {}
    tAddUserMsg(tUser *_user) : tMessage(MSG_ADD_USER), user(*_user) {}
};

class tIDMsg : public tMessage
{
    public:
    short ID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    tIDMsg() : tMessage(MSG_ID) {}
    tIDMsg(short _ID) : tMessage(MSG_ID), ID(_ID) {}
};

class tDropUserMsg : public tMessage
{
    public:
    short ID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, ID)
        return (int) (ptr - buffer);
    }
    tDropUserMsg() : tMessage(MSG_DROP_USER) {}
    tDropUserMsg(short _ID) : tMessage(MSG_DROP_USER), ID(_ID) {}
};

class tTextMsg : public tMessage
{
    public:
    eTextMessage textType;
    tString text;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type, cTextType=(char)textType;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(char, cTextType)
        WRITE_STRING(CHARPTR(text))
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType,cTextType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(char, cTextType)
        textType=(eTextMessage)cTextType;
        READ_STRING(text)
        return (int) (ptr - buffer);
    }
    tTextMsg() : tMessage(MSG_TEXT) {}
    tTextMsg(const tString& _text, eTextMessage _textType) : tMessage(MSG_TEXT),
        text(_text), textType(_textType) {}
};

class tSetTimingMsg : public tMessage
{
    public:
    long latency;
    long netInterval;
    long gameInterval;
    float gameSpeed;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        if(type==MSG_SET_TIMING_IN_GAME || type==MSG_TIMING_DIVIDER)
            WRITE_TYPE(short, playerID)
        WRITE_TYPE(long, latency)
        WRITE_TYPE(long, netInterval)
        WRITE_TYPE(long, gameInterval)
        WRITE_TYPE(float, gameSpeed)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        if(type==MSG_SET_TIMING_IN_GAME || type==MSG_TIMING_DIVIDER)
            READ_TYPE(short, playerID)
        READ_TYPE(long, latency)
        READ_TYPE(long, netInterval)
        READ_TYPE(long, gameInterval)
        READ_TYPE(float, gameSpeed)
        return (int) (ptr - buffer);
    }
    tSetTimingMsg() : tMessage() {}
    tSetTimingMsg(eMessage type, long _latency, long _netInterval, long _gameInterval, float _gameSpeed) :
        tMessage(type), latency(_latency), netInterval(_netInterval), gameInterval(_gameInterval),
        gameSpeed(_gameSpeed) {}
};

class tPingMsg : public tMessage
{
    public:
    long ID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(long, ID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(long, ID)
        return (int) (ptr - buffer);
    }
    tPingMsg() : tMessage(MSG_PING) {}
    tPingMsg(long _ID) : tMessage(MSG_PING), ID(_ID) {}
};

class tPongMsg : public tMessage
{
    public:
    long ID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(long, ID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(long, ID)
        return (int) (ptr - buffer);
    }
    tPongMsg() : tMessage(MSG_PONG) {}
    tPongMsg(long _ID) : tMessage(MSG_PONG), ID(_ID) {}
};

/**********************
 File Transfer Messages
 **********************/
class tFileTransferMsg : public tMessage
{
    public:
    tFile file;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_STRING(CHARPTR(file.name))
        WRITE_TYPE(long, file.size)
        WRITE_TYPE(time_t, file.time)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_STRING(file.name)
        READ_TYPE(long, file.size)
        READ_TYPE(time_t, file.time)
        file.path=DOWNLOAD_PATH;
        return (int) (ptr - buffer);
    }
    tFileTransferMsg() : tMessage(MSG_FILE_TRANSFER) {}
    tFileTransferMsg(const tFile& _file) :
        tMessage(MSG_FILE_TRANSFER), file(_file) {}
};

class tDataMsg : public tMessage
{
    public:
    short bytes;
    char *data;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
        if(data)
        {
            char cType=(char)type;
			WRITE_TYPE(char, cType)
            WRITE_TYPE(short, bytes)
            WRITE_MEMORY(data, bytes)
        }
        else
            bug("tFilePacketMsg::packageMessage: NULL data pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, bytes)
        data = mwNew char[bytes];
        READ_MEMORY(data, bytes)
        return (int) (ptr - buffer);
    }
    tDataMsg() : tMessage(), bytes(0), data(NULL) {}
    tDataMsg(eMessage type, short _bytes, char *_data) : tMessage(type), bytes(_bytes)
    {
        data=mwNew char[bytes];
        memcpy(data,_data,bytes);
    }
    //Added 1/1/03:  Even though the network module never uses the equal operator,
    //this is a disaster waiting to happen.  Thus, I implement the equal operator
    //as a precautionary measure.
    tDataMsg& operator=(const tDataMsg& x)
    {
        if(data)
            mwDelete data;

        memcpy(this,&x,sizeof(*this));

        if(data)
        {
            data=mwNew char[bytes];
            memcpy(data,x.data,bytes);
        }

        return *this;
    }
    tDataMsg(const tDataMsg& x) : tMessage(), bytes(0), data(NULL) //Copy constructor
    {
        operator=(x);
    }
    ~tDataMsg()
    {
        if(data)
            mwDelete data;
    }
};

/************
 UDP Messages
 ************/
//>>>>>>>>>>> UDP ONLY <<<<<<<<<<<
class tResendMsg : public tMessage
{
    public:
    long firstID;
    long lastID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(long, firstID)
        WRITE_TYPE(long, lastID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(long, firstID)
        READ_TYPE(long, lastID)
        return (int) (ptr - buffer);
    }
    tResendMsg() : tMessage(MSG_RESEND) {}
    tResendMsg(long _firstID, long _lastID) : tMessage(MSG_RESEND), firstID(_firstID), lastID(_lastID) {}
};

class tAckPacketsMsg : public tMessage
{
    public:
    long IDs[ACK_INTERVAL];
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        for(int i=0; i<ACK_INTERVAL; i++)
            WRITE_TYPE(long, IDs[i])
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        for(int i=0; i<ACK_INTERVAL; i++)
            READ_TYPE(long, IDs[i])
        return (int) (ptr - buffer);
    }
    tAckPacketsMsg() : tMessage(MSG_ACK_PACKETS) {}
    tAckPacketsMsg(long _IDs[]) : tMessage(MSG_ACK_PACKETS)
    {
        for(int i=0; i<ACK_INTERVAL; i++)
            IDs[i] = _IDs[i];
    }
};
//>>>>>>>>> END UDP ONLY <<<<<<<<<<<

/*************
 Game Messages
 *************/
class tChooseFilesMsg : public tMessage
{
    public:
    tFile mapFile;
    tFile worldFile;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)

        WRITE_STRING(CHARPTR(mapFile.name))
        WRITE_TYPE(long, mapFile.size)
        WRITE_TYPE(time_t, mapFile.time)

        WRITE_STRING(CHARPTR(worldFile.name))
        WRITE_TYPE(long, worldFile.size)
        WRITE_TYPE(time_t, worldFile.time)

        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;

        READ_STRING(mapFile.name)
        READ_TYPE(long, mapFile.size)
        READ_TYPE(time_t, mapFile.time)

        READ_STRING(worldFile.name)
        READ_TYPE(long, worldFile.size)
        READ_TYPE(time_t, worldFile.time)

        mapFile.path=DOWNLOAD_PATH;
        worldFile.path=DOWNLOAD_PATH;
        return (int) (ptr - buffer);
    }
    tChooseFilesMsg() : tMessage() {}
    tChooseFilesMsg(eMessage type, const tFile& _mapFile, const tFile& _worldFile) :
        tMessage(type), mapFile(_mapFile), worldFile(_worldFile) {}
};

class tAckFilesMsg : public tMessage
{
    public:
    bool haveMap;
    bool haveWorld;
    tFile mapFile;
    tFile worldFile;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
		char cHaveMap=(char)haveMap;
        char cHaveWorld=(char)haveWorld;

        WRITE_TYPE(char, cType)
        WRITE_TYPE(char, cHaveMap)
        WRITE_TYPE(char, cHaveWorld)

        WRITE_STRING(CHARPTR(mapFile.name))
        WRITE_TYPE(long, mapFile.size)
        WRITE_TYPE(time_t, mapFile.time)

        WRITE_STRING(CHARPTR(worldFile.name))
        WRITE_TYPE(long, worldFile.size)
        WRITE_TYPE(time_t, worldFile.time)

        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        char cType, cHaveMap, cHaveWorld;
		READ_TYPE(char, cType)
        READ_TYPE(char, cHaveMap)
        READ_TYPE(char, cHaveWorld)
		type=(eMessage)cType;
        haveMap=(bool)cHaveMap;
        haveWorld=(bool)cHaveWorld;

        READ_STRING(mapFile.name)
        READ_TYPE(long, mapFile.size)
        READ_TYPE(time_t, mapFile.time)

        READ_STRING(worldFile.name)
        READ_TYPE(long, worldFile.size)
        READ_TYPE(time_t, worldFile.time)

        mapFile.path=DOWNLOAD_PATH;
        worldFile.path=DOWNLOAD_PATH;
        return (int) (ptr - buffer);
    }
    tAckFilesMsg() : tMessage() {}
    tAckFilesMsg(bool _haveMap, bool _haveWorld, const tFile& _mapFile, const tFile& _worldFile) :
        tMessage(MSG_ACK_FILES), haveMap(_haveMap), haveWorld(_haveWorld), mapFile(_mapFile), worldFile(_worldFile) {}
};

class tSetPlayerMsg : public tMessage
{
    public:
    ePlayer playerType;
    short userID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
		char cPlayerType=(char)playerType;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(char, cPlayerType)
        WRITE_TYPE(short, userID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        char cType, cPlayerType;
		READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(char, cPlayerType)
        playerType=(ePlayer)cPlayerType;
        READ_TYPE(short, userID)
        return (int) (ptr - buffer);
    }
    tSetPlayerMsg() : tMessage(MSG_SET_PLAYER) {}
    tSetPlayerMsg(char _playerID, ePlayer _playerType, short _userID) : tMessage(MSG_SET_PLAYER),
        playerType(_playerType), userID(_userID) { playerID=_playerID; }
};

//Use this class to post game message without any parameters.
//Example: tEndIntervalMsg
class tGameMsg : public tMessage
{
    public:
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(char, playerID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(char, playerID)
        return (int) (ptr - buffer);
    }
    tGameMsg() : tMessage() {}
    tGameMsg(eMessage type) : tMessage(type) {}
};

class tSetAllianceMsg : public tMessage
{
    public:
    short sourcePlayerID;
    short targetPlayerID;
    eAlliance alliance;
    bool sharedControl;
    bool sharedResources;
    bool sharedVision;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
		char cAlliance=(char)alliance;
        char cSharedControl=(char)sharedControl;
        char cSharedResources=(char)sharedResources;
        char cSharedVision=(char)sharedVision;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(short, sourcePlayerID)
        WRITE_TYPE(short, targetPlayerID)
        WRITE_TYPE(char, cAlliance)
        WRITE_TYPE(char, cSharedControl)
        WRITE_TYPE(char, cSharedResources)
        WRITE_TYPE(char, cSharedVision)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        char cType, cAlliance, cSharedControl, cSharedResources, cSharedVision;
		READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(short, sourcePlayerID)
        READ_TYPE(short, targetPlayerID)
        READ_TYPE(char, cAlliance)
        READ_TYPE(char, cSharedControl)
        READ_TYPE(char, cSharedResources)
        READ_TYPE(char, cSharedVision)
        alliance=(eAlliance)cAlliance;
        sharedControl=(bool)cSharedControl;
        sharedResources=(bool)cSharedResources;
        sharedVision=(bool)cSharedVision;
        return (int) (ptr - buffer);
    }
    tSetAllianceMsg() : tMessage(MSG_SET_ALLIANCE) {}
    tSetAllianceMsg(short _sourcePlayerID, short _targetPlayerID, eAlliance _alliance,
        bool _sharedControl, bool _sharedResources, bool _sharedVision) :
        tMessage(MSG_SET_ALLIANCE), sourcePlayerID(_sourcePlayerID),
        targetPlayerID(_targetPlayerID), alliance(_alliance), sharedControl(_sharedControl),
        sharedResources(_sharedResources), sharedVision(_sharedVision) {}
};

class tSetResourceMsg : public tMessage
{
    public:
    short targetPlayerID;
    short resourceID;
    float amount;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(short, targetPlayerID)
        WRITE_TYPE(short, resourceID)
        WRITE_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
        char cType;
		READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(short, targetPlayerID)
        READ_TYPE(short, resourceID)
        READ_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    tSetResourceMsg() : tMessage(MSG_SET_RESOURCE) {}
    tSetResourceMsg(short _targetPlayerID, short _resourceID, float _amount) :
        tMessage(MSG_SET_RESOURCE), targetPlayerID(_targetPlayerID),
        resourceID(_resourceID), amount(_amount) {}
};

class tCommandMsg : public tMessage
{
    public:
    eCommand command;
    long *units;
    long unitCount;
    tVector2D pos;
    long *targets;
    long targetCount;
    short quantity;
    bool repeat;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType, cCommand, cRepeat;
        if(units)
        {
			cType=(char)type;
			cCommand=(char)command;
			WRITE_TYPE(char, cType)
            WRITE_TYPE(char, cCommand)
            WRITE_TYPE(short, playerID)
            WRITE_TYPE(long, unitCount)
            WRITE_MEMORY(units,unitCount*sizeof(long))
            if(type==MSG_COMMAND_POS)
                WRITE_TYPE(tVector2D, pos)
            else if(type==MSG_COMMAND_UNIT)
            {
                if(targets)
                {
                    WRITE_TYPE(long, targetCount)
                    WRITE_MEMORY(targets,targetCount*sizeof(long))
                }
                else
                    bug("tCommandMsg::packageMessage: NULL target pointer");
            }
            else if(type==MSG_COMMAND_PRODUCTION)
            {
                WRITE_TYPE(short,quantity)
                cRepeat=(char)repeat;
                WRITE_TYPE(char,cRepeat)
            }
        }
        else
            bug("tCommandMsg::packageMessage: NULL unit pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType, cCommand, cRepeat;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(char, cCommand)
		command=(eCommand)cCommand;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitCount)
        units=mwNew long[unitCount];
        READ_MEMORY(units,unitCount*sizeof(long))
        if(type==MSG_COMMAND_POS)
            READ_TYPE(tVector2D, pos)
        else if(type==MSG_COMMAND_UNIT)
        {
            READ_TYPE(long, targetCount)
            targets=mwNew long[targetCount];
            READ_MEMORY(targets,targetCount*sizeof(long))
        }
        else if(type==MSG_COMMAND_PRODUCTION)
        {
            READ_TYPE(short, quantity)
            READ_TYPE(char, cRepeat)
            repeat=(bool)cRepeat;
        }
        return (int) (ptr - buffer);
    }
    tCommandMsg() : tMessage(), units(NULL), unitCount(0), pos(zeroVector2D),
        targets(NULL), targetCount(0) {}

    tCommandMsg(eCommand _command, long *_units, long _unitCount) :
        tMessage(MSG_COMMAND), command(_command), units(_units), unitCount(_unitCount),
        pos(zeroVector2D), targets(NULL), targetCount(0), quantity(0), repeat(false)
    {
        if(units)
        {
            units=mwNew long[unitCount];
            memcpy(units,_units,unitCount*sizeof(long));
        }
    }
    tCommandMsg(eCommand _command, long *_units, long _unitCount, tVector2D _pos) :
        tMessage(MSG_COMMAND_POS), command(_command), units(_units), unitCount(_unitCount),
        pos(_pos), targets(NULL), targetCount(0), quantity(0), repeat(false)
    {
        if(units)
        {
            units=mwNew long[unitCount];
            memcpy(units,_units,unitCount*sizeof(long));
        }
    }
    tCommandMsg(eCommand _command, long *_units, long _unitCount, long *_targets, long _targetCount) :
        tMessage(MSG_COMMAND_UNIT), command(_command), units(_units), unitCount(_unitCount),
        pos(zeroVector2D), targets(_targets), targetCount(_targetCount), quantity(0), repeat(false)
    {
        if(units)
        {
            units=mwNew long[unitCount];
            memcpy(units,_units,unitCount*sizeof(long));
        }
        if(targets)
        {
            targets=mwNew long[targetCount];
            memcpy(targets,_targets,targetCount*sizeof(long));
        }
    }
    tCommandMsg(eCommand _command, long *_units, long _unitCount, int _quantity, bool _repeat) :
        tMessage(MSG_COMMAND_PRODUCTION), command(_command), units(_units), unitCount(_unitCount),
        pos(zeroVector2D), targets(NULL), targetCount(0), quantity(_quantity), repeat(_repeat)
    {
        if(units)
        {
            units=mwNew long[unitCount];
            memcpy(units,_units,unitCount*sizeof(long));
        }
    }
    tCommandMsg& operator=(const tCommandMsg& x)
    {
        if(units)
            mwDelete units;
        if(targets)
            mwDelete targets;

        memcpy(this,&x,sizeof(*this));

        if(units)
        {
            units=mwNew long[unitCount];
            memcpy(units,x.units,unitCount*sizeof(long));
        }
        if(targets)
        {
            targets=mwNew long[targetCount];
            memcpy(targets,x.targets,targetCount*sizeof(long));
        }
        return *this;
    }
    tCommandMsg(const tCommandMsg& x) : tMessage(x.type), units(NULL), unitCount(0),
        targets(NULL), targetCount(0) //Copy constructor
    {
        operator=(x);
    }
    ~tCommandMsg()
    {
        if(units)
            mwDelete units;
        if(targets)
            mwDelete targets;
    }
};

class tUnitCreateMsg : public tMessage
{
    public:
    long unitTypeID;
    short ownerID;
    tVector2D pos;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(long, unitTypeID)
        WRITE_TYPE(short, ownerID)
        WRITE_TYPE(tVector2D, pos)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitTypeID)
        READ_TYPE(short, ownerID)
        READ_TYPE(tVector2D, pos)
        return (int) (ptr - buffer);
    }
    tUnitCreateMsg() : tMessage() {}
    tUnitCreateMsg(eMessage type, short _ownerID, long _unitTypeID, tVector2D _pos) : tMessage(type),
        ownerID(_ownerID), unitTypeID(_unitTypeID), pos(_pos) {}
};

/*
 * The following four structures provide templates for administrative unit messages.
 * For instance, one could use tUnitLongMsg to set a unit's hitpoints, shield points,
 * or mana points.  Add a 'typedef' at the end of this file for each new message
 * which uses one of these templates.  Some messages may require their own structure
 * such as tUnitCreateMsg.
 */
class tUnitMsg : public tMessage
{
    public:
    long *units;
    long unitCount;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType;
        if(units)
        {
			cType=(char)type;
			WRITE_TYPE(char, cType)
            WRITE_TYPE(short, playerID)
            WRITE_TYPE(long, unitCount)
            WRITE_MEMORY(units,unitCount*sizeof(long))
        }
        else
            bug("tUnitMsg::packageMessage: NULL unit pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitCount)
        units=mwNew long[unitCount];
        READ_MEMORY(units,unitCount*sizeof(long))
        return (int) (ptr - buffer);
    }
    tUnitMsg() : tMessage(), units(NULL), unitCount(0) {}
    tUnitMsg(eMessage type, long _units[], long _unitCount) : tMessage(type),
        unitCount(_unitCount)
    {
        units=mwNew long[unitCount];
        memcpy(units,_units,unitCount*sizeof(long));
    }
    tUnitMsg& operator=(const tUnitMsg& x)
    {
        if(units)
            mwDelete units;

        memcpy(this,&x,sizeof(*this));

        units=mwNew long[unitCount];
        memcpy(units,x.units,unitCount*sizeof(long));
        return *this;
    }
    tUnitMsg(const tUnitMsg& x) : units(NULL), unitCount(0) //Copy constructor
    {
        operator=(x);
    }
    ~tUnitMsg()
    {
        if(units)
            mwDelete units;
    }
};

class tUnitVector2DMsg : public tMessage
{
    public:
    long *units;
    long unitCount;
    tVector2D v;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType;
        if(units)
        {
			cType=(char)type;
			WRITE_TYPE(char, cType)
            WRITE_TYPE(short, playerID)
            WRITE_TYPE(long, unitCount)
            WRITE_MEMORY(units,unitCount*sizeof(long))
            WRITE_TYPE(tVector2D, v);
        }
        else
            bug("tUnitVector2DMsg::packageMessage: NULL unit pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitCount)
        units=mwNew long[unitCount];
        READ_MEMORY(units,unitCount*sizeof(long))
        READ_TYPE(tVector2D, v)
        return (int) (ptr - buffer);
    }
    tUnitVector2DMsg() : tMessage(), units(NULL), unitCount(0) {}
    tUnitVector2DMsg(eMessage type, long _units[], long _unitCount, tVector2D _v) :
        tMessage(type), unitCount(_unitCount), v(_v)
    {
        units=mwNew long[unitCount];
        memcpy(units,_units,unitCount*sizeof(long));
    }
    tUnitVector2DMsg& operator=(const tUnitVector2DMsg& x)
    {
        if(units)
            mwDelete units;

        memcpy(this,&x,sizeof(*this));

        units=mwNew long[unitCount];
        memcpy(units,x.units,unitCount*sizeof(long));
        return *this;
    }
    tUnitVector2DMsg(const tUnitVector2DMsg& x) : units(NULL), unitCount(0) //Copy constructor
    {
        operator=(x);
    }
    ~tUnitVector2DMsg()
    {
        if(units)
            mwDelete units;
    }
};

class tUnitLongMsg : public tMessage
{
    public:
    long *units;
    long unitCount;
    long val;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType;
        if(units)
        {
			cType=(char)type;
			WRITE_TYPE(char, cType)            
            WRITE_TYPE(short, playerID)
            WRITE_TYPE(long, unitCount)
            WRITE_MEMORY(units,unitCount*sizeof(long))
            WRITE_TYPE(long, val)
        }
        else
            bug("tUnitLongMsg::packageMessage: NULL unit pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitCount)
        units=mwNew long[unitCount];
        READ_MEMORY(units,unitCount*sizeof(long))
        READ_TYPE(long, val)
        return (int) (ptr - buffer);
    }
    tUnitLongMsg() : tMessage(), units(NULL), unitCount(0) {}
    tUnitLongMsg(eMessage type, long _units[], long _unitCount, long _val) :
        tMessage(type), unitCount(_unitCount), val(_val)
    {
        units=mwNew long[unitCount];
        memcpy(units,_units,unitCount*sizeof(long));
    }
    tUnitLongMsg& operator=(const tUnitLongMsg& x)
    {
        if(units)
            mwDelete units;

        memcpy(this,&x,sizeof(*this));

        units=mwNew long[unitCount];
        memcpy(units,x.units,unitCount*sizeof(long));
        return *this;
    }
    tUnitLongMsg(const tUnitLongMsg& x) : units(NULL), unitCount(0) //Copy constructor
    {
        operator=(x);
    }
    ~tUnitLongMsg()
    {
        if(units)
            mwDelete units;
    }
};

class tUnitFloatMsg : public tMessage
{
    public:
    long *units;
    long unitCount;
    float val;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType;
        if(units)
        {
			cType=(char)type;
			WRITE_TYPE(char, cType)            
            WRITE_TYPE(short, playerID)
            WRITE_TYPE(long, unitCount)
            WRITE_MEMORY(units,unitCount*sizeof(long))
            WRITE_TYPE(float, val)
        }
        else
            bug("tUnitFloatMsg::packageMessage: NULL unit pointer");
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, unitCount)
        units=mwNew long[unitCount];
        READ_MEMORY(units,unitCount*sizeof(long))
        READ_TYPE(float, val)
        return (int) (ptr - buffer);
    }
    tUnitFloatMsg() : tMessage(), units(NULL), unitCount(0) {}
    tUnitFloatMsg(eMessage type, long _units[], long _unitCount, float _val) :
        tMessage(type), unitCount(_unitCount), val(_val)
    {
        units=mwNew long[unitCount];
        memcpy(units,_units,unitCount*sizeof(long));
    }
    tUnitFloatMsg& operator=(const tUnitFloatMsg& x)
    {
        if(units)
            mwDelete units;

        memcpy(this,&x,sizeof(*this));

        units=mwNew long[unitCount];
        memcpy(units,x.units,unitCount*sizeof(long));
        return *this;
    }
    tUnitFloatMsg(const tUnitFloatMsg& x) : units(NULL), unitCount(0) //Copy constructor
    {
        operator=(x);
    }
    ~tUnitFloatMsg()
    {
        if(units)
            mwDelete units;
    }
};

class tDepositCreateMsg : public tMessage
{
    public:
    short resourceID;
    tVector2D center;
    float radius;
    float amount;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(short, resourceID)
        WRITE_TYPE(tVector2D, center)
        WRITE_TYPE(float, radius)
        WRITE_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(short, resourceID)
        READ_TYPE(tVector2D, center)
        READ_TYPE(float, radius)
        READ_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    tDepositCreateMsg() : tMessage(MSG_DEPOSIT_CREATE) {}
    tDepositCreateMsg(short _resourceID, tVector2D _center, float _radius,
        float _amount) : tMessage(MSG_DEPOSIT_CREATE), resourceID(_resourceID),
        center(_center), radius(_radius), amount(_amount) {}
};

class tDepositDestroyMsg : public tMessage
{
    public:
    long depositID;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(long, depositID)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, depositID)
        return (int) (ptr - buffer);
    }
    tDepositDestroyMsg() : tMessage(MSG_DEPOSIT_DESTROY) {}
    tDepositDestroyMsg(long _depositID) : tMessage(MSG_DEPOSIT_DESTROY),
        depositID(_depositID) {}
};

class tDepositSetMsg : public tMessage
{
    public:
    long depositID;
    float amount;
    int packageMessage(char *buffer, int length)
    {
        char *ptr = buffer;
		char cType=(char)type;
        WRITE_TYPE(char, cType)
        WRITE_TYPE(short, playerID)
        WRITE_TYPE(long, depositID)
        WRITE_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    int unpackageMessage(const char *buffer, int length)
    {
        const char *ptr = buffer;
		char cType;
        READ_TYPE(char, cType)
		type=(eMessage)cType;
        READ_TYPE(short, playerID)
        READ_TYPE(long, depositID)
        READ_TYPE(float, amount)
        return (int) (ptr - buffer);
    }
    tDepositSetMsg() : tMessage(MSG_DEPOSIT_SET) {}
    tDepositSetMsg(long _depositID, float _amount) : tMessage(MSG_DEPOSIT_SET),
        depositID(_depositID), amount(_amount) {}
};
/****************************************************************************/
#endif
