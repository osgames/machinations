/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef sockwrapH
#define sockwrapH
/**************************************************************************
                                  Defines
 **************************************************************************/
//Windows/Linux does not define the following constants.
#ifdef WINDOWS
    #define socklen_t int
    #define EWOULDBLOCK WSAEWOULDBLOCK
    #define ENOBUFS WSAENOBUFS
    #define EINPROGRESS WSAEINPROGRESS
#endif //WINDOWS

#ifndef INVALID_SOCKET
    //This is used instead of -1 since socket handles are unsigned.
    #define INVALID_SOCKET (~0)
#endif
#ifndef SOCKET_ERROR
    #define SOCKET_ERROR -1
#endif

/**************************************************************************
                             Type Definitions
 **************************************************************************/
typedef unsigned int tSocket;

/**************************************************************************
                             Global Functions
 **************************************************************************/
extern int  netGetError();
extern bool	netIsBufferFull();
extern bool netStartup();
extern bool netShutdown();
extern bool netNewSocket(bool useTCPIP, tSocket& s);
extern bool netGetMaxMsgSize(tSocket& s, unsigned int& maxMsgSize);
extern bool netGetSendBufSize(tSocket& s, int& bufSize);
extern bool netGetRecvBufSize(tSocket& s, int& bufSize);
extern bool netSetReuseAddr(tSocket& s);
extern bool netSetNonBlocking(tSocket& s);
extern bool netBindSocket(tSocket& s, long& IP, int& port);
extern bool netListen(tSocket& s);
extern bool netConnect(tSocket& s, long IP, int port);
extern bool netSend(tSocket s, const char *buf, int& len);
extern bool netSendTo(tSocket s, long IP, int port, const char *buf, int& len);
extern bool netRecv(tSocket s, char *buf, int& len);
extern bool netRecvFrom(tSocket s, long& IP, int& port, char *buf, int& len, bool peek);
extern bool netAccept(tSocket s, long& IP, int& port, tSocket& newSocket);
extern bool netGetAddress(tSocket s, long& IP, int& port, const char*& peerName);
extern bool netCloseSocket(tSocket& s);
extern bool netDestroySocket(tSocket& s);

extern void netClearSets();
extern void netIncludeSocket(tSocket s);
extern bool netSelect();
extern bool netCanRead(tSocket s);
extern bool netCanWrite(tSocket s);
extern bool netHasError(tSocket s);

extern const char** netGetLocalIPs();
extern const char*  netGetLocalName();
/****************************************************************************/
#endif
