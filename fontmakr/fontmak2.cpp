//---------------------------------------------------------------------------
#include <vcl.h>
#include <vector.h>
#pragma hdrstop

#include "fontmak1.h"
#include "fontmak2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TAddFont *AddFont;
//---------------------------------------------------------------------------
int __stdcall EnumFontsProc(TLogFontA &logFont,
                            TTextMetricA &textMetric,
                            int fontType,
                            long data)
{
    TAddFont *addFont=(TAddFont*)data;
    if(fontType==TRUETYPE_FONTTYPE)
        addFont->lbTrueTypeFonts->Items->Add((AnsiString)logFont.lfFaceName);
    return 1;
}
//---------------------------------------------------------------------------
__fastcall TAddFont::TAddFont(const AnsiString& caption, tFont *_font, TComponent* Owner)
    : TForm(Owner), font(_font)
{
    hWnd = GetActiveWindow();
    hDC = GetDC(hWnd);
    EnumFonts(hDC, NULL, (FONTENUMPROC) EnumFontsProc, (long)this);

    Caption=caption;
    edName->Text=font->name;
    lbTrueTypeFonts->ItemIndex=lbTrueTypeFonts->Items->IndexOf(font->trueTypeFont);
    if(font->fontSize>0)
        edFontSize->Text=IntToStr((int)font->fontSize);

    cbBold->Checked=font->bold;
    cbItalic->Checked=font->italic;
    cbUnderline->Checked=font->underline;
    cbStrikeout->Checked=font->strikeout;
    edCharsToConvert->Text=font->charsToConvert;
    cbAntialias->Checked=font->antialias;
    cbCaseInsensitive->Checked=font->caseInsensitive;

    lbTrueTypeFontsClick(NULL);
    edFontSizeChange(NULL);
    cbStyleClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::cbStyleClick(TObject *Sender)
{
    TFontStyle style;
    TCheckBox *checkBox=(TCheckBox*)Sender;

    if (Sender == cbBold)
        style = fsBold;
    else if (Sender == cbItalic)
        style = fsItalic;
    else if (Sender == cbUnderline)
        style = fsUnderline;
    else if (Sender == cbStrikeout)
        style = fsStrikeOut;
    else
        return;

    TFontStyles styles = meTest->Font->Style;
    if(checkBox->Checked)
        styles << style;
    else
        styles >> style;
    meTest->Font->Style = styles;
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::edFontSizeChange(TObject *Sender)
{
    int fontSize = atoi(edFontSize->Text.c_str());
    if(fontSize < 4 || fontSize > 1000)
        return;
    meTest->Font->Size = fontSize;
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::lbTrueTypeFontsClick(TObject *Sender)
{
    if(lbTrueTypeFonts->ItemIndex>=0)
        meTest->Font->Name = lbTrueTypeFonts->Items->Strings[lbTrueTypeFonts->ItemIndex];
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::FormDestroy(TObject *Sender)
{
    ReleaseDC(hWnd,hDC);
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::btOKClick(TObject *Sender)
{
    HFONT fontHandle;
    bool charsToConvert[256]={false};
    int val,start=-1,i;
    char *ptr;
    MAT2 matrix = {{0,1},{0,0},{0,0},{0,1}};
    char temp[10000],buf[1000];
    int x,y,w,h,w2;

    if(edName->Text.Length()==0)
    {
        Application->MessageBox("Please enter a name for this font.",
            "Error", MB_OK|MB_ICONERROR);
        ActiveControl=edName;
        return;
    }

    if(lbTrueTypeFonts->ItemIndex == -1)
    {
        Application->MessageBox("Please select a font from the list.",
            "Error", MB_OK|MB_ICONERROR);
        ActiveControl=lbTrueTypeFonts;
        return;
    }
    AnsiString trueTypeFont=lbTrueTypeFonts->Items->Strings[lbTrueTypeFonts->ItemIndex].c_str();

    int fontSize = atoi(edFontSize->Text.c_str());
    if(fontSize < 4 || fontSize > 1000)
    {
        Application->MessageBox("Please enter a font size between 4 and 1,000.",
            "Error", MB_OK|MB_ICONERROR);
        ActiveControl=edFontSize;
        return;
    }

    bool bold=cbBold->Checked;
    bool italic=cbItalic->Checked;
    bool underline=cbUnderline->Checked;
    bool strikeout=cbStrikeout->Checked;

    strncpy(buf,edCharsToConvert->Text.c_str(),sizeof(buf));
    ptr=buf;
    
    try
    {
        while(true)
        {
            if(isdigit(*ptr))
            {
                val=0;
                while(isdigit(*ptr))
                    val=val*10+(*(ptr++)-'0');
                if(val>=256)
                    throw "Out of range";
            }
            else
                throw "Value expected";
            if(*ptr=='-')
                start=val;
            else if(*ptr==',' || *ptr=='\0')
            {
                if(start<0)
                    start=val;
                for(i=start;i<=val;i++)
                    charsToConvert[i]=true;
                start=-1;
                if(*ptr=='\0')
                    break;
            }
            else
                throw "Hyphen or comma expected";
            ptr++;
        }
    }
    catch(char *p)
    {
        Application->MessageBox(p, "Invalid range of characters.", MB_OK|MB_ICONERROR);
        ActiveControl=edCharsToConvert;
        return;
    }

    fontHandle = CreateFont(-MulDiv(fontSize, GetDeviceCaps(hDC, LOGPIXELSY), 72),
        0, 0, 0,
        bold ? FW_BOLD : FW_NORMAL,
        italic, underline, strikeout,
        ANSI_CHARSET,
        OUT_TT_PRECIS,
        CLIP_DEFAULT_PRECIS,
        DEFAULT_QUALITY,
        DEFAULT_PITCH,
        trueTypeFont.c_str());
    SelectObject(hDC,fontHandle);

    font->name=edName->Text;
    font->trueTypeFont=trueTypeFont;
    font->fontSize=fontSize;
    font->bold=bold;
    font->italic=italic;
    font->underline=underline;
    font->strikeout=strikeout;
    font->charsToConvert=edCharsToConvert->Text;
    font->antialias=cbAntialias->Checked;
    font->caseInsensitive=cbCaseInsensitive->Checked;

    for(i=0;i<256;i++)
    {
        tCharacter& c=font->characters[i];
        c.glyphMetrics=zeroMetrics;
        if(c.data)
        {
            delete c.data;
            c.data=NULL;
        }

        if(!charsToConvert[i])
            continue;

        if(cbAntialias->Checked)
            GetGlyphOutline(hDC, i, GGO_GRAY8_BITMAP, &c.glyphMetrics, sizeof(temp), temp, &matrix);
        else
            GetGlyphOutline(hDC, i, GGO_BITMAP, &c.glyphMetrics, sizeof(temp), temp, &matrix);

        w = ALIGN(c.glyphMetrics.gmBlackBoxX, 4);
        h = c.glyphMetrics.gmBlackBoxY;
        if(w>0 && h>0)
        {
            c.data=new char[w*h];

            if(cbAntialias->Checked)
            {
                for(y=0;y<h;y++)
                    for(x=0;x<w;x++)
                    {
                        int val=(int)temp[(h-y-1)*w+x]*4;
                        if(val>255)
                            val=255;
                        c.data[y*w+x] = (char)val;
                    }
            }
            else
            {
                w2 = ALIGN(c.glyphMetrics.gmBlackBoxX, 32)/8;
                for(y=0;y<c.glyphMetrics.gmBlackBoxY;y++)
                    for(x=0;x<c.glyphMetrics.gmBlackBoxX;x++)
                        c.data[(h-y-1)*w+x]=((temp[y*w2+x/8]>> (7-(x%8)) )&1) * 255;
            }
        }
    }
    DeleteObject(fontHandle);

    ModalResult=mrOk;
}
//---------------------------------------------------------------------------
void __fastcall TAddFont::FormKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_ESCAPE)
        Close();
}
//---------------------------------------------------------------------------

