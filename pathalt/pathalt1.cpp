/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glut.h>
/****************************************************************************/
#include "timer.h"
#include "vector.h"
#include "types.h"

#include "pathalt1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
TMain *Main;
struct tNodeGTE : public binary_function<tNode, tNode, bool>
{
    bool operator() (const tNode& a, const tNode& b) const
    {
        return a.value >= b.value;
    }
};
//---------------------------------------------------------------------------
__fastcall TMain::TMain(TComponent* Owner)
    : TForm(Owner)
{
    startVertex=NULL;
    endVertex=NULL;
}
//---------------------------------------------------------------------------
void TMain::createObstacles()
{
    int count=atoi(edCount->Text.c_str());
    if(count<=0 || count>1000)
    {
        Application->MessageBox("Number of obstacles must be between 1 and 1000", "Error", MB_OK | MB_ICONEXCLAMATION);
        return;
    }
    int size=atoi(edSize->Text.c_str());
    if(size<1 || size>100)
    {
        Application->MessageBox("Average size must be between 1 and 100", "Error", MB_OK | MB_ICONEXCLAMATION);
        return;
    }
    int us=atoi(edUnitSize->Text.c_str());
    if(us<0 || us>100)
    {
        Application->MessageBox("Unit size must be between 0 and 100", "Error", MB_OK | MB_ICONEXCLAMATION);
        return;
    }
    int x, y, w, h;
    float angle;
    cleanup();
    time_t t;
    srand((unsigned) time(&t));

    char buf[100];
    updateTime();
    tTime start = currentTime;
    for(int i=0; i<count; i++)
    {
        x = rand()%400;
        y = rand()%400;
        w = rand()%(size*2)+1;
        h = rand()%(size*2)+1;
        angle = (rand()%1000)/1000.0*M_PI_2;

        //These points must be stored in counter-clockwise order
        tPoint points[4];
        points[0].x=x;
        points[0].y=y;
        points[1].x=x+cos(angle)*(w+us+us);
        points[1].y=y+sin(angle)*(w+us+us);
        points[2].x=x+cos(angle)*(w+us+us)+cos(angle+M_PI_2)*(h+us+us);
        points[2].y=y+sin(angle)*(w+us+us)+sin(angle+M_PI_2)*(h+us+us);
        points[3].x=x+cos(angle+M_PI_2)*(h+us+us);
        points[3].y=y+sin(angle+M_PI_2)*(h+us+us);

        tRectangle *r=new tRectangle(points, angle);
        r->visPoints[0].x=x+cos(angle)*us+cos(angle+M_PI_2)*us;
        r->visPoints[0].y=y+sin(angle)*us+sin(angle+M_PI_2)*us;
        r->visPoints[1].x=x+cos(angle)*(w+us)+cos(angle+M_PI_2)*us;
        r->visPoints[1].y=y+sin(angle)*(w+us)+sin(angle+M_PI_2)*us;
        r->visPoints[2].x=x+cos(angle)*(w+us)+cos(angle+M_PI_2)*(h+us);
        r->visPoints[2].y=y+sin(angle)*(w+us)+sin(angle+M_PI_2)*(h+us);
        r->visPoints[3].x=x+cos(angle)*us+cos(angle+M_PI_2)*(h+us);
        r->visPoints[3].y=y+sin(angle)*us+sin(angle+M_PI_2)*(h+us);

        addRectangle(r);
    }
    calcVertices();
    sprintf(buf, "%d", vertices.size());
    laVertices->Caption=buf;
    sprintf(buf, "%d", edges.size());
    laEdges->Caption=buf;
    updateTime();
    sprintf(buf, "%6.3f", currentTime-start);
    laSetupTime->Caption=buf;
}
//---------------------------------------------------------------------------
void TMain::drawObstacles()
{
    imWorld->Canvas->Brush->Color = clWhite;
    imWorld->Canvas->FillRect(imWorld->Canvas->ClipRect);

    imWorld->Canvas->Brush->Color=clLime;
    for(int x=0;x<HASH_SIZE;x++)
        for(int y=0;y<HASH_SIZE;y++)
            if(rectHash[x][y][0])
                imWorld->Canvas->FrameRect(TRect(invHashFunc(x),invHashFunc(y),invHashFunc(x+1),invHashFunc(y+1)));

    imWorld->Canvas->Brush->Color = clGray;
    imWorld->Canvas->Pen->Color = clGray;
    for(int i=0; i<rectangles.size(); i++)
    {
        tPoint points[4];
        for(int j=0; j<4; j++)
        {
            points[j].x=rectangles[i]->visPoints[j].x;
            points[j].y=rectangles[i]->visPoints[j].y;
        }
        imWorld->Canvas->Polygon(points, 3);
    }
    imWorld->Canvas->Brush->Color = clRed;
    imWorld->Canvas->Pen->Color = clBlack;
    imWorld->Canvas->Ellipse(origin.x-5,origin.y-5,origin.x+5,origin.y+5);

    imWorld->Canvas->Brush->Color = clBlue;
    imWorld->Canvas->Pen->Color = clBlack;
    imWorld->Canvas->Ellipse(destination.x-5,destination.y-5,destination.x+5,destination.y+5);
}
//---------------------------------------------------------------------------
void TMain::drawEdges()
{
    imWorld->Canvas->Pen->Color=clGray;
    for(int i=0;i<edges.size();i++)
    {
        imWorld->Canvas->MoveTo(edges[i]->start->point.x,edges[i]->start->point.y);
        imWorld->Canvas->LineTo(edges[i]->end->point.x,edges[i]->end->point.y);
    }
}
//---------------------------------------------------------------------------
void TMain::drawVertices()
{
    imWorld->Canvas->Pen->Color=clBlack;
    imWorld->Canvas->Brush->Color=clBlue;
    for(int i=0;i<vertices.size();i++)
        imWorld->Canvas->Ellipse(vertices[i]->point.x-3,vertices[i]->point.y-3,
            vertices[i]->point.x+3,vertices[i]->point.y+3);
}
//---------------------------------------------------------------------------
void __fastcall TMain::btcreateObstaclesClick(TObject *Sender)
{
    createObstacles();
    drawObstacles();
    if(cbEdges->Checked)
        drawEdges();
    if(cbVertices->Checked)
        drawVertices();
}
//---------------------------------------------------------------------------
void __fastcall TMain::btExitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void TMain::addHash(int x, int y, tRectangle *r)
{
    int i;
    for(i=0;i<HASH_DEPTH-1;i++)
        if(rectHash[x][y][i]==NULL)
            break;
    if(i<HASH_DEPTH-1)
        rectHash[x][y][i]=r;
    else
        bug("TMain::addHash: hash overflow");
}
//---------------------------------------------------------------------------
void TMain::addRectangle(tRectangle *r)
{
    int i,x,vb,vt;
    int curb,curt,newb,newt;
    float fx;
    tLine *lb,*lt;
/*
    for(i=0;i<4;i++)
    {
        x=hashFunc(r->points[i].x);
        y=hashFunc(r->points[i].y);
        addHash(x,y,r);
    }
*/
    int x0=hashFunc(r->x0);
    int x1=hashFunc(r->x1);
    int y0=hashFunc(r->y0);
    int y1=hashFunc(r->y1);
    if(x0==x1)
    {
        if(y0>y1)
            SWAP(int,y0,y1)
        for(i=y0;i<=y1;i++)
            addHash(x0,i,r);
    }
    else if(y0==y1)
    {
        if(x0>x1)
            SWAP(int,x0,x1)
        for(i=x0;i<=x1;i++)
            addHash(i,y0,r);
    }
    else
    {
        vb=0;
        for(i=1;i<4;i++)
            if(r->points[i].x<r->points[vb].x)
                vb=i;
        vt=vb;
        curb=curt=hashFunc(r->points[vb].y);
        for(x=x0;x<x1;x++)
        {
            fx=invHashFunc(x+1);
            while(fx>r->points[(vb+1)&3].x)
            {
                vb=(vb+1)&3;
                newb=hashFunc(r->points[vb].y);
                if(newb<curb)
                    curb=newb;
            }
            while(fx>r->points[(vt+3)&3].x)
            {
                vt=(vt+3)&3;
                newt=hashFunc(r->points[vt].y);
                if(newt>curt)
                    curt=newt;
            }
            lb=&r->lines[vb];
            lt=&r->lines[(vt+3)&3];
            newb=hashFunc((lb->c-lb->a*fx)/lb->b);
            if(newb<curb)
                curb=newb;
            newt=hashFunc((lt->c-lt->a*fx)/lt->b);
            if(newt>curt)
                curt=newt;
            for(i=curb;i<=curt;i++)
                addHash(x,i,r);
            curb=newb;
            curt=newt;
        }
        vb=(vb+1)&3;
        while(vb!=vt)
        {
            newt=newb=hashFunc(r->points[vb].y);
            if(newb<curb)
                curb=newb;
            if(newt>curt)
                curt=newt;
            vb=(vb+1)&3;
        }
        for(i=curb;i<=curt;i++)
            addHash(x1,i,r);
    }
    rectangles.push_back(r);
}
//---------------------------------------------------------------------------
void TMain::calcVertices()
{
    int i,j,k;
    tRectangle *r;
    int x,y;
    tVertex *verts[4],*a,*b,*v;
    tEdge *e;
    for(i=0;i<rectangles.size();i++)
    {
        r=rectangles[i];
        for(k=0;k<4;k++)
        {
            tPoint p=r->points[k];
            x=hashFunc(p.x);
            y=hashFunc(p.y);
            for(j=0;rectHash[x][y][j]!=NULL;j++)
                if(rectHash[x][y][j]!=r && rectHash[x][y][j]->contains(p))
                    break;
            if(rectHash[x][y][j]==NULL)
            {
                tVertex _v={{p.x,p.y}, r, &r->lines[k], &r->lines[(k+3)&3],
                    NULL, -1, 0};
                v=new tVertex;
                *v=_v;
                addVertex(v);
                verts[k]=v;
            }
            else
                verts[k]=NULL;
        }
        for(k=0;k<4;k++)
        {
            a=verts[k];
            b=verts[(k+1)&3];
            if(a==NULL||b==NULL)
                continue;
            tLine l(a->point,b->point);
            if(!isLineBlocked(l,r,r))
            {
                tEdge _e={a, false, b, true, DISTANCE(a->point.x,a->point.y,
                    b->point.x,b->point.y)};
                e=new tEdge;
                *e=_e;
                edges.push_back(e);
            }
        }
    }
}
//---------------------------------------------------------------------------
void TMain::removeRectangle(tRectangle *r)
{
    int i;
    for(i=0; i<vertices.size(); )
        if(vertices[i]->rectangle==r)
        {
            delete vertices[i];
            vertices.erase(vertices.begin()+i);
        }
        else
            i++;
    for(i=0; i<rectangles.size(); i++)
        if(rectangles[i]==r)
        {
            rectangles.erase(rectangles.begin()+i);
            break;
        }
    delete r;
}
//---------------------------------------------------------------------------
#define CHECKCELL(x,y)  \
    for(q=0;rectHash[x][y][q]!=NULL;q++)    \
        if(rectHash[x][y][q]!=r1 && \
            rectHash[x][y][q]!=r2 &&  \
            rectHash[x][y][q]->intersects(l))   \
                return true;

bool TMain::isLineBlocked(const tLine& l, tRectangle *r1, tRectangle *r2)
{
    int x0,x1,y0,y1;
    int j,k,y,q,cur;
    x0=hashFunc(l.e1.x);
    y0=hashFunc(l.e1.y);
    x1=hashFunc(l.e2.x);
    y1=hashFunc(l.e2.y);
    if(x0==x1)
    {
        if(y0>y1)
            SWAP(int,y0,y1)
        for(j=y0;j<=y1;j++)
            CHECKCELL(x0,j)
    }
    else if(y0==y1)
    {
        if(x0>x1)
            SWAP(int,x0,x1)
        for(j=x0;j<=x1;j++)
            CHECKCELL(j,y0)
    }
    else
    {
        if(x0>x1)
        {
            SWAP(int,x0,x1)
            SWAP(int,y0,y1)
        }
        cur=y0;
        if(y0<y1)
        {
            for(j=x0;j<x1;j++)
            {
                y=hashFunc((l.c-l.a*invHashFunc(j+1))/l.b);
                for(k=cur;k<=y;k++)
                    CHECKCELL(j,k)
                cur=y;
            }
            for(k=cur;k<=y1;k++)
                CHECKCELL(x1,k)
        }
        else
        {
            for(j=x0;j<x1;j++)
            {
                y=hashFunc((l.c-l.a*invHashFunc(j+1))/l.b);
                for(k=cur;k>=y;k--)
                    CHECKCELL(j,k)
                cur=y;
            }
            for(k=cur;k>=y1;k--)
                CHECKCELL(x1,k)
        }
    }
    return false;
}
//---------------------------------------------------------------------------
void TMain::addVertex(tVertex *v)
{
    int i;
    bool c0,c1,startCCW,endCCW;
    tEdge *e;
    for(i=0;i<vertices.size();i++)
    {
        if(v->rectangle!=NULL && vertices[i]->rectangle==v->rectangle)
            continue;
        tLine l(vertices[i]->point, v->point);
        if(vertices[i]->rectangle != v->rectangle)
        {
            if(vertices[i]->rectangle!=NULL)
            {
                c0=vertices[i]->l0->regionContains(v->point);
                c1=vertices[i]->l1->regionContains(v->point);
                if(c0&&!c1)
                    startCCW=true;
                else if(!c0&&c1)
                    startCCW=false;
                else
                    continue;
            }
            else
                startCCW=false;
            if(v->rectangle!=NULL)
            {
                c0=v->l0->regionContains(vertices[i]->point);
                c1=v->l1->regionContains(vertices[i]->point);
                if(c0&&!c1)
                    endCCW=true;
                else if(!c0&&c1)
                    endCCW=false;
                else
                    continue;
            }
            else
                endCCW=false;
        }
        if(!isLineBlocked(l,vertices[i]->rectangle,v->rectangle))
        {
            tEdge _e={vertices[i], startCCW, v, endCCW, DISTANCE(vertices[i]->point.x,vertices[i]->point.y,
                v->point.x,v->point.y)};
            e=new tEdge;
            *e=_e;
            edges.push_back(e);
        }
    }
    vertices.push_back(v);
}
//---------------------------------------------------------------------------
void TMain::removeVertex(tVertex *v)
{
    int i;
    for(i=0; i<edges.size(); )
        if(edges[i]->start==v || edges[i]->end==v)
        {
            delete edges[i];
            edges.erase(edges.begin()+i);
        }
        else
            i++;
    for(i=0; i<vertices.size(); i++)
        if(vertices[i]==v)
        {
            vertices.erase(vertices.begin()+i);
            break;
        }
    delete v;
}
//---------------------------------------------------------------------------
bool TMain::calcPath()
{
    float newdistance;
    tVertex *destVertex;
    int i;
    bool CCW;
    for(i=0;i<vertices.size();i++)
    {
        vertices[i]->distance=-1;
        vertices[i]->previous=NULL;
        vertices[i]->destDistance=DISTANCE(vertices[i]->point.x,vertices[i]->point.y,destination.x,destination.y);
    }
    startVertex->distance=0;
    nodes=0;
    vector<tNode> heap;
    tNode n={startVertex, startVertex->destDistance, true};
    heap.push_back(n);
    push_heap(heap.begin(), heap.end(), tNodeGTE());
    nodes++;
    while(heap.size()>0)
    {
        pop_heap(heap.begin(), heap.end(), tNodeGTE());
        n = heap.back();
        heap.pop_back();
        if(n.vertex==endVertex)
            break;
        for(i=0;i<edges.size();i++)
        {
            if(edges[i]->start==n.vertex && n.CCW != edges[i]->startCCW)
            {
                CCW=edges[i]->endCCW;
                destVertex=edges[i]->end;
            }
            else if(edges[i]->end==n.vertex && n.CCW != edges[i]->endCCW)
            {
                CCW=edges[i]->startCCW;
                destVertex=edges[i]->start;
            }
            else
                continue;
            newdistance = n.vertex->distance+edges[i]->length;
            if(destVertex->distance<0 || newdistance<destVertex->distance)
            {
                destVertex->distance=newdistance;
                destVertex->previous=n.vertex;
                tNode newnode = {destVertex, newdistance+destVertex->destDistance,CCW};
                heap.push_back(newnode);
                push_heap(heap.begin(), heap.end(), tNodeGTE());
                nodes++;
            }
        }
    }
    if(heap.size()==0)
        return false;
    else
        return true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btFindPathClick(TObject *Sender)
{
    bool found;
    char buf[100];

    tVertex _v0={{origin.x, origin.y}, NULL, NULL, NULL,
        NULL, 0, DISTANCE(origin.x,origin.y,destination.x,destination.y)};
    startVertex=new tVertex;
    *startVertex=_v0;
    addVertex(startVertex);
    tVertex _v1={{destination.x,destination.y}, NULL, NULL, NULL,
        NULL, -1, 0};
    endVertex=new tVertex;
    *endVertex=_v1;
    addVertex(endVertex);

    drawObstacles();
    if(cbEdges->Checked)
        drawEdges();
    if(cbVertices->Checked)
        drawVertices();

    updateTime();
    tTime start = currentTime;
    //imWorld->Canvas->Pen->Color = clRed;
    //imWorld->Canvas->Brush->Color = clRed;
    found=calcPath();
    if(found)
        laPathFound->Caption="Path found";
    else
        laPathFound->Caption="Path not found";
    updateTime();
    sprintf(buf, "%6.3f", currentTime-start);
    laAStarTime->Caption=buf;

    sprintf(buf, "%d", vertices.size());
    laVertices->Caption=buf;
    sprintf(buf, "%d", edges.size());
    laEdges->Caption=buf;
    sprintf(buf, "%d", nodes);
    laNodes->Caption=buf;
    if(found)
    {
        imWorld->Canvas->Pen->Color = clRed;
        tVertex *vertex=endVertex;
        while(vertex->previous!=NULL)
        {
            imWorld->Canvas->MoveTo(vertex->point.x,vertex->point.y);
            vertex=vertex->previous;
            imWorld->Canvas->LineTo(vertex->point.x,vertex->point.y);
        }
    }

    removeVertex(startVertex);
    removeVertex(endVertex);
    startVertex=NULL;
    endVertex=NULL;
}
//---------------------------------------------------------------------------
void __fastcall TMain::imWorldMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    if(Button==mbLeft)
    {
        origin.x=X;
        origin.y=Y;
    }
    else
    {
        destination.x=X;
        destination.y=Y;
    }
    drawObstacles();
    if(cbEdges->Checked)
        drawEdges();
    if(cbVertices->Checked)
        drawVertices();
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormDestroy(TObject *Sender)
{
    cleanup();
}
//---------------------------------------------------------------------------
void TMain::cleanup()
{
    int i;
    for(i=0;i<rectangles.size();i++)
        delete rectangles[i];
    rectangles.clear();
    for(i=0;i<vertices.size();i++)
        delete vertices[i];
    vertices.clear();
    for(i=0;i<edges.size();i++)
        delete edges[i];
    edges.clear();
    memset(&rectHash[0][0][0], 0, sizeof(rectHash));
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbClick(TObject *Sender)
{
    drawObstacles();
    if(cbEdges->Checked)
        drawEdges();
    if(cbVertices->Checked)
        drawVertices();
}
//---------------------------------------------------------------------------

