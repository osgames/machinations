/****************************************************************************
* Machinations copyright (C) 2001, 2002 by Jon Sargeant and Jindra Kolman   *
*                                                                           *
* ASTAR.CPP:    Basic implementation of A* pathfinding algorithm            *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include <dir>
#include <io>
#include <math>
#include <stdarg>
#include <stdio>
#include <stdlib>
#include <string>
#include <time>
#include <float>
#include <function>
#include <vector.h>
#include <windows>
#include <winsock2>
#ifdef SGI
    #include <c:\oglsdk\include\gl\gl>		// Header File For The SGI OpenGL Libraay
    #include <c:\oglsdk\include\gl\glu>		// Header File For The SGI GLu Libraay
#else
    #include <gl\gl>		    // Header File For The OpenGL32 Libraay
    #include <gl\glu>		    // Header File For The GLu32 Libraay
#endif
#pragma hdrstop
//---------------------------------------------------------------------------
#include "types.h"
#include "astar.h"
#include "pathfnd1.h"
#pragma package(smart_init)
//---------------------------------------------------------------------------
// PATH FINDER
//---------------------------------------------------------------------------
char dirleni[] = {29, 41, 29, 41, 29, 41, 29, 41, 29, 0};
float dirlenf[] = {1, 1.4142, 1, 1.4142, 1, 1.4142, 1, 1.4142, 1, 0};

direnum oppdir[] = { DIR_LEFT, DIR_DOWNLEFT, DIR_DOWN, DIR_DOWNRIGHT, DIR_RIGHT, DIR_UPRIGHT, DIR_UP,
    DIR_UPLEFT, DIR_LEFT, DIR_NONE };
int indices[] = {0, 2, 4, 6, 1, 3, 5, 7};
int curvemod[] = {-1, 3, 1, 0, 1, 3, -1, -1, -1, 3, 1, 0, 1, 3, -1};
ttile tiles[MAPSIZE][MAPSIZE];
vector<tregion> regions;
//---------------------------------------------------------------------------
int inline tpathfinder::heuristic(ttilepos tile1, ttilepos tile2)
//The heuristic returns the smallest possible weighted distance between (x1,y1) and (x2,y2) if only 8 directions are allowed
//To eliminate floating-point calculating wherever possible, I multiply weight by 10 and distance by 29
//sqrt(2) * 29 is about 41.
{
    short dx = DIFF(tile1.x,tile2.x);
    short dy = DIFF(tile1.y,tile2.y);
    return MIN(dx,dy) * 410 + DIFF(dx,dy) * 290;
}
//---------------------------------------------------------------------------
int inline tpathfinder::heuristic(tareapos area1, tareapos area2)
{
    short dx = DIFF(area1.x,area2.x);
    short dy = DIFF(area1.y,area2.y);
    return MIN(dx,dy) * 410 + DIFF(dx,dy) * 290;
}
//---------------------------------------------------------------------------
float tpathfinder::weighteddistance(ttilepos tile1, ttilepos tile2)
//calculates the approximate weighted distance between (x1,y1) and (x2,y2)
//weightdistance samples the weight at regular intervals on the line, averages these weights,
//and estimates the weighted distance by multiplying this average by the unweighted distance
//since the line only passes through half of the end tiles, the weight of the end tiles is halved,
//and they are counted as one tile
{
    short dx = DIFF(tile1.x,tile2.x);
    short dy = DIFF(tile1.y,tile2.y);
    ttilepos newtile;
    int i;
    if(dx==0&&dy==0)
        return 0;
    float total=(tiles[tile1.x][tile1.y].weight+tiles[tile2.x][tile2.y].weight)/2;
    if(dx<dy) //line is nearly vertical (|slope| > 1)
    {
        if(tile1.y>tile2.y)
        {
            SWAP(short,tile1.x,tile2.x);
            SWAP(short,tile1.y,tile2.y);
        }
        for(i=tile1.y+1;i<tile2.y;i++)
        {
            newtile.x = (int) ((float) (i-tile1.y)/dy*(tile2.x-tile1.x)+tile1.x + .5);
            newtile.y = i;
            total += tiles[newtile.x][newtile.y].weight;
        }
        total /= dy;
    }
    else //line is nearly horizontal (|slope| <= 1)
    {
        if(tile1.x>tile2.x)
        {
            SWAP(short,tile1.x,tile2.x);
            SWAP(short,tile1.y,tile2.y);
        }
        for(i=tile1.x+1;i<tile2.x;i++)
        {
            newtile.x = i;
            newtile.y = (int) ((float) (i-tile1.x)/dx*(tile2.y-tile1.y)+tile1.y + .5);
            total += tiles[newtile.x][newtile.y].weight;
        }
        total /= dx;
    }
    return total * DISTANCE(tile1.x,tile1.y,tile2.x,tile2.y);
}
//---------------------------------------------------------------------------
int tpathfinder::shiftpoint(ttilepos tile1, ttilepos& tile2, int weight, bool counterclockwise)
{
    int dx = tile2.x - tile1.x;
    int dy = tile2.y - tile1.y;
    ttilepos newtile;
    for(int i=0; i<2; i++)
    {
        int icounterclockwise = counterclockwise ? 1 : -1;
        newtile=tile2;
        if(dx>=0 && dy>=0)
        {
            newtile.x-=icounterclockwise;
            newtile.y+=icounterclockwise;
        }
        else if(dx<0 && dy>=0)
        {
            newtile.x-=icounterclockwise;
            newtile.y-=icounterclockwise;
        }
        else if(dx<0 && dy<0)
        {
            newtile.x+=icounterclockwise;
            newtile.y-=icounterclockwise;
        }
        else if(dx>=0 && dy<0)
        {
            newtile.x+=icounterclockwise;
            newtile.y+=icounterclockwise;
        }
        makevalid(newtile);
        if(tiles[newtile.x][newtile.y].weight <= weight)
        {
            tile2=newtile;
            return 0;
        }

        newtile=tile2;
        if(dy > ABS(dx))
            newtile.x -= icounterclockwise;
        else if(-dy > ABS(dx))
            newtile.x += icounterclockwise;
        else if(dx > 0)
            newtile.y += icounterclockwise;
        else
            newtile.y -= icounterclockwise;
        makevalid(newtile);
        if(tiles[newtile.x][newtile.y].weight <= weight)
        {
            tile2=newtile;
            return 0;
        }
        counterclockwise=!counterclockwise;
    }
    bug("tpathfinder::shiftpoint: failed to shift waypoint to acceptable tile");
    return -1;
}
//---------------------------------------------------------------------------
bool tpathfinder::ispathclear(ttilepos start, ttilepos end, int weight, ttilepos& out)
{
    if(weight<0)
        weight=SHRT_MAX-1;
    int i, j, inc;
    ttilepos s = start, e = end, newtile;
    float angle, x, y;
    float m, b1, b2;
    int curtop, newtop, curbottom, newbottom, maxy, miny;
    if(s.x==e.x)
    {
        inc = e.y>s.y ? -1 : 1;
        for(i=e.y; inc*i<=inc*s.y; i+=inc)
        {
            newtile.x = s.x;
            newtile.y = i;
            if(tiles[newtile.x][newtile.y].weight>weight)
            {
                out.x = s.x;
                out.y = i + (s.y < e.y ? 1 : -1);
                return false;
            }
        }
    }
    else if(s.y==e.y)
    {
        inc = e.x>s.x ? -1 : 1;
        for(i=e.x; inc*i<=inc*s.x; i+=inc)
        {
            newtile.x=i;
            newtile.y=s.y;
            if(tiles[newtile.x][newtile.y].weight>weight)
            {
                out.x = i + (s.x < e.x ? 1 : -1);
                out.y = s.y;
                return false;
            }
        }
    }
    else
    {
        if(e.x < s.x)
        {
            SWAP(int, s.x, e.x)
            SWAP(int, s.y, e.y)
        }
        m = (float) (e.y-s.y)/(e.x-s.x);
        angle = pointtoangle(e.x-s.x, e.y-s.y);
        x = s.x + .5 + .2 * cos(angle+M_PI/2);
        y = s.y + .5 + .2 * sin(angle+M_PI/2);
        b1 = -m * x + y;
        x = s.x + .5 + .2 * cos(angle-M_PI/2);
        y = s.y + .5 + .2 * sin(angle-M_PI/2);
        b2 = -m * x + y;

        curtop = curbottom = s.y;
        miny = MIN(s.y, e.y);
        maxy = MAX(s.y, e.y);
        for(i = s.x; i <= e.x; i++)
        {
            newtop = (int) (m*(i+1) + b1);
            if(newtop > curtop)
                curtop = MIN(newtop, maxy);
            newbottom = (int) (m*(i+1) + b2);
            if(newbottom < curbottom)
                curbottom = MAX(newbottom, miny);
            for(j=curbottom; j<=curtop; j++)
            {
                newtile.x=i;
                newtile.y=j;
                if(tiles[newtile.x][newtile.y].weight>weight)
                {
                    out.x = i;
                    out.y = j;
                    return false;
                }
            }
            curtop = newtop;
            curbottom = newbottom;
        }
    }
    return true;
}
//---------------------------------------------------------------------------
inline void tpathfinder::addwaypoint(ttilepos tile)
{
    /*
    ttilepos dummy;
    if(!ispathclear(origtile1,tile,-1,dummy) || weighteddistance(origtile1,tile) > weighteddistance(origtile1,oldtile1)
        + weighteddistance(oldtile1,tile) + .001)
    {
        waypoints.insert(waypoints.begin(), oldtile1);
        origtile1 = oldtile1;
    }
    oldtile1 = tile;
    */
    waypoints.insert(waypoints.begin(), tile);
}
//---------------------------------------------------------------------------
void tpathfinder::highastar()
{
    //Initialize high-level A*
    int i, a, h, x, y, newdist;
    vector<unsigned long> distances;
    vector<short> prevregs;
    vector<direnum> prevdirs;
    tregionnode node, newnode;
    int sr=tiles[starttile.x][starttile.y].region;
    int er=tiles[endtile.x][endtile.y].region;
    short r, cr, nr;
    direnum dir;
    int bestreg=sr;
    int minheuristic=INT_MAX;
    vector<tregionnode> heap;

    distances.resize(regions.size());
    prevregs.resize(regions.size());
    prevdirs.resize(regions.size());
    memset(&distances[0], 0xff, distances.size()*sizeof(unsigned long));
    prevregs[sr]=-1;
    prevdirs[sr]=DIR_NONE;
    distances[sr]=0;
    tregionnode firstnode={heuristic(regions[sr].pos,regions[er].pos), sr};
    heap.push_back(firstnode);
    push_heap(heap.begin(), heap.end(), tregiongte());
#ifdef DEBUGASTAR
    highlevelnodes++;
#endif

    //Begin high-level A*
    while(heap.size()>0)
    {
        pop_heap(heap.begin(), heap.end(), tregiongte());
        node=heap.back();
        heap.pop_back();
        cr=node.region;
        if(cr == er)
            break;
        for(i=0;i<regions[cr].neighborcount;i++)
        {
            nr=regions[cr].neighbors[i];
            dir=regions[cr].neighbordirs[i];

            if(prevdirs[cr]!=DIR_NONE)
                a = curvemod[prevdirs[cr]-dir+7];
            if(a < 0)
                continue;

            newdist = distances[cr] + dirleni[dir] * regions[nr].weight + a;

            if(newdist < distances[nr])
            {
                distances[nr] = newdist;
                prevregs[nr] = cr;
                prevdirs[nr] = oppdir[dir];
                newnode.region = nr;
                h = heuristic(regions[nr].pos, regions[er].pos);
                if(h<minheuristic)
                {
                    minheuristic=h;
                    bestreg=nr;
                }
                newnode.value = newdist + h;
                heap.push_back(newnode);
                push_heap(heap.begin(), heap.end(), tregiongte());
#ifdef DEBUGASTAR
                highlevelnodes++;
#endif
            }
        }
    }
    if(heap.size()==0) //failed to find path
        er = bestreg;

    //Back-track
    openregions.resize(regions.size(),false);
    cr=er;
    openregions[cr]=true;
    //Main->imImage->Canvas->Brush->Color=clBlue;
    //Main->imImage->Canvas->FrameRect(TRect(regions[cr].pos.x*AREASIZE,regions[cr].pos.y*AREASIZE,
    //    (regions[cr].pos.x+1)*AREASIZE,(regions[cr].pos.y+1)*AREASIZE));
    while(true)
    {
        if(prevdirs[cr] == DIR_NONE)
            break;
        x=regions[cr].pos.x*AREASIZE;
        y=regions[cr].pos.y*AREASIZE;
        if(prevdirs[cr]==DIR_UPRIGHT)
        {
            r=tiles[x+AREASIZE-1][y+AREASIZE].region;
            if(r>=0)    openregions[r]=true;
            r=tiles[x+AREASIZE][y+AREASIZE-1].region;
            if(r>=0)    openregions[r]=true;
        }
        else if(prevdirs[cr]==DIR_UPLEFT)
        {
            r=tiles[x][y+AREASIZE].region;
            if(r>=0)    openregions[r]=true;
            r=tiles[x-1][y+AREASIZE-1].region;
            if(r>=0)    openregions[r]=true;
        }
        else if(prevdirs[cr]==DIR_DOWNRIGHT)
        {
            r=tiles[x+AREASIZE-1][y-1].region;
            if(r>=0)    openregions[r]=true;
            r=tiles[x+AREASIZE][y].region;
            if(r>=0)    openregions[r]=true;
        }
        else if(prevdirs[cr]==DIR_DOWNLEFT)
        {
            r=tiles[x][y-1].region;
            if(r>=0)    openregions[r]=true;
            r=tiles[x-1][y].region;
            if(r>=0)    openregions[r]=true;
        }

        cr=prevregs[cr];
        openregions[cr]=true;
        //Main->imImage->Canvas->FrameRect(TRect(regions[cr].pos.x*AREASIZE,regions[cr].pos.y*AREASIZE,
        //    (regions[cr].pos.x+1)*AREASIZE,(regions[cr].pos.y+1)*AREASIZE));
    }
}
//---------------------------------------------------------------------------
void tpathfinder::lowastar()
{
    //Initialize low-level A*
    int i, j, a=0, h, x, y;
    int newdist;
    bool blockeddirs[8];
    ttilenode node, newnode;
    ttilepos newtile;
    ttilepos besttile=starttile;
    int minheuristic=INT_MAX;
    //short=deltah;

    unsigned long distances[MAPSIZE][MAPSIZE];
    direnum prevs[MAPSIZE][MAPSIZE];
    vector<ttilenode> heap;
    memset(&distances[0][0], 0xff, MAPSIZE*MAPSIZE*sizeof(unsigned long));

    /*
    for(x=0; x<width; x++)
        for(y=0; y<height; y++)
            distances[x][y] = INT_MAX;
    */

    prevs[starttile.x][starttile.y] = DIR_NONE;
    distances[starttile.x][starttile.y] = 0;
    ttilenode firstnode = {heuristic(starttile,endtile), {starttile.x, starttile.y}};
    heap.push_back(firstnode);
    push_heap(heap.begin(), heap.end(), ttilegte());
#ifdef DEBUGASTAR
    lowlevelnodes++;
#endif

    //Begin low-level A*
    while(heap.size()>0)
    {
        pop_heap(heap.begin(), heap.end(), ttilegte());
        node=heap.back();
        heap.pop_back();
        if(node.tile.x == endtile.x && node.tile.y == endtile.y)
            break;
        for(j=0;j<8;j++)
        {
            i = indices[j];
            blockeddirs[i] = false;
            if((i&1) && (blockeddirs[i-1] || blockeddirs[(i+1)&7]))
                continue;

            if(prevs[node.tile.x][node.tile.y]!=DIR_NONE)
                a = curvemod[prevs[node.tile.x][node.tile.y]-i+7];
            if(a < 0)
                continue;

            newtile.x = node.tile.x+dirx[i];
            newtile.y = node.tile.y+diry[i];
            if(!isvalid(newtile) || tiles[newtile.x][newtile.y].region<0)
            {
                blockeddirs[i] = true;
                continue;
            }
            if(openregions[tiles[newtile.x][newtile.y].region]==false)
                continue;
            //deltah=tiles[newtile.x][newtile.y].elevation - tiles[node.tile.x][node.tile.y].elevation;
            newdist = distances[node.tile.x][node.tile.y] + dirleni[i] * tiles[newtile.x][newtile.y].weight + a;

            if(newdist < distances[newtile.x][newtile.y])
            {
                distances[newtile.x][newtile.y] = newdist;
                prevs[newtile.x][newtile.y] = oppdir[i];
                newnode.tile = newtile;
                h = heuristic(newtile, endtile);
                if(h<minheuristic)
                {
                    minheuristic=h;
                    besttile=newtile;
                }
                newnode.value = newdist + h;
                heap.push_back(newnode);
                push_heap(heap.begin(), heap.end(), ttilegte());
#ifdef DEBUGASTAR
                //Main->imZoom->Canvas->MoveTo(node.tile.x*8+4,node.tile.y*8+4);
                //Main->imZoom->Canvas->LineTo(newtile.x*8+4,newtile.y*8+4);

                //Main->imImage->Canvas->Pixels[newtile.x][newtile.y]=clGreen;
                lowlevelnodes++;
#endif
            }
        }
    }
    if(heap.size()==0) //failed to find path
        endtile = besttile;

    /*
    //initialize variables for adjustment
    curtile = oldtile = origtile = endtile;
    //initialize variables for addwaypoint
    oldtile1 = origtile1 = endtile;
    waypoints.push_back(endtile);
    curweight = tiles[origtile.x][origtile.y].weight; //this can't be placed in the constructor since virtual functions can't be overloaded in constructor
    */

    //Initialize back-tracking
    ttilepos desttile, curtile=endtile;
    direnum curdir=DIR_NONE, d;

    //Begin back-tracking
    while(true)
    {
        /*
        if(tiles[curtile.x][curtile.y].weight != curweight)
        {
            origtile = curtile;
            if(tiles[curtile.x][curtile.y].weight > curweight)
                addwaypoint(oldtile);
            else
                addwaypoint(curtile);
            curweight = tiles[curtile.x][curtile.y].weight;
        }
        else if(!ispathclear(origtile,curtile,curweight,desttile))
        {
            if( shiftpoint(origtile,desttile,curweight,
                anglecompare(oldtile.x-origtile.x,oldtile.y-origtile.y,curtile.x-origtile.x,curtile.y-origtile.y)<0) < 0)
            {
                bug("tpathfinder::tick: could not find suitable pivot point");
                finished = true;
                onpathnotfound();
                return; //FAIL
            }
            origtile = desttile;
            addwaypoint(desttile);
        }
        else
        {
        */
            if(prevs[curtile.x][curtile.y] == DIR_NONE)
            {
                finished = true;
                addwaypoint(curtile);
#ifdef DEBUGASTAR
                lowleveltime=ttime::current()-start;
#endif
                onpathfound();
                return;
            }
            //oldtile = curtile;
            //don't substitute this expression below since x and y change!!
            d = prevs[curtile.x][curtile.y];
            if(d!=curdir)
            {
                addwaypoint(curtile);
                curdir=d;
            }
            curtile.x+=dirx[d];
            curtile.y+=diry[d];
            if(!isvalid(curtile))
            {
                bug("tpathfinder::tick: invalid tile encountered when backtracking");
                finished = true;
#ifdef DEBUGASTAR
                lowleveltime=ttime::current()-start;
#endif
                onpathnotfound();
                return; //FAIL
            }
        //}
    }
}
//---------------------------------------------------------------------------
void tpathfinder::tick(ttime starttime, float duration)
{
#ifdef DEBUGASTAR
    start=ttime::current();
#endif
    highastar();
#ifdef DEBUGASTAR
    highleveltime=ttime::current()-start;
    start=ttime::current();
#endif
    lowastar();
}
//---------------------------------------------------------------------------
tpathfinder::tpathfinder(ttilepos _starttile, ttilepos _endtile, int _width, int _height) :
    starttile(_starttile), endtile(_endtile), width(_width), height(_height)
{
#ifdef DEBUGASTAR
    highlevelnodes=0;
    lowlevelnodes=0;
    highleveltime=0;
    lowleveltime=0;
#endif
    if(!isvalid(starttile) || !isvalid(endtile))
    {
        bug("tpathfinder::tpathfinder: invalid start tile or end tile");
        finished = true;
        onpathnotfound();
        return;
    }
}
//---------------------------------------------------------------------------
inline void tpathfinder::makevalid(ttilepos tile)
{
    if(tile.x<0)    tile.x=0;
    if(tile.y<0)    tile.y=0;
    if(tile.x>=width)   tile.x=width-1;
    if(tile.y>=height)  tile.y=height-1;
}
//---------------------------------------------------------------------------
static inline void regionsadjacent(short r1, short r2, direnum d)
{
    for(int i=0; i<regions[r1].neighborcount; i++)
        if(regions[r1].neighbors[i]==r2)
            return;
    regions[r1].neighbordirs[regions[r1].neighborcount]=d;
    regions[r1].neighbors[regions[r1].neighborcount++]=r2;
    regions[r2].neighbordirs[regions[r2].neighborcount]=oppdir[d];
    regions[r2].neighbors[regions[r2].neighborcount++]=r1;
}
//---------------------------------------------------------------------------
void calcregions()
{
    int i, j, k, s, x, y, ax, ay;
    //int tweight, televation, count;
    short minweight, minelevation;
    short curregion=0;
    bool found;
    tregion r={{0,0},0,0,{0}};
    //Clear old regions
    regions.clear();
    //Initialize all tiles to region -1 (No region will have an id < 0)
    //-1 means a tile is blocked
    //-2 means a tile belongs to a region, but hasn't yet saturated adjacent tiles
    //-3 means a tile isn't blocked but doesn't yet belong to a region
    for(y=0;y<MAPSIZE;y++)
        for(x=0;x<MAPSIZE;x++)
        {
            if(tiles[x][y].weight<0)
                tiles[x][y].region=-1;
            else
                tiles[x][y].region=-3;
        }
    s=DIVIDEUP(MAPSIZE,AREASIZE);

    for(i=0; i<s; i++)
    {
        ax=i*AREASIZE;
        for(j=0; j<s; j++)
        {
            ay=j*AREASIZE;
            while(true)
            {
                //Find first passable tile
                found=false;
                for(x=ax;x<MIN(ax+AREASIZE,MAPSIZE);x++)
                {
                    for(y=ay;y<MIN(ay+AREASIZE,MAPSIZE);y++)
                        if(tiles[x][y].region==-3)
                        {
                            found=true;
                            //Mark this tile with -2
                            tiles[x][y].region=-2;
                            break;
                        }
                    if(found)
                        break;
                }
                if(!found)
                    break;

                //Saturate tiles
                //tweight=0;
                //televation=0;
                minweight=SHRT_MAX;
                minelevation=SHRT_MAX;
                //count=0;
                do
                {
                    found=false;
                    for(x=ax;x<MIN(ax+AREASIZE,MAPSIZE);x++)
                        for(y=ay;y<MIN(ay+AREASIZE,MAPSIZE);y++)
                            if(tiles[x][y].region==-2)
                            {
                                found=true;
                                if(x>ax && tiles[x-1][y].region==-3)
                                    tiles[x-1][y].region=-2;
                                if(x+1<MIN(ax+AREASIZE,MAPSIZE) && tiles[x+1][y].region==-3)
                                    tiles[x+1][y].region=-2;
                                if(y>ay && tiles[x][y-1].region==-3)
                                    tiles[x][y-1].region=-2;
                                if(y+1<MIN(ay+AREASIZE,MAPSIZE) && tiles[x][y+1].region==-3)
                                    tiles[x][y+1].region=-2;
                                tiles[x][y].region=curregion;
                                //tweight+=tiles[x][y].weight;
                                //televation+=tiles[x][y].elevation;
                                //count++;
                                if(tiles[x][y].weight<minweight)
                                    minweight=tiles[x][y].weight;
                                if(tiles[x][y].elevation<minelevation)
                                    minelevation=tiles[x][y].elevation;
                            }
                }
                while(found);
                r.pos.x=i;
                r.pos.y=j;
                //r.weight=tweight/count; //Average weight and elevation of the region
                //r.elevation=televation/count; //integer division is fine
                r.weight=minweight;
                r.elevation=minelevation;
                regions.push_back(r);
                curregion++;
            }
        }
    }
    //Now, calculate neighbors:
    //Compare regions touching right and left edges of an area
    for(i=0; i<s-1; i++)
    {
        ax=(i+1)*AREASIZE;
        for(j=0; j<s; j++)
        {
            ay=j*AREASIZE;
            for(k=0;k<AREASIZE-1;k++)
                if(tiles[ax-1][ay+k].region>=0 && tiles[ax][ay+k].region>=0)
                    regionsadjacent(tiles[ax-1][ay+k].region,tiles[ax][ay+k].region, DIR_RIGHT);
        }
    }
    //Compare regions touching top and bottom edges of an area
    for(i=0; i<s; i++)
    {
        ax=i*AREASIZE;
        for(j=0; j<s-1; j++)
        {
            ay=(j+1)*AREASIZE;
            for(k=0;k<AREASIZE-1;k++)
                if(tiles[ax+k][ay-1].region>=0 && tiles[ax+k][ay].region>=0)
                    regionsadjacent(tiles[ax+k][ay-1].region,tiles[ax+k][ay].region, DIR_UP);
        }
    }
    //Compare regions touching corners of an area
    for(i=0; i<s-1; i++)
    {
        ax=(i+1)*AREASIZE;
        for(j=0; j<s-1; j++)
        {
            ay=(j+1)*AREASIZE;
            if(tiles[ax-1][ay-1].region>=0 && tiles[ax-1][ay].region>=0 &&
                tiles[ax][ay-1].region>=0 && tiles[ax][ay].region>=0)
            {
                regionsadjacent(tiles[ax-1][ay-1].region,tiles[ax][ay].region, DIR_UPRIGHT);
                regionsadjacent(tiles[ax][ay-1].region,tiles[ax-1][ay].region, DIR_UPLEFT);
            }
        }
    }
}
//---------------------------------------------------------------------------
