//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("pathfind.res");
USEFORM("pathfnd1.cpp", Main);
USEUNIT("astar.cpp");
USEUNIT("globals.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->CreateForm(__classid(TMain), &Main);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
