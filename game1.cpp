/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* GAME1.CPP     Contains program entry point and GLUT interface.            *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
#include <GL/glfw.h>
#include <AL/al.h>
#include <AL/alut.h>
#include <IL/ilut.h>
/****************************************************************************/
#ifdef BORLAND
    #include<vcl>
#endif
#ifdef WINDOWS
    #include<windows.h> //For sleep
#endif
#if !defined(CACHE_HEADERS) && defined(LINUX)
    #include<unistd.h> //for usleep
#endif
#include "game.h"
#include "memwatch.h"
#include "parse.h"
#include "client02.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif
/****************************************************************************/
#ifdef BORLAND
USEUNIT("client.cpp");
USEUNIT("client01.cpp");
USEUNIT("engine.cpp");
USEUNIT("font.cpp");
USEUNIT("message.cpp");
USEUNIT("network.cpp");
USEUNIT("oglwrap.cpp");
USEUNIT("sockwrap.cpp");
USEUNIT("texture.cpp");
USEUNIT("client02.cpp");
USEUNIT("types.cpp");
USEUNIT("unit.cpp");
USELIB("..\Program Files\Borland\CBuilder4\Lib\openal32.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\alu.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\ALut.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\alc.lib");
USEUNIT("memwatch.cpp");
USEUNIT("sound.cpp");
USERC("icon.rc");
USEUNIT("noise.cpp");
USEUNIT("model.cpp");
USEUNIT("missile.cpp");
USEUNIT("parse.cpp");
USEUNIT("terrain2.cpp");
USELIB("..\Program Files\Borland\CBuilder4\Lib\glfw.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\devil.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\ilu.lib");
USELIB("..\Program Files\Borland\CBuilder4\Lib\ilut.lib");
USEUNIT("vec.cpp");
USEUNIT("mtrand.cpp");
USEUNIT("cache.cpp");
USEUNIT("behavior.cpp");
USEUNIT("mathexpr.cpp");
//---------------------------------------------------------------------------
#endif

/****************************************************************************
                            Static Variables
 ****************************************************************************/
static tTime        nextFrame;
static bool         programFinished=false;

static float frameDelay = 0; //Minimum delay between each frame



/****************************************************************************
                            Global Variables
 ****************************************************************************/
//Program state variables:
static bool    windowActive = true;        //Is the program window active, meaning it has focus?
static bool    fullScreen = false;         //Is the program running in full-screen mode?
static int     screenWidth=640;            //Width of the client window (or screen)
static int     screenHeight=480;           //Height of the client window (or screen)

//Internal variables used for keyboard input:
static tKey    currentKey=0;
static char    currentChar=0;
static int     currentAction=0;

//Internal variables for mouse wheel:
static int     currentMouseWheel=0;

static tTime previousTime=0;
static double currentInterval=0.01;

//Variables and flags contained in game initiation file:
tString globalName;                 //Player's name (referenced throughout project)
tString initialWorld;               //Initial world file
tString initialMap;                 //Initial map file

tTime    currentTime=0;

/****************************************************************************
                             Static Functions
 ****************************************************************************/
static void parseGameINI(tStream& s) throw(tString);
static void GLFWCALL callbackResize(int width,int height);
static void GLFWCALL callbackChar(int ch, int action);
static void GLFWCALL callbackKey(int key, int action);
static void GLFWCALL callbackButton(int button, int action);
static void GLFWCALL callbackMousePos(int x, int y);
static void GLFWCALL callbackMouseWheel(int pos);
static void setupGLFW();
static void initTime();
static void updateTime();

void setupGLFW()
{
    char buf[80];
    snprintf(buf,sizeof(buf), "Machinations %s", VERSION_STRING);
    glfwSetWindowTitle(buf);

    glfwSetWindowSizeCallback(callbackResize);
    glfwSetCharCallback(callbackChar);
    glfwSetKeyCallback(callbackKey);
    glfwSetMouseButtonCallback(callbackButton);
    glfwSetMousePosCallback(callbackMousePos);
    glfwSetMouseWheelCallback(callbackMouseWheel);

    glfwSwapInterval( 1 ); //At least one monitor vertical retrace between each buffer swap.
    glfwEnable(GLFW_KEY_REPEAT);
}

int main(int argc,char** argv)
{
    ALenum error;
    int retval;
    //int a=0,b=0,c=0;
    memoryStartup();
    
#ifdef LINUX
    clampFrameRate(100);
#endif

    //Load initiation file if one exists:
    FILE *fp=fopen(INITIALIZATION_FILE,"r");
    tStream stream;

    if(fp)
    {
        try
        {
            stream.load(fp);
            parseGameINI(stream);
        }
        catch(const tString& message)
        {
            //If we encounter an error in initiation file, don't halt the
            //program!  Just make a note of the error and move on.
            tString msg = stream.parseErrorMessage(INITIALIZATION_FILE, message);
            bug(CHARPTR(msg));
        }
        fclose(fp);
    }

    retval=glfwInit();
    if(retval==GL_FALSE)
    {
        bug("main: Error initializing GLFW!");
        return 0;
    }

    //Initialize ALUT:
    alutInit(&argc, argv);
    error=alGetError();
    if(error != AL_NO_ERROR)
    {
        bug("main: Error initializing OpenAL with error code %d",error);
        return 0;
    }

    //Initialize IL:
    if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION ||
        iluGetInteger(ILU_VERSION_NUM) < ILU_VERSION ||
        ilutGetInteger(ILUT_VERSION_NUM) < ILUT_VERSION)
    {
        bug("main: You are using an older version of DevIL.  Please upgrade.");
        return 0;
    }

    if(glfwOpenWindow(screenWidth, screenHeight,
        /*redbits=*/0,/*greenbits=*/0,/*bluebits=*/0,
        /*alphabits=*/0,/*depthbits=*/24,/*stencilbits=*/0,
        /*mode=*/fullScreen?GLFW_FULLSCREEN:GLFW_WINDOW)==GL_FALSE)
    {
        bug("main: glfwOpenWindow failed");
        glfwTerminate();
        return 0;
    }

    //We must initialize DevIL AFTER we have an OpenGL context.
    ilInit();
    iluInit();
    ilutRenderer(ILUT_OPENGL);

    //Set the origin of all images to the lower-left
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilEnable(IL_ORIGIN_SET);

    if(fullScreen)
        glfwEnable(GLFW_MOUSE_CURSOR);

    setupGLFW();

    //Startup interface:
    try
	{
		interfaceStartup();
	}
	catch(...)	//Catch all exceptions.  Ideally, I would like to
				//display a message box; however, a universal function
				//for this does not exist.
	{
		return 0;	//Abort.  The output file will contain the error message.
	}

    initTime();
    nextFrame=currentTime;
    windowActive=true;

    //Setup mouse wheel:
    currentMouseWheel=getMouseWheel();

    //MAIN LOOP:
    while(glfwGetWindowParam(GLFW_OPENED) && !programFinished)
    {
        START_TIMER(TIMER_GAME_LOOP)

        updateTime();

        if(windowActive && currentTime>=nextFrame)
        {
            //This is a preliminary effort to reduce the CPU usage.  If there is
            //any time leftover after the frame is drawn, I suspend the thread.
            //This way, the frame rate will remain constant, but the CPU usage
            //will increase as the game becomes more active.

            nextFrame+=frameDelay;

            if(currentTime<nextFrame)
            {
#if defined(WINDOWS)
                int millisecs=(int)((nextFrame-currentTime)*1000);
                Sleep(millisecs);
#elif defined(LINUX)
                int microsecs=(int)((nextFrame-currentTime)*1000000);
                usleep(microsecs);
#else
                continue;
#endif
            }

            //Poll interface:
            START_TIMER(TIMER_INTERFACE_POLL)
            interfacePoll();
            STOP_TIMER(TIMER_INTERFACE_POLL)

            //Update screen:
            START_GRAPHICS_TIMER(TIMER_INTERFACE_DRAW)
            interfaceDraw();
            STOP_GRAPHICS_TIMER(TIMER_INTERFACE_DRAW)

            /*a=((int)(glfwGetTime()*100))%screenWidth;
            b=((int)(currentTime*100))%screenWidth;
            c++;
            if(c>=screenWidth)
                c=0;

            glBegin(GL_QUADS);
                glVertex2i(a,0);
                glVertex2i(a+20,0);
                glVertex2i(a+20,20);
                glVertex2i(a,20);
                glVertex2i(b,20);
                glVertex2i(b+20,20);
                glVertex2i(b+20,40);
                glVertex2i(b,40);
                glVertex2i(c,40);
                glVertex2i(c+20,40);
                glVertex2i(c+20,60);
                glVertex2i(c,60);
            glEnd();*/

            //This function will poll events also!
            START_TIMER(TIMER_SWAP_BUFFERS)
            glfwSwapBuffers();
            STOP_TIMER(TIMER_SWAP_BUFFERS)
        }
        else
            glfwPollEvents();

        if(currentKey!=0)
        {
            if(currentAction==GLFW_PRESS)
                onKeyDown(currentKey, currentChar);
            else
                onKeyUp(currentKey, currentChar);
        }
        currentKey=0;
        currentAction=0;
        currentChar=0;

        checkGLError();

        STOP_TIMER(TIMER_GAME_LOOP)
    }

    interfaceShutdown();

    ilShutDown();

    //Check for memory leaks:
    memoryShutdown();

    glfwTerminate();

    return 0;
}

void parseGameINI(tStream& s) throw(tString)
{
    int x,y,fps;
    float rate;
    int size;
    //Scan the initiation file for the following tags:
    //fullscreen, showquitmessage, username, and resolution

    while(!s.atEndOfStream())
    {
		tString tag=parseString("tag",s);
		s.matchOptionalToken("=");

		if(strPrefix(tag,"fullscreen") || strPrefix(tag,"fs"))
			fullScreen=parseBool("start full screen", s);
		else if(strPrefix(tag,"username"))
			globalName=parseString("username", s);
		else if(strPrefix(tag,"resolution"))
		{
			x=parseIntRange("resolution width", 640, 1280, s);
			y=parseIntRange("resolution height", 480, 1024, s);

			screenWidth=x;
			screenHeight=y;
		}
		else if(strPrefix(tag,"panrate"))
		{
			rate=parseFloatRange("keyboard and mouse pan rate", 0, 100, s);
			setPanRate(rate);
		}
		else if(strPrefix(tag,"spinrate"))
		{
			rate=parseFloatRange("radians per second", 0, 100, s);
			setSpinRate(rate);
		}
		else if(strPrefix(tag,"zoomrate"))
		{
			rate=parseFloatRange("exponential constant", 0, 100, s);
			setZoomRate(rate);
		}
		else if(strPrefix(tag,"tiltrate"))
		{
			rate=parseFloatRange("radians per second", 0, 100, s);
			setTiltRate(rate);
		}
		else if(strPrefix(tag,"mousezoomrate"))
		{
			rate=parseFloatRange("zoom rate", 0, 100, s);
			setMouseZoomRate(rate);
		}
		else if(strPrefix(tag,"maxfps")||strPrefix(tag,"fps"))
		{
			fps=parseIntMin("maximum frame rate",1, s);
			clampFrameRate(fps);
		}
		else if(strPrefix(tag,"defaultmap")||strPrefix(tag,"map"))
		{
			initialMap=parseString("initial map file", s);
			initialWorld=parseString("initial world file", s);
		}
		else if(strPrefix(tag,"shademap"))
		{
			size=parseInt("shade map size",s);
			if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
				throw tString("Shade map size must be 0, 256, 512, 1024, or 2048");
			setShadeMapSize(size);
		}
		else if(strPrefix(tag,"alphamap"))
		{
			size=parseInt("alpha map size",s);
			if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
				throw tString("Alpha map size must be 0, 256, 512, 1024, or 2048");
			setAlphaMapSize(size);
		}
		else if(strPrefix(tag,"watermap"))
		{
			size=parseInt("water map size",s);
			if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
				throw tString("Water map size must be 0, 256, 512, 1024, or 2048");
			setWaterMapSize(size);
		}
		else if(strPrefix(tag,"shadowmap"))
		{
			size=parseInt("shadow map size",s);
			if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
				throw tString("Shadow map size must be 0, 256, 512, 1024, or 2048");
			setShadowMapSize(size);
		}
		else if(strPrefix(tag,"showtimers"))
			showTimers();
		else
			throw tString("Invalid tag '")+tag+"'";

		s.endStatement();
	}
}

void endApplication()
{
    programFinished=true;
}

bool setVideoMode(bool _fullScreen, int width, int height)
{
    if(_fullScreen==fullScreen && width==screenWidth && height==screenHeight)
        return true;

    glfwCloseWindow();

    fullScreen=_fullScreen;
    screenWidth=width;
    screenHeight=height;

    log("setVideoMode: Entering the twilight zone...");
    if(glfwOpenWindow(screenWidth, screenHeight,
        /*redbits=*/0,/*greenbits=*/0,/*bluebits=*/0,
        /*alphabits=*/0,/*depthbits=*/24,/*stencilbits=*/0,
        /*mode=*/fullScreen?GLFW_FULLSCREEN:GLFW_WINDOW)==GL_FALSE)
    {
        bug("setVideoMode: glfwOpenWindow failed");
        //If we're trying to change to full screen mode, then revert back to windowed-mode
        if(!fullScreen ||
            !glfwOpenWindow(screenWidth, screenHeight,
            /*redbits=*/0,/*greenbits=*/0,/*bluebits=*/0,
            /*alphabits=*/0,/*depthbits=*/24,/*stencilbits=*/0,
            /*mode=*/GLFW_WINDOW)==GL_FALSE)
        {
            bug("setVideoMode: glfwOpenWindow failed");
            programFinished=true; //gracefully finish during the next pass (hopefully)
            return false;
        }
    }
    log("setVideoMode: ...Leaving the twilight zone");

    //Refresh textures on new drawing context:
    interfaceRefresh();

    setupGLFW();

    return true;
}

void clampFrameRate(int frameRate)
{
#ifdef DEBUG
    if(frameRate<1)
    {
        bug("clampFrameRate: invalid frame rate");
        return;
    }
#endif
    frameDelay=1.0/frameRate;
}

bool isKeyPressed(tKey key)
{
    return glfwGetKey((int)key)==GLFW_PRESS;
}
bool isShiftPressed()
{
    return glfwGetKey(GLFW_KEY_LSHIFT)==GLFW_PRESS || glfwGetKey(GLFW_KEY_RSHIFT)==GLFW_PRESS;
}
bool isAltPressed()
{
    return glfwGetKey(GLFW_KEY_LALT)==GLFW_PRESS || glfwGetKey(GLFW_KEY_RALT)==GLFW_PRESS;
}
bool isCtrlPressed()
{
    return glfwGetKey(GLFW_KEY_LCTRL)==GLFW_PRESS || glfwGetKey(GLFW_KEY_RCTRL)==GLFW_PRESS;
}
bool isModifierKey(tKey key)
{
    return key==GLFW_KEY_LSHIFT || key==GLFW_KEY_RSHIFT || key==GLFW_KEY_LALT || key==GLFW_KEY_RALT ||
        key==GLFW_KEY_LCTRL || key==GLFW_KEY_RCTRL;
}
bool setFullScreen(bool _fullScreen)
{
    return setVideoMode(_fullScreen, screenWidth, screenHeight);
}
bool setResolution(int width, int height)
{
    return setVideoMode(fullScreen, width, height);
}
bool isLeftButtonPressed()
{
    return glfwGetMouseButton(GLFW_MOUSE_BUTTON_LEFT)==GLFW_PRESS;
}
bool isRightButtonPressed()
{
    return glfwGetMouseButton(GLFW_MOUSE_BUTTON_RIGHT)==GLFW_PRESS;
}
bool isMousePressed()
{
    return isLeftButtonPressed() || isRightButtonPressed();
}
int getMouseX()
{
    int x, y;
    glfwGetMousePos(&x, &y);
    return x;
}
int getMouseY()
{
    int x, y;
    glfwGetMousePos(&x, &y);
    //Convert from window coordinates to cartesian coordinates:
    y = screenHeight-y-1;
    return y;
}
int getMouseWheel()
{
    return glfwGetMouseWheel();
}
int getScreenWidth()
{
    return screenWidth;
}
int getScreenHeight()
{
    return screenHeight;
}
bool isFullScreen()
{
    return fullScreen;
}

void GLFWCALL callbackResize(int width,int height)
{
    //IMPORTANT: If the width or height becomes zero, we can no longer render
    //the screen lest we generate a "Divide by Zero" exception.  The effects
    //of calling glViewport and glOrtho with four zeros are unpredictable.
    if(width<1 || height<1)
    {
        windowActive=false;
        return;
    }
    else
        windowActive=true;

    screenWidth=width;
    screenHeight=height;
    onSetResolution(width,height);

    //Adjust the viewport:
    glViewport(0,0,width,height);

    //Load the projection matrix with a 1:1 orthographic projection:
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
    glOrtho(0,width,0,height,1,-1);
	glMatrixMode(GL_MODELVIEW);
}

void GLFWCALL callbackChar(int ch, int action)
{
    if(ch<256)
        currentChar=ch;
}
void GLFWCALL callbackKey(int key, int action)
{
    if(currentKey!=0)
    {
        if(currentAction==GLFW_PRESS)
            onKeyDown(currentKey, currentChar);
        else
            onKeyUp(currentKey, currentChar);
    }
    currentKey=(tKey)key;
    currentAction=action;
    currentChar=0;

    //ALT+Q will always terminate the program, even if the user-interface is frozen:
    if(key=='Q' && isAltPressed())
        programFinished=true;
}
void GLFWCALL callbackButton(int button, int action)
{
    int x,y;
    eMouseButton b;
    glfwGetMousePos(&x,&y);
    //Convert from window coordinates to cartesian coordinates:
    y = screenHeight-y-1;

    switch(button)
    {
        case GLFW_MOUSE_BUTTON_LEFT:
            b=MOUSE_BUTTON_LEFT;
            break;
        case GLFW_MOUSE_BUTTON_RIGHT:
            b=MOUSE_BUTTON_RIGHT;
            break;
        case GLFW_MOUSE_BUTTON_MIDDLE:
            b=MOUSE_BUTTON_MIDDLE;
            break;
    }

    if(action==GLFW_PRESS)
        onMouseDown(x,y,b);
    else
        onMouseUp(x,y,b);
}
void GLFWCALL callbackMousePos(int x, int y)
{
    //Convert from window coordinates to cartesian coordinates:
    y = screenHeight-y-1;
    onMouseMove(x,y);
}
void GLFWCALL callbackMouseWheel(int pos)
{
    int delta=pos-currentMouseWheel;
    eMouseWheel direction=MOUSE_WHEEL_OTHER;
    if(delta<=-1)
        direction=MOUSE_WHEEL_UP;
    else if(delta>=1)
        direction=MOUSE_WHEEL_DOWN;
    onMouseWheel(pos,direction);
    currentMouseWheel=pos;
}

void initTime()
{
    currentTime=0;
    previousTime=glfwGetTime();
    currentInterval=0.01;
}

void updateTime()
{
    tTime t=glfwGetTime();
    //currentTime=(double)clock()/CLK_TCK;
    double interval=t-previousTime;
    if(interval<0.00001) //Clamp the interval at 0.00001 to prevent divide-by-zero errors
        interval=0.00001;
    double ratio=interval/currentInterval;
    clampRange(ratio,0.9,1.1);
    currentInterval*=ratio;
    currentTime+=currentInterval;

    previousTime=t;
}

void invalidateScreen()
{
    //Update screen:
    interfaceDraw();

    //This function will poll events also!
    glfwSwapBuffers();
}
void invalidateMousePosition()
{
    onMouseMove(getMouseX(),getMouseY());
}
