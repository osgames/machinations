/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef engineH
#define engineH

/**************************************************************************
                                Headers
 **************************************************************************/
#include "network.h"
#include "unit.h"

/**************************************************************************
                                Structures
 **************************************************************************/

//Temporary structure for storing information about an obstacle:
class tObstacle
{
    public:
    tVector2D position; //Position of obstacle
    float radius;       //Radius of obstacle
    float color[3];     //Color of obstacle
    tObstacle(tVector2D _position, float _radius, float _color[3]) : position(_position),
        radius(_radius)
    {
        for(int i=0;i<3;i++)
            color[i]=_color[i];
    }
};

//This structure represents one underground deposit containing a finite number
//of resources.  A player can extract resources from this deposit if they build
//the appropriate extractor within a certain radius of the deposit's center.
struct tDeposit
{
    int         ID;         //Each deposit has a unique identifier so that units can link to it.
    int         resource;   //Resource that this deposit contains
    tVector3D   center;     //Deposit's position on world map
    float       radius;     //Radius within which players must build extractors
    float       initialAmount;
                            //Initial amount of the resource available.
    float       amount;     //Amount of the resource remaining in the deposit.  When this value
                                //reaches zero, the deposit will become depleted and all units
                                //linked to it will no longer extract anything.
    float       a0;         //Amount at the last interval tick.
    float       rate;       //The net rate at which units are mining this deposit.
    bool        destroyed;  //Set this flag to remove the deposit during the next pass.
    tDeposit(int _ID, int _resource, const tVector3D& _center, float _radius, float _amount) :
        ID(_ID), resource(_resource), center(_center), radius(_radius), amount(_amount),
        initialAmount(_amount), rate(0), destroyed(false), a0(amount) {}
};

//Declare tUser here since tPlayer references it:
struct tUser;

//This structure contains information about a player including the controller
//(e.g. human or computer), timing, start location, and alliances.
struct tPlayer
{
    tString name;               //Player's name (e.g. empire)
    tString caption;            //Player's name (e.g. The Evil Empire)
    ePlayer type;               //The player type defined in the map file

    bool exists;                //Does the player exist in the current game?
    ePlayer assignedType;       //Which player type did the host assign to this player?
    tUser *assignedUser;        //Who controls this player? (PLAYER_HUMAN only)
                                //NULL for other player types.

    tGameTime   stopTime;       //How much longer can the game proceed without
                                //receiving any messages from this player?
    long        netInterval;    //This player's network interval--usually equals
                                //global netInterval variable, except during
                                //timing transitions.
    tGameTime   lowerBound;     //Lower bound on the stopTime that is used during
                                //timing transitions.
    bool        gamePaused;     //Has this player manually paused the game?

    tVector2D   startLocation;  //Where is view centered when game starts? (PLAYER_HUMAN only)

    //What is this player's alliance with every other player?  NOTE: An alliance
    //need not be reciprocated.  You could be my ally but I could be your enemy.
    eAlliance alliances[MAX_PLAYERS];

    float resources[MAX_RESOURCES];     //The quantity of each resource type that player owns
    float maxResources[MAX_RESOURCES];  //The maximum quantity of each resource type that the
                                            //player can accumulate
    float revenues[MAX_RESOURCES];      //The rate at which player earns each type of resource
    float costs[MAX_RESOURCES];         //The rate at which player spends each type of resource
    float foreignAid[MAX_RESOURCES];    //The rate at which other players provide you with resources (+)
                                            //or the rate at which you supply other players with resources (-)
    float r0[MAX_RESOURCES];            //Quantity after last intervalTick

    bool resourceAvailable[MAX_RESOURCES];
                                        //Indicates whether units can consume each resource
    bool resourceCollectable[MAX_RESOURCES];
                                        //Indicates whether units can collect each resource

    //Determines if other players can control your units.  If true, player X
    //can control all of your units.  If false, player X can't control any of
    //your units.  Note, you cannot control player X's units unless the setting
    //is mutual.
    bool sharedControl[MAX_PLAYERS];

    //Determines if your share resources with every other player.  If enabled,
    //you will supply resources to another player at a rate equal to their
    //cost minus their revenue (if greater than zero).  Note, you cannot siphon
    //the other players resources unless the setting is mutual.
    bool sharedResources[MAX_PLAYERS];

    //Determines if you share vision with every other player.  If you share
    //vision with Player X, then player X will see your explored areas and
    //all of your units.
    bool sharedVision[MAX_PLAYERS];

    //Dynamic array of floating-point visibility factors between 0 and 1.
    //Each player maintains his own visibility map regardless of vision sharing.
    //NOTE: A maximum of 32767 units can reveal a tile.
    short *fogOfWar;

    tPlayer() : type(PLAYER_NONE), exists(false), stopTime(0),
        netInterval(0), lowerBound(0), gamePaused(false), startLocation(zeroVector2D),
        fogOfWar(NULL), assignedType(PLAYER_NONE), assignedUser(NULL) {}
};

struct tResource
{
    tString name;
    tString caption;
    bool exists;
};

struct tArmorClass
{
    tString name;
    bool exists;
};

class tBullsEye
{
    public:
    tAnim a;
    tVector3D v;
    tBullsEye(tAnimType* type, tVector3D _v) : a(type), v(_v) {}
};

struct tLight
{
    float ambient[4];
    float diffuse[4];
    float specular[4];
    float position[4];
    float spotDirection[3];
    float spotExponent;
    float spotCutoff;
    float constantAttenuation;
    float linearAttenuation;
    float quadraticAttenuation;
    tLight();
};

 /**************************************************************************
                             Global Variables
 **************************************************************************/
//See engine.cpp for descriptions:
extern long         latency;
extern long         netInterval;
extern long         gameInterval;
extern float        gameSpeed;
extern bool         gameRunning;
extern bool         gameFrozen;

extern tVector2D    mapDimensions;

extern bool         mapLoaded;
extern bool         objectsLoaded;
extern bool         worldLoaded;
extern tFile        mapFile;
extern tFile        worldFile;

extern const tRGB   playerColors[MAX_PLAYERS];
extern const tRGB   resourceColors[MAX_RESOURCES];
extern const tRGB   allianceColors[4];
extern const char*  allianceNames[4];

extern int          hashWidth;
extern int          hashHeight;
extern long         hashSize;

extern tVector2D    mapOffset;
extern float        mapRotation;
extern float        mapScale;
extern float        mapTilt;

extern GLfloat      viewMatrix[16];
extern GLfloat      invViewMatrix[16];
extern GLfloat      minimapMatrix[16];
extern GLfloat      invMinimapMatrix[16];
extern GLfloat      panMatrix[4];
extern GLfloat      shadowTextureMatrix[16];

extern tTerrain*    terrain;
//Generic quadric object for circles and spheres:
extern GLUquadricObj* globalQuadricObject;

extern int          viewWidth;
extern int          viewHeight;
extern int          minimapWidth;
extern int          minimapHeight;

extern float        viewMinX;
extern float        viewMaxX;
extern float        viewMinY;
extern float        viewMaxY;

extern int          shadeMapSize;
extern int          alphaMapSize;
extern int          waterMapSize;
extern int          shadowMapSize;
extern bool         terrainWireframe;

extern int          testValue, testValue2; //Debugging purposes only

extern iTextureIndex shadowTexture;
/**************************************************************************
                             Global Functions
 **************************************************************************/
//See engine.cpp for descriptions:
extern void         engineStartup();   //Called during program startup
extern void         engineShutdown();  //Called during program shutdown
extern void         engineRefresh();   //Called when user toggles full screen mode

extern void         engineTick();
extern bool         startGame();
extern bool         endGame();
extern bool         pauseGame();
extern bool         resumeGame();
extern void         postMessage(tMessage *message);
extern void         receiveMessage(tMessage *message) throw(int);
extern bool         chooseMap(const tString& mapFilename, const tString& worldFilename, bool reset);
extern bool         setPlayer(int index, ePlayer type, tUser *u);
extern bool         setTiming(long _latency, long _netInterval, long _gameInterval, float _gameSpeed);
extern void         parseShellCommand(const tString& commandString) throw(tString);

extern void         drawMap();
extern void         drawMinimap();
extern void         spinMap(float _mapRotation);
extern void         zoomMap(float _mapScale);
extern void         tiltMap(float _mapTilt);
extern void         panMap(const tVector2D& _mapOffset);
extern void         panMinimap(int x, int y);
extern tVector3D    getViewCenter();
extern void         centerView(const tVector3D& v);
extern void         setViewDimensions(int _viewWidth, int _viewHeight);
extern void         setMinimapDimensions(int _minimapWidth, int _minimapHeight);
extern void         getScaleAtPoint(const tVector3D& point, float& projXScale, float& projYScale, float& projZScale);

extern int          getLocalPlayerID();

extern eAlliance    getAlliance(int player1, int player2);
extern eAlliance    getAlliance(int player);
extern eAlliance    getReverseAlliance(int player);

extern int          getPlayerCount();
extern const tString& getPlayerCaption(int player, bool useUsername);
extern ePlayer      getPlayerType(int player);
extern bool         doesPlayerExist(int player);
extern bool         isPlayerVisible(int player);

extern bool         doesResourceExist(int resource);
extern const tString& getResourceCaption(int resource);
extern float        getResourceRevenue(int resource);
extern float        getResourceCost(int resource);
extern float        getResourceAmount(int resource);
extern float        getResourceCapacity(int resource);
extern float        getForeignAid(int resource);

extern float        getNetProfit(int player, int resource);
extern bool         isResourceAvailable(int player, int resource);
extern bool         isResourceAvailable(int player, int resource, float amount);
extern bool         canCollectResource(int player, int resource);

extern bool         canReceiveForeignAid();
extern float        getCombinedRevenue(int resource);
extern float        getCombinedCost(int resource);
extern float        getCombinedAmount(int resource);
extern float        getCombinedCapacity(int resource);

extern bool         isGamePaused();

extern tVector2D    pixelToVector2D(int x, int y);
extern tVector3D    pixelToVector3D(int x, int y);
extern void         vector2DToPixel(const tVector2D& v, int& x, int& y);
extern void         vector3DToPixel(const tVector3D& v, int& x, int& y);

extern tVector2D    pixelToTerrain2D(int x, int y);
extern tVector3D    pixelToTerrain3D(int x, int y);
extern tVector2D    pixelToVector2D(int x, int y, float z);
extern tVector3D    pixelToVector3D(int x, int y, float z);
extern tVector2D    pixelToVector2DAtElevation(int x, int y, float r);
extern tVector3D    pixelToVector3DAtElevation(int x, int y, float r);
extern void         terrain2DToPixel(const tVector2D& v, int& x, int& y);

extern bool         isValid(const tVector2D& pos, float margin);
extern void         makeValid(tVector2D& pos, float margin);

extern tMissile*    createMissile(tUnit *source, const tVector3D& origin, tUnit *target,
                        tMissileType *type, tWorldTime startTime);
extern void         startTask(tTask *task);
extern void         endTask(tTask *task);

extern void         sendTextToAllies(const char *text, eTextMessage type);
extern void         sendTextToEnemies(const char *text, eTextMessage type);

extern bool         setAlliance(int player1, int player2, eAlliance a,
                        bool sharedControl, bool sharedResources, bool sharedVision);
extern bool         setAlliance(int player, eAlliance a,
                        bool sharedControl, bool sharedResources, bool sharedVision);

extern bool         getSharedVision(int player1, int player2);
extern bool         getSharedVision(int player);
extern bool         getSharedResources(int player1, int player2);
extern bool         getSharedResources(int player);
extern bool         getSharedControl(int player1, int player2);
extern bool         getSharedControl(int player);

extern void         adjustResource(int playerID, int resourceID, float amount);
extern void         adjustCost(int playerID, int resourceID, float amount);
extern void         adjustRevenue(int playerID, int resourceID, float amount);
extern void         adjustCapacity(int playerID, int resourceID, float amount);

extern void         revealCircle(int playerID, int x, int y, int radius);
extern void         concealCircle(int playerID, int x, int y, int radius);
extern bool         isTileVisible(int playerID, int x, int y);
extern bool         isPointVisible(int playerID, const tVector2D& v);

extern tDeposit*    findDepositAt(const tVector2D& p, int resourceID);
extern void         adjustExtractionRate(tDeposit *d, float amount);
extern bool         isWithinRange(tDeposit *d, const tVector2D& p);

extern int          playerLookup(const tString& name, bool exact);
extern int          resourceLookup(const tString& name, bool exact);
extern int          armorClassLookup(const tString& name, bool exact);

extern float        calculateShade(tVector3D& pos, tVector3D& normal);
extern void         addLightsToCacheHeader(tCache& cache);

//Low-level parsing functions:
extern int          parsePlayer(const char *fieldName, tStream& s) throw(tString);
extern tVector2D    parseWorldPos(const char *fieldName, tStream& s) throw(tString);
extern eAlliance    parseAlliance(const char *fieldName, tStream& s) throw(tString);
extern int          parseResource(const char *fieldName, tStream& s) throw(tString);
extern int          parseArmorClass(const char *fieldName, tStream& s) throw(tString);

//High-level parsing functions:
extern void         parseResourceBlock(float r[], tStream& s) throw(tString);

//Terrain functions:
extern bool         isTerrainIntervalFixed();
extern int          getTerrainInterval();
extern void         setTerrainInterval(int _interval);
extern void         resetTerrainInterval();
extern void         setShadeMapSize(int size);
extern void         setAlphaMapSize(int size);
extern void         setWaterMapSize(int size);
extern void         setShadowMapSize(int size);

/**************************************************************************
                               Unit Functions
 **************************************************************************/
//See engine.cpp for descriptions:
extern tUnit*       unitLookup(int ID);
extern tUnit*       createUnit(int owner, tUnitType *type, const tVector2D& position,
                               const tVector2D& forward, tWorldTime t);
extern void         destroyUnit(tUnit *u, tWorldTime t);

extern void         selectUnit(tUnit *u);
extern void         deselectUnit(tUnit *u);
extern void         clearSelection();

extern void         selectAll(bool shiftPressed);
extern void         selectRange(int x1, int y1, int x2, int y2, bool shiftPressed);
extern void         selectRange(int x1, int y1, int x2, int y2, const tVector2D& selectionOrigin,
                        bool shiftPressed);
extern void         selectPoint(int x, int y, bool altPressed, bool ctrlPressed, bool shiftPressed);

extern void         targetAll(bool shiftPressed, eCommand command);
extern void         targetRange(int x1, int y1, int x2, int y2, bool shiftPressed, eCommand command);
extern void         targetRange(int x1, int y1, int x2, int y2, const tVector2D& selectionOrigin,
                        bool shiftPressed, eCommand command);
extern void         targetMinimap(int x, int y, bool shiftPressed, eCommand command);
extern void         targetPoint(int x, int y, bool altPressed, bool ctrlPressed, bool shiftPressed,
                        eCommand command);

extern void         commandSel(eCommand command);
extern void         commandSel(eCommand command, tVector3D v);
extern void         commandSel(eCommand command, tUnit *targets[], int targetCount);
extern void         commandSel(eCommand command, int quantity, bool repeat);

extern void         clearGroups(int group);
extern void         assignToGroup(int group, bool shiftPressed);
extern void         disbandGroup();
extern void         selectGroup(int group, bool shiftPressed);
extern void         targetGroup(int group, bool shiftPressed, eCommand command);

extern bool         unitExists(tUnit *unit);
extern bool         canControl(int player, tUnit *u);
extern bool         canControl(tUnit *u);
extern bool         canPlayerSee(int player, tUnit *u);
extern bool         canPlayerSee(tUnit *u);

extern tUnit*       acquireTarget(tUnit *unit, float range, int flags,
                        const tVector3D& origin, const tVector3D& direction, float cosTheta,
                        const vector<tUnit *>& vec);
extern tUnit*       acquireTarget(tUnit *unit, float range, float fieldOfView, int flags,
                        const tVector3D& origin, const tVector3D& forward, float cosTheta);
extern void         createUnitExplosion(tUnitExplosion *u);

extern void         createApparition(eCommand command, int x, int y);
extern void         destroyApparition();

extern bool         isWithinView(float radius3D, const tVector3D& p);

//Group behaviors:
extern tVector2D    separation(tUnit *unit, float maxDistance, float cosMaxAngle);
extern tVector2D    collisionAvoidance(tUnit *unit, const tVector2D& v);
/**************************************************************************
                                   Events
 **************************************************************************/
//These functions are called by the engine in response to certain events:

//User-interface events:
extern void onStartGame();                          //A game starts
extern void onEndGame();                            //The game ends
extern void onSetTiming();                          //The timing was changed
extern void onSetGameSpeed();                       //The game interval and/or speed was changed
extern void onFreezeGame();                         //Game is halted because engine has not received network response
extern void onUnfreezeGame();                       //Game is resumed because engine receives network response
extern void onPauseGame(int playerID);              //Game is paused by user
extern void onResumeGame(int playerID);             //Game is resumed by user
extern void onChooseFiles();                        //Host has chosen a new map and world for the game
extern void onResetPlayers();                       //Erase the old player assignments
extern void onLoadMap();                            //The game engine loaded a new map
extern void onUnloadMap();                          //The game engine unloaded the map
extern void onLoadWorld();                          //The game engine loaded a new world
extern void onUnloadWorld();                        //The game engine unloaded the world
extern void onSetPlayer(int index, ePlayer type, tUser *user);
                                                    //Update player settings
extern void onSetAlliance(int playerID);            //Called when someone changes their alliance with you
extern void onDiscrepancyDetected();                //Called when the engine detects a discrepancy
extern void onParseError(const tString& error);     //chooseMap generates this event if it encounters any errors in
                                                    //the world or map files.
extern void onMapChanged();                         //Called if the engine discovers that the map or world file
extern void onWorldChanged();                       //was modified by an external program.
extern void onHostMissing();                        //You need to assign the host to a player.
extern void onGameWaiting();                        //The host is waiting on one or more clients to start the game.
extern void onAbortGame();                          //The host aborted a waiting game because an error occurred.

extern tSkin* shockwaveSkin;
/****************************************************************************/
#endif
