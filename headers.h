/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifdef CACHE_HEADERS
    #include <float.h>
    #include <math.h> //for sqrt
    #include <stdarg.h>
    #include <stdio.h>
    #include <stdlib.h> //for size_t
    #include <sys/stat.h> //for mkdir (Linux) and stat
    #include <string.h>
    #include <string>
    #include <time.h> //for time_t
    #include <ctype.h>
#endif

#ifdef LINUX
    #ifdef CACHE_HEADERS
        #include <unistd.h>
        #include <functional>
        #include <utime.h> //for utime
        #include <vector> //for vector
        #include <list> //for list 
        #include <stack> //for stack
    #endif

    //Linux uses utime instead of _utime
    #define _utime utime
    //Linux doesn't have the __int64 language extension
    #define __int64 long long int
    #define __uint64 unsigned long long int
    //Linux doesn't have the __fastcall language extension
    #define __fastcall
    //Linux has an extra parameter in mkdir
    #define my_mkdir(x) mkdir(x,0)
    //There doesn't appear to be a universal case-insensitive string comparison function:
    #define strnicmp strncasecmp
    #define stricmp strcasecmp

#else //WINDOWS

    #define my_mkdir(x) mkdir(x)
	#ifndef WINDOWS
		#define WINDOWS //make sure WINDOWS is defined
	#endif
    #ifdef BORLAND
        #ifdef CACHE_HEADERS
            #include <memory.h>
            #include <sys/timeb.h>
            #include <dir.h> //for mkdir
            #include <function.h>
            #include <utime.h> //for _utime
            #include <vector.h> //for vector
            #include <list.h> //for list
            #include <stack.h> //for stack
        #endif
    #elif defined(VCNET)
        #include "vsnet/snprintf.h" //for snprintf and vsnprintf
    #else //VISUALC
        #define snprintf _snprintf
        #define vsnprintf _vsnprintf

		#ifndef VISUALC
			#define VISUALC //make sure VISUALC is defined
		#endif
		#ifdef CACHE_HEADERS
    		#include <sys/timeb.h>
            #include <direct.h> //for mkdir
            #include <functional>
            #include <sys/utime.h> //for _utime
            #include <vector> //for vector
            #include <list> //for list
            #include <stack> //for stack
        #endif
    #endif
#endif
