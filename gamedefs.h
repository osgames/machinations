/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef gamedefsH
#define gamedefsH

 /**************************************************************************
                                Defines
 **************************************************************************/
#define MAX_PLAYERS                 8           //Maximum players in a game
#define MAX_RESOURCES               8           //Maximum types of resources in the game
#define MAX_ARMOR_CLASSES           8           //Maximum armor classes in the game
#define MAX_COMMANDS                256         //Maximum commands in commandTable (including build commands)
#define PICK_BUFFER_SIZE            1000        //Maximum number of entries (units) in pick buffer
#define MAP_EXTENSION               ".map"      //Default map filename extension
#define WORLD_EXTENSION             ".world"    //Default world filename extension
#define TASK_DURATION               .01         //Seconds each task has to execute at each pass
#define SYNC_DELAY                  500         //Number of milliseconds client waits before
                                                //responding to synchronization query
#define FILE_INSPECTION_INTERVAL    1           //Delay between file inspections in seconds

//Ensures unit isn't a dangling point and unit hasn't been destroyed:
#ifdef DEBUG
#define CHECK_UNIT(unit) \
    if(unit && unit->getState()==STATE_DESTROYED) \
    { \
        bug("Unit destroyed"); \
        unit=NULL; \
    }
#else
#define CHECK_UNIT(unit)
#endif

//Determines if unit 'u' can see unit 'v'.  This depends on whether unit 'u'
//can see (e.g. not blind) and whether unit 'v' is visible to the owner of
//unit 'u'.
#define CAN_SEE(u,v)        ((u)->canSee() && canPlayerSee((u)->getOwner(), (v)))

//Maximum field of view (used for clipped units outside viewing window)
#define MAX_FIELD_OF_VIEW   15

//Maximum radius of a unit (used for optimizing separation behavior)
#define MAX_UNIT_RADIUS     2

 //This macro calculates the distance between two units:
#define UNIT_DISTANCE(x,y) (x->getPosition()-y->getPosition()).length()

//This macro calculates the square of the distance between two units:
#define UNIT_DISTANCE_SQUARED(x,y) (x->getPosition()-y->getPosition()).lengthSquared()

//Dimensions of one hash cell:
#define HASH_SPACING        20

//Radius within which a unit will pursue enemy units in pursue mode
#define PURSUE_RADIUS       20

//Converts a unit's x- or y-component to an x or y hash value:
#define HASH_FUNC(x)        ((int)((x)/HASH_SPACING))

//Maximum distance between unit and its current waypoint before it moves to the next waypoint:
#define MOVE_PRECISION          0.5
#define MOVE_PRECISION_SQUARED  (MOVE_PRECISION*MOVE_PRECISION)
#define PATROL_PRECISION        2
#define PATROL_PRECISION_SQUARED  (PATROL_PRECISION*PATROL_PRECISION)

//Target acquisition flags:
#define ACQUIRE_ALLY_ONLY       0x01
#define ACQUIRE_ENEMY_ONLY      0x02
#define ACQUIRE_ALIVE_ONLY      0x04
#define ACQUIRE_DAMAGED_ONLY    0x08
#define ACQUIRE_VULNERABLE_ONLY 0x10
#define ACQUIRE_REPAIRABLE_ONLY 0x20
#define ACQUIRE_LIMIT_ANGLE     0x40

//Maximum distance between the point on the minimap or view where you click and
//the unit you want to select:
#define MINIMAP_TOLERANCE       2
#define VIEW_TOLERANCE          2

//Near clipping plane:
#define NEAR_PLANE              5.0f
//Far clipping plane:
#define FAR_PLANE               1000.0f

/**************************************************************************
                              Enumerations
 **************************************************************************/

//At times, we need to represent a unit's size with a discrete value.  For
//instance, we design five different sizes of selection circles.  Or, we want
//to decide how many units of a particular type will fit on a transport.
enum eUnitSize {SIZE_TINY, SIZE_SMALL, SIZE_MEDIUM, SIZE_LARGE, SIZE_HUGE};

//eCommand enumerates ten preset commands.  User-defined units may define
//additional custom commands.  IMPORTANT: Make sure this enumeration stays
//in sync with the command table!
enum eCommand
{
    //Basic commands:
    COMMAND_NONE,               //0

    COMMAND_MOVE,               //1
    COMMAND_ATTACK,             //2
    COMMAND_FOLLOW,             //3
    COMMAND_PATROL,             //4
    COMMAND_STOP,               //5

    //Movement modes:
    //A player can choose a movement mode for unit when it is selected.
    COMMAND_WANDER,             //6     The unit is free to stray from its present course to engage enemies
                                     // or repair friendly units.
    COMMAND_MANEUVER,           //7     The unit follows its movement orders.
    COMMAND_HOLD_POSITION,      //8     The unit stands its ground, even when "pushed" by adjacent units.
    COMMAND_EVADE,              //9     The unit avoids hostile enemy units by moving away from them.  When
                                     // not in the presence of enemy units, this mode is equivalent to maneuver.

    //Extended commands:
    COMMAND_DISBAND,            //10
    COMMAND_DELETE,             //11
    COMMAND_SELF_DESTRUCT,      //12
    STANDARD_COMMANDS,          //13    Number of commands which are always visible

    //Custom commands:
    COMMAND_CANCEL_PRODUCTION=  //13
        STANDARD_COMMANDS,
    COMMAND_FIRE_AT_WILL,       //14
    COMMAND_RETURN_FIRE,        //15
    COMMAND_HOLD_FIRE,          //16
    COMMAND_AUTO_REPAIR,        //17
    COMMAND_MANUAL_REPAIR,      //18
    COMMAND_REPAIR,             //19
    COMMAND_DISASSEMBLE,        //20
    COMMAND_DIVIDE,             //21
    COMMAND_CANCEL_DIVIDE,      //22
    COMMAND_COLLECTION_ON,      //23
    COMMAND_COLLECTION_OFF,     //24
    COMMAND_PRODUCTION_ON,      //25
    COMMAND_PRODUCTION_OFF,     //26
    COMMAND_DETECTION_ON,       //27
    COMMAND_DETECTION_OFF,      //28
    PREDEFINED_COMMANDS         //29
};

//Some commands have no parameters (e.g. stop).  Some commands have a map
//position parameter (e.g. move).  Some commands have a unit parameter
//(e.g. attack).  The remaining commands are production orders containing
//a quantity and a repeat flag.  PARAM_CONSTRUCTION is a special case of PARAM_POSITION.
enum eParamType { PARAM_NONE, PARAM_UNIT, PARAM_POSITION, PARAM_CONSTRUCTION,
    PARAM_PRODUCTION };

//The following states determine how a unit appears and how it interacts with
//other units:
enum eUnitState {
    STATE_DESTROYED,    //The unit no longer exists and the engine will remove it during
                        //  the next pass (lazy delete).
    STATE_SELECTABLE,   //The player can select the unit, but it does not have
                        //  hitpoints.
    STATE_ALIVE,        //The unit has hitpoints.
};

//eAlliance describes a player's relationship with another player.
enum eAlliance
{
    ALLIANCE_SELF,      //This is the alliance you have with yourself
    ALLIANCE_ALLY,      //Allied (another player can control your units and share your resources)
    ALLIANCE_NEUTRAL,   //Neutral (you're neither allied nor enemies--i.e. critters)
    ALLIANCE_ENEMY      //Enemy (you will automatically open fire on enemy units)
};

//ePlayer describes the type of player (or lack thereof) occupying each slot.
enum ePlayer
{
    PLAYER_NONE,        //Map does not support this player
    PLAYER_HUMAN,       //Player controlled by a human
    PLAYER_COMPUTER,    //Player controlled by an artificial intelligence routine
    PLAYER_HIDDEN,      //Neutral computer player which doesn't appear in
                            //game setup screen or alliance screen
    PLAYER_OPEN,        //Slot is open (users can choose this player)
    PLAYER_CLOSED       //Slot is closed (users cannot choose this player)
};

enum eAbility
{
    ABILITY_NONE,
    ABILITY_MOVE,     //If disabled, the unit cannot move
    ABILITY_ATTACK,   //If disabled, the unit cannot fire using any of its weapons
    ABILITY_REPAIR,   //If disabled, the unit cannot repair, disassemble, or construct units
    ABILITY_PRODUCE,  //If disabled, the unit cannot produce units
    MAX_ABILITIES
};

//Return values for getLeader:
enum ePriority
{
    PRIORITY_NONE,      //0
    PRIORITY_SECONDARY, //1
    PRIORITY_PRIMARY    //2
};

//Draw flags:
#define DRAW_MODEL              0x01
#define DRAW_GREEN_WIREFRAME    0x02
#define DRAW_GREY_WIREFRAME     0x04
#define DRAW_SHADOW             0x08
#define DRAW_CONVEX_SHELL       0x10
#define DRAW_TEXTURE            0x20
/****************************************************************************/
#endif
