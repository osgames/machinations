//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
USERES("texmngr.res");
USEFORM("texmngr1.cpp", Main);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
    try
    {
         Application->Initialize();
         Application->Title = "Machinations Texture Manager";
         Application->CreateForm(__classid(TMain), &Main);
         Application->Run();
    }
    catch (Exception &exception)
    {
         Application->ShowException(&exception);
    }
    return 0;
}
//---------------------------------------------------------------------------
