//---------------------------------------------------------------------------
#include <vcl.h>
#include <GL/gl.h>
#include <IL/il.h>
#include <IL/ilu.h>
#include <IL/ilut.h>
#include <vector>
#include <string>
#pragma hdrstop

#include "texmngr1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMain *Main;
//---------------------------------------------------------------------------
static const char *TPKFileSignature="MACHTPK1";

__fastcall TMain::TMain(TComponent* Owner)
    : TForm(Owner), hdc(0), hrc(0), viewX(0), viewY(0), viewWidth(0), viewHeight(0),
    canvasWidth(0), canvasHeight(0), needsRedraw(false), imageWidth(0), imageHeight(0),
    imageTexture(0), imageScale(1), texturesVisible(true), hotspotsVisible(true),
    highlightedTexture(NULL), eventMask(false), readyToDrawRectangle(false),
    drawingRectangle(false), rectangleX1(0), rectangleY1(0), rectangleX2(0), rectangleY2(0),
    resizeMask(0), resizing(false), lastMouseX(0), lastMouseY(0), postSelect(false),
    imageLoaded(false), drawingBoundingBox(false)
{
    Application->OnIdle = IdleLoop;
    cbBackground->ItemIndex=0;
    for(int i=0;i<3;i++)
        backgroundColor[i]=0;
    setModified(false);
    setCurrentFile("");
    setImageFile("");
    loadWorkingDirectory();

    odSelectImage->InitialDir=workingDirectory.c_str();
    odOpenTexturePack->InitialDir=workingDirectory.c_str();
    sdSaveTexturePack->InitialDir=workingDirectory.c_str();

    lbTexturesClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TMain::IdleLoop(TObject*, bool& done)
{
    if(needsRedraw)
    {
        drawCanvas();
        needsRedraw=false;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormCreate(TObject *Sender)
{
    hdc = GetDC(paView->Handle);
    setPixelFormatDescriptor();
    hrc = wglCreateContext(hdc);
    if(hrc == NULL)
    	Application->MessageBox("Failed to create context","Initialization Error",
            MB_OK|MB_ICONERROR);
    if(wglMakeCurrent(hdc, hrc) == false)
    	Application->MessageBox("Failed to MakeCurrent","Initialization Error",
            MB_OK|MB_ICONERROR);

    //Initialize IL:
    if (ilGetInteger(IL_VERSION_NUM) < IL_VERSION ||
        iluGetInteger(ILU_VERSION_NUM) < ILU_VERSION ||
        ilutGetInteger(ILUT_VERSION_NUM) < ILUT_VERSION)
    {
    	Application->MessageBox("You are using an older version of DevIL.  Please upgrade.","Initialization Error",
            MB_OK|MB_ICONERROR);
    }

    //We must initialize DevIL AFTER we have an OpenGL context.
    ilInit();
    iluInit();
    ilutRenderer(ILUT_OPENGL);

    //Set the origin of all images to the lower-left
    ilOriginFunc(IL_ORIGIN_LOWER_LEFT);
    ilEnable(IL_ORIGIN_SET);

    setupCanvas();

    paViewResize(NULL);
}
//---------------------------------------------------------------------------
void TMain::setPixelFormatDescriptor()
{
    PIXELFORMATDESCRIPTOR pfd = {
    	sizeof(PIXELFORMATDESCRIPTOR),
        1,
        PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER,
        PFD_TYPE_RGBA,
        24,
        0,0,0,0,0,0,
        0,0,
        0,0,0,0,0,
        32,
        0,
        0,
        PFD_MAIN_PLANE,
        0,
        0,0,
    };
    int pixelFormat = ChoosePixelFormat(hdc, &pfd);
    SetPixelFormat(hdc, pixelFormat, &pfd);
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormDestroy(TObject *Sender)
{
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
        delete (tTexture*)lbTextures->Items->Objects[i];

    destroyCanvas();

    ilShutDown();

    wglMakeCurrent(NULL, NULL);
    wglDeleteContext(hrc);
}
//---------------------------------------------------------------------------
void TMain::setupCanvas()
{
    glClearColor(backgroundColor[0],
        backgroundColor[1],
        backgroundColor[2], 1.0f);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glGenTextures(1,&imageTexture);
}
//---------------------------------------------------------------------------
void TMain::drawCanvas()
{
    int i;
    int x,y,x1,x2,y1,y2;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

    if(imageLoaded)
    {
        glColor3f(1,1,1);
        glEnable(GL_TEXTURE_2D);
        glBindTexture(GL_TEXTURE_2D,imageTexture);
        glEnable(GL_BLEND);
        glBegin(GL_QUADS);
            glTexCoord2f(0,0);
            glVertex2i(0,0);

            glTexCoord2f(1,0);
            glVertex2i(canvasWidth,0);

            glTexCoord2f(1,1);
            glVertex2i(canvasWidth,canvasHeight);

            glTexCoord2f(0,1);
            glVertex2i(0,canvasHeight);
        glEnd();
        glDisable(GL_BLEND);
        glDisable(GL_TEXTURE_2D);
    }

    if(texturesVisible)
    {
        for(i=0;i<lbTextures->Items->Count;i++)
            if(!lbTextures->Selected[i])
            {
                tTexture *texture=(tTexture *)lbTextures->Items->Objects[i];

                imageToCanvasCoords(texture->left,texture->bottom,x1,y1);
                imageToCanvasCoords(texture->left+texture->width,texture->bottom+texture->height,x2,y2);

                if(texture==highlightedTexture)
                    glColor3f(1,1,0);
                else
                    glColor3f(0,1,0);
                glRecti(x1,y1,x1+5,y1+5);
                glRecti(x2-5,y1,x2,y1+5);
                glRecti(x1,y2-5,x1+5,y2);
                glRecti(x2-5,y2-5,x2,y2);

                glColor3f(1,0,0);
                glBegin(GL_LINE_LOOP);
                    glVertex2i(x1,y1);
                    glVertex2i(x2,y1);
                    glVertex2i(x2,y2);
                    glVertex2i(x1,y2);
                glEnd();
            }

        for(i=0;i<lbTextures->Items->Count;i++)
            if(lbTextures->Selected[i])
            {
                tTexture *texture=(tTexture *)lbTextures->Items->Objects[i];

                imageToCanvasCoords(texture->left,texture->bottom,x1,y1);
                imageToCanvasCoords(texture->left+texture->width,texture->bottom+texture->height,x2,y2);

                if(texture==highlightedTexture)
                    glColor3f(1,1,0);
                else
                    glColor3f(0,1,0);
                glRecti(x1,y1,x1+5,y1+5);
                glRecti(x2-5,y1,x2,y1+5);
                glRecti(x1,y2-5,x1+5,y2);
                glRecti(x2-5,y2-5,x2,y2);

                glColor3f(0,1,1);
                glBegin(GL_LINE_LOOP);
                    glVertex2i(x1,y1);
                    glVertex2i(x2,y1);
                    glVertex2i(x2,y2);
                    glVertex2i(x1,y2);
                glEnd();
            }
    }

    if(hotspotsVisible)
    {
        glEnable(GL_BLEND);
        glColor4f(0,0,1,0.6);
        for(i=0;i<lbTextures->Items->Count;i++)
        {
            tTexture *texture=(tTexture *)lbTextures->Items->Objects[i];

            imageToCanvasCoords(texture->left+texture->hotspotX,
                texture->bottom+texture->hotspotY,x,y);

            glBegin(GL_TRIANGLES);
                glVertex2i(x-10,y-10);
                glVertex2i(x,y);
                glVertex2i(x-10,y+10);

                glVertex2i(x+10,y-10);
                glVertex2i(x+10,y+10);
                glVertex2i(x,y);
            glEnd();
        }
        glDisable(GL_BLEND);
    }

    if(drawingRectangle || drawingBoundingBox)
    {
        if(drawingBoundingBox)
        {
            glLineStipple(1,0x0F0F);
            glEnable(GL_LINE_STIPPLE);
        }

        imageToCanvasCoords(rectangleX1,rectangleY1,x1,y1);
        imageToCanvasCoords(rectangleX2,rectangleY2,x2,y2);

        glColor3f(1-backgroundColor[0],1-backgroundColor[1],1-backgroundColor[2]);
        glBegin(GL_LINE_LOOP);
            glVertex2i(x1,y1);
            glVertex2i(x2,y1);
            glVertex2i(x2,y2);
            glVertex2i(x1,y2);
        glEnd();

        glDisable(GL_LINE_STIPPLE);
    }

    SwapBuffers(hdc);
}
//---------------------------------------------------------------------------
void TMain::destroyCanvas()
{
    glDeleteTextures(1,&imageTexture);
}
//---------------------------------------------------------------------------
bool TMain::loadImage()
{
    iImageIndex imageName;

    imageWidth=0;
    imageHeight=0;
    imageLoaded=false;
    ilGenImages(1, &imageName);
    ilBindImage(imageName);
    if(ilLoadImage((char*)(imageFile.c_str()))==IL_FALSE)
        return false;

    glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, imageTexture);
    ilutGLTexImage(0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glDisable(GL_TEXTURE_2D);

    imageWidth=ilGetInteger(IL_IMAGE_WIDTH);
    imageHeight=ilGetInteger(IL_IMAGE_HEIGHT);
    imageLoaded=true;

    ilDeleteImages(1, &imageName);
    return true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::paViewResize(TObject *Sender)
{
    viewWidth=paView->Width;
    viewHeight=paView->Height;
    sbHorizontal->Max=MAX(canvasWidth-viewWidth,0);
    sbVertical->Max=MAX(canvasHeight-viewHeight,0);
    clampViewXY();
    updateProjection();
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::sbScroll(TObject *Sender, TScrollCode ScrollCode, int &ScrollPos)
{
    viewX=sbHorizontal->Position;
    viewY=MAX(canvasHeight-viewHeight-sbVertical->Position,0);
    updateProjection();
    drawCanvas();
}
//---------------------------------------------------------------------------
void TMain::updateCanvasSize()
{
    imageToCanvasCoords(imageWidth,imageHeight,canvasWidth,canvasHeight);

    sbHorizontal->Max=MAX(canvasWidth-viewWidth,0);
    sbVertical->Max=MAX(canvasHeight-viewHeight,0);
    clampViewXY();
    updateProjection();
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void TMain::updateProjection()
{
    if(viewWidth>0 && viewHeight>0)
    {
        glViewport(0, 0, viewWidth, viewHeight);

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(viewX,viewX+viewWidth,viewY,viewY+viewHeight,-1,1);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }
}
//---------------------------------------------------------------------------
void TMain::clampViewXY()
{
    int maxViewX=MAX(canvasWidth-viewWidth,0);
    int maxViewY=MAX(canvasHeight-viewHeight,0);
    clampRange(viewX,0,maxViewX);
    clampRange(viewY,0,maxViewY);

    sbHorizontal->Position=viewX;
    sbVertical->Position=MAX(canvasHeight-viewHeight-viewY,0);
}
//---------------------------------------------------------------------------
void __fastcall TMain::tbZoomChange(TObject *Sender)
{
    int centerX,centerY;

    canvasToImageCoords(viewX+viewWidth/2,viewY+viewHeight/2,centerX,centerY);
    imageScale=(1<<tbZoom->Position);
    imageToCanvasCoords(centerX,centerY,viewX,viewY);
    viewX-=viewWidth/2;
    viewY-=viewHeight/2;
    updateCanvasSize();
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbBackgroundChange(TObject *Sender)
{
    int i,index;
    const float colors[5][3]= {{0,0,0},
                            {1,1,1},
                            {1,0,0},
                            {0,1,0},
                            {0,0,1}};

    index=cbBackground->ItemIndex;
    if(index>=0 && index<5)
    {
        for(i=0;i<3;i++)
            backgroundColor[i]=colors[index][i];
        glClearColor(backgroundColor[0],
            backgroundColor[1],
            backgroundColor[2], 1.0f);
        needsRedraw=true;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::btNewTextureClick(TObject *Sender)
{
    resetMouseState();
    tTexture *newTexture=new tTexture("Untitled",0,0,0,0,0,0,false,false);
    clearSelection();
    addTexture(newTexture);
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btRemoveTextureClick(TObject *Sender)
{
    int i;
    resetMouseState();
    for(i=0;i<lbTextures->Items->Count;)
    {
        if(lbTextures->Selected[i])
        {
            delete (tTexture*)lbTextures->Items->Objects[i];
            lbTextures->Items->Delete(i);
            setModified(true);
        }
        else
            i++;
    }
    lbTexturesClick(NULL);
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDuplicateClick(TObject *Sender)
{
    int i,count=lbTextures->Items->Count;
    resetMouseState();
    for(i=0;i<count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *newTexture=new tTexture(*((tTexture*)lbTextures->Items->Objects[i]));
            newTexture->name=incrementNumber(newTexture->name);
            lbTextures->Selected[i]=false;
            addTexture(newTexture);
        }
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDuplicateUpClick(TObject *Sender)
{
    int i,count=lbTextures->Items->Count;
    resetMouseState();
    for(i=0;i<count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *oldTexture=(tTexture*)lbTextures->Items->Objects[i];
            tTexture *newTexture=new tTexture(*oldTexture);
            newTexture->name=incrementNumber(newTexture->name);
            newTexture->bottom=oldTexture->bottom+oldTexture->height;
            lbTextures->Selected[i]=false;
            addTexture(newTexture);
        }
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDuplicateLeftClick(TObject *Sender)
{
    int i,count=lbTextures->Items->Count;
    resetMouseState();
    for(i=0;i<count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *oldTexture=(tTexture*)lbTextures->Items->Objects[i];
            tTexture *newTexture=new tTexture(*oldTexture);
            newTexture->name=incrementNumber(newTexture->name);
            newTexture->left=oldTexture->left-oldTexture->width;
            lbTextures->Selected[i]=false;
            addTexture(newTexture);
        }
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDuplicateRightClick(TObject *Sender)
{
    int i,count=lbTextures->Items->Count;
    resetMouseState();
    for(i=0;i<count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *oldTexture=(tTexture*)lbTextures->Items->Objects[i];
            tTexture *newTexture=new tTexture(*oldTexture);
            newTexture->name=incrementNumber(newTexture->name);
            newTexture->left=oldTexture->left+oldTexture->width;
            lbTextures->Selected[i]=false;
            addTexture(newTexture);
        }
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDuplicateDownClick(TObject *Sender)
{
    int i,count=lbTextures->Items->Count;
    resetMouseState();
    for(i=0;i<count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *oldTexture=(tTexture*)lbTextures->Items->Objects[i];
            tTexture *newTexture=new tTexture(*oldTexture);
            newTexture->name=incrementNumber(newTexture->name);
            newTexture->bottom=oldTexture->bottom-oldTexture->height;
            lbTextures->Selected[i]=false;
            addTexture(newTexture);
        }
    ActiveControl=edName;
}
//---------------------------------------------------------------------------
tString TMain::incrementNumber(const tString& s)
{
    char buf[1000];
    int i,number;
    tTexture *t;

    strncpy(buf,s.c_str(),990);
    buf[990-1]='\0';
    char *ptr=buf+strlen(buf);

    while((--ptr)>=buf)
        if(!isdigit(*ptr))
            break;
    ptr++;
    if(isdigit(*ptr))
        number=atoi(ptr)+1;
    else
        number=2;
    for(;;number++)
    {
        sprintf(ptr,"%d",number);
        for(i=0;i<lbTextures->Items->Count;i++)
        {
            t=(tTexture*)lbTextures->Items->Objects[i];
            if(stricmp(buf,t->name.c_str())==0)
                break;
        }
        if(i==lbTextures->Items->Count)
            return tString(buf);
    }
}
//---------------------------------------------------------------------------
void TMain::clearSelection()
{
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
        lbTextures->Selected[i]=false;
}
//---------------------------------------------------------------------------
void TMain::addTexture(tTexture *newTexture)
{
    lbTextures->Items->AddObject(newTexture->name.c_str(),(TObject*)newTexture);
    lbTextures->Selected[lbTextures->Items->Count-1]=true;
    lbTexturesClick(NULL);
    setModified(true);
}
//---------------------------------------------------------------------------
void __fastcall TMain::lbTexturesClick(TObject *Sender)
{
    tTexture current;

    bool firstTexture=true;
    bool leftConflict=false,bottomConflict=false;
    bool widthConflict=false,heightConflict=false;
    bool hotspotXConflict=false,hotspotYConflict=false;
    bool alphaTestingConflict=false;
    bool alphaBlendingConflict=false;

    tTexture *texture;
    int i;

    resetMouseState();
    if(lbTextures->SelCount<1)
    {
        eventMask=true;
        edName->Text="";
        edName->Color=clBtnFace;
        edName->Enabled=false;
        edLeft->Text="";
        edLeft->Color=clBtnFace;
        edLeft->Enabled=false;
        edBottom->Text="";
        edBottom->Color=clBtnFace;
        edBottom->Enabled=false;
        edWidth->Text="";
        edWidth->Color=clBtnFace;
        edWidth->Enabled=false;
        edHeight->Text="";
        edHeight->Color=clBtnFace;
        edHeight->Enabled=false;
        edHotspotX->Text="";
        edHotspotX->Color=clBtnFace;
        edHotspotX->Enabled=false;
        edHotspotY->Text="";
        edHotspotY->Color=clBtnFace;
        edHotspotY->Enabled=false;
        cbAlphaTesting->Checked=false;
        cbAlphaTesting->Enabled=false;
        cbAlphaBlending->Checked=false;
        cbAlphaBlending->Enabled=false;
        eventMask=false;
        return;
    }

    edName->Color=clWindow;
    edName->Enabled=true;
    edLeft->Color=clWindow;
    edLeft->Enabled=true;
    edBottom->Color=clWindow;
    edBottom->Enabled=true;
    edWidth->Color=clWindow;
    edWidth->Enabled=true;
    edHeight->Color=clWindow;
    edHeight->Enabled=true;
    edHotspotX->Color=clWindow;
    edHotspotX->Enabled=true;
    edHotspotY->Color=clWindow;
    edHotspotY->Enabled=true;
    cbAlphaTesting->Enabled=true;
    cbAlphaBlending->Enabled=true;

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            texture=(tTexture *)lbTextures->Items->Objects[i];
            if(firstTexture)
            {
                firstTexture=false;
                current=*texture;
            }
            else
            {
                if(texture->name.compare(current.name)!=0)
                    current.name="";
                if(!leftConflict && texture->left!=current.left)
                    leftConflict=true;
                if(!bottomConflict && texture->bottom!=current.bottom)
                    bottomConflict=true;
                if(!widthConflict && texture->width!=current.width)
                    widthConflict=true;
                if(!heightConflict && texture->height!=current.height)
                    heightConflict=true;
                if(!hotspotXConflict && texture->hotspotX!=current.hotspotX)
                    hotspotXConflict=true;
                if(!hotspotYConflict && texture->hotspotY!=current.hotspotY)
                    hotspotYConflict=true;
                if(!alphaTestingConflict && texture->alphaTesting!=current.alphaTesting)
                    alphaTestingConflict=true;
                if(!alphaBlendingConflict && texture->alphaBlending!=current.alphaBlending)
                    alphaBlendingConflict=true;
            }
        }

    eventMask=true;
    edName->Text=current.name.c_str();
    if(leftConflict)
        edLeft->Text="";
    else
        edLeft->Text=IntToStr(current.left);
    if(bottomConflict)
        edBottom->Text="";
    else
        edBottom->Text=IntToStr(current.bottom);
    if(widthConflict)
        edWidth->Text="";
    else
        edWidth->Text=IntToStr(current.width);
    if(heightConflict)
        edHeight->Text="";
    else
        edHeight->Text=IntToStr(current.height);
    if(hotspotXConflict)
        edHotspotX->Text="";
    else
        edHotspotX->Text=IntToStr(current.hotspotX);
    if(hotspotYConflict)
        edHotspotY->Text="";
    else
        edHotspotY->Text=IntToStr(current.hotspotY);
    if(alphaTestingConflict)
        cbAlphaTesting->State=cbGrayed;
    else
        cbAlphaTesting->Checked=current.alphaTesting;
    if(alphaBlendingConflict)
        cbAlphaBlending->State=cbGrayed;
    else
        cbAlphaBlending->Checked=current.alphaBlending;
    eventMask=false;

    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edNameChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    resetMouseState();
    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            lbTextures->Items->Strings[i]=edName->Text;
            lbTextures->Selected[i]=true;
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->name=edName->Text.c_str();
            setModified(true);
        }
}
//---------------------------------------------------------------------------
void __fastcall TMain::edLeftChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edLeft->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->left=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edBottomChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edBottom->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->bottom=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edWidthChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edWidth->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->width=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edHeightChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edHeight->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->height=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edHotspotXChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edHotspotX->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->hotspotX=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::edHotspotYChange(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    int val;
    resetMouseState();
    try
    {
        val=StrToInt(edHotspotY->Text);
    }
    catch(const EConvertError& e)
    {
        return;
    }

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->hotspotY=val;
            setModified(true);
        }
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbAlphaTestingClick(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    bool checked=cbAlphaTesting->Checked;

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->alphaTesting=checked;
            setModified(true);
        }
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbAlphaBlendingClick(TObject *Sender)
{
    if(eventMask)
        return;

    int i;
    bool checked=cbAlphaBlending->Checked;

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            tTexture *texture=(tTexture*)(lbTextures->Items->Objects[i]);
            texture->alphaBlending=checked;
            setModified(true);
        }
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbTexturesVisibleClick(TObject *Sender)
{
    texturesVisible=cbTexturesVisible->Checked;
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::cbHotspotsVisibleClick(TObject *Sender)
{
    hotspotsVisible=cbHotspotsVisible->Checked;
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void TMain::imageToCanvasCoords(int ix, int iy, int& cx, int& cy)
{
    cx=ix*imageScale;
    cy=iy*imageScale;
}
//---------------------------------------------------------------------------
void TMain::canvasToImageCoords(int cx, int cy, int& ix, int& iy)
{
    ix=ROUND((float)cx/imageScale);
    iy=ROUND((float)cy/imageScale);
}
//---------------------------------------------------------------------------
tTexture *TMain::findTextureAt(int x, int y)
{
    tTexture *t;
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
    {
        t=(tTexture*)lbTextures->Items->Objects[i];
        if(x>=t->left && y>=t->bottom && x<=t->left+t->width && y<=t->bottom+t->height)
            return t;
    }
    return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btSortTexturesClick(TObject *Sender)
{
    resetMouseState();
    lbTextures->Sorted=true;
    lbTextures->Sorted=false;
}
//---------------------------------------------------------------------------
void __fastcall TMain::lbTexturesKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
    if(Key==VK_DELETE)
        btRemoveTextureClick(NULL);
    if(Key=='A' && Shift.Contains(ssCtrl))
    {
        selectAllTextures();
        lbTexturesClick(NULL);
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormPaint(TObject *Sender)
{
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::btDrawRectangleClick(TObject *Sender)
{
    resetMouseState();
    if(lbTextures->SelCount<1)
    {
        Application->MessageBox("You need to select one or more textures before you can place the textures",
            "Error",MB_OK|MB_ICONERROR);
        return;
    }
    readyToDrawRectangle=true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::paViewMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
    int i;
    int canvasX=viewX+X;
    int canvasY=viewY+(paView->Height-1-Y);
    int imageX,imageY;
    tTexture *t;
    canvasToImageCoords(canvasX,canvasY,imageX,imageY);

    if(Button==mbLeft)
    {
        if(readyToDrawRectangle || Shift.Contains(ssCtrl))
        {
            rectangleX1=rectangleX2=imageX;
            rectangleY1=rectangleY2=imageY;
            drawingRectangle=true;
            needsRedraw=true;
        }
        else if(positioningHotspot)
        {
            for(i=0;i<lbTextures->Items->Count;i++)
                if(lbTextures->Selected[i])
                {
                    t=(tTexture*)lbTextures->Items->Objects[i];
                    t->hotspotX=imageX-t->left;
                    t->hotspotY=imageY-t->bottom;
                    setModified(true);
                }

            lbTexturesClick(NULL);
        }
        else
        {
            if(highlightedTexture)
            {
                if(Shift.Contains(ssShift))
                {
                    invertSelection(highlightedTexture);
                    lbTexturesClick(NULL);
                }
                else
                {
                    if(isSelected(highlightedTexture))
                        postSelect=true;
                    else
                    {
                        clearSelection();
                        invertSelection(highlightedTexture);
                        lbTexturesClick(NULL);
                    }
                }

                lastMouseX=imageX;
                lastMouseY=imageY;
                resizing=true;
            }
            else
            {
                if(!Shift.Contains(ssShift))
                {
                    clearSelection();
                    lbTexturesClick(NULL);
                }
                rectangleX1=rectangleX2=imageX;
                rectangleY1=rectangleY2=imageY;
                drawingBoundingBox=true;
                needsRedraw=true;
            }
            ActiveControl=lbTextures;
        }
    }
    else if(Button==mbRight)
    {
        if(drawingRectangle || drawingBoundingBox)
        {
            drawingRectangle=false;
            drawingBoundingBox=false;
            needsRedraw=true;
        }
        else
        {
            for(i=0;i<lbTextures->Items->Count;i++)
                if(lbTextures->Selected[i])
                {
                    t=(tTexture*)lbTextures->Items->Objects[i];
                    t->hotspotX=imageX-t->left;
                    t->hotspotY=imageY-t->bottom;
                    setModified(true);
                }

            lbTexturesClick(NULL);
        }
    }

    readyToDrawRectangle=false;
    positioningHotspot=false;
}
//---------------------------------------------------------------------------
void __fastcall TMain::paViewMouseMove(TObject *Sender, TShiftState Shift,
      int X, int Y)
{
    int canvasX=viewX+X;
    int canvasY=viewY+(paView->Height-1-Y);
    int imageX,imageY;
    canvasToImageCoords(canvasX,canvasY,imageX,imageY);

    if(drawingRectangle || drawingBoundingBox)
    {
        rectangleX2=imageX;
        rectangleY2=imageY;
        needsRedraw=true;
    }
    else if(resizing)
    {
        postSelect=false;

        int deltaX=imageX-lastMouseX;
        int deltaY=imageY-lastMouseY;
        lastMouseX=imageX;
        lastMouseY=imageY;

        tTexture *t;
        int i;

        for(i=0;i<lbTextures->Items->Count;i++)
            if(lbTextures->Selected[i])
            {
                t=(tTexture*)lbTextures->Items->Objects[i];
                if(resizeMask&RESIZE_LEFT)
                {
                    t->left+=deltaX;
                    t->width-=deltaX;
                }
                if(resizeMask&RESIZE_RIGHT)
                    t->width+=deltaX;
                if(resizeMask&RESIZE_BOTTOM)
                {
                    t->bottom+=deltaY;
                    t->height-=deltaY;
                }
                if(resizeMask&RESIZE_TOP)
                    t->height+=deltaY;
                setModified(true);
            }
        lbTexturesClick(NULL);
    }
    else
        updateHighlightedTexture(canvasX, canvasY);
}
//---------------------------------------------------------------------------
void __fastcall TMain::paViewMouseUp(TObject *Sender, TMouseButton Button,
      TShiftState Shift, int X, int Y)
{
    int i;
    tTexture *t;
    int canvasX=viewX+X;
    int canvasY=viewY+(paView->Height-1-Y);
    int imageX,imageY;
    canvasToImageCoords(canvasX,canvasY,imageX,imageY);

    if(Button==mbLeft)
    {
        if(drawingRectangle || drawingBoundingBox)
        {
            if(rectangleX2<rectangleX1)
                swap(rectangleX1,rectangleX2);
            if(rectangleY2<rectangleY1)
                swap(rectangleY1,rectangleY2);

            if(drawingRectangle)
            {
                for(i=0;i<lbTextures->Items->Count;i++)
                    if(lbTextures->Selected[i])
                    {
                        t=(tTexture*)lbTextures->Items->Objects[i];
                        t->left=rectangleX1;
                        t->bottom=rectangleY1;
                        t->width=rectangleX2-rectangleX1;
                        t->height=rectangleY2-rectangleY1;
                        setModified(true);
                    }
            }
            else
            {
                for(i=0;i<lbTextures->Items->Count;i++)
                {
                    t=(tTexture*)lbTextures->Items->Objects[i];
                    if(!(t->left>rectangleX2 || t->left+t->width<rectangleX1 ||
                        t->bottom>rectangleY2 || t->bottom+t->height<rectangleY1))
                        lbTextures->Selected[i]=true;
                }
            }
            lbTexturesClick(NULL);
        }
        else if(postSelect)
        {
            if(!Shift.Contains(ssShift))
                clearSelection();
            if(highlightedTexture)
                invertSelection(highlightedTexture);
            lbTexturesClick(NULL);
        }
    }

    drawingRectangle=false;
    drawingBoundingBox=false;
    resizing=false;
    postSelect=false;

    updateHighlightedTexture(canvasX, canvasY);
}
//---------------------------------------------------------------------------
void __fastcall TMain::btPositionHotspotClick(TObject *Sender)
{
    resetMouseState();
    if(lbTextures->SelCount<1)
    {
        Application->MessageBox("You need to select one or more textures before you can position the hotspot",
            "Error",MB_OK|MB_ICONERROR);
        return;
    }
    positioningHotspot=true;
}
//---------------------------------------------------------------------------
void TMain::resetMouseState()
{
    readyToDrawRectangle=false;
    drawingRectangle=false;
    drawingBoundingBox=false;
    positioningHotspot=false;
    needsRedraw=true;
}
//---------------------------------------------------------------------------
void TMain::updateHighlightedTexture(int canvasX, int canvasY)
{
    int imageX,imageY;
    int l,r,b,t;
    canvasToImageCoords(canvasX,canvasY,imageX,imageY);
    setHighlightedTexture(findTextureAt(imageX,imageY));
    needsRedraw=true;

    resizeMask=0;
    if(highlightedTexture)
    {
        imageToCanvasCoords(highlightedTexture->left,highlightedTexture->bottom,l,b);
        imageToCanvasCoords(highlightedTexture->left+highlightedTexture->width,
            highlightedTexture->bottom+highlightedTexture->height,r,t);

        if(canvasX<l+5)
        {
            if(canvasY<b+5)
            {
                paView->Cursor=crSizeNESW;
                resizeMask|=RESIZE_LEFT|RESIZE_BOTTOM;
            }
            else if(canvasY>t-5)
            {
                paView->Cursor=crSizeNWSE;
                resizeMask|=RESIZE_LEFT|RESIZE_TOP;
            }
            else
            {
                paView->Cursor=crSizeWE;
                resizeMask|=RESIZE_LEFT;
            }
        }
        else if(canvasX>r-5)
        {
            if(canvasY<b+5)
            {
                paView->Cursor=crSizeNWSE;
                resizeMask|=RESIZE_RIGHT|RESIZE_BOTTOM;
            }
            else if(canvasY>t-5)
            {
                paView->Cursor=crSizeNESW;
                resizeMask|=RESIZE_RIGHT|RESIZE_TOP;
            }
            else
            {
                paView->Cursor=crSizeWE;
                resizeMask|=RESIZE_RIGHT;
            }
        }
        else
        {
            if(canvasY<b+5)
            {
                paView->Cursor=crSizeNS;
                resizeMask|=RESIZE_BOTTOM;
            }
            else if(canvasY>t-5)
            {
                paView->Cursor=crSizeNS;
                resizeMask|=RESIZE_TOP;
            }
            else
            {
                paView->Cursor=crSizeAll;
                resizeMask|=RESIZE_LEFT|RESIZE_BOTTOM|RESIZE_RIGHT|RESIZE_TOP;
            }
        }
    }
    else
        paView->Cursor=crDefault;
}
//---------------------------------------------------------------------------
void TMain::invertSelection(tTexture *texture)
{
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
        if((tTexture*)(lbTextures->Items->Objects[i])==texture)
        {
            lbTextures->Selected[i]=!lbTextures->Selected[i];
            return;
        }
}
//---------------------------------------------------------------------------
bool TMain::isSelected(tTexture *texture)
{
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
        if((tTexture*)(lbTextures->Items->Objects[i])==texture)
            return lbTextures->Selected[i];
    return false;
}
//---------------------------------------------------------------------------
void __fastcall TMain::miSelectImageClick(TObject *Sender)
{
    if(odSelectImage->Execute())
    {
        tString _imageFile=odSelectImage->FileName.c_str();
        _imageFile=trimPath(_imageFile);
        setImageFile(_imageFile);
        setModified(true);

        if(imageLoaded)
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Successfully loaded '")+
                imageFile.c_str()+"'";
        else
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Failed to loaded '")+
                imageFile.c_str()+"'";
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::miQuitClick(TObject *Sender)
{
    Close();
}
//---------------------------------------------------------------------------
void TMain::setModified(bool _modified)
{
    modified=_modified;
    if(modified)
        sbStatusBar->Panels->Items[1]->Text="Modified";
    else
        sbStatusBar->Panels->Items[1]->Text="Not Modified";
}
//---------------------------------------------------------------------------
void TMain::setCurrentFile(const tString& _currentFile)
{
    currentFile=_currentFile;
    if(currentFile.length()==0)
        Caption="Machinations Texture Manager - Untitled";
    else
        Caption=AnsiString("Machinations Texture Manager - ")+currentFile.c_str();
}
//---------------------------------------------------------------------------
void TMain::setImageFile(const tString& _imageFile)
{
    imageFile=_imageFile;
    if(imageFile.length()>0)
    {
        loadImage();
        if(!isPowerOf2(imageWidth) || !isPowerOf2(imageHeight))
            Application->MessageBox("The dimensions of the current image are not a power of 2","Warning",
                MB_OK|MB_ICONEXCLAMATION);
        sbStatusBar->Panels->Items[0]->Text=imageFile.c_str();
    }
    else
    {
        sbStatusBar->Panels->Items[0]->Text="<no image selected>";
        imageWidth=0;
        imageHeight=0;
        imageLoaded=false;
    }
    updateCanvasSize();
}
//---------------------------------------------------------------------------
void TMain::setHighlightedTexture(tTexture *_highlightedTexture)
{
    highlightedTexture=_highlightedTexture;
    if(highlightedTexture)
        sbStatusBar->Panels->Items[2]->Text=highlightedTexture->name.c_str();
    else
        sbStatusBar->Panels->Items[2]->Text="";
}
//---------------------------------------------------------------------------
void TMain::newTexturePack()
{
    int i;
    resetMouseState();
    setHighlightedTexture(NULL);

    setImageFile("");
    for(i=0;i<lbTextures->Items->Count;i++)
        delete (tTexture*)lbTextures->Items->Objects[i];
    lbTextures->Clear();
    lbTexturesClick(NULL);
}
//---------------------------------------------------------------------------
bool TMain::saveTexturePack(const char *filename)
{
    int i;
    long l;
    char ch;
    tTexture *t;
    FILE *fp=fopen(filename,"wb");
    if(fp==NULL)
    {
        AnsiString message=AnsiString("Failed to open '")+filename+"' for writing";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        return false;
    }
    fwrite(TPKFileSignature,strlen(TPKFileSignature),1,fp);
    writeString(imageFile,fp);

    l=(long)lbTextures->Items->Count;
    fwrite(&l,sizeof(l),1,fp);

    for(i=0;i<lbTextures->Items->Count;i++)
    {
        t=(tTexture*)lbTextures->Items->Objects[i];
        writeString(t->name,fp);

        l=(long)t->left;    fwrite(&l,sizeof(l),1,fp);
        l=(long)t->bottom;  fwrite(&l,sizeof(l),1,fp);
        l=(long)t->width;   fwrite(&l,sizeof(l),1,fp);
        l=(long)t->height;  fwrite(&l,sizeof(l),1,fp);
        l=(long)t->hotspotX;fwrite(&l,sizeof(l),1,fp);
        l=(long)t->hotspotY;fwrite(&l,sizeof(l),1,fp);

        ch=(char)t->alphaTesting;
        fwrite(&ch,sizeof(ch),1,fp);
        ch=(char)t->alphaBlending;
        fwrite(&ch,sizeof(ch),1,fp);
    }

    fclose(fp);
    lbTexturesClick(NULL);
    return true;
}
//---------------------------------------------------------------------------
bool TMain::loadTexturePack(const char *filename)
{
    char buf[8];
    int i,count;
    long l;
    char ch;
    tTexture *t;

    FILE *fp=fopen(filename,"rb");
    if(fp==NULL)
    {
        AnsiString message=AnsiString("Failed to open '")+filename+"' for reading";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        return false;
    }

    if(fread(buf,sizeof(buf),1,fp)<1 || memcmp(buf,TPKFileSignature,8)!=0)
    {
        AnsiString message=AnsiString("'")+filename+"' is not a valid Machinations texture pack";
        Application->MessageBox(message.c_str(),"Error",MB_OK|MB_ICONERROR);
        fclose(fp);
        return false;
    }

    setImageFile(readString(fp));

    fread(&l,sizeof(l),1,fp);
    count=(int)l;

    for(i=0;i<count;i++)
    {
        t=new tTexture;
        t->name=readString(fp);

        fread(&l,sizeof(l),1,fp);   t->left=(int)l;
        fread(&l,sizeof(l),1,fp);   t->bottom=(int)l;
        fread(&l,sizeof(l),1,fp);   t->width=(int)l;
        fread(&l,sizeof(l),1,fp);   t->height=(int)l;
        fread(&l,sizeof(l),1,fp);   t->hotspotX=(int)l;
        fread(&l,sizeof(l),1,fp);   t->hotspotY=(int)l;

        fread(&ch,sizeof(ch),1,fp);
        t->alphaTesting=(bool)ch;
        fread(&ch,sizeof(ch),1,fp);
        t->alphaBlending=(bool)ch;

        lbTextures->Items->AddObject(t->name.c_str(),(TObject*)t);
    }

    fclose(fp);
    return true;
}
//---------------------------------------------------------------------------
void TMain::writeString(const tString& string, FILE *fp)
{
    tString s=string+"\n";
    fputs(s.c_str(),fp);
}
//---------------------------------------------------------------------------
tString TMain::readString(FILE *fp)
{
    char buf[1000];
    fgets(buf,1000,fp);
    int len=strlen(buf);
    if(len>0 && buf[len-1]=='\n')
        buf[len-1]='\0';
    return tString(buf);
}
//---------------------------------------------------------------------------
void __fastcall TMain::miNewClick(TObject *Sender)
{
    if(!saveChangesQuery())
        return;
    newTexturePack();
    sbStatusBar->Panels->Items[3]->Text="Successfully created new texture pack";

    setCurrentFile("");
    setModified(false);

    ActiveControl=lbTextures;
}
//---------------------------------------------------------------------------
void __fastcall TMain::miOpenClick(TObject *Sender)
{
    if(!saveChangesQuery())
        return;
    if(odOpenTexturePack->Execute())
    {
        newTexturePack();

        if(loadTexturePack(odOpenTexturePack->FileName.c_str()))
        {
            setCurrentFile(odOpenTexturePack->FileName.c_str());
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Successfully opened '")+
                currentFile.c_str()+"'";
        }
        else
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Failed to open '")+
                odOpenTexturePack->FileName.c_str()+"'";

        ActiveControl=lbTextures;
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::miSaveClick(TObject *Sender)
{
    if(currentFile.length()==0)
        miSaveAsClick(NULL);
    else
    {
        saveTexturePack(currentFile.c_str());
        setModified(false);
        sbStatusBar->Panels->Items[3]->Text=AnsiString("Successfully saved '")+
            currentFile.c_str()+"'";
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::miSaveAsClick(TObject *Sender)
{
    if(sdSaveTexturePack->Execute())
    {
        if(saveTexturePack(sdSaveTexturePack->FileName.c_str()))
        {
            setCurrentFile(sdSaveTexturePack->FileName.c_str());
            setModified(false);
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Successfully saved '")+
                currentFile.c_str()+"'";
        }
        else
            sbStatusBar->Panels->Items[3]->Text=AnsiString("Failed to save '")+
                sdSaveTexturePack->FileName+"'";
    }
}
//---------------------------------------------------------------------------
void __fastcall TMain::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    CanClose=saveChangesQuery();
}
//---------------------------------------------------------------------------
bool TMain::saveChangesQuery()
{
    if(modified)
    {
        AnsiString message="Save changes to '";
        if(currentFile.length()==0)
            message+="Untitled";
        else
            message+=currentFile.c_str();
        message+="'?";
        int retval=Application->MessageBox(message.c_str(),"Confirmation",MB_YESNOCANCEL|MB_ICONQUESTION);
        if(retval==IDCANCEL)
            return false;
        else if(retval==IDYES)
            miSaveClick(NULL);
    }
    return true;
}
//---------------------------------------------------------------------------
void TMain::loadWorkingDirectory()
{
    FILE *fp=fopen("texmngr.ini","rb");
    if(fp==NULL)
        return;
    workingDirectory=readString(fp);
    fclose(fp);
}
//---------------------------------------------------------------------------
bool TMain::saveWorkingDirectory(const tString& _workingDirectory)
{
    workingDirectory=_workingDirectory;

    FILE *fp=fopen("texmngr.ini","wb");
    if(fp==NULL)
        return false;
    writeString(workingDirectory,fp);
    fclose(fp);

    return true;
}
//---------------------------------------------------------------------------
void __fastcall TMain::miSetWorkingDirectoryClick(TObject *Sender)
{
    AnsiString _workingDirectory=
        InputBox("Set Working Directory","Enter new working directory:",workingDirectory.c_str());

    if(saveWorkingDirectory(_workingDirectory.c_str()))
        sbStatusBar->Panels->Items[3]->Text=AnsiString("Successfully saved new working directory to 'texmngr.ini'");
    else
        sbStatusBar->Panels->Items[3]->Text=AnsiString("Failed to save new working directory to 'texmngr.ini'");

    odSelectImage->InitialDir=workingDirectory.c_str();
    odOpenTexturePack->InitialDir=workingDirectory.c_str();
    sdSaveTexturePack->InitialDir=workingDirectory.c_str();
}
//---------------------------------------------------------------------------
void TMain::selectAllTextures()
{
    int i;
    for(i=0;i<lbTextures->Items->Count;i++)
        lbTextures->Selected[i]=true;
}
//---------------------------------------------------------------------------
tString TMain::trimPath(const tString& s)
{
    int i=s.length();
    for(i--;i>=0;i--)
        if(s[i]=='/' || s[i]=='\\')
            break;
    i++;
    return tString(&s[i]);
}
//---------------------------------------------------------------------------
bool TMain::isPowerOf2(int x)
{
    while((x&1)==0)
        x>>=1;
    return (x==1);
}
//---------------------------------------------------------------------------
/* OLD CODE FOR CONVERTING OLD FORMAT

    int i;
    tTexture *texture;
    AnsiString s=InputBox("Enter values","Enter values","");
    int l,r,t,b,hx,hy;
    if(sscanf(s.c_str(),"%d %d %d %d %d %d",&l,&r,&t,&b,&hx,&hy)<6)
        return;

    for(i=0;i<lbTextures->Items->Count;i++)
        if(lbTextures->Selected[i])
        {
            texture=(tTexture*)lbTextures->Items->Objects[i];
            texture->left=l;
            texture->bottom=imageHeight-b;
            texture->width=r-l;
            texture->height=b-t;
            texture->hotspotX=-(l-hx);
            texture->hotspotY=-(hy-b);
            setModified(true);
        }

    lbTexturesClick(NULL);*/

