/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
****************************************************************************/
#ifndef memwatchH
#define memwatchH
/****************************************************************************/
#ifndef CACHE_HEADERS
    #include <stdlib.h> //for size_t
#endif
#define MAX_RECORDS  1000000

#include "types.h"

struct tMemoryRecord
{
    void *address;
    size_t bytes;
    const char *file;
    int line;
};

#ifdef MEMWATCH
    #define mwNew new(__FILE__,__LINE__)
    #define mwDelete (mwFile=__FILE__,mwLine=__LINE__),delete
    #define MEMORY_VIOLATION(x)     (!isMemoryValid((void*)(x),sizeof(*x),__FILE__,__LINE__))
    #define ARRAY_VIOLATION(x,y)    (!isMemoryValid((void*)(x),sizeof(*x)*(y),__FILE__,__LINE__))
    extern const char *mwFile;
    extern int mwLine;

    extern void *operator new(size_t size);
    extern void *operator new[](size_t size);
    extern void *operator new(size_t size, const char *file, int line);
    extern void *operator new[](size_t size, const char *file, int line);
    extern void operator delete(void *ptr);
    extern void operator delete[](void *ptr);
    extern bool isMemoryValid(void *ptr, size_t bytes, const char *file, int line);
#else
    #define mwNew new
    #define mwDelete delete
    #if defined(DEBUG)
        #define MEMORY_VIOLATION(x)     (!isMemoryValid((void*)(x),sizeof(*x),__FILE__,__LINE__))
        #define ARRAY_VIOLATION(x,y)    (!isMemoryValid((void*)(x),sizeof(*x)*(y),__FILE__,__LINE__))
    #else
        #define MEMORY_VIOLATION(x)   false
        #define ARRAY_VIOLATION(x,y)  false
    #endif
    inline bool isMemoryValid(void *ptr, size_t bytes, const char *file, int line)
    {
        if(ptr==NULL)
        {
            bug("NULL pointer detected in %s:%d!",file,line);
            return false;
        }
        return true;
    }
#endif

extern void memoryStartup();
extern void memoryShutdown();
/****************************************************************************/
#endif


