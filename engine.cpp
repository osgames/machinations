/****************************************************************************
* Machinations copyright (C) 2001-2003 by Jon Sargeant and Jindra Kolman    *
*                                                                           *
* ENGINE.CPP:      Stores and manages unit, terrain, and player data.       *
*                                                                           *
* This program is free software; you can redistribute it and/or             *
* modify it under the terms of the GNU General Public License               *
* version 2 as published by the Free Software Foundation                    *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License for more details.                              *
****************************************************************************/
#include "headers.h"
#pragma hdrstop
/****************************************************************************/
#include "engine.h"
#include "message.h"
#include "oglwrap.h"
#include "client02.h"
#include "game.h"
#include "unit.h"
#include "cache.h"
#ifndef BORLAND
    #include "snprintf.h"
#endif

#pragma package(smart_init)

/****************************************************************************
                              Global Variables
 ****************************************************************************/
long        latency = 200;          //Game's latency (milliseconds)
long        netInterval = 200;      //Frequency at which engine executes high-level commands (milliseconds)
long        gameInterval = 100;     //Frequency at which engine makes calculations (milliseconds)
float       gameSpeed = 1.0;        //Proportion of game seconds to real seconds
bool        gameRunning = false;    //Is a game in progress? (true even if game is frozen)
bool        gameFrozen = false;     //Is the game frozen?  The game freezes if a user
                                    //pauses the game or the engine fails to receive
                                    //a packet from a remote engine.
tVector2D   mapDimensions = zeroVector2D;
                                    //Vector that traverses the entire map to the upper-right corner

bool        mapLoaded = false;      //Has the engine retrieved map information?
bool        objectsLoaded = false;  //Has the engine initialized game objects?
bool        worldLoaded = false;    //Has the engine loaded the world?
tFile       mapFile;                //File details for map (undefined if not loaded)
tFile       worldFile;              //File details for world file (undefined if not loaded)

int         hashWidth = 0;          //Number of columns in unit hash
int         hashHeight = 0;         //Number of rows in unit hash
long        hashSize = 0;           //Total number of cells in unit hash
                                        //Equals hashWidth * hashHeight * MAX_PLAYERS

//Coordinates of the point that is at the center of the view:
tVector2D   mapOffset=zeroVector2D;
//View's current rotation (in radians)
//  CCW -> POSITIVE
//  CW -> NEGATIVE
float       mapRotation=0;
//View's tilt (in radians)
float       mapTilt=M_PI/4; //0 is upright, M_PI/2 is sideways
//View's scale
float       mapScale=0.025;

//Data for loading the projection matrix:
//                               col1     col2     col3     col4
GLfloat viewMatrix[16] =        {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
GLfloat invViewMatrix[16] =     {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
GLfloat minimapMatrix[16] =     {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
GLfloat invMinimapMatrix[16] =  {1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};
GLfloat panMatrix[4]=           {1,0,     0,1                      };
GLfloat shadowTextureMatrix[16]={1,0,0,0, 0,1,0,0, 0,0,1,0, 0,0,0,1};

//Terrain data:
tTerrain*            terrain = NULL;

//Multi-purpose quad:
GLUquadricObj*       globalQuadricObject=NULL;

//The engine remembers the dimensions of the view and minimap for convenience.
//We must initialize these variables to non-zero values so that orientMap will
//function properly.
int                  viewWidth=1;
int                  viewHeight=1;
int                  minimapWidth=1;
int                  minimapHeight=1;

float                viewMinX=0;
float                viewMaxX=0;
float                viewMinY=0;
float                viewMaxY=0;

int                  shadeMapSize=0;
int                  alphaMapSize=0;
int                  waterMapSize=0;
int                  shadowMapSize=0;
bool                 terrainWireframe=false;

int                  testValue=20, testValue2=0; //Debugging purposes only

tSkin*               shockwaveSkin=NULL; //Temporary texture for experimentation

iTextureIndex        shadowTexture=0; //Texture containing the shadows--updated before each frame.

/*****************************************************************************
                              Static Variables
 *****************************************************************************/
static int                  localPlayerID = -1;             //Index of local player in the array below
static tPlayer              players[MAX_PLAYERS];           //Player data
static tResource            resources[MAX_RESOURCES];       //Resource type data (e.g. minerals, gas)
static tArmorClass          armorClasses[MAX_ARMOR_CLASSES];//Armor class data (e.g. piercing, bashing, etc.)
static float                shadowOpacity=0;                //The shadow opacity defined in the world file.  This
                                                              //The variable doesn't belong anywhere, so I decided
                                                              //to put it with the other map state variables.
static float                shadowLightPosition[4]={0,0,0,0};//A homogeneous vector representing the position
                                                            //of the light that casts shadows
static vector<tLight>       lights;                         //Light data
static float                ambientLight[4];                //Ambient light for the whole map

static tGameTime            gameTime = 0;                   //Game milliseconds elapsed sinced the game started
static tGameTime            nextIntervalTick = 0;           //Time when next worldIntervalTick will occur
static tGameTime            gameTimeBias = 0;               //Used to calculate gameTime
static tTime                gameTimeRef;                    //Used to calculate gameTime
static tWorldTime           t0 = 0;                         //Time of last intervalTick
static tWorldTime           worldTimeBias = 0;              //Used to calculate worldTime from gameTime

//Dynamic array of game messages sorted by execution time (next message to be executed on top):
static vector<tMessage *>   messages;

/* On some compilers, the variable name 'messages' conflicts with the std
 * namespace producing the following error message:
 *      Ambiguity between 'messages' and 'std::messages'
 * Therefore, I explicitily define messages without a namespace after the
 * definition above.
 */
#define messages ::messages

//Dynamic array of tasks that the engine will repeatedly execute:
static vector<tTask *>      tasks;

//Pointer to a triple-array containing linked lists of units organized by
//position and owner:
static tUnit**              unitHash = NULL;

//Dynamic array containing concentric rings of circles where the user has issued
//a move command.
static vector<tBullsEye*>   bullsEyes;

//Dynamic array containing unit explosion--must be separated from units since units
//may become invisible after death.
static vector<tUnitExplosion *> unitExplosions;

static bool                 showUnitStats = false;  //Display unit ID, health, and hash value?

static bool                 syncEnabled = true;     //Will the host periodically look for discrepancies?
static int                  syncInterval = 1000;    //Number of milliseconds between each sync query
static tGameTime            syncQueryTimer = 0;     //Time when the host posted the last sync query
static tGameTime            syncResponseTimer = 0;  //Time when the client processed the last sync query
static bool                 syncReceived = false;   //Has the client processed a new sync query since its
                                                        //last response?
static long                 syncUnitCount = 0;      //Number of units at the last sync query

//Additional data for ensuring synchronization of missiles:
#ifdef DEBUGMISSILE
static long                 syncMissileCount;
static long                 syncMissileData[4000];
#endif

//Apparitions show the position of new buildings as the player moves the mouse.
static tUnitType*           apparitionType = NULL;
static tVector3D            apparitionPosition = zeroVector3D;
static tVector3D            apparitionForward = zeroVector3D;
static tVector3D            apparitionSide = zeroVector3D;
static tVector3D            apparitionUp = zeroVector3D;
static int                  apparitionOwner = 0;

//Command flooding data:
//If enabled, the engine will flood the network with random commands at a set
//interval.  This allows me to test network synchronization under maximum load.
static bool                 floodingEnabled = false;    //Is command flooding enabled?
static tGameTime            lastRandomCommand = 0;      //When was the last random command posted?
static tGameTime            floodingInterval = 0;       //How often does the engine post random commands?

//Fog of war data:
static int                  fogOfWarWidth = 0;          //Number of columns in each player's fog of war table
                                                            //Equals (int)mapDimensions.x+1
static int                  fogOfWarHeight = 0;         //Number of rows in each player's fog of war table
                                                            //Equals (int)mapDimensions.y+1
static int                  fogOfWarSize = 0;           //Total number of cells in each player's fog of war table
                                                            //Equals fogOfWarWidth * fogOfWarHeight

static tTime                fileInspectionTimer;        //Used to time file inspections (see engineTick)

static bool                 gameWaiting=false;          //True when the host is waiting on one or more clients

//I use these variables to quickly clip units that are outside the view:
static tVector3D            frustumCenter=zeroVector3D; //The average of the frustum's eight vertices
static float                frustumRadius=0;            //The maximum distance between the frustum's center and a vertex
static float                frustumMinX=0;
static float                frustumMaxX=0;
static float                frustumMinY=0;
static float                frustumMaxY=0;
static float                frustumMinZ=0;
static float                frustumMaxZ=0;

/****************************************************************************
                               Static Functions
 ****************************************************************************/
static void         initializeGame();
static void         shutdownGame();
static void         processMessages(tGameTime t, bool inclusive);
static void         processMessage(tMessage *message) throw(int);
static void         dispatchMessage(tMessage *message);
static void         transmitSettings(tDescriptor *desc);
static void         broadcastPlayers(tMessage *message);
static void         broadcastNonPlayers(tMessage *message);
static void         broadcastAllies(tMessage *message);
static void         broadcastEnemies(tMessage *message);

static void         deleteObjects();
static void         resetPlayers();
static void         loadWorld() throw(tString);
static void         unloadWorld();
static void         loadMap() throw(tString);
static void         unloadMap();
static void         worldTick(tWorldTime t);
static void         worldIntervalTick(tWorldTime t);

static int          playerLookup(tUser *user);
static int          getNextSlot();

static void         flashPoint(tVector3D v);
static void         addNeighbor(tUnit *unit, long hashValue);
static void         removeNeighbor(tUnit *unit, long hashValue);

static void         orientMap();
static void         drawApparition();
static tWorldTime   toWorldTime(tGameTime t);
static bool         inspectFiles();
static void         refreshShadowTexture();
static void         refreshLights();

//Message events:
static void         onSetTimingInGameMsg(tSetTimingMsg *message)        throw(int);
static void         onSyncQueryMsg      (tGameMsg *message)             throw(int);
static void         onUnitPlanMsg       (tUnitCreateMsg *message)       throw(int);
static void         onUnitCreateMsg     (tUnitCreateMsg *message)       throw(int);
static void         onUnitDestroyMsg    (tUnitMsg *message)             throw(int);
static void         onUnitSetPosMsg     (tUnitVector2DMsg *message)     throw(int);
static void         onUnitSetVelocityMsg(tUnitVector2DMsg *message)     throw(int);
static void         onUnitSetForwardMsg (tUnitVector2DMsg *message)     throw(int);
static void         onUnitSetHPMsg      (tUnitFloatMsg *message)        throw(int);
static void         onUnitSetOwnerMsg   (tUnitLongMsg *message)         throw(int);
static void         onSetAllianceMsg    (tSetAllianceMsg *message)      throw(int);
static void         onSetResourceMsg    (tSetResourceMsg *message)      throw(int);
static void         onDepositCreateMsg  (tDepositCreateMsg *message)    throw(int);
static void         onDepositDestroyMsg (tDepositDestroyMsg *message)   throw(int);
static void         onDepositSetMsg     (tDepositSetMsg *message)       throw(int);

//High-level parsing functions for loading world and map files:
static void         parseWorld(tStream& s) throw(tString);
static void         parseMap(tStream& s) throw(tString);
static void         parsePlayerBlock(int slot, tStream& s) throw(tString);
static void         parseLightBlock(tStream& s) throw(tString);
static void         parseDepositBlock(tStream& s) throw(tString);
static void			parseResourceBlock(int slot, tStream& s) throw(tString);

//Low-level parsing functions used by parseShellCommand:
static tUnit*       parseUnit(const char *fieldName, tStream& s) throw(tString);
static void         parseUnits(const char *fieldName, vector<long>& u, tStream& s) throw(tString);
static int          parseShellPlayer(const char *fieldName, tStream& s) throw(tString);
static void         parsePlayers(const char *fieldName, vector<int>& p, tStream& s) throw(tString);
static void         parseResources(const char *fieldName, vector<int>& r, tStream& s) throw(tString);
static tVector2D    parseShellPos(const char *fieldName, tStream& s) throw(tString);
static tVector2D    parseVelocity(const char *fieldName, tStream& s) throw(tString);
static tVector2D    parseForward(const char *fieldName, tStream& s) throw(tString);
static ePlayer      parsePlayerType(const char *fieldName, tStream& s) throw(tString);



/****************************************************************************
                            Unit Static Variables
 ****************************************************************************/
static vector<tUnit *>      units;                  //Unit container sorted by ID
static vector<tMissile *>   missiles;               //Missile container
static vector<tObstacle>    obstacles;              //Obstacle container
static vector<tUnit *>      selectedUnits;          //Current selection container
static vector<tDeposit *>   deposits;               //Deposit container
static int                  nextUnitID=1;           //Unique unit ID--does not reuse earlier ID's
static int                  nextDepositID=1;        //Unique deposit ID--does not reuse earlier ID's



/****************************************************************************
                            Unit Static Functions
 ****************************************************************************/
static tUnit*       findUnitAt(int x, int y, bool alliesFirst);
static tUnit*       findUnitOnMinimap(int centerX, int centerY, bool alliesFirst);
static void         findUnitsInRange(const tVector2D& orig, const tVector2D& dest, vector<tUnit *>& output);
static int          pickUnits(int x, int y, int w, int h, GLuint *buf, bool convexShell);
static void         selectUnitType(tUnitType *ut, eAlliance alliance);
static void         deselectUnitType(tUnitType *ut, eAlliance alliance);
static void         selectUnitTypeInView(tUnitType *ut, eAlliance alliance);
static void         deselectUnitTypeInView(tUnitType *ut, eAlliance alliance);
static void         updateApparitionOwner();
static tDeposit*    depositLookup(int ID);
static tDeposit*    createDeposit(int resource, const tVector2D& center, float radius, float amount, tWorldTime t);
static void         destroyDeposit(tDeposit *d, tWorldTime t);

static inline bool  isWithinView(tUnit *u);
static inline bool  isWithinView(tUnitExplosion *u);


/***************************************************************************\
engineStartup

Call this function during program startup to initialize the engine.  The
function creates a multi-purpose quadric, initializes the player slots,
and loads the model types.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void engineStartup()
{
    globalQuadricObject=gluNewQuadric();

    shockwaveSkin=getSkin("data/textures/shockwave.png");

    glGenTextures(1, &shadowTexture);
    refreshShadowTexture();
}

/***************************************************************************\
engineRefresh

Call this function when the user toggles windowed/full screen mode.  The
function calls tTerrain::refresh to reconstruct the minimap.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void engineRefresh()
{
    if(terrain)
        terrain->refresh();

    refreshModelTypes();
    refreshShadowTexture();
    refreshLights();
}

/***************************************************************************\
engineShutdown

Call this function during program shutdown.  The function destroys the multi-
purpose quadric, deallocates all memory, and shuts down the current network
session.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void engineShutdown()
{
    gluDeleteQuadric(globalQuadricObject);

    //Free any memory still allocated by the map and world
    unloadMap();
    unloadWorld();

    sessionShutdown();
    networkShutdown();

    releaseSkin(shockwaveSkin);
    shockwaveSkin=NULL;

    glDeleteTextures(1, &shadowTexture);
    shadowTexture=0;
}

/***************************************************************************\
engineTick

Call this function during each program pass to manage socket connection and
execute tasks.  The function will also update the game if one is in progress.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void engineTick()
{
    int i,j,q;
    tGameTime minStopTime=LONG_MAX;
    bool paused,r;
    long u1,u2;
    tVector2D v;
    tUser *u;

    //Manage socket connections:
    START_TIMER(TIMER_NETWORK_TICK)
    networkTick();
    STOP_TIMER(TIMER_NETWORK_TICK)

    //Update the game if one is in progress:
    if(gameRunning)
    {
        //Reality check!  Verify that localPlayerID fits within the allowed
        //range just in case it got cleared somehow...
#ifdef DEBUG
        if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
        {
            bug("engineTick: local player not in range");
            return;
        }
#endif
        /* Calculate how much longer the game can proceed before the engine
         * must receive another 'end interval' message.  Naturally when you
         * are playing alone, the game can proceed indefinately.  When the
         * network contains three or more computers, the game is limited by
         * the slowest connection.
         */
        if(networkGame)
            for(int i=0; i<MAX_PLAYERS; i++)
                if(players[i].assignedType==PLAYER_HUMAN &&
                    MAX(players[i].stopTime,players[i].lowerBound)<minStopTime)
                    minStopTime=players[i].stopTime;

        //Determine if local player has paused the game:
        paused=players[localPlayerID].gamePaused;

        //Unfreeze the game if the stop time exceeds the game time and
        //the local player hasn't paused the game.
        if(gameFrozen && gameTime<minStopTime && !paused)
        {
            //The current system time become the new reference point in the
            //equation for gameTime below:
            gameTimeRef = currentTime;

            gameFrozen = false;
            onUnfreezeGame();
        }

        if(!gameFrozen)
        {
            //Calculate the new game time using a bias and a reference point:
            gameTime = gameTimeBias + (long)((currentTime - gameTimeRef)*1000);

            //Freeze the game if the game time exceeds the stop time or the
            //local player paused the game.
            if(gameTime>minStopTime || paused)
            {
                //Cap the game time at the stop time:
                if(gameTime>minStopTime)
                    gameTime = minStopTime;

                //The game clock will begin at gameTime when the game resumes:
                gameTimeBias = gameTime;

                gameFrozen = true;
                onFreezeGame();
            }

            /* After each network interval, broadcast an 'end of interval' message.
             * This message informs peers that no more messages will arrive from
             * this interval, so the game can proceed to the next interval.  If we
             * haven't called this function for a few seconds, we may need to
             * send multiple 'end of interval' messages.
             */
            if(networkGame) //Single-player games do not use the network.
                while(MAX(players[localPlayerID].stopTime,players[localPlayerID].lowerBound) -
                    gameTime < latency)
                {
                    tGameMsg message(MSG_END_INTERVAL);
                    message.playerID = localPlayerID;
                    broadcastPlayers(&message);

                    /* According to our algorithm, we should send an 'end of
                     * interval' message to every peer, including ourselves.
                     * Hence, we add netInterval to our own stopTime as if we
                     * had just received an 'end of interval' from ourselves.
                     */
                    players[localPlayerID].stopTime += netInterval;
                }
            if(!gameFrozen)
            {
                /* After each game interval, we call interval tick to perform
                 * crucial calculations that need to be synchronized.  Before
                 * calling interval tick, we need refresh the world via
                 * worldTick.  We also need to ensure that we have received
                 * and processed all messages up that point.  NOTE: We cannot
                 * call interval tick -exactly- one game interval after the
                 * last interval tick because we the game interval may coincide
                 * with a network interval.
                 */
                while(gameTime > nextIntervalTick)
                {
                    //Process queued messages -through- lastIntervalTick and
                    //tick the map:
                    processMessages(nextIntervalTick, /* inclusive = */ true);

                    worldIntervalTick(toWorldTime(nextIntervalTick));

                    nextIntervalTick += gameInterval;
                }

                //Process queued messages -up to- gameTime and tick the map:
                processMessages(gameTime, /* inclusive =*/ false);
            }
        }

        //Update bull's eyes:
        //Tick each bull's eye and destroy it if it has finished.
        for(i=0; i<bullsEyes.size();)
        {
            if(bullsEyes[i]->a.isFinished())
            {
                mwDelete bullsEyes[i];
                bullsEyes.erase(bullsEyes.begin()+i);
            }
            else
            {
                bullsEyes[i]->a.tick(currentTime);
                i++;
            }
        }

        //Ensure game synchronization periodically:
        if(isGameHost && networkGame && syncEnabled && gameTime-syncQueryTimer>=syncInterval)
        {
            syncQueryTimer=gameTime;
            tGameMsg message(MSG_SYNC_QUERY);
            postMessage(&message);
        }

        //Clients wait to acknowledge sync query:
        if(syncReceived && gameTime-syncResponseTimer>SYNC_DELAY)
        {
            syncReceived=false;
            char buf[BUFFER_SIZE], *ptr=buf;

            *((tGameTime *)ptr)=syncResponseTimer;
            ptr+=sizeof(tGameTime);
            *((short *)ptr)=localPlayerID;
			ptr+=sizeof(short);
#ifdef DEBUGMISSILE
            *((long *)ptr)=syncMissileCount;
			ptr+=sizeof(long);
            for(i=0;i<syncMissileCount*4;i++)
			{
                *((long *)ptr)=syncMissileData[i];
				ptr+=sizeof(long);
			}
#endif
            *((long *)ptr)=syncUnitCount;
			ptr+=sizeof(long);
            for(i=0;i<units.size();i++)
            {
                *((long *)ptr)=units[i]->getID();
				ptr+=sizeof(long);
                *((float *)ptr)=units[i]->getSyncPos().x;
				ptr+=sizeof(float);
                *((float *)ptr)=units[i]->getSyncPos().y;
				ptr+=sizeof(float);
                *((float *)ptr)=units[i]->getSyncHP();
				ptr+=sizeof(float);
            }
            tSyncMsg message(MSG_SYNC, ptr-buf, buf);
            sendToHost(&message);
        }

        //Command flooding routine:
        if(floodingEnabled)
        {
            while(gameTime-lastRandomCommand>floodingInterval)
            {
                lastRandomCommand+=floodingInterval;
                i=(rand()%(commandCount-1))+1; //Select a command at random between 1 and commandCount-1
                if(i==COMMAND_SELF_DESTRUCT)
                    continue;

                j=rand()%units.size();
                u1=units[j]->getID();
                switch(commandTable[i].paramType)
                {
                    case PARAM_NONE:
                    {
                        tCommandMsg m1((eCommand)i,&u1,1);
                        postMessage(&m1);
                        break;
                    }
                    case PARAM_POSITION:
                    case PARAM_CONSTRUCTION:
                    {
                        v.x=rand()%((int)mapDimensions.x);
                        v.y=rand()%((int)mapDimensions.y);
                        tCommandMsg m2((eCommand)i,&u1,1,v);
                        postMessage(&m2);
                        break;
                    }
                    case PARAM_UNIT:
                    {
                        j=rand()%units.size();
                        u2=units[j]->getID();
                        tCommandMsg m3((eCommand)i,&u1,1,&u2,1);
                        postMessage(&m3);
                        break;
                    }
                    case PARAM_PRODUCTION:
                    {
                        q=(rand()%5)-2;
                        r=(bool)(rand()%2);
                        tCommandMsg m4((eCommand)i,&u1,1,q,r);
                        postMessage(&m4);
                        break;
                    }
                }
            }
        }
    }

    //Network game hosts and single-players inspect map and world files to see if they have changed.
    //We restrict the inspection to a reasonable interval in case 'stat' blocks
    //or does other ugly things to processor usage.
    if(currentTime-fileInspectionTimer>FILE_INSPECTION_INTERVAL && !gameRunning && isGameHost)
    {
        fileInspectionTimer=currentTime;
        inspectFiles();
    }
    //The primary cause for delay is waiting to transmit files or receive acknowledgements.
    //Thus, this function will not flood 'stat' and there's no reason to clamp the interval.
    if(gameWaiting)
        startGame();

    //Send the world first since we cannot load the map until we load the world.
    if(isGameHost)
        for(u=firstUser;u;u=u->next)
            if(u->descriptor && u->descriptor->fOut==NULL && u->ackReceived)
            {
                if(!u->haveWorld)
                    sendFile(u->descriptor, worldFile);
                else if(!u->haveMap)
                    sendFile(u->descriptor, mapFile);
            }

    //Update tasks:
    //Tick each task or destroy it if it has finished.
    for(i=0; i<tasks.size();)
    {
        if(tasks[i]->finished)
        {
            mwDelete tasks[i];
            tasks.erase(tasks.begin() + i);
        }
        else
        {
            //Each task has permission to execute for TASK_DURATION starting
            //at currentTime:
            tasks[i]->tick(currentTime, TASK_DURATION);
            i++;
        }
    }
}

/***************************************************************************\
processMessages

Processes overdue messages -up to- or -through- the specified time.  The function
ticks the map before each set of concurrent messages and once more at the specified
time.

Inputs:
    t           Time through which to process messages/tick map
    inclusive   Processes messages -through- the specified time if true
                Processes messages -up to- the specified time if false
Outputs:
    none
\***************************************************************************/
void processMessages(tGameTime t, bool inclusive)
{
    tGameTime tt=0;
    //Traverse the message queue in search of overdue messages.
    while(messages.size() > 0 && (t > messages[0]->executionTime ||
        inclusive && t == messages[0]->executionTime))
    {
        //Don't tick the world unless the message's execution time differs from
        //the former.
        if(messages[0]->executionTime > tt)
        {
            tt=messages[0]->executionTime;
            worldTick(toWorldTime(tt));
        }
        //It is processMessage's responsibility to delete the message:
        processMessage(messages[0]);

        //Remove the expired message reference from the queue:
        messages.erase(messages.begin());
    }
    //Tick the world if the specified time differs from the last message:
    if(t>tt)
        worldTick(toWorldTime(t));
}

/***************************************************************************\
broadcastPlayers

Broadcasts a message to users who are playing in this game.  The function
calls writeToBuffer for each eligible user with a descriptor (hence exluding
yourself).

Inputs:
    message     Message to broadcast
Outputs:
    none
\***************************************************************************/
void broadcastPlayers(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
#endif
    for(int i=0; i<MAX_PLAYERS; i++)
        if(players[i].assignedUser && players[i].assignedUser->descriptor)
            writeToBuffer(players[i].assignedUser->descriptor, message);
}

/***************************************************************************\
broadcastNonPlayers

Broadcasts a message to users who are *not* participating in this game.  The
function calls writeToBuffer for each eligible user with a descriptor (hence
exluding yourself).

Inputs:
    message     Message to broadcast
Outputs:
    none
\***************************************************************************/
void broadcastNonPlayers(tMessage *message)
{
    int i;
    tUser *u;
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
#endif
    for(u=firstUser; u; u=u->next)
    {
        for(i=0; i<MAX_PLAYERS; i++)
            if(u==players[i].assignedUser)
                break;
        //Skip users who aren't playing or don't have a descriptor:
        if(i==MAX_PLAYERS && u->descriptor)
            writeToBuffer(u->descriptor, message);
    }
}

/***************************************************************************\
broadcastAllies

Broadcasts a message to your allies in the current game.  The function
calls writeToBuffer for each eligible user with a descriptor (hence exluding
yourself).

Inputs:
    message     Message to broadcast
Outputs:
    none
\***************************************************************************/
void broadcastAllies(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
    if(!gameRunning)
    {
        bug("broadcastAllies: game must be running");
        return;
    }
#endif
    for(int i=0; i<MAX_PLAYERS; i++)
        if(players[i].assignedUser && players[i].assignedUser->descriptor && getAlliance(i)<=ALLIANCE_ALLY)
            writeToBuffer(players[i].assignedUser->descriptor, message);
}

/***************************************************************************\
broadcastEnemies

Broadcasts a message to enemies and neutral players in the current game.  The
function calls writeToBuffer for each eligible user with a descriptor (hence
exluding yourself).

Inputs:
    message     Message to broadcast
Outputs:
    none
\***************************************************************************/
void broadcastEnemies(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
    if(!gameRunning)
    {
        bug("broadcastEnemies: game must be running");
        return;
    }
#endif
    for(int i=0; i<MAX_PLAYERS; i++)
        if(players[i].assignedUser && players[i].assignedUser->descriptor && getAlliance(i)>=ALLIANCE_NEUTRAL)
            writeToBuffer(players[i].assignedUser->descriptor, message);
}

/***************************************************************************\
sendTextToAllies

Broadcasts a text message to your allies in the current game.

Inputs:
    text        Null-terminated string
Outputs:
    none
\***************************************************************************/
void sendTextToAllies(const char *text, eTextMessage type)
{
    tTextMsg message(text,type);
    broadcastAllies(&message);
}

/***************************************************************************\
sendTextToEnemies

Broadcasts a text message to your enemies in the current game.

Inputs:
    text        Null-terminated string
Outputs:
    none
\***************************************************************************/
void sendTextToEnemies(const char *text, eTextMessage type)
{
    tTextMsg message(text,type);
    broadcastEnemies(&message);
}

/***************************************************************************\
startGame

The host calls this function to commence the game after choosing a map,
assigning players, and setting the timing.  It ensures that every player
has the map and world.  It also ensures that the user has assigned
the host to a player, as the host must participate in every game.  This done,
it closes open slots, broadcasts a 'start game' message, and initializes the
game.

Added 5/12/03: The function also inspects the map and world files to make sure
that they are current.

Inputs:
    none
Outputs:
    Returns true if successful.  Returns false is the game failed to start
    because the map and world files were not current or a user did not have
    one of the files.
\***************************************************************************/
bool startGame()
{
	int i;
    if(!gameWaiting)
    {
        gameWaiting=true; //Game will begin when the following conditions are met.
        onGameWaiting();
    }
#ifdef DEBUG
    if(gameRunning || !isGameHost)
    {
        bug("startGame: game is already running or you're not game host");
        gameWaiting=false;
        return false;
    }
    if(!mapLoaded)
    {
        bug("startGame: map not loaded");
        gameWaiting=false;
        return false;
    }
    if(!worldLoaded)
    {
        bug("startGame: world not loaded");
        gameWaiting=false;
        return false;
    }
    if(networkGame && latency == 0)
    {
        bug("startGame: latency needs to be set for network games");
        gameWaiting=false;
        return false;
    }
    if(networkGame && netInterval == 0)
    {
        bug("startGame: network interval needs to be set for network games");
        gameWaiting=false;
        return false;
    }
    if(gameInterval == 0)
    {
        bug("startGame: game interval needs to be set");
        gameWaiting=false;
        return false;
    }
    if(gameSpeed == 0.0)
    {
        bug("startGame: game speed needs to be set");
        gameWaiting=false;
        return false;
    }
#endif

    //Ensure everyone has world and map:
    for(i=0;i<MAX_PLAYERS;i++)
        if(players[i].assignedUser &&
            (!players[i].assignedUser->haveWorld || !players[i].assignedUser->haveMap))
            return false;

    if(localPlayerID<0)
    {
        gameWaiting=false;
        onAbortGame();
        onHostMissing();
        return false;
    }

    //Inspect the files one last time to make sure they are current.  If one
    //of the files has changed, we initiate a file transfer and exit the function.
    //The function may also abort the waiting game if a fatal error occurs.
    if(!inspectFiles())
        return false;

    gameWaiting=false; //Let's get ready to rumble!!!

    //Set open slots to closed:
    for(i=0; i<MAX_PLAYERS; i++)
        if(players[i].assignedType==PLAYER_OPEN)
            setPlayer(i,PLAYER_CLOSED,NULL);

    //Added 1/1/03:  Double-check that everyone has the same timing!
    setTiming(latency, netInterval, gameInterval, gameSpeed);

    //Notify everyone that the game will commence:
    tMessage message(MSG_START_GAME);
    broadcast(&message);

    //This concludes the host-specific setup.  Complete the game initialization
    //with initializeGame:
    initializeGame();

    return true;
}

/***************************************************************************\
endGame

The host calls this function to conclude the game.  The function broadcasts
an 'end game' message and shut downs the game.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
bool endGame()
{
#ifdef DEBUG
    if(!gameRunning || !isGameHost)
    {
        bug("endGame: game isn't running or you're not the game host");
        return false;
    }
#endif
    //Notify everyone that the game will conclude:
    tMessage message(MSG_END_GAME);
    broadcast(&message);

    //Complete the game shutdown with shutdownGame:
    shutdownGame();

    return true;
}

/***************************************************************************\
pauseGame

Any player can call this function to pause the game.  The game can proceed
no further until the player calls resumeGame.  The function spreads the word
to other peers, sets a flag, and generates an event.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
bool pauseGame()
{
#ifdef DEBUG
    if(!gameRunning || localPlayerID<0)
    {
        bug("pauseGame: game isn't running or you're not playing in this game");
        return false;
    }
    if(players[localPlayerID].gamePaused)
    {
        bug("pauseGame: you already paused the game");
        return false;
    }
#endif
    //Inform other peers that you have paused the game:
    tMessage message(MSG_PAUSE_GAME);
    broadcastPlayers(&message);

    //Remember who has paused/resumed the game (could be more than one player):
    players[localPlayerID].gamePaused=true;

    //Inform the user-interface that a player has paused the game:
    onPauseGame(localPlayerID);

    return true;
}

/***************************************************************************\
resumeGame

Any player can call this function to resume the game after calling pauseGame.
The function spreads the word to other clients, clears a flag, and generates
an event.

Inputs:
    none
Outputs:
    Returns true if successful
\***************************************************************************/
bool resumeGame()
{
#ifdef DEBUG
    if(!gameRunning || localPlayerID<0)
    {
        bug("resumeGame: game isn't running or you're not playing in this game");
        return false;
    }
    if(!players[localPlayerID].gamePaused)
    {
        bug("resumeGame: you haven't paused the game");
        return false;
    }
#endif
    //Inform other peers that you have resumed the game:
    tMessage message(MSG_RESUME_GAME);
    broadcastPlayers(&message);

    //Remember who has paused/resumed the game (could be more than one player):
    players[localPlayerID].gamePaused=false;

    //Inform the user-interface that a player has resumed the game:
    onResumeGame(localPlayerID);

    return true;
}

/***************************************************************************\
initializeGame

Initializes the game after the host has selected a map and assigned players.
The host calls this function from within startGame.  A client calls it in
response to a 'start game' message.  The function loads game objects (if
necessary), removes objects belonging to unused players, initializes the
timing system, and generates an event.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void initializeGame()
{
    int i;
#ifdef DEBUG
    if(!mapLoaded)
    {
        bug("initializeGame: map not loaded");
        return;
    }
    if(!worldLoaded)
    {
        bug("initializeGame: world not loaded");
        return;
    }
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("initializeGame: invalid localPlayerID");
        return;
    }
    if(players[localPlayerID].assignedType!=PLAYER_HUMAN)
    {
        bug("initializeGame: localPlayerID isn't human");
        return;
    }
#endif
    //If we play more than one game with the same map, we will need to reload
    //the game objects from the map file.  Notice that we use the same function
    //to load the map and load the game objects.
    if(!objectsLoaded)
    {
        //We don't care about the error message:
        tString dummy;
        try
        {
            loadMap();
        }
        catch(const tString& dummy)
        {
            //There's no conceivable reason why the map should fail to load a
            //second time.  We put this check here to prevent the game from
            //crashing if anything goes awry.
            bug("initializeGame: failed to reload map");
            return;
        }
    }

    //Destroy any units belonging to closed players:
    for(i=0;i<MAX_PLAYERS;i++)
        if(players[i].assignedType==PLAYER_CLOSED)
            players[i].exists=false;

    for(i=0; i<units.size(); )
    {
        if(!players[units[i]->getOwner()].exists)
        {
            removeNeighbor(units[i],units[i]->getHashValue());
            mwDelete units[i];
            units.erase(units.begin()+i);
        }
        else
            i++;
    }

    //Put the game in drive and release the hand-brake...
    gameRunning = true;
    gameFrozen = false;

    //Nobody has paused the game yet:
    for(i=0;i<MAX_PLAYERS;i++)
        players[i].gamePaused = false;

    //Reset the game clock to zero and initialize the bias and reference point:
    gameTime = 0;
    gameTimeBias = 0;
    gameTimeRef = currentTime;
    worldTimeBias = 0;
    t0 = 0;

    //We will call worldIntervalTick for the first time after one game interval
    //has elapsed:
    nextIntervalTick=gameInterval;

    //The following instructions apply to network games only:
    if(networkGame)
    {
        //We must receive an 'end interval' message from each peer by the
        //end of the latency:
        for(i=0;i<MAX_PLAYERS;i++)
        {
            players[i].stopTime = latency;
            players[i].netInterval = netInterval;
            players[i].lowerBound = 0;
        }

        //Each peer sends his first 'end interval' message immediately:
        tGameMsg message(MSG_END_INTERVAL);
        message.playerID = localPlayerID;
        broadcastPlayers(&message);

        //Pretending that we immediately received the 'end interval' message
        //we sent to ourselves, we increment our own stop time by network
        //interval:
        players[localPlayerID].stopTime += netInterval;

        //Ensure game sychronization periodically:
        syncQueryTimer=0;
        syncResponseTimer=0;
        syncReceived=false;
    }

    //Center the map at the player's start location:
    panMap(players[localPlayerID].startLocation);

    //Inform the user-interface that the game has started:
    onStartGame();
}

/***************************************************************************\
shutdownGame

Shuts down the game.  The host calls this function from within endGame.  A
client calls it in response to an 'end game' message.  The function resets game
variables, deletes game objects, and generates an event.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void shutdownGame()
{
    //Put the game in park and turn off the engine...
    gameRunning = false;
    gameFrozen = false;
    gameTime = 0;
    syncReceived = false;

    //Clean-up memory:
    deleteObjects();

    //Inform the user-interface that the game has ended:
    onEndGame();
}

/***************************************************************************\
postMessage

Primary function for posting sychronized game messages.  In single-player
mode, the message will execute immediately (in terms of game time, that is.
The message will not actually execute until the next call to engineTick.)
For network games, the message will execute one latency period after the
next interval.  In terms of the current game time, the delay will vary
between latency and latency + netInterval.  The function stamps the message
with the localPlayerID and execution time, and broadcasts it.  It saves one
copy in the local message queue also.

Inputs:
    message         Pointer to any descendent of tMessage
Outputs:
    none
\***************************************************************************/
void postMessage(tMessage *message)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
    if(!gameRunning)
    {
        bug("postMessage: game must be running to post a message");
        return;
    }
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("postMessage: invalid localPlayerID");
        return;
    }
#endif
    //We do not broadcast the execution time with the message since it can be
    //calculated by the receiving computer.  We calculate it here for the local
    //copy only.  In single-player mode, messages execute immediately.
    if(networkGame)
        message->executionTime = MAX(players[localPlayerID].stopTime,players[localPlayerID].lowerBound);
    else
        message->executionTime = gameTime;

    //Stamp the message with the local player ID:
    message->playerID = localPlayerID;

    //Dispatch one copy to the local message queue:
    dispatchMessage(tMessage::duplicateMessage(message));

    //Broadcast the message to the other players:
    broadcastPlayers(message);
}

/***************************************************************************\
receiveMessage

The network module calls this function to receive synchronized game messages.
The function determines who sent the message from its playerID.  One type of
game message deserves special mention.  When the engine receives an 'end
interval' message, it increments the player's stopTime by net interval.
For all other messages, it sets the executionTime to the player's stopTime,
and dispatches the message to dispatchMessage.

Inputs:
    message         Pointer to any descendent of tMessage
Outputs:
    none
Exceptions:
    Throws -1 if the message is corrupt
\***************************************************************************/
void receiveMessage(tMessage *message) throw (int)
{
    int ID;
#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;

    //When the host ends the game, there may be several game messages still
    //travelling through the network.  If we receive any, we discard them.
    if(!gameRunning)
    {
        mwDelete message;
        return;
    }
#endif
    //Determine who sent the message:
    ID=message->playerID;
#ifdef DEBUG
    if(ID<0 || ID>=MAX_PLAYERS)
    {
        bug("receiveMessage: invalid playerID");
        mwDelete message;
        throw -1;
    }
#endif
    //If we receive an 'end interval' message, increment the player's stop
    //time and discard the message:
    if(message->type == MSG_END_INTERVAL)
    {
        players[ID].stopTime += players[ID].netInterval;
        mwDelete message;
        return;
    }

    //Calculate the message's execution time.  Incidently, this equals the
    //player's stop time.
    message->executionTime = MAX(players[ID].stopTime,players[ID].lowerBound);

    //Dispatch the message to the local message queue:
    dispatchMessage(message);
}

/***************************************************************************\
dispatchMessage

Inserts a message at the correct location in the local message queue based
on execution time and player ID.  The messages are sorted first by execution time
(ascending order) and second by player ID (ascending order).  The rationale
behind this function and the local message queue is this:  Messages from
different players will arrive in different orders on different peers, but we
need to execute the messages in the same order to prevent discrepances.

Inputs:
    message         Pointer to any descendent of tMessage
Outputs:
    none
\***************************************************************************/
void dispatchMessage(tMessage *message)
{
    int i;

#ifdef DEBUG
    if(MEMORY_VIOLATION(message))
        return;
#endif

    //A 'timing divider' message divides subsequent messages with different
    //timing parameters from previous messages.  When we receive this type
    //of message, we update the player's local timing parameters and discard
    //the message:
    if(message->type == MSG_TIMING_DIVIDER)
    {
        int ID=message->playerID;
        players[ID].netInterval = ((tSetTimingMsg *)message)->netInterval;
        players[ID].lowerBound = players[ID].stopTime;
        players[ID].stopTime += ((tSetTimingMsg *)message)->latency;
        mwDelete message;
        return;
    }

    //Determine if a message is overdue.  This should never happen since the
    //gameTime cannot exceed any peer's stopTime.
    if(message->executionTime < gameTime)
        bug("dispatchMessage: overdue message received");

    //Insert the message at the proper location.  This involves two cases.
    //Case #1: We push the message onto the end of the queue.  Case #2:
    //We insert the message in front of another.
    else if(messages.size()==0 || message->executionTime > messages.back()->executionTime ||
        message->executionTime == messages.back()->executionTime &&
        message->playerID >= messages.back()->playerID)
        messages.push_back(message);
    else
    {
        for(i=messages.size(); i>0; i--)
            if(message->executionTime > messages[i-1]->executionTime ||
                message->executionTime == messages[i-1]->executionTime &&
                message->playerID >= messages[i-1]->playerID)
                break;
        messages.insert(messages.begin()+i, message);
    }
}

/***************************************************************************\
setTiming

The host calls this function to set the timing for the subsequent or current
game.  If a game is not in progress, the function simply broadcasts
a timing message.  If a game is in progress, the function posts a special
sychronized message to players and a general message to non-players.  In this
manner, players will adjust their timing in unison, while everyone on the network
still agrees on the same timing parameters.  Finally, it generates an event.

Inputs:
    _latency        New latency (milliseconds)
    _netInterval    New network interval (milliseconds)
    _gameInterval   New game interval (milliseconds)
    _gameSpeed      New game speed (ratio between game seconds and real seconds)
Outputs:
    Returns true
\***************************************************************************/
bool setTiming(long _latency, long _netInterval, long _gameInterval, float _gameSpeed)
{
    if(gameRunning)
    {
        tSetTimingMsg message(MSG_SET_TIMING_IN_GAME, _latency, _netInterval, _gameInterval, _gameSpeed);
        postMessage(&message);
        tSetTimingMsg message2(MSG_SET_TIMING,_latency, _netInterval, _gameInterval, _gameSpeed);
        broadcastNonPlayers(&message2);
    }
    else
    {
        latency = _latency;
        netInterval = _netInterval;
        gameInterval = _gameInterval;
        gameSpeed = _gameSpeed;
        tSetTimingMsg message(MSG_SET_TIMING,latency, netInterval, gameInterval, gameSpeed);
        broadcast(&message);
        onSetTiming();
    }
    return true;
}

/***************************************************************************\
startTask

Starts a task.  The function pushes the task onto a dynamic array for execution.

Inputs:
    task        Pointer to a tTask object
Outputs:
    none
\***************************************************************************/
void startTask(tTask *task)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(task))
        return;
#endif
    tasks.push_back(task);
}

/***************************************************************************\
endTask

Ends a task prematurely.  Removes the task from the dynamic array but does
not delete it.

Inputs:
    task        Pointer to a tTask object
Outputs:
    none
\***************************************************************************/
void endTask(tTask *task)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(task))
        return;
#endif
    for(int i=0; i<tasks.size(); i++)
        if(tasks[i] == task)
        {
            tasks.erase(tasks.begin() + i);
            return;
        }
    bug("endTask: task not found");
}

/***************************************************************************\
chooseMap

The host calls this function prior to starting the game to choose a map and
world file.  It parses the world and map filenames, and attempts
to load them in that order.  If it encounters any errors, it will throw a
tString containing a detailed error message.  It will also throw an exception
if the map and world filenames match.  If the map and world files load
successfully, the function will broadcast a 'choose world' message and
begin assigning users to player slots.  The host is assigned to the first
available PLAYER_HUMAN slot.  The clients are assigned to the remaining
PLAYER_HUMAN slots in the same order that they are stored in the units list.
The function generates a message and an event for each player assignment.

Inputs:
    mapFilename     File containing map data (you may omit map extension)
    worldFilename   File containing world data (you may omit world extension)
Outputs:
    Returns true if successful
Exceptions:
    Throws tString describing why the map or world failed to load
\***************************************************************************/
bool chooseMap(const tString& mapFilename, const tString& worldFilename, bool reset)
{
    int i;
    tUser *u;
    tString s;
    ePlayer oldType,newType;
#ifdef DEBUG
    if(gameRunning || !isGameHost || !localUser)
    {
        bug("chooseMap: game is already running or you're not game host or user pointer is NULL");
        return false;
    }
#endif
    //Removed 5/11/03:
    //There's no need to abort all uploads.  If we discover that a client does
    //not have the map or world, then we'll simply abort the current upload and
    //send the new file.
    //abortAllUploads();

    //Assume that no one has the map or world files to begin with.  We will soon
    //find out who has these files already and who needs them.
    for(u=firstUser; u; u=u->next)
    {
        u->haveWorld=false;
        u->haveMap=false;
        u->ackReceived=false;
    }

    //Automatically append the world extension if necessary.
    s=worldFilename;
    if(!containsExtension(CHARPTR(s)))
        s+=WORLD_EXTENSION;
    worldFile.setName(s);

    //Automatically append the map extension if necessary.
    s=mapFilename;
    if(!containsExtension(CHARPTR(s)))
        s+=MAP_EXTENSION;
    mapFile.setName(s);

    //Map and world files cannot have the same filename!
    if(STREQUAL(mapFile.name,worldFile.name))
    {
        onParseError("World and map filenames can't be the same");
        return false;
    }

    //Attempt to load the world and map files.  These functions will throw a
    //tString if they encounter any errors.
    try
    {
        loadWorld();
        loadMap();
    }
    catch(const tString& error)
    {
        onParseError(error);

        //Erase the old player assignments:
        resetPlayers();
        tMessage message3(MSG_RESET_PLAYERS);
        broadcast(&message3);

        return false;
    }

    //The host has the map and world since we just loaded them!
    localUser->haveMap=true;
    localUser->haveWorld=true;

    //Query the clients about the map and world files.  The clients will respond "yes,
    //I have this files" or "no, I need these files".
    tChooseFilesMsg message1(MSG_CHOOSE_FILES, mapFile, worldFile);
    broadcast(&message1);

    if(reset)
    {
        //Erase the old player assignments:
        resetPlayers();
        tMessage message3(MSG_RESET_PLAYERS);
        broadcast(&message3);

        for(i=0;i<MAX_PLAYERS;i++)
        {
            if(players[i].type==PLAYER_HUMAN)
                players[i].assignedType=PLAYER_OPEN;
            else
                players[i].assignedType=players[i].type;
        }

        //Look for the first available open slot:
        int slot=getNextSlot();
        if(slot>=0)
        {
            //Assign the host to this slot:
            players[slot].assignedType=PLAYER_HUMAN;
            players[slot].assignedUser=localUser;
            localPlayerID=slot;
        }

        //Traverse the user list and assign each to the next available open
        //slot.  NOTE: Some user may not be assigned to slots if there are more
        //users than slots.
        for(u=firstUser; u; u=u->next)
        {
            if(u==localUser)
                continue;
            slot=getNextSlot();
            if(slot<0)
                break;
            players[slot].assignedType=PLAYER_HUMAN;
            players[slot].assignedUser=u;
        }

        /* Broadcast the player assignments and inform the user-interface of each.
         * Notice that a client may receive the player assignments before it
         * loads the map.  Thus, the client can participate in the player assignments
         * while still receiving the map.  When it finally loads the map, the player
         * settings in the map file will not interfere with the host's player
         * assignments.
         */
        for(i=0;i<MAX_PLAYERS;i++)
            if(players[i].assignedType!=PLAYER_NONE)
            {
                tSetPlayerMsg message2(i,players[i].assignedType,
                    players[i].assignedUser?players[i].assignedUser->ID:0);
                broadcast(&message2);
                onSetPlayer(i,players[i].assignedType,players[i].assignedUser);
            }

    }
    else
    {
        for(i=0; i<MAX_PLAYERS; i++)
        {
            oldType=players[i].assignedType;
            if(oldType==PLAYER_HUMAN||oldType==PLAYER_CLOSED)
                oldType=PLAYER_OPEN;
            newType=players[i].type;
            if(newType==PLAYER_HUMAN||newType==PLAYER_CLOSED)
                newType=PLAYER_OPEN;
            if(newType!=oldType)
            {
                players[i].assignedType=newType;
                players[i].assignedUser=NULL;

                tSetPlayerMsg message3(i,newType,0);
                broadcast(&message3);
                onSetPlayer(i,newType,NULL);
            }
        }
    }

    onChooseFiles();
    return true;
}

/***************************************************************************\
setPlayer

The host calls this function to set a player slot.  If you set the slot to
PLAYER_COMPUTER, PLAYER_OPEN, or PLAYER_CLOSED, you must pass a NULL user
pointer.  If you assign a user, you must pass a valid user pointer.  In
the latter case, the function determines if the user is already assigned to
another slot and opens it first.  The function broadcasts the player assignment
and generates an event.  It also keeps localPlayerID current.

Inputs:
    index       Index of the slot to affect
    type        PLAYER_HUMAN, PLAYER_COMPUTER, PLAYER_OPEN, etc.
    u           User pointer (PLAYER_HUMAN) or NULL (all others)
Outputs:
    Returns true if successful
\***************************************************************************/
bool setPlayer(int index, ePlayer type, tUser *u)
{
    int ID;
#ifdef DEBUG
    if(!isGameHost)
    {
        bug("setPlayer: you're not the game host");
        return false;
    }
    if(index<0 || index>=MAX_PLAYERS)
    {
        bug("setPlayer: invalid index");
        return false;
    }
    if(players[index].assignedType==PLAYER_NONE || players[index].assignedType==PLAYER_HIDDEN)
    {
        bug("setPlayer: you can't set an empty or hidden slot");
        return false;
    }
    if(type==PLAYER_NONE || type==PLAYER_HIDDEN)
    {
        bug("setPlayer: you can't set a slot to empty or hidden");
        return false;
    }
#endif
    //Return immediately if the new player assignment matches the existing one:
    if(players[index].assignedType==type)
    {
        if(type!=PLAYER_HUMAN)
            return true;
        else if(u==players[index].assignedUser)
            return true;
    }

    if(type==PLAYER_HUMAN)
    {
        //Ensure the caller passed a valid user pointer:
        if(MEMORY_VIOLATION(u))
            return false;

        //Determine if the user is already assigned to another slot.  If so,
        //open the slot by recursively calling this function.
        ID=playerLookup(u);
        if(ID>=0)
            setPlayer(ID, PLAYER_OPEN, NULL);
    }
    else
    {
        //Ensure the caller passes a null user pointer:
        if(u!=NULL)
        {
            bug("setPlayer: user is not NULL");
            return false;
        }
    }

    //Examine the user reference that we are about to "lose" and reset
    //localPlayerID if necessary.
    if(players[index].assignedUser==localUser)
        localPlayerID=-1;

    //Make the player assignment:
    players[index].assignedType=type;
    players[index].assignedUser=u;

    //Examine the user reference that we just stored and set localPlayerID
    //if necessary.
    if(players[index].assignedUser==localUser)
        localPlayerID=index;

    //Spread the word!
    tSetPlayerMsg message1(index, type, u?u->ID:0);
    broadcast(&message1);

    //Inform the user-interface of the player assignment:
    onSetPlayer(index,type,u);

    return true;
}

/***************************************************************************\
transmitSettings

The host calls this function after establishing a connection with a new client
to transmit game settings.  The function tells the client which map and/or
world file is loaded.  If a map is loaded, it transmits the player assignments.
It transmits the timing settings and tells the client if a game has already
started.  The client cannot participate in the current game, but may
participate in subsequent games provided he has the map and world.

Inputs:
    desc        Descriptor belonging to the new client
Outputs:
    none
\***************************************************************************/
void transmitSettings(tDescriptor *desc)
{
    int i;
    //Query the client about the selected files (if any):
    if(mapLoaded && worldLoaded)
    {
        tChooseFilesMsg message(MSG_CHOOSE_FILES, mapFile, worldFile);
        writeToBuffer(desc, &message);
    }
    //Transmit player assignments if a map is loaded:
    if(mapLoaded)
        for(i=0; i<MAX_PLAYERS; i++)
            if(players[i].assignedType!=PLAYER_NONE)
            {
                tSetPlayerMsg message(i, players[i].assignedType, players[i].assignedUser?players[i].assignedUser->ID:0);
                writeToBuffer(desc, &message);
            }

    //Transmit timing settings:
    tSetTimingMsg message(MSG_SET_TIMING, latency, netInterval, gameInterval, gameSpeed);
    broadcast(&message);

    //Notify the client if a game is already in progress:
    if(gameRunning)
    {
        tMessage message(MSG_START_GAME);
        writeToBuffer(desc, &message);
    }
}

/***************************************************************************\
onSetTimingMsg (message handler)

Called when the host adjusts the timing.  Replaces the old timing parameters
with the new ones and generates an event.
\***************************************************************************/
void onSetTimingMsg(tDescriptor *desc, tSetTimingMsg *message) throw(int)
{
    //If the game isn't running, we need only update latency, netInterval,
    //gameInterval, and gameSpeed:
    latency = message->latency;
    netInterval = message->netInterval;
    gameInterval = message->gameInterval;
    gameSpeed = message->gameSpeed;
    onSetTiming();
}

/***************************************************************************\
onStartGameMsg (message handler)

Called when the host starts the game.  If you are playing, initialize the game.
If you are not playing, notify the user-interface just the same.
\***************************************************************************/
void onStartGameMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    if(localPlayerID>=0)
        initializeGame();
    else
        onStartGame();
}

/***************************************************************************\
onEndGameMsg (message handler)

Called when the host ends the game.  If you are playing, shut down the game.
If you are not playing, notify the user-interface just the same.
\***************************************************************************/
void onEndGameMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    if(localPlayerID>=0)
        shutdownGame();
    else
        onEndGame();
}

/***************************************************************************\
onPauseGameMsg (message handler)

Called when a peer pauses the game.  Set the player's gamePaused flag and
generate an event.  NOTE: The function has no bearing on the engine's timing
algorithm; it serves only to notify the user-interface.

The message handler throws a -1 if the message is corrupt.
\***************************************************************************/
void onPauseGameMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    int playerID;
    if(desc->user)
    {
        //Determine which player initiated the message:
        playerID=playerLookup(desc->user);
#ifdef DEBUG
        if(playerID<0)
        {
            bug("onPauseGameMsg: unknown player");
            throw -1;
        }
#endif
        //Set the player's gamePaused flag (for future reference by the UI):
        players[playerID].gamePaused=true;

        //Notify the user-interface that the player has paused the game:
        onPauseGame(playerID);
    }
}

/***************************************************************************\
onResumeGameMsg (message handler)

Called when a peer resumes the game.  Clear the player's gamePaused flag and
generate an event.  NOTE: The function has no bearing on the engine's timing
algorithm; it serves only to notify the user-interface.

The message handler throws a -1 if the message is corrupt.
\***************************************************************************/
void onResumeGameMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    int playerID;
    if(desc->user)
    {
        //Determine which player initiated the message:
        playerID=playerLookup(desc->user);
#ifdef DEBUG
        if(playerID<0)
        {
            bug("onResumeGameMsg: unknown player");
            throw -1;
        }
#endif
        //Clear the player's gamePaused flag (for future reference by the UI):
        players[playerID].gamePaused=false;

        //Notify the user-interface that the player has resumed the game:
        onResumeGame(playerID);
    }
}

/***************************************************************************\
onResetPlayersMsg (message handler)

Called when the host resets the players.  Calls resetPlayers.
\***************************************************************************/
void onResetPlayersMsg(tDescriptor *desc, tMessage *message) throw(int)
{
    resetPlayers();
}

/***************************************************************************\
onChooseFilesMsg (message handler)

Called when the host chooses a map/world.  The function determines if the files
exist in the download directory and saves the file details in the map/world file
"descriptor".  If the files exists, it attempts to load them and catches
any exceptions.  If the client does indeed have the same files as the host, they
should load successfully if they loaded successfully on the host.  If they do
not, we assume the files have become corrupt and pretend the files do not exist.
In either case, we acknowledge the files saying either "we already have the map/world",
or "we don't have the map/world; please send it".
\***************************************************************************/
void onChooseFilesMsg(tDescriptor *desc, tChooseFilesMsg *message) throw(int)
{
    tString error;
    //Does a file with the same name, size, and date already exist?
    bool mapExists = message->mapFile.exists();
    bool worldExists = message->worldFile.exists();

    //If we've already loaded the correct world, then there is no sense in loading it again...
    if(!worldLoaded || worldFile!=message->worldFile)
    {
        //Save the file details:
        worldFile=message->worldFile;

        //Attempt to load the world if it already exists in our download directory.
        //Log any exceptions and re-request the file from the host.
        if(worldExists)
        {
            try
            {
                loadWorld();
            }
            catch(const tString& error)
            {
                bug("onChooseFilesMsg: failed to load world with error:\n%s", CHARPTR(error));
                worldExists=false;
            }
        }
    }

    if(!mapLoaded || mapFile!=message->mapFile)
    {
        mapFile=message->mapFile;

        if(mapExists)
        {
            try
            {
                loadMap();
            }
            catch(const tString& error)
            {
                bug("onChooseFilesMsg: failed to load map with error:\n%s", CHARPTR(error));
                mapExists=false;
            }
        }
    }

    //Acknowledge the files: do we have them?
    tAckFilesMsg message1(mapExists, worldExists, message->mapFile, message->worldFile);
    writeToBuffer(desc, &message1);

    onChooseFiles();
}

/***************************************************************************\
onAckFilesMsg (message handler)

Called when a client acknowledges the map/world.  We compare the message's file details
to the selected file details to make sure the client has acknowledged the correct
map/world.  There is a slim chance that the host chose a new map/world before the
client had a chance to acknowledge the old ones.  Next, we set the
user's haveMap/haveWorld flag and initiate a file transfer if one of the flags
is false.
\***************************************************************************/
void onAckFilesMsg(tDescriptor *desc, tAckFilesMsg *message) throw(int)
{
    if(desc->user)
    {
        desc->user->ackReceived=true;
        if(message->mapFile==mapFile)
            desc->user->haveMap=message->haveMap;
        if(message->worldFile==worldFile) //CAUTION: the message contains two files!
            desc->user->haveWorld=message->haveWorld;
    }
}

/***************************************************************************\
onSetPlayerMsg (message handler)

Called when the host makes a player assignment.  The function closely
resembles setPlayer.  It looks up the user (if any) based on the userID
and assigns the playerType and user to the specified slot.  It also updates
localPlayerID as needed and generates an event.

The message handler throws a -1 if the message is corrupt.
\***************************************************************************/
void onSetPlayerMsg(tDescriptor *desc, tSetPlayerMsg *message) throw(int)
{
    tUser *u=NULL;
    int ID=message->playerID;
#ifdef DEBUG
    if(ID<0 || ID>=MAX_PLAYERS)
    {
        bug("onSetPlayerMsg: invalid playerID");
        throw -1;
    }
#endif
    if(message->playerType == PLAYER_HUMAN)
    {
        //Look-up the user if the message dictates a human player:
        u=userLookup(message->userID);
        if(u==NULL)
        {
            bug("onSetPlayerMsg: invalid userID");
            throw -1;
        }
    }
    //Examine the user reference that we are about to "lose" and reset
    //localPlayerID if necessary.
    if(players[ID].assignedUser==localUser)
        localPlayerID=-1;

    //Make the player assignment:
    players[ID].assignedType=message->playerType;
    players[ID].assignedUser=u;

    //Examine the user reference that we just stored and set localPlayerID
    //if necessary.
    if(players[ID].assignedUser==localUser)
        localPlayerID=ID;

    //Inform the user-interface of the player assignment:
    onSetPlayer(ID, message->playerType, u);
}

/***************************************************************************\
onFileSent (event handler)

The network module generates this event just after a file finishes uploading.
The host determines if it successfully sent a world or map file.
In either case, it sets the appropriate flag indicating that the client now
has the file (haveWorld or haveMap).  In the former case, it also sends the
map selection.

Inputs:
    desc        Descriptor through which the file was sent
Outputs:
    none
\***************************************************************************/
void onFileSent(tDescriptor *desc)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(desc->user))
        return;
#endif
    if(desc->fileOut==worldFile) //Remote engine has received correct world file.
        desc->user->haveWorld=true;
    else if(desc->fileOut==mapFile) //Remote engine has received correct map file.
        desc->user->haveMap=true;
}

/***************************************************************************\
onFileReceived (event handler)

The network module generates this event just after a file finishes downloading.
The client determines if it received a world file or a map file.  In
either case, it attempts to load the file in a try-catch block.  It should
not encounter any errors since the files successfully loaded on the host.

Inputs:
    desc        Descriptor through which the file was received
Outputs:
    none
\***************************************************************************/
void onFileReceived(tDescriptor *desc)
{
    tString error;
    if(desc->fileIn==worldFile) //Received file was the world we need.
    {
        try
        {
            loadWorld();
        }
        catch(const tString& error)
        {
            bug("onFileReceived: failed to load world with error %s", CHARPTR(error));
        }
    }
    else if(desc->fileIn==mapFile) //Received file was the map we need.
    {
        try
        {
            loadMap();
        }
        catch(const tString& error)
        {
            bug("onFileReceived: failed to load map with error %s", CHARPTR(error));
        }
    }
}

/***************************************************************************\
onDeleteUser (event handler)

The network module generates this event just before deleting a user.  The
engine should no longer reference the user when it is deleted since the host
will close the slot first.  The only exception is when the host deletes its
own user structure.

Inputs:
    user        User which is about to be deleted
Outputs:
    none
\***************************************************************************/
void onDeleteUser(tUser *user)
{
    //Determine if any player slots reference the user:
    int ID=playerLookup(user);
    if(ID>=0)
    {
        //Remove the human player from this slot:
        players[ID].assignedType=PLAYER_CLOSED;
        players[ID].assignedUser=NULL;

        //Inform the user-interface:
        onSetPlayer(ID,PLAYER_CLOSED,NULL);
    }
}

/***************************************************************************\
onClientConnect (event handler)

The network module calls this event on the host just after a client connects
and negotiates a connection.  The function transmits the current game settings
and loads the client in the next open slot (if one is available).

Inputs:
    user        User representing the new client
Outputs:
    none
\***************************************************************************/
void onClientConnect(tUser *user)
{
    //Send current game settings to user:
    if(user->descriptor)
        transmitSettings(user->descriptor);

    //Automatically load user in next available slot:
    if(mapLoaded)
    {
        int slot=getNextSlot();
        if(slot>=0)
            setPlayer(slot,PLAYER_HUMAN,user);
    }
}

/***************************************************************************\
onClientDisconnect (event handler)

The network module calls this event on the host just after a client disconnects.
The function removes the client from the game and instructs its clients to
do likewise.

Inputs:
    user        User representing the old client
Outputs:
    none
\***************************************************************************/
void onClientDisconnect(tUser *user)
{
    //If the user is participating in this game, remove him from the appropriate
    //player slot:
    int ID=playerLookup(user);
    if(ID>=0)
        setPlayer(ID, PLAYER_CLOSED, NULL);
}

/***************************************************************************\
onSessionStartup (event handler)

The network module calls this event just after onHostGame, onConnect, or
onSinglePlayer and the subsequent onAddUser events.  The function automatically
loads the map and unit file defined in the initialization file.  It generates
a parse error event if it encounters any errors.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void onSessionStartup()
{
    if(isGameHost && STRLEN(initialMap)>0 && STRLEN(initialWorld)>0)
    {
        try
        {
            chooseMap(MAP_PATH+initialMap, WORLD_PATH+initialWorld, /*resetPlayers=*/true);
        }
        catch(const tString& error)
        {
            onParseError(error);
        }
    }
}

/***************************************************************************\
onSessionShutdown (event handler)

The network module calls this event just after a host, client, or single-player
session ends.  The function prematurely ends the game and resets the world
in preparation for the next session.

Inputs:
    none
Outputs:
    none
\***************************************************************************/
void onSessionShutdown()
{
    if(gameRunning)
    {
        shutdownGame();
        onEndGame();
    }
    resetPlayers();
    unloadMap();
    unloadWorld();
}

/***************************************************************************\
playerLookup

Finds the index of a human player with a matching user reference.

Inputs:
    user        User reference for which to search
Outputs:
    Returns the index of a player or -1 if no match was found
\***************************************************************************/
int playerLookup(tUser *user)
{
    for(int i=0;i<MAX_PLAYERS;i++)
        if(players[i].assignedUser==user)
            return i;
    return -1;
}



/****************************************************************************/
void drawMap()
//draws a portion of map
//set viewport prior to calling this function
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("drawMap: map isn't initialized");
        return;
    }
#endif
    //assume background does not need to be cleared

    int i;
    int cx,x,y,px,py;
    int sw,sh;
    long hashValue;
    tUnit *u;
    char buf[20];
    int oldViewport[4];
    float mat[16];
    
    static vector<tUnit *> visibleUnits;    //we refresh this vector each time we call drawMap
                                            //the vector is static so that we can use the same
                                            //memory block over and over.

    tVector3D v1,v2,v3,normal;
    float minx,miny,maxx,maxy;

    glGetIntegerv(GL_VIEWPORT, oldViewport);
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();

    //Determine hash extents:
    int left=HASH_FUNC(viewMinX-MAX_FIELD_OF_VIEW);
    int right=HASH_FUNC(viewMaxX+MAX_FIELD_OF_VIEW);
    clampRange(left,0,hashWidth-1);
    clampRange(right,0,hashWidth-1);

    int bottom=HASH_FUNC(viewMinY-MAX_FIELD_OF_VIEW);
    int top=HASH_FUNC(viewMaxY+MAX_FIELD_OF_VIEW);
    clampRange(bottom,0,hashHeight-1);
    clampRange(top,0,hashHeight-1);

    //compile a list of visible units
    START_TIMER(TIMER_COMPILE_VISIBLE_UNITS)
    visibleUnits.clear();
    for(i=0;i<MAX_PLAYERS;i++)
    {
        if(!players[i].exists)
            continue;
        for(x=left;x<=right;x++)
            for(y=bottom;y<=top;y++)
            {
                hashValue=(y*hashWidth+x)*MAX_PLAYERS+i;
                for(u=unitHash[hashValue];u;u=u->getNextNeighbor())
                    if(canPlayerSee(u)&&isWithinView(u))
                        visibleUnits.push_back(u);
            }
    }
    STOP_TIMER(TIMER_COMPILE_VISIBLE_UNITS)

    glEnable(GL_COLOR_MATERIAL);

    /********************
     STEP 1: DRAW SHADOWS
     ********************/

    if(shadowMapSize>0)
    {
        //Determine the best plane on which to project the shadows using three
        //points: the lower-left corner of the screen, the lower-right corner of
        //the screen and the top-center point on the screen.

        //Although this method will not give us the best results, it is faster than
        //pixelToTerrain2D.
        if(terrain)
        {
            v1=pixelToVector3DAtElevation(0,0,0);
            v2=pixelToVector3DAtElevation(viewWidth,0,0);
            v3=pixelToVector3DAtElevation(viewWidth/2,viewHeight,0);
            v1.z=terrain->getTileInfo(to2D(v1),DOMAIN_NONE,SEA_FLOOR_DEPTH).height;
            v2.z=terrain->getTileInfo(to2D(v2),DOMAIN_NONE,SEA_FLOOR_DEPTH).height;
            v3.z=terrain->getTileInfo(to2D(v3),DOMAIN_NONE,SEA_FLOOR_DEPTH).height;
            normal=(v1-v3).cross(v2-v3).normalize();
        }
        else
        {
            v1=zeroVector3D;
            normal=kHat3D;
        }

        float ground[4]={normal.x,normal.y,normal.z,-normal.dot(v1)};
        findShadowMatrix(ground, shadowLightPosition, mat);

        //Hack for the Radeon 8500.  Evidently, this video card chokes on singular
        //projection matrices, although the projection matrix should not affect
        //the clipping plane in any way.
        mat[10]*=0.9;

        //Set up the texture matrix:
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        glOrtho(0,viewWidth,0,viewHeight,1,-1);
        glMultMatrixf(viewMatrix);
        glMultMatrixf(mat);
        glGetFloatv(GL_PROJECTION_MATRIX, shadowTextureMatrix);

        if(terrain)
        {
            START_TIMER(TIMER_CALC_VISIBLE_AREA)
            terrain->calcVisibleArea(/*calcShadowTextureExtents=*/true);
            STOP_TIMER(TIMER_CALC_VISIBLE_AREA)

            minx=terrain->getShadowTextureMinX();
            maxx=terrain->getShadowTextureMaxX();
            miny=terrain->getShadowTextureMinY();
            maxy=terrain->getShadowTextureMaxY();

            if(maxx>minx && maxy>miny)
            {
                glLoadIdentity();
                glScalef(2/(maxx-minx),2/(maxy-miny),1);
                glTranslatef(-(maxx+minx)/2,-(maxy+miny)/2,0);
                glMultMatrixf(shadowTextureMatrix);
                glGetFloatv(GL_PROJECTION_MATRIX, shadowTextureMatrix);
            }
        }

        glMatrixMode(GL_MODELVIEW);

        sw=MIN(shadowMapSize, getScreenWidth());
        sh=MIN(shadowMapSize, getScreenHeight());

        glLoadIdentity();
        glScalef(0.5*sw/shadowMapSize,0.5*sh/shadowMapSize,0);
        glTranslatef(1,1,0);
        glMultMatrixf(shadowTextureMatrix);
        glGetFloatv(GL_MODELVIEW_MATRIX, shadowTextureMatrix);
        glLoadIdentity();

        //Divide all of the values in the shadow matrix by 1000 to avoid nasty artifacts
        //on the ATI Radeon 8500
        for(i=0;i<16;i++)
            shadowTextureMatrix[i]/=1000;

        glViewport(0,0,sw,sh);

        glClearColor(1,1,1,0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear The Screen

        //Draw shadows:
        glEnable(GL_CLIP_PLANE0);
        START_GRAPHICS_TIMER(TIMER_DRAW_UNIT_SHADOWS)
        for(i=0;i<visibleUnits.size();i++)
            visibleUnits[i]->drawShadow();
        STOP_GRAPHICS_TIMER(TIMER_DRAW_UNIT_SHADOWS)
        START_GRAPHICS_TIMER(TIMER_DRAW_DECORATION_SHADOWS)
        if(terrain)
            terrain->drawDecorationShadows();
        STOP_GRAPHICS_TIMER(TIMER_DRAW_DECORATION_SHADOWS)
        glDisable(GL_CLIP_PLANE0);

        glEnable(GL_BLEND);
        glColor4f(1,1,1,1-shadowOpacity);
        glRectf(-10,-10,mapDimensions.x+10,mapDimensions.y+10);
        glDisable(GL_BLEND);

        //Copy frame buffer to shadowTexture:
        START_GRAPHICS_TIMER(TIMER_COPY_TEX_SUB_IMAGE)
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(shadowTexture);
        glCopyTexSubImage2D(GL_TEXTURE_2D, /*level=*/ 0, /*xoffset=*/ 0, /*yoffset=*/ 0,
            /*x=*/ 0, /*y=*/ 0, /*width=*/ sw, /*height*/ sh);
        glDisable(GL_TEXTURE_2D);
        ATIRadeon8500Hack();
        STOP_GRAPHICS_TIMER(TIMER_COPY_TEX_SUB_IMAGE)
    }
    else //We still need to calculate the visible area if shadows are disabled
    {
        START_TIMER(TIMER_CALC_VISIBLE_AREA)
        terrain->calcVisibleArea(/*calcShadowTextureExtents=*/false);
        STOP_TIMER(TIMER_CALC_VISIBLE_AREA)
    }

    /********************
     STEP 2: DRAW TERRAIN
     ********************/

    glViewport(0,0,viewWidth,viewHeight);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewWidth,0,viewHeight,1,-1);
    glMultMatrixf(viewMatrix);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glClearColor(0,0,0,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glEnable(GL_DEPTH_TEST);

    START_GRAPHICS_TIMER(TIMER_DRAW_TERRAIN)
    if(terrain)
        terrain->draw();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_TERRAIN)

    /*for(i=0; i<obstacles.size(); i++)
    {
        glPushMatrix();
        glColor3fv(obstacles[i].color);
        glTranslatef(obstacles[i].position.x,obstacles[i].position.y,0);
        gluDisk(globalQuadricObject,0,obstacles[i].radius,30,1);
        glPopMatrix();
    }*/

    /******************
     STEP 3: DRAW WATER
     ******************/

    START_GRAPHICS_TIMER(TIMER_POSTDRAW_TERRAIN)
    if(terrain)
        terrain->postDraw();
    STOP_GRAPHICS_TIMER(TIMER_POSTDRAW_TERRAIN)

    glDisable(GL_DEPTH_TEST);
    glEnable(GL_BLEND);
    glDepthMask(false);
    for(i=0; i<deposits.size(); i++)
    {
        glPushMatrix();
        const unsigned char clr[4]={resourceColors[deposits[i]->resource].r,
            resourceColors[deposits[i]->resource].g,
            resourceColors[deposits[i]->resource].b,
            (char)(64*deposits[i]->amount/deposits[i]->initialAmount+16)};
        glColor4ubv(clr);
        glTranslatef(deposits[i]->center.x,deposits[i]->center.y,deposits[i]->center.z);
        mglFuzzyDisk(deposits[i]->radius/2,deposits[i]->radius,30);
        glPopMatrix();
    }
    glDepthMask(true);
    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);

    /******************
     STEP 4: DRAW UNITS
     ******************/

    START_GRAPHICS_TIMER(TIMER_PREDRAW_UNITS)
    glDisable(GL_DEPTH_TEST);
    for(i=0;i<visibleUnits.size();i++)
        visibleUnits[i]->preDraw();
    glEnable(GL_DEPTH_TEST);
    STOP_GRAPHICS_TIMER(TIMER_PREDRAW_UNITS)

    START_GRAPHICS_TIMER(TIMER_DRAW_UNITS)
    for(i=0;i<visibleUnits.size();i++)
        visibleUnits[i]->draw();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_UNITS)

    if(apparitionType)
        drawApparition();

    /************************
     STEP 5: DRAW DECORATIONS
     ************************/

    START_GRAPHICS_TIMER(TIMER_DRAW_DECORATIONS)
    if(terrain)
        terrain->drawDecorations();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_DECORATIONS)

    /*********************
     STEP 6: DRAW MISSILES
     *********************/

    START_GRAPHICS_TIMER(TIMER_DRAW_MISSILES)
    for(i=0; i<missiles.size(); i++)
        missiles[i]->draw();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_MISSILES)

    /***********************
     STEP 7: DRAW EXPLOSIONS
     ***********************/

    START_GRAPHICS_TIMER(TIMER_DRAW_EXPLOSIONS)
    for(i=0; i<unitExplosions.size(); i++)
        if(isWithinView(unitExplosions[i]))
            unitExplosions[i]->draw();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_EXPLOSIONS)

    /******************************
     STEP 8: DRAW TRANSPARENT POLYS
     ******************************/

    START_GRAPHICS_TIMER(TIMER_POSTDRAW_UNITS)
    for(i=0;i<visibleUnits.size();i++)
        visibleUnits[i]->postDraw();
    STOP_GRAPHICS_TIMER(TIMER_POSTDRAW_UNITS)

    glDisable(GL_COLOR_MATERIAL);
	glDisable(GL_DEPTH_TEST);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0,viewWidth,0,viewHeight,1,-1);
    glMatrixMode(GL_MODELVIEW);

    /***********************
     STEP 9: DRAW 2D SPRITES
     ***********************/

    glColor3f(1,1,1);
    for(i=0; i<bullsEyes.size(); i++)
    {
        float projXScale, projYScale, projZScale;
        getScaleAtPoint(bullsEyes[i]->v,projXScale,projYScale,projZScale);
#ifdef DEBUG
        if(projXScale==0)
        {
            bug("drawMap: projXScale is zero");
            continue;
        }
#endif
        vector3DToPixel(bullsEyes[i]->v,px,py);
        glPushMatrix();
        glTranslatef(px,py,0);
        glScalef(1,projYScale/projXScale,0);
        bullsEyes[i]->a.draw(0,0);
        glPopMatrix();
    }

    START_GRAPHICS_TIMER(TIMER_DRAW_UNIT_OVERLAYS)
    for(i=0;i<visibleUnits.size();i++)
        visibleUnits[i]->drawOverlay();
    STOP_GRAPHICS_TIMER(TIMER_DRAW_UNIT_OVERLAYS)

    //TODO: This routine should only display deposits that are visible within the view
    for(i=0; i<deposits.size(); i++)
    {
        vector3DToPixel(deposits[i]->center,cx,y);

        x=cx-stringWidth(CHARPTR(resources[deposits[i]->resource].caption),smallFont)/2;
        glColor3f(.5,.5,.5);
        drawString(CHARPTR(resources[deposits[i]->resource].caption),x+1,y,smallFont);
        glColor3f(1,1,1);
        drawString(CHARPTR(resources[deposits[i]->resource].caption),x,y+1,smallFont);

        y-=20;

        if(showUnitStats)
            snprintf(buf,sizeof(buf),"%d [%d]",(int)deposits[i]->amount,deposits[i]->ID);
        else
            snprintf(buf,sizeof(buf),"%d",(int)deposits[i]->amount);

        x=cx-stringWidth(buf,smallFont)/2;
        glColor3f(.5,.5,.5);
        drawString(buf,x+1,y,smallFont);
        glColor3f(1,1,1);
        drawString(buf,x,y+1,smallFont);
    }

    if(showUnitStats)
        for(i=0;i<units.size();i++)
            units[i]->drawStats();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glViewport(oldViewport[0], oldViewport[1], oldViewport[2], oldViewport[3]);
}
/****************************************************************************/
//draw units within range [x,x+w]x[y,y+h]
//if selfFirst=true, draw units belonging to local player first
//if selfFirst=false, draw units belonging to local player last
int pickUnits(int x, int y, int w, int h, GLuint *buf, bool convexShell)
{
    int i;

    glSelectBuffer(PICK_BUFFER_SIZE, buf);
    glRenderMode(GL_SELECT);

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(x,x+w,y,y+h,1,-1);
    glMultMatrixf(viewMatrix);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glInitNames();
    glPushName(0); //default name--this will be replaced by unit ID

    for(i=0; i<units.size(); i++)
    {
        if(units[i]->getState()>=STATE_SELECTABLE &&
            canPlayerSee(units[i])&&isWithinView(units[i]))
        {
            glLoadName(units[i]->getID());
            units[i]->drawPickShell(convexShell);
        }
    }

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    return glRenderMode(GL_RENDER);
}
/****************************************************************************/
void drawMinimap()
//draws a portion of minimap (only selectable units are drawn)
//set viewport prior to calling this function
{
    int i;
    tVector2D v[4];
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("drawMinimap: map isn't initialized");
        return;
    }
#endif
    glPushMatrix();
    glLoadIdentity();
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    glOrtho(0,minimapWidth,0,minimapHeight,1,-1);
    glMultMatrixf(minimapMatrix);
    glMatrixMode(GL_MODELVIEW);

    if(terrain)
    {
        glColor3f(1,1,1);
        terrain->drawMinimap();
    }
    else
    {
        glColor3f(.5,.5,.5);
        glBegin(GL_LINE_LOOP);
            glVertex2f(0,0);
            glVertex2f(mapDimensions.x,0);
            glVertex2f(mapDimensions.x,mapDimensions.y);
            glVertex2f(0,mapDimensions.y);
        glEnd();
    }

    glEnable(GL_BLEND);
    glEnable(GL_POINT_SMOOTH);
    glPointSize(5);
    glBegin(GL_POINTS);
        for(i=0; i<units.size(); i++)
            //Changed 1/2/03: I no longer show blueprints as these clutter the minimap
            if(units[i]->getState()==STATE_ALIVE && canPlayerSee(units[i]) && !units[i]->isFlashOn())
            {
                glColor3ubv( (const unsigned char *) &allianceColors[getAlliance(units[i]->getOwner())] );
                glVertex2f(units[i]->getPosition().x,units[i]->getPosition().y);
            }
    glEnd();
    glPointSize(1);
    glDisable(GL_POINT_SMOOTH);
    glDisable(GL_BLEND);

    glColor3f(1,1,0);
    glBegin(GL_POINTS);
        for(i=0;i<missiles.size();i++)
            if(missiles[i]->isEnRoute())
                glVertex2f(missiles[i]->getPosition().x,missiles[i]->getPosition().y);
    glEnd();

    int cornersX[4]={0,viewWidth,viewWidth,0};
    int cornersY[4]={0,0,viewHeight,viewHeight};
    float elevation=0;
    if(terrain)
        elevation=terrain->getAverageElevation();
    for(i=0;i<4;i++)
        v[i]=pixelToVector2DAtElevation(cornersX[i],cornersY[i],elevation);

    glColor3f(1,1,1);
    glBegin(GL_LINE_LOOP);
        glVertex2fv((float *)&v[0]);
        glVertex2fv((float *)&v[1]);
        glVertex2fv((float *)&v[2]);
        glVertex2fv((float *)&v[3]);
    glEnd();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
}
/****************************************************************************/
//process a message received from engine
#define PROCESS_MESSAGE_CASE(tag, fcn, type) \
    case tag: \
        fcn((type *) message); \
        mwDelete message; \
        return;

//processMessage throws -1 if the message is corrupt.
void processMessage(tMessage *message) throw(int)
{
    int i, owner;
    tUnit *u;
    tUnitType *ut;
    tCommandMsg *m;
    tCommand *c=NULL;
#ifdef DEBUG
    if(!gameRunning)
    {
        bug("processMessage: game isn't running");
        return;
    }
    if(MEMORY_VIOLATION(message))
        return;
#endif
    //process player, game, and shell messages here
    //Any of these cases may throw an exception which will abort processMessage
    //and display the exception details in readFromBuffer.
    switch(message->type)
    {
        PROCESS_MESSAGE_CASE(MSG_SET_TIMING_IN_GAME,onSetTimingInGameMsg,   tSetTimingMsg)
        PROCESS_MESSAGE_CASE(MSG_SYNC_QUERY,        onSyncQueryMsg,         tGameMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_PLAN,         onUnitPlanMsg,          tUnitCreateMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_CREATE,       onUnitCreateMsg,        tUnitCreateMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_DESTROY,      onUnitDestroyMsg,       tUnitMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_SET_POS,      onUnitSetPosMsg,        tUnitVector2DMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_SET_VELOCITY, onUnitSetVelocityMsg,   tUnitVector2DMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_SET_FORWARD,  onUnitSetForwardMsg,    tUnitVector2DMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_SET_HP,       onUnitSetHPMsg,         tUnitFloatMsg)
        PROCESS_MESSAGE_CASE(MSG_UNIT_SET_OWNER,    onUnitSetOwnerMsg,      tUnitLongMsg)
        PROCESS_MESSAGE_CASE(MSG_SET_ALLIANCE,      onSetAllianceMsg,       tSetAllianceMsg)
        PROCESS_MESSAGE_CASE(MSG_SET_RESOURCE,      onSetResourceMsg,       tSetResourceMsg)
        PROCESS_MESSAGE_CASE(MSG_DEPOSIT_CREATE,    onDepositCreateMsg,     tDepositCreateMsg)
        PROCESS_MESSAGE_CASE(MSG_DEPOSIT_DESTROY,   onDepositDestroyMsg,    tDepositDestroyMsg)
        PROCESS_MESSAGE_CASE(MSG_DEPOSIT_SET,       onDepositSetMsg,        tDepositSetMsg)
    }

    //unit commands
#ifdef DEBUG
    if( message->type!=MSG_COMMAND &&
        message->type!=MSG_COMMAND_POS &&
        message->type!=MSG_COMMAND_UNIT &&
        message->type!=MSG_COMMAND_PRODUCTION)
    {
        bug("processMessage: unrecognized message type");
        mwDelete message;
        return;
    }
#endif

    m=(tCommandMsg *)message;

#ifdef DEBUG
    if(m->unitCount<1)
    {
        bug("processMessage: command doesn't contain any units");
        mwDelete message;
        return;
    }
    if(m->command<0 || m->command>=commandCount)
    {
        bug("processMessage: invalid command");
        mwDelete message;
        return;
    }
#endif

#ifdef LOGCOMMAND
    eParamType pt;
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld %s:",CHARPTR(players[m->playerID].name),m->executionTime,
            CHARPTR(commandTable[m->command].name));
        for(i=0;i<m->unitCount;i++)
            fprintf(fp, " %ld",m->units[i]);
        if(m->command==COMMAND_NONE)
        {
            if(m->targetCount>0)
                pt=PARAM_UNIT;
            else
                pt=PARAM_POSITION;
        }
        else
            pt=commandTable[m->command].paramType;
        switch(pt)
        {
            case PARAM_POSITION:
                fprintf(fp, " Position: (%f,%f)", m->pos.x, m->pos.y);
                break;
            case PARAM_CONSTRUCTION:
                fprintf(fp, " Position: (%f,%f) Next Unit ID: %ld", m->pos.x, m->pos.y, nextUnitID);
                break;
            case PARAM_UNIT:
                fprintf(fp, " Targets:");
                for(i=0;i<m->targetCount;i++)
                    fprintf(fp, " %ld",m->targets[i]);
                break;
            case PARAM_PRODUCTION:
                fprintf(fp, " Quantity: %d Repeat: %s", m->quantity, m->repeat?"yes":"no");
                break;
        }
        fprintf(fp,"\n");
        fclose(fp);
    }
#endif

    if(commandTable[m->command].paramType==PARAM_CONSTRUCTION)
    {
        //In cases where the selection consists of units belonging to different
        //players, the building's owner is arbitrary.  This shouldn't cause a
        //problem unless allies dissolve their alliance later in the game.
        for(i=0;i<m->unitCount;i++)
        {
            u=unitLookup(m->units[i]);
            if(u)
            {
                //If all of the construction units sent to build this unit have been
                //destroyed, then cancel the order.  this *should* be synchronized.
                owner=u->getOwner();

                ut=unitTypeLookup(commandTable[m->command].unitID);
                u=createUnit(owner, ut, m->pos, iHat2D, toWorldTime(m->executionTime));
                if(u)
                    c=mwNew tCommand(m->command, PARAM_UNIT, u->getID());
                break;
            }
        }
    }
    else
        c=mwNew tCommand(*m);

    if(c)
        for(i=0;i<m->unitCount;i++)
        {
            u=unitLookup(m->units[i]);
            if(u)
                u->processCommand(*c, toWorldTime(m->executionTime));
        }

    //If the unit ID wasn't found, the unit was probably just deleted
    //In this event, ignore the message (this *should* be sychronized)
    //bug("processMessage: unit ID %ld not found", message->unitID);
    if(c)
        mwDelete c;
    mwDelete m;
}
/****************************************************************************/
void onSetTimingInGameMsg(tSetTimingMsg *message) throw(int)
{
#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Latency set to %ld, Net Interval set to %ld, Game Interval set to %ld, "
            "Game speed set to %3.1fx\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->latency,message->netInterval,message->gameInterval,
            message->gameSpeed);
        fclose(fp);
    }
#endif

    //Change game interval and speed right away as these have no bearing on the network.
    gameInterval = message->gameInterval;
    worldTimeBias = toWorldTime(message->executionTime)-message->executionTime*message->gameSpeed/1000.0;
    gameSpeed = message->gameSpeed;

    //Only broadcast a 'timing divider' message if the latency and/or network
    //interval have changed.
    if(latency != message->latency || netInterval != message->netInterval)
    {
        /* Broadcast a 'timing divider' message to every peer.  The game interval/speed is
         * not used, but we pass it anyway for compatibility with the tSetTimingMsg
         * structure.  The latency parameter actually determines the amount to adjust
         * the stop time on each peer.
         */
        netInterval = message->netInterval;
        tSetTimingMsg timingDivider(MSG_TIMING_DIVIDER, latency - message->latency, netInterval, gameInterval, gameSpeed);
        postMessage(&timingDivider);

        //Update the latency after we have calculated the difference.
        latency=message->latency;
        onSetTiming();
    }
    else
        onSetGameSpeed();
    //I define a separate event handler for game speed adjustment so that
    //the player's screen doesn't fill with timing notifications as the
    //game host holds down '+' or '-'.
}
/****************************************************************************/
void onSyncQueryMsg(tGameMsg *message) throw(int)
{
    int i;
    syncUnitCount=0;
    for(i=0;i<units.size();i++)
    {
        if(units[i]->getState()>=STATE_SELECTABLE)
            syncUnitCount++;
        units[i]->setSyncPos();
        units[i]->setSyncHP();
    }
#ifdef DEBUGMISSILE
    int j=0;
    syncMissileCount=0;
    for(i=0;i<missiles.size();i++)
        if(missiles[i]->endTime<message->executionTime)
        {
            syncMissileData[j++]=missiles[i]->source?missiles[i]->source->getID():-1;
            syncMissileData[j++]=missiles[i]->target?missiles[i]->target->getID():-1;
            syncMissileData[j++]=missiles[i]->initTime;
            syncMissileData[j++]=missiles[i]->endTime;
            syncMissileCount++;
        }
#endif
    //This variable holds the synchronization time on the host and client.
    //We use it ensure that the host compares synchronization data for the same instant in time.
    syncResponseTimer=message->executionTime;
    if(isGameClient)
        syncReceived=true;
}
/****************************************************************************/
void onSyncMsg(tDescriptor *desc, tSyncMsg *message) throw(int)
{
    char *values = message->data;
    char *end = message->data + message->bytes;
    long id,unitCount;
    float HP;
    tVector2D pos=zeroVector2D;
    int i,playerID;
    bool found=false;
    tGameTime t;

    t=*((tGameTime *)values);
    values+=sizeof(tGameTime);
    playerID=*((short *)values);
	values+=sizeof(short);

    //Outdated sync message received.  Discard it.
    if(t!=syncResponseTimer)
        return;

#ifdef DEBUGMISSILE
    int j=0;
    long missileCount,sid1,tid1,initTime1,endTime1;
    long sid2,tid2,initTime2,endTime2;
    missileCount=*((long *)values);
    values+=sizeof(long);
    if(syncMissileCount!=missileCount)
    {
        log("Discrepancy for Player %d\n    Missile Count 1: %ld"
                                      "    Missile Count 2: %ld",
            playerID, syncMissileCount, missileCount);
        found=true;
    }
    for(i=0;i<syncMissileCount;i++)
    {
        sid1=syncMissileData[j++];
        tid1=syncMissileData[j++];
        initTime1=syncMissileData[j++];
        endTime1=syncMissileData[j++];

        sid2=*((long *)values);
        values+=sizeof(long);
        tid2=*((long *)values);
        values+=sizeof(long);
        initTime2=*((long *)values);
        values+=sizeof(long);
        endTime2=*((long *)values);
        values+=sizeof(long);
        if(sid1!=sid2 || tid1!=tid2 || initTime1!=initTime2 || endTime1!=endTime2)
        {
            log("Discrepancy for Player %d\n    Source 1: %ld  Target 1: %ld  Init Time 1: %ld  End Time 1: %ld"
                                          "    Source 2: %ld  Target 2: %ld  Init Time 2: %ld  End Time 2: %ld",
                playerID, sid1,tid1,initTime1,endTime1,
                sid2,tid2,initTime2,endTime2);
            found=true;
        }
    }
#endif
    unitCount=*((long *)values);
	values+=sizeof(long);
    if(syncUnitCount!=unitCount)
    {
        log("Discrepancy for Player %d\n    Unit Count 1: %ld"
                                      "    Unit Count 2: %ld",
            playerID, syncUnitCount, unitCount);
        found=true;
    }
    i=0;
    while(values<end)
    {
        id=*((long *)values);
		values+=sizeof(long);
        pos.x=*((float *)values);
		values+=sizeof(float);
        pos.y=*((float *)values);
		values+=sizeof(float);
        HP=*((float *)values);
		values+=sizeof(float);
        while(i<units.size() && units[i]->getID()<id)
            i++;
        if(i==units.size())
            return;
        if(id==units[i]->getID())
        {
            if(units[i]->getSyncPos() != pos || units[i]->getSyncHP() != HP)
            {
                log("Discrepancy for Player %d Unit %d\n    Position 1: (%.20f,%.20f) HP 1: %.20f\n"
                                                      "    Position 2: (%.20f,%.20f) HP 2: %.20f",
                    playerID, id,
                    units[i]->getSyncPos().x,units[i]->getSyncPos().y, units[i]->getSyncHP(),
                    pos.x,pos.y,HP);
                found=true;
            }

        }
    }
    if(found)
        onDiscrepancyDetected();
}
/****************************************************************************/
void onUnitPlanMsg(tUnitCreateMsg *message) throw(int)
{
    tUnitType *ut=unitTypeLookup(message->unitTypeID);
#ifdef DEBUG
    if(MEMORY_VIOLATION(ut))
        return;
    if(message->ownerID<0 || message->ownerID>=MAX_PLAYERS)
    {
        bug("onUnitPlanMsg: invalid owner id");
        mwDelete message;
        throw -1;
    }
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Planned a %s for %s at (%f,%f)\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            CHARPTR(ut->name), CHARPTR(players[message->ownerID].name),
            message->pos.x, message->pos.y);
        fclose(fp);
    }
#endif

    createUnit(message->ownerID, ut, message->pos, iHat2D, toWorldTime(message->executionTime));
}
/****************************************************************************/
void onUnitCreateMsg(tUnitCreateMsg *message) throw(int)
{
    tUnitType *ut=unitTypeLookup(message->unitTypeID);
    tUnit *u;
#ifdef DEBUG
    if(MEMORY_VIOLATION(ut))
        return;
    if(message->ownerID<0 || message->ownerID>=MAX_PLAYERS)
    {
        bug("onUnitCreateMsg: invalid owner id");
        mwDelete message;
        throw -1;
    }
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Created a %s for %s at (%f,%f)\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            CHARPTR(ut->name), CHARPTR(players[message->ownerID].name),
            message->pos.x, message->pos.y);
        fclose(fp);
    }
#endif

    u=createUnit(message->ownerID, ut, message->pos, iHat2D, toWorldTime(message->executionTime));
    if(u)
        u->setHP(u->getMaxHP(), toWorldTime(message->executionTime));
}
/****************************************************************************/
void onUnitDestroyMsg(tUnitMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Destroyed units:",
            CHARPTR(players[message->playerID].name),message->executionTime);
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
            destroyUnit(u, toWorldTime(message->executionTime));
    }
}
/****************************************************************************/
void onUnitSetPosMsg(tUnitVector2DMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Moved units to (%f,%f):",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->v.x,message->v.y);
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
        {
            removeNeighbor(u,u->getHashValue());
            u->setPosition(message->v, toWorldTime(message->executionTime));
            addNeighbor(u,u->getHashValue());
        }
    }
}
/****************************************************************************/
void onUnitSetVelocityMsg(tUnitVector2DMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Set velocity vector to (%f,%f):",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->v.x,message->v.y);
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
            u->setVelocity(message->v, toWorldTime(message->executionTime));
    }
}
/****************************************************************************/
void onUnitSetForwardMsg(tUnitVector2DMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Set forward vector to (%f,%f):",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->v.x,message->v.y);
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
            u->setForward(message->v, toWorldTime(message->executionTime));
    }
}/****************************************************************************/
void onUnitSetHPMsg(tUnitFloatMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Set units' health to %f:",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->val);
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
            u->setHP(message->val, toWorldTime(message->executionTime));
    }
}
/****************************************************************************/
void onUnitSetOwnerMsg(tUnitLongMsg *message) throw(int)
{
    int i;
    tUnit *u;

#ifdef DEBUG
    if(message->val<0 || message->val>=MAX_PLAYERS)
    {
        bug("onUnitSetOwnerMsg: invalid val");
        mwDelete message;
        throw -1;
    }
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Gave units to %s:",
            CHARPTR(players[message->playerID].name),message->executionTime,
            CHARPTR(players[message->val].name));
        for(i=0;i<message->unitCount;i++)
            fprintf(fp, " %ld",message->units[i]);
        fprintf(fp, "\n");
        fclose(fp);
    }
#endif

    for(i=0;i<message->unitCount;i++)
    {
        u=unitLookup(message->units[i]);
        if(u)
        {
            removeNeighbor(u,u->getHashValue());
            u->setOwner(message->val, toWorldTime(message->executionTime));
            addNeighbor(u,u->getHashValue());
        }
    }
}
/****************************************************************************/
void onSetAllianceMsg(tSetAllianceMsg *message) throw(int)
{
    int i;
#ifdef DEBUG
    if(message->sourcePlayerID<0 || message->sourcePlayerID>=MAX_PLAYERS)
    {
        bug("onSetAllianceMsg: invalid sourcePlayerID");
        mwDelete message;
        throw -1;
    }
    if(message->targetPlayerID<0 || message->targetPlayerID>=MAX_PLAYERS)
    {
        bug("onSetAllianceMsg: invalid targetPlayerID");
        mwDelete message;
        throw -1;
    }
    if(message->alliance<ALLIANCE_SELF || message->alliance>ALLIANCE_ENEMY)
    {
        bug("onSetAllianceMsg: invalid alliance");
        mwDelete message;
        throw -1;
    }
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Set the alliance of %s with %s to %s; "
            "Shared control: %s Shared resources: %s Shared vision: %s\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            CHARPTR(players[message->sourcePlayerID].name),
            CHARPTR(players[message->targetPlayerID].name),
            allianceNames[message->alliance],
            message->sharedControl?"yes":"no",
            message->sharedResources?"yes":"no",
            message->sharedVision?"yes":"no");
        fclose(fp);
    }
#endif

	for(i=0;i<selectedUnits.size();i++)
        selectedUnits[i]->deselect();
    players[message->sourcePlayerID].alliances[message->targetPlayerID] =
        message->alliance;
    players[message->sourcePlayerID].sharedControl[message->targetPlayerID] =
        message->sharedControl;
    players[message->sourcePlayerID].sharedResources[message->targetPlayerID] =
        message->sharedResources;
    players[message->sourcePlayerID].sharedVision[message->targetPlayerID] =
        message->sharedVision;
    for(i=0;i<selectedUnits.size();i++)
        selectedUnits[i]->select();
    updateApparitionOwner();

    //Someone changed their alliance with you:
    if(localPlayerID==message->targetPlayerID)
        onSetAlliance(message->sourcePlayerID);
}
/****************************************************************************/
void onSetResourceMsg(tSetResourceMsg *message) throw(int)
{
#ifdef DEBUG
    if(message->targetPlayerID<0 || message->targetPlayerID>=MAX_PLAYERS)
    {
        bug("onSetResourceMsg: invalid targetPlayerID");
        mwDelete message;
        throw -1;
    }
    if(message->resourceID<0 || message->resourceID>=MAX_RESOURCES)
    {
        bug("onSetResourceMsg: invalid resourceID");
        mwDelete message;
        throw -1;
    }
    if(message->amount<0)
    {
        bug("onSetResourceMsg: invalid amount");
        mwDelete message;
        throw -1;
    }
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Assigned %f %s to %s\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->amount,CHARPTR(resources[message->resourceID].name),
            CHARPTR(players[message->targetPlayerID].name));
        fclose(fp);
    }
#endif

    players[message->targetPlayerID].resources[message->resourceID] =
        MIN(message->amount,players[message->targetPlayerID].maxResources[message->resourceID]);
    players[message->targetPlayerID].r0[message->resourceID] =
        players[message->targetPlayerID].resources[message->resourceID];
}
/****************************************************************************/
void onDepositCreateMsg(tDepositCreateMsg *message) throw(int)
{
#ifdef DEBUG
    if(!doesResourceExist(message->resourceID))
        bug("onDepositCreateMsg: invalid resource");
#endif

#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Created a deposit of %f %s at (%f,%f) with a radius of %f\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->amount, CHARPTR(resources[message->resourceID].name),
            message->center.x, message->center.y, message->radius);
        fclose(fp);
    }
#endif

    createDeposit(message->resourceID, message->center, message->radius, message->amount, toWorldTime(message->executionTime));
}
/****************************************************************************/
void onDepositDestroyMsg(tDepositDestroyMsg *message) throw(int)
{
#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Destroyed deposit %ld\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->depositID);
        fclose(fp);
    }
#endif
    tDeposit *d=depositLookup(message->depositID);
    if(d)
        destroyDeposit(d, toWorldTime(message->executionTime));
}
/****************************************************************************/
void onDepositSetMsg(tDepositSetMsg *message) throw(int)
{
#ifdef LOGCOMMAND
    FILE *fp=fopen(OUTPUT_FILE,"a");
    if(fp)
    {
        fprintf(fp, "%10s %10ld Assigned %f resources to deposit %ld\n",
            CHARPTR(players[message->playerID].name),message->executionTime,
            message->amount, message->depositID);
        fclose(fp);
    }
#endif
    tDeposit *d=depositLookup(message->depositID);
    if(d)
        d->a0=d->amount=message->amount;
}
/****************************************************************************/
//reset player assignments
void resetPlayers()
{
    localPlayerID=-1;
    for(int i=0;i<MAX_PLAYERS;i++)
    {
        players[i].assignedType=PLAYER_NONE;
        players[i].assignedUser=NULL;
    }

    onResetPlayers();
}
/****************************************************************************/
void unloadMap()
{
    deleteObjects();
    mapLoaded=false;

    //Reset important variables to eliminate artifacts:
    mapDimensions=zeroVector2D;

    onUnloadMap();
}
/****************************************************************************/
void unloadWorld()
{
    int i;
    if(mapLoaded)
        unloadMap();
    worldLoaded=false;

    destroyUnitTypes();
    //It's better to destroy missile types after unit types since behaviors contain
    //references to missile types.
    destroyMissileTypes();

    destroyModelTypes();

    for(i=0;i<MAX_RESOURCES;i++)
    {
        resources[i].name="";
		resources[i].caption="";
        resources[i].exists=false;
    }

    for(i=0;i<MAX_ARMOR_CLASSES;i++)
    {
        armorClasses[i].name="";
        armorClasses[i].exists=false;
    }

	resetCommandTable();

    onUnloadWorld();
}
/****************************************************************************/
void parseWorld(tStream& s) throw(tString)
{
    int currentResourceSlot=0;
    int currentArmorClassSlot=0;
    char buf[100];

    while(!s.atEndOfStream())
    {
		tString str=parseString("tag",s);
		s.matchOptionalToken("=");

		if(strPrefix(str,"resource"))
		{
			if(currentResourceSlot==MAX_RESOURCES)
			{
				snprintf(buf,sizeof(buf),"You can only define %d resources!",MAX_RESOURCES);
				throw tString(buf);
			}
			parseResourceBlock(currentResourceSlot,s);
			currentResourceSlot++;
		}
		else if(strPrefix(str,"armor_class"))
		{
			if(currentArmorClassSlot==MAX_ARMOR_CLASSES)
			{
				snprintf(buf,sizeof(buf),"You can only define %d armor classes!",MAX_ARMOR_CLASSES);
				throw tString(buf);
			}
			tString identifier=parseIdentifier("armor class name",s);
			if(armorClassLookup(identifier,/*exact=*/true)>=0)
				throw tString("An armor class by the name of '")+identifier+"' already exists";
			armorClasses[currentArmorClassSlot].name=identifier;
			armorClasses[currentArmorClassSlot].exists=true;
			currentArmorClassSlot++;
		}
		else if(strPrefix(str,"unit_type"))
			parseUnitTypeBlock(s);
		else if(strPrefix(str,"missile_type"))
			parseMissileTypeBlock(s);
		else if(strPrefix(str,"model_type"))
			parseModelTypeBlock(s);
		else if(strPrefix(str,"command"))
			parseCommandBlock(s);
		else if(strPrefix(str,"end"))
			return;
		else
			throw tString("Unrecognized tag '")+str+"'";

		s.endStatement();
	}
}
/****************************************************************************/
//load world from file
//precondition: you must set world filename with setWorldFile or setWorldFilename
//throws a tString containing error message if world fails to load
void loadWorld() throw(tString)
{
    FILE *fp;

    if(worldLoaded)
        unloadWorld();

    fp=worldFile.open("r");
    tStream stream;

    if(fp==NULL)
        throw "Failed to open '"+worldFile.getFullName()+"'";

    try
    {
        stream.load(fp);
        parseWorld(stream);

        //Look-up references to unit types in construction and production behaviors.
        lookupUnitTypes();
    }
    catch(const tString& message)
    {
        tString error=stream.parseErrorMessage(CHARPTR(worldFile.getFullName()),message);
        fclose(fp);
        unloadWorld();
        throw error;
    }

    fclose(fp);
    worldLoaded=true;
    onLoadWorld();
}
/****************************************************************************/
void parseMap(tStream& s) throw(tString)
{
    int currentPlayerSlot=0;
    int pnum, i;
    tUnitType *ut;
    tUnit *u;
    tVector2D v;
    char buf[100];

    while(!s.atEndOfStream())
    {
		tString str=parseString("tag",s);
		s.matchOptionalToken("=");

		if(strPrefix(str,"size"))
		{
			if(mapDimensions.x!=0)
				throw tString("You have already defined the map size!");
			mapDimensions.x=parseFloatMin("map width",1,s);
			mapDimensions.y=parseFloatMin("map height",1,s);

			hashWidth=(int)(mapDimensions.x/HASH_SPACING)+1;
			hashHeight=(int)(mapDimensions.y/HASH_SPACING)+1;
			hashSize=hashWidth*hashHeight*MAX_PLAYERS;
			unitHash=mwNew tUnit *[hashSize];
			memset((char *)unitHash,0,hashSize*sizeof(tUnit *));

			fogOfWarWidth=(int)mapDimensions.x+1;
			fogOfWarHeight=(int)mapDimensions.y+1;
			fogOfWarSize=fogOfWarWidth*fogOfWarHeight;

			//Allocate enough memory to assign a counter to every tile.  Every tile
			//is initially invisible.  We must allocate memory here since subsequent
			//unit placements affect the fog of war.
			for(i=0;i<MAX_PLAYERS;i++)
			{
				players[i].fogOfWar=mwNew short[fogOfWarSize];
				memset((void*)players[i].fogOfWar,'\0',fogOfWarSize*sizeof(short));
			}
		}
		else if(strPrefix(str,"terrain"))
		{
			if(mapDimensions.x==0)
				throw tString("You need to define the map size before you define the terrain!");
			if(terrain)
				throw tString("You have already defined the terrain!");
			terrain = mwNew tTerrain(mapDimensions.x, mapDimensions.y, s);

			//Orient the map with the new min and max terrain elevation.
			orientMap();
			//Call onSetViewDimensions() so that the terrain can initializes
			//its level of detail properly.
			if(terrain)
				terrain->onSetViewDimensions();
		}
		else if(strPrefix(str,"deposit"))
		{
			if(mapDimensions.x==0)
				throw tString("You need to define the map size before you define any deposits!");
			if(terrain==NULL)
				throw tString("You need to define the terrain before you define any deposits!");
			parseDepositBlock(s);
		}
		else if(strPrefix(str,"player"))
		{
			if(currentPlayerSlot==MAX_PLAYERS)
			{
				snprintf(buf,sizeof(buf),"You can only define %d players!",MAX_PLAYERS);
				throw tString(buf);
			}
			if(mapDimensions.x==0)
				throw tString("You need to define the map size before you define any players!");
			parsePlayerBlock(currentPlayerSlot,s);
			currentPlayerSlot++;
		}
		else if(strPrefix(str,"light"))
		{
			if(lights.size()==GL_MAX_LIGHTS)
			{
				snprintf(buf,sizeof(buf),"You can only define %d lights!",GL_MAX_LIGHTS);
				throw tString(buf);
			}
			parseLightBlock(s);
		}
		else if(strPrefix(str,"unit"))
		{
			if(mapDimensions.x==0)
				throw tString("You need to define the map size before you define any units!");
			if(terrain==NULL)
				throw tString("You need to define the terrain before you define any units!");
			pnum=parsePlayer("owner",s);
			ut=parseUnitType("unit type",s);
			v=parseWorldPos("location",s);

			u=createUnit(pnum, ut, v, iHat2D, 0);
			if(u)
				u->setHP(u->getMaxHP(), 0);
		}
		else if(strPrefix(str,"shadow_opacity"))
			shadowOpacity=parseFloatRange("shadow opacity",0,1,s);
		else if(strPrefix(str,"shadow_light_position"))
		{
			shadowLightPosition[0]=parseFloat("x-component of light position",s);
			shadowLightPosition[1]=parseFloat("y-component of light position",s);
			shadowLightPosition[2]=parseFloat("z-component of light position",s);
			shadowLightPosition[3]=parseFloat("w-component of light position",s);
		}
		else if(strPrefix(str,"ambient_light"))
		{
			ambientLight[0]=parseFloatRange("red component of ambient light",0,1,s);
			ambientLight[1]=parseFloatRange("green component of ambient light",0,1,s);
			ambientLight[2]=parseFloatRange("blue component of ambient light",0,1,s);
			ambientLight[3]=parseFloatRange("alpha component of ambient light",0,1,s);
		}
		else if(strPrefix(str,"end"))
			break;
		else
			throw tString("Unrecognized tag '")+str+"'";

		s.endStatement();
	}
    if(mapDimensions.x==0)
        throw tString("You need to define the size of the map!");
}
/****************************************************************************/
void parseDepositBlock(tStream& s) throw(tString)
{
    //Flag these variables so we know if we have initialized them yet.
    int resourceID=-1;
    float radius=-1;
    float amount=-1;
    tVector2D v={-1,-1};

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"resource"))
				resourceID=parseResource("resource",s);
			else if(strPrefix(str,"position"))
				v=parseWorldPos("position",s);
			else if(strPrefix(str,"radius"))
				radius=parseFloatMin("radius",0,s);
			else if(strPrefix(str,"amount")||strPrefix(str,"quantity"))
				amount=parseFloatMin("amount",0,s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}
	if(resourceID<0)
		throw tString("You need to define a resource for this deposit!");
	if(v.x<0)
		throw tString("You need to define a position for this deposit!");
	if(radius<0)
		throw tString("You need to define a radius for this deposit!");
	if(amount<0)
		throw tString("You need to define an amount for this deposit!");

	createDeposit(resourceID,v,radius,amount,0);
}
/****************************************************************************/
void parsePlayerBlock(int slot, tStream& s) throw(tString)
{
    char buf[100];
    tString name;

#ifdef DEBUG
    if(slot<0 || slot>=MAX_PLAYERS)
    {
        bug("parsePlayerBlock: invalid slot");
        return;
    }
#endif

	if(!s.atEndOfLine())
	{
		name=parseIdentifier("player name",s);
		if(playerLookup(name,/*exact=*/true)>=0)
			throw tString("A player by the name of '")+name+"' already exists";
		players[slot].name=name;
	}
	else //Assign a default name to the player if one does not exist
	{
		snprintf(buf,sizeof(buf),"Player%d",slot);
		players[slot].name=tString(buf);
	}

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag", s);
			s.matchOptionalToken("=");

            if(strPrefix(str,"caption"))
                players[slot].caption=parseString("caption",s);
            else if(strPrefix(str,"type"))
                players[slot].type=parsePlayerType("player type",s);
            else if(strPrefix(str,"start_position"))
                players[slot].startLocation=parseWorldPos("start location", s);
            else if(strPrefix(str,"resources"))
                parseResourceBlock(players[slot].resources, s);
            else if(strPrefix(str,"maximum_resources") || strPrefix(str,"max_resources"))
                parseResourceBlock(players[slot].maxResources, s);
            else
                throw tString("Unrecognized tag '")+str+"'";
        }
    }

	if(players[slot].caption=="")
		players[slot].caption=name;
    if(players[slot].type==PLAYER_NONE)
        throw tString("You need to define a type for this player!");
    players[slot].exists=true;
}
/****************************************************************************/
void parseResourceBlock(int slot, tStream& s) throw(tString)
{	 
	tString name;
    
#ifdef DEBUG
	if(slot<0 || slot>=MAX_RESOURCES)
	{
		bug("parseResourceBlock: invalid slot");
		return;
	}
#endif

	name=parseIdentifier("resource name",s);
	if(resourceLookup(name,/*exact=*/true)>=0)
		throw tString("A resource by the name of '")+name+"' already exists";
	resources[slot].name=name;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"caption"))
				resources[slot].caption=parseString("caption",s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

	if(resources[slot].caption=="")
		resources[slot].caption=resources[slot].name;

	resources[slot].exists=true;
}
/****************************************************************************/
void parseLightBlock(tStream& s) throw(tString)
{
    tString name;

    tLight l;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
            tString str=parseString("tag",s);
			s.matchOptionalToken("=");

			if(strPrefix(str,"ambient"))
			{
				l.ambient[0]=parseFloatRange("red component of ambient",0,1,s);
				l.ambient[1]=parseFloatRange("blue component of ambient",0,1,s);
				l.ambient[2]=parseFloatRange("green component of ambient",0,1,s);
				l.ambient[3]=parseFloatRange("alpha component of ambient",0,1,s);
			}
			else if(strPrefix(str,"diffuse"))
			{
				l.diffuse[0]=parseFloatRange("red component of diffuse",0,1,s);
				l.diffuse[1]=parseFloatRange("blue component of diffuse",0,1,s);
				l.diffuse[2]=parseFloatRange("green component of diffuse",0,1,s);
				l.diffuse[3]=parseFloatRange("alpha component of diffuse",0,1,s);
			}
			else if(strPrefix(str,"specular"))
			{
				l.specular[0]=parseFloatRange("red component of specular",0,1,s);
				l.specular[1]=parseFloatRange("blue component of specular",0,1,s);
				l.specular[2]=parseFloatRange("green component of specular",0,1,s);
				l.specular[3]=parseFloatRange("alpha component of specular",0,1,s);
			}
			else if(strPrefix(str,"position"))
			{
				tVector3D v=parseVector3D("position",s);
				float w=parseFloat("w-component of position",s);
				if(w==0)
					v=v.normalize();
				else
					v/=w;
				l.position[0]=v.x;
				l.position[1]=v.y;
				l.position[2]=v.z;
				l.position[3]=w;
			}
			else if(strPrefix(str,"spot_direction"))
			{
				l.spotDirection[0]=parseFloat("x-component of spot direction",s);
				l.spotDirection[1]=parseFloat("y-component of spot direction",s);
				l.spotDirection[2]=parseFloat("z-component of spot direction",s);
			}
			else if(strPrefix(str,"spot_exponent"))
				l.spotExponent=parseFloatRange("spot exponent",0,128,s);
			else if(strPrefix(str,"spot_cutoff"))
			{
				l.spotCutoff=parseFloatRange("spot cutoff",0,180,s);
				if(l.spotCutoff>90 && l.spotCutoff<180)
					throw tString("Please specify a spot cutoff of 180 or a value between 0 and 90");
			}
			else if(strPrefix(str,"constant_attenuation"))
				l.constantAttenuation=parseFloatMin("constant attenuation",0,s);
			else if(strPrefix(str,"linear_attenuation"))
				l.linearAttenuation=parseFloatMin("linear attenuation",0,s);
			else if(strPrefix(str,"quadratic_attenuation"))
				l.quadraticAttenuation=parseFloatMin("quadratic attenuation",0,s);
			else
				throw tString("Unrecognized tag '")+str+"'";

			s.endStatement();
		}
	}

    lights.push_back(l);
}
/****************************************************************************/
//load map data from file and initialize objects
//you must call setMapFile or setMapName prior to calling this function
//throws a tString containing error message if map failed to load
void loadMap() throw(tString)
{
    int i,j;
    tString name;
    const float _position[4]={0,0,1,0};
    const float _ambientLight[4]={0.2,0.2,0.2,1.0};

    if(mapLoaded)
        unloadMap();

    if(!worldLoaded)
        throw tString("You must load world file first.");

    FILE *fp=mapFile.open("r");
    tStream stream;
    if(fp==NULL)
        throw "Failed to open file '"+mapFile.getFullName()+"'";

    for(i=0;i<MAX_PLAYERS;i++)
    {
        //Clear player name.  The player name may be set in the map file (below)
        //or in initializeGame.
        players[i].name="";
		players[i].caption="";

        //parsePlayerBlock will set this flag to true
        players[i].exists=false;

        //Reset start location to zero for good measure.
        players[i].startLocation=zeroVector2D;

        //Resources default to zero:
        for(j=0;j<MAX_RESOURCES;j++)
        {
            players[i].resources[j]=0;
            players[i].maxResources[j]=0;
            players[i].revenues[j]=0;
            players[i].costs[j]=0;
            players[i].foreignAid[j]=0;
            players[i].r0[j]=0;
            players[i].resourceAvailable[j]=true;
            players[i].resourceCollectable[j]=true;
        }
    }

    mapDimensions=zeroVector2D;
    nextUnitID=1;
    nextDepositID=1;

    for(i=0;i<4;i++)
        ambientLight[i]=_ambientLight[i];
    for(i=0;i<4;i++)
        shadowLightPosition[i]=_position[i];
    shadowOpacity=0.2;

    try
    {
        stream.load(fp);
        parseMap(stream);
    }
    catch(const tString& message)
    {
        tString error=stream.parseErrorMessage(CHARPTR(mapFile.getFullName()),message);
        fclose(fp);
        unloadMap();
        throw error;
    }

    fclose(fp);

    //Everyone starts out as enemies but you're allied to yourself:
    //NOTE: Everyone is neutral with hidden players and vice versa.
    //In the beginning, no two players share vision, resources, nor control.
    for(i=0;i<MAX_PLAYERS;i++)
    {
        for(j=0;j<MAX_RESOURCES;j++)
        {
            if(players[i].resources[j]>players[i].maxResources[j])
                players[i].resources[j]=players[i].maxResources[j];
            players[i].r0[j]=players[i].resources[j];
        }
        for(j=0;j<MAX_PLAYERS;j++)
        {
            if(players[i].type==PLAYER_HIDDEN || players[j].type==PLAYER_HIDDEN)
                players[i].alliances[j]=ALLIANCE_NEUTRAL;
            else
                players[i].alliances[j]=ALLIANCE_ENEMY;
            players[i].sharedControl[j]=false;
            players[i].sharedResources[j]=false;
            players[i].sharedVision[j]=false;
        }
        players[i].alliances[i]=ALLIANCE_SELF;
        //You can see and control your own units.  However, you cannot provide
        //foreign aid to yourself.
        players[i].sharedControl[i]=true;
        players[i].sharedResources[i]=false;
        players[i].sharedVision[i]=true;
    }

    /*i=0;
    tTime startTime=currentTime;
    while(i<500)
    {
        if(currentTime-startTime>5)
            break;
        tVector2D v=mapDimensions;
        v.x*=(rand()%1024)/1024.0;
        v.y*=(rand()%1024)/1024.0;
        float radius=(rand()%1024)/1024.0*5+3;
        float color[3],max;
        for(j=0;j<obstacles.size();j++)
            if((obstacles[j].position-v).length()<obstacles[j].radius+radius+3)
                break;
        if(j<obstacles.size())
            continue;
        color[0]=(rand()%1024)/1024.0;
        color[1]=(rand()%1024)/1024.0;
        color[2]=(rand()%1024)/1024.0;
        max=MAX3(color[0],color[1],color[2]);
        if(max>0)
        {
            color[0]/=max;
            color[1]/=max;
            color[2]/=max;
        }
        tObstacle ob(v,radius,color);
        obstacles.push_back(ob);
        i++;
    }
    */
    refreshLights();

    mapLoaded=true;
    objectsLoaded=true;
    onLoadMap();
}
/****************************************************************************/
//free memory allocated by the map (post condition: isMapInit()==false)
void deleteObjects()
{
    int i;
    objectsLoaded=false;
    for(i=0; i<units.size(); i++)
        mwDelete units[i];
    units.clear();
    for(i=0; i<missiles.size(); i++)
        mwDelete missiles[i];
    missiles.clear();
    for(i=0; i<unitExplosions.size(); i++)
        mwDelete unitExplosions[i];
    unitExplosions.clear();
    selectedUnits.clear();
    for(i=0; i<messages.size(); i++)
        mwDelete messages[i];
    messages.clear();
    if(unitHash)
    {
        mwDelete unitHash;
        unitHash=NULL;
    }
    if(terrain)
    {
        mwDelete terrain;
        terrain=NULL;
    }
    obstacles.clear();
    for(i=0;i<MAX_PLAYERS;i++)
        if(players[i].fogOfWar)
        {
            mwDelete players[i].fogOfWar;
            players[i].fogOfWar=NULL;
        }
    for(i=0;i<deposits.size();i++)
        mwDelete deposits[i];
    deposits.clear();
    for(i=0;i<lights.size();i++)
    {
        GLenum lightName=getLightName(i);
        glDisable(lightName);
    }
    lights.clear();
}
/****************************************************************************/
//update the map (prior to each frame)
void worldTick(tWorldTime t)
{
	int i,j;
    float dt=t-t0;
#ifdef DEBUG
    if(!gameRunning)
    {
        bug("worldTick: game isn't running");
        return;
    }
#endif
    //players
    for(i=0; i<MAX_PLAYERS; i++)
    {
        //resources
        for(j=0; j<MAX_RESOURCES; j++)
        {
            players[i].resources[j]=
                players[i].r0[j]+getNetProfit(i,j)*dt;
            //A player cannot exceed his resource storage capacity.  However,
            //his resources can drop below zero, representing a debt.  This ensures
            //that resources are conserved, even during a black-out.
            if(players[i].resources[j]>players[i].maxResources[j])
                players[i].resources[j]=players[i].maxResources[j];
        }
    }

    //missiles
    for(i=0; i<missiles.size(); )
    {
        missiles[i]->tick(t);
        if(missiles[i]->isFinished())
        {
            mwDelete missiles[i];
            missiles.erase(missiles.begin()+i);
        }
        else
            i++;
    }

    //units
    for(i=0; i<units.size(); )
    {
        units[i]->tick(t);
        if(units[i]->getState()==STATE_DESTROYED)
        {
            removeNeighbor(units[i],units[i]->getHashValue());
            mwDelete units[i];
            units.erase(units.begin()+i);
        }
        else
            i++;
    }

    //unit explosions
    for(i=0; i<unitExplosions.size(); )
    {
        unitExplosions[i]->tick(t);
        if(unitExplosions[i]->isFinished())
        {
            mwDelete unitExplosions[i];
            unitExplosions.erase(unitExplosions.begin()+i);
        }
        else
            i++;
    }

    //terrain
    if(terrain)
        terrain->tick(t);

    //deposits
    for(i=0; i<deposits.size(); )
    {
        if(deposits[i]->destroyed)
        {
            mwDelete deposits[i];
            deposits.erase(deposits.begin()+i);
        }
        else
        {
            //Ordinarily, this code would go in the deposit's tick function; however
            //it is easier to make tDeposit a structure without member functions.
            deposits[i]->amount = deposits[i]->a0 - deposits[i]->rate*(t-t0);
            if(deposits[i]->amount<0)
                deposits[i]->amount=0;

            i++;
        }
    }
}
/****************************************************************************/
//update the map (at gameInterval intervals)
void worldIntervalTick(tWorldTime t)
{
    int i,j,k;
    tUnit *u, *next;
    float deficit, surplus, amt;

#ifdef DEBUG
    if(!gameRunning)
    {
        bug("worldIntervalTick: game isn't running");
        return;
    }
#endif
    //units
    for(i=0; i<units.size(); i++)
        if(units[i]->getState()>STATE_DESTROYED)
            units[i]->updateState(t);
    for(i=0; i<units.size(); i++)
        if(units[i]->getState()>STATE_DESTROYED)
            units[i]->intervalTick(t);
    for(i=0; i<hashSize; i++)
        for(u=unitHash[i];u;u=next)
        {
            next=u->getNextNeighbor();
            if(u->getHashValue()!=i)
            {
                removeNeighbor(u,i);
                addNeighbor(u,u->getHashValue());
            }
        }

    //missiles
    for(i=0; i<missiles.size(); i++)
        if(!missiles[i]->isFinished())
            missiles[i]->intervalTick(t);

    //Reset all foreign aid:
    for(i=0; i<MAX_PLAYERS; i++)
        for(j=0; j<MAX_RESOURCES; j++)
            players[i].foreignAid[j]=0;

    //players
    for(i=0; i<MAX_PLAYERS; i++)
    {
        if(!players[i].exists)
            continue;

        //resources
        for(j=0; j<MAX_RESOURCES; j++)
        {
            if(!resources[j].exists)
                continue;

            players[i].r0[j]=players[i].resources[j];

            //If player i runs out or incurs a debt, then make the resource unavailable.
            if(players[i].resources[j]<=0)
                players[i].resourceAvailable[j]=false;
            else
                players[i].resourceAvailable[j]=true;

            //Check for shortage during the next game interval.
            if(players[i].resources[j] +
                (players[i].revenues[j]-players[i].costs[j])*gameInterval/1000.0 < 0)
            {
                //First, determine if one or more allies can provide foreign
                //aid.  We need to displace this deficit:
                deficit=players[i].costs[j]-players[i].revenues[j];

                //Ideally, we can find someone with a surplus:
                if(deficit>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[k].sharedResources[i] && players[k].resources[j]==players[k].maxResources[j])
                        {
                            amt=MIN(deficit, players[k].revenues[j]-players[k].costs[j]);
                            if(amt>0)
                            {
                                players[k].foreignAid[j]-=amt;
                                players[i].foreignAid[j]+=amt;
                                deficit-=amt;
                                if(deficit==0)
                                    break;
                            }
                        }

                //If player i still has a deficit, look for another player whose
                //revenue exceeds their costs:
                if(deficit>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[k].sharedResources[i] && players[k].resources[j]>0)
                        {
                            amt=MIN(deficit, players[k].revenues[j]-players[k].costs[j]);
                            if(amt>0)
                            {
                                players[k].foreignAid[j]-=amt;
                                players[i].foreignAid[j]+=amt;
                                deficit-=amt;
                                if(deficit==0)
                                    break;
                            }
                        }

                //If player i still has a deficit, find the first player with
                //resources to spare:
                if(deficit>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[k].sharedResources[i] && players[k].resources[j]>0)
                        {
                            players[k].foreignAid[j]-=deficit;
                            players[i].foreignAid[j]+=deficit;
                            deficit=0;
                            break;
                        }
            }

            //If player i exceeds his storage capacity and cannot share his resources with another player,
            //then set the resourceFull flag.  This will suspend collection of this resource.
            if(players[i].resources[j]>=players[i].maxResources[j])
                players[i].resourceCollectable[j]=false;
            else
                players[i].resourceCollectable[j]=true;

            //Check for a surplus during the next game interval
            if(players[i].resources[j] +
                (players[i].revenues[j]-players[i].costs[j])*gameInterval/1000.0 > players[i].maxResources[j])
            {
                //Determine if we can provide one or more allies with foreign aid.
                //Ideally, we can disperse this surplus eliminating all waste:
                surplus=players[i].revenues[j]-players[i].costs[j];

                //Ideally, look for one or more players with a deficit:
                if(surplus>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[i].sharedResources[k] && players[k].resources[j]<=0)
                        {
                            amt=MIN(surplus, players[k].costs[j]-players[k].revenues[j]);
                            if(amt>0)
                            {
                                players[k].foreignAid[j]+=amt;
                                players[i].foreignAid[j]-=amt;
                                surplus-=amt;
                                if(surplus==0)
                                    break;
                            }
                        }

                //If player i still has a surplus, look for one or more players
                //with a negative profit:
                if(surplus>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[i].sharedResources[k] && players[k].resources[j]<players[k].maxResources[j])
                        {
                            amt=MIN(surplus, players[k].costs[j]-players[k].revenues[j]);
                            if(amt>0)
                            {
                                players[k].foreignAid[j]+=amt;
                                players[i].foreignAid[j]-=amt;
                                surplus-=amt;
                                if(surplus==0)
                                    break;
                            }
                        }

                //If player i still has a surplus, look for a player that doesn't
                //have the maximum amount of resources.
                if(surplus>0)
                    for(k=0;k<MAX_PLAYERS;k++)
                        if(players[i].sharedResources[k] && players[k].resources[j]<players[k].maxResources[j])
                        {
                            players[k].foreignAid[j]+=surplus;
                            players[i].foreignAid[j]-=surplus;
                            surplus=0;
                            break;
                        }
            }
        }
    }

    //Update 'repairable' flags for each unit type:
    updateUnitTypes();

    //terrain
    if(terrain)
        terrain->intervalTick(t);

    //deposits
    for(i=0;i<deposits.size();i++)
    {
        deposits[i]->a0 = deposits[i]->amount;

        if(!deposits[i]->destroyed && deposits[i]->amount==0)
            destroyDeposit(deposits[i],t);
    }
    t0=t;
}
/****************************************************************************/
//creates unit u at time t
tUnit *createUnit(int owner, tUnitType *type, const tVector2D& position,
                  const tVector2D& forward, tWorldTime t)
{
#ifdef DEBUG
    //Do not check if the game is running here since we create units before
    //the game starts when we load the map.
    if(MEMORY_VIOLATION(type))
        return NULL;
#endif
    tUnit *u=NULL;
    try
    {
        u=mwNew tUnit(nextUnitID++, owner, type, position, forward, t);
    }
    catch(...)
    {
        //If the constructor throws an exception, then the memory will automatically
        //be deallocated.  Hence, there is no need to delete u here.
        return NULL;
    }

    units.push_back(u);
    addNeighbor(u,u->getHashValue());
    return u;
}
/****************************************************************************/
//destroy unit u at time t (does not show death animation)
void destroyUnit(tUnit *u, tWorldTime t)
{
	int i;
#ifdef DEBUG
    if(!gameRunning)
    {
        bug("destroyUnit: game isn't running");
        return;
    }
    CHECK_UNIT(u)
    if(MEMORY_VIOLATION(u))
        return;
#endif
    deselectUnit(u);
    u->destroy(t);
    for(i=0; i<missiles.size(); i++)
        missiles[i]->onUnitDestroyed(u,t);
    for(i=0; i<units.size(); i++)
        units[i]->onUnitDestroyed(u,t);
}
/****************************************************************************/
//adds unit to current selection
void selectUnit(tUnit *u)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("selectUnit: map isn't initialized");
        return;
    }
    CHECK_UNIT(u)
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(!u->isSelected())
    {
        u->select();
        selectedUnits.push_back(u);
    }
}
/****************************************************************************/
//removes unit from current selection
void deselectUnit(tUnit *u)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("deselectUnit: map isn't initialized");
        return;
    }
    CHECK_UNIT(u)
    if(MEMORY_VIOLATION(u))
        return;
#endif
    if(u->isSelected())
    {
        u->deselect();
        for(int i=0; i<selectedUnits.size(); i++)
            if(selectedUnits[i]==u)
            {
                selectedUnits.erase(selectedUnits.begin()+i);
                return;
            }
        bug("deselectUnit: unit not found in selectedUnits vector");
    }
}
/****************************************************************************/
//clears current selection
void clearSelection()
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("clearSelection: map isn't initialized");
        return;
    }
#endif
    for(int i=0; i<selectedUnits.size(); i++)
        selectedUnits[i]->deselect();
    selectedUnits.clear();
}
/****************************************************************************/
//uses the OpenGL pick buffer to find a unit which contains (x,y)
//if there are multiple units, function returns front-most unit
//NOTE: x and y are measured in pixels
tUnit *findUnitAt(int x, int y, bool alliesFirst)
{
    int hits, names, i, j;
    GLuint buf[PICK_BUFFER_SIZE], *ptr=buf, minz, maxz;
    GLuint best1=0xffffffff,best2=0xffffffff,best3=0xffffffff;
    tUnit *u, *bestUnit=NULL,*bestAlly=NULL,*bestEnemy=NULL;

    hits = pickUnits(x-VIEW_TOLERANCE,y-VIEW_TOLERANCE,VIEW_TOLERANCE*2,VIEW_TOLERANCE*2,buf,/*convexShell=*/false);

    for(i=0; i<hits; i++)
    {
        names=*(ptr++);
        minz=*(ptr++);
        maxz=*(ptr++);
        for(j=0;j<names;j++)
        {
            u=unitLookup(*ptr);
            if(u==NULL)
            {
                bug("findUnitAt: invalid unit ID");
                return NULL;
            }
            if(minz<best1)
            {
                best1=minz;
                bestUnit=u;
            }
            if(minz<best2 && getAlliance(u->getOwner())<=ALLIANCE_ALLY)
            {
                best2=minz;
                bestAlly=u;
            }
            if(minz<best3 && getAlliance(u->getOwner())>=ALLIANCE_NEUTRAL)
            {
                best3=minz;
                bestEnemy=u;
            }
            ptr++;
        }
    }
    if(alliesFirst && bestAlly)
        return bestAlly;
    else if(!alliesFirst && bestEnemy)
        return bestEnemy;
    else if(bestUnit)
        return bestUnit;
    else
    {
        hits = pickUnits(x-VIEW_TOLERANCE,y-VIEW_TOLERANCE,VIEW_TOLERANCE*2,VIEW_TOLERANCE*2,buf,/*convexShell=*/true);
        for(i=0; i<hits; i++)
        {
            names=*(ptr++);
            minz=*(ptr++);
            maxz=*(ptr++);
            for(j=0;j<names;j++)
            {
                u=unitLookup(*ptr);
                if(u==NULL)
                {
                    bug("findUnitAt: invalid unit ID");
                    return NULL;
                }
                if(minz<best1)
                {
                    best1=minz;
                    bestUnit=u;
                }
                ptr++;
            }
        }
        return bestUnit;
    }
}
/****************************************************************************/
//finds unit closest to pixel position (within d pixels)
tUnit *findUnitOnMinimap(int centerX, int centerY, bool alliesFirst)
{
    int dx,dy,sum,bestSum1=INT_MAX,bestSum2=INT_MAX,bestSum3=INT_MAX;
    tUnit *bestUnit=NULL,*bestAlly=NULL,*bestEnemy=NULL;
    tVector2D p;
    int x,y;
    for(int i=0; i<units.size(); i++)
        if(units[i]->getState()>=STATE_SELECTABLE && canPlayerSee(units[i]))
        {
            p=units[i]->getPosition();
            x=(int)(minimapMatrix[0]*p.x + minimapMatrix[4]*p.y + minimapMatrix[12]);
            y=(int)(minimapMatrix[1]*p.x + minimapMatrix[5]*p.y + minimapMatrix[13]);

            dx=x-centerX;
            dy=y-centerY;
            sum=dx*dx+dy*dy;
            if(sum<=MINIMAP_TOLERANCE*MINIMAP_TOLERANCE)
            {
                if(sum<bestSum1)
                {
                    bestSum1=sum;
                    bestUnit=units[i];
                }
                if(sum<bestSum2 && getAlliance(units[i]->getOwner())<=ALLIANCE_ALLY)
                {
                    bestSum2=sum;
                    bestAlly=units[i];
                }
                if(sum<bestSum3 && getAlliance(units[i]->getOwner())>=ALLIANCE_NEUTRAL)
                {
                    bestSum3=sum;
                    bestEnemy=units[i];
                }
            }
       }
    if(alliesFirst && bestAlly)
        return bestAlly;
    else if(!alliesFirst && bestEnemy)
        return bestEnemy;
    else
        return bestUnit;
}
/****************************************************************************/
//select units with same unit type and owner
void selectUnitType(tUnitType *ut, eAlliance alliance)
{
    eAlliance a;
    for(int i=0; i<units.size(); i++)
        if(units[i]->getState()>=STATE_SELECTABLE &&
            units[i]->getType()==ut &&
            canPlayerSee(units[i]))
        {
            a=getAlliance(units[i]->getOwner());
            if(a==ALLIANCE_SELF)
                a=ALLIANCE_ALLY;
            if(a==alliance)
                selectUnit(units[i]);
        }
}
/****************************************************************************/
//deselect units wih same unit type and owner
void deselectUnitType(tUnitType *ut, eAlliance alliance)
{
    eAlliance a;
    for(int i=0; i<units.size(); i++)
        if(units[i]->getState()>=STATE_SELECTABLE &&
            units[i]->getType()==ut &&
            canPlayerSee(units[i]))
        {
            a=getAlliance(units[i]->getOwner());
            if(a==ALLIANCE_SELF)
                a=ALLIANCE_ALLY;
            if(a==alliance)
                deselectUnit(units[i]);
        }
}
/****************************************************************************/
//select units in range with same unit type and owner
//precondition: we can select this type of units belonging to the player--
//for instance, we will never try to select all of an enemy's blueprints
void selectUnitTypeInView(tUnitType *ut, eAlliance alliance)
{
    eAlliance a;
    for(int i=0; i<units.size(); i++)
        if(units[i]->getState()>=STATE_SELECTABLE &&
            units[i]->getType()==ut &&
            canPlayerSee(units[i]) &&
            isWithinView(units[i]))
        {
            a=getAlliance(units[i]->getOwner());
            if(a==ALLIANCE_SELF)
                a=ALLIANCE_ALLY;
            if(a==alliance)
                selectUnit(units[i]);
        }
}
/****************************************************************************/
//deselect units in range wih same unit type and owner
void deselectUnitTypeInView(tUnitType *ut, eAlliance alliance)
{
    eAlliance a;
    for(int i=0; i<units.size(); i++)
        if(units[i]->getState()>=STATE_SELECTABLE &&
            units[i]->getType()==ut &&
            canPlayerSee(units[i]) &&
            isWithinView(units[i]))
        {
            a=getAlliance(units[i]->getOwner());
            if(a==ALLIANCE_SELF)
                a=ALLIANCE_ALLY;
            if(a==alliance)
                deselectUnit(units[i]);
        }
}
/****************************************************************************/
void selectAll(bool shiftPressed)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("selectAll: map isn't initialized");
        return;
    }
#endif
    for(int i=0;i<units.size();i++)
        if(units[i]->getState()>=STATE_SELECTABLE)
            selectUnit(units[i]);
}
/****************************************************************************/
void selectPoint(int x, int y, bool altPressed, bool ctrlPressed, bool shiftPressed)
{
    eAlliance a;
    tUnit *u=findUnitAt(x,y, /* alliesFirst = */ true);
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("selectPoint: map isn't initialized");
        return;
    }
#endif
    if(!shiftPressed)
        clearSelection();
    if(u==NULL)
        return;

    a=getAlliance(u->getOwner());
    if(a==ALLIANCE_SELF)
        a=ALLIANCE_ALLY;

    if(ctrlPressed)
    {
        if(u->isSelected())
            deselectUnitTypeInView(u->getType(),a);
        else
            selectUnitTypeInView(u->getType(),a);
    }
    else if(altPressed)
    {
        if(u->isSelected())
            deselectUnitType(u->getType(),a);
        else
            selectUnitType(u->getType(),a);
    }
    else
    {
        if(u->isSelected())
            deselectUnit(u);
        else
            selectUnit(u);
    }
}
/****************************************************************************/
void findUnitsInRange(const tVector2D& orig, const tVector2D& dest, vector<tUnit *>& output)
{
    //Rotate everything by mapRotation so that the bounding box will be orthogonal.
    float cosr=cos(mapRotation);
    float sinr=sin(mapRotation);
    tVector2D xHat={cosr, -sinr};
    tVector2D yHat={sinr, cosr};

    tVector2D adjOrig={orig.dot(xHat), orig.dot(yHat)};
    tVector2D adjDest={dest.dot(xHat), dest.dot(yHat)};
    float x1=MIN(adjOrig.x, adjDest.x);
    float x2=MAX(adjOrig.x, adjDest.x);
    float y1=MIN(adjOrig.y, adjDest.y);
    float y2=MAX(adjOrig.y, adjDest.y);
    tVector2D pos,adjPos;
    float r;
    
    for(int i=0;i<units.size();i++)
    {
        pos=units[i]->getPosition();
        adjPos.x=pos.dot(xHat);
        adjPos.y=pos.dot(yHat);
        r=units[i]->getModel()->getMaxRadius();

        if(adjPos.x>=x1-r && adjPos.x<=x2+r && adjPos.y>=y1-r && adjPos.y<=y2+r)
            output.push_back(units[i]);
    }
}
/****************************************************************************/
//select units within region [x1,x2]x[y1,y2]; is shift pressed?
void static selectRange(int x1, int y1, int x2, int y2, bool hasSelectionOrigin,
    const tVector2D& selectionOrigin, bool shiftPressed)
{
    int hits, names, i, j;
    GLuint buf[PICK_BUFFER_SIZE], *ptr=buf, minz, maxz;
    tUnit *u;
    bool found=false;
    int x=MIN(x1,x2);
    int y=MIN(y1,y2);
    int w=DIFF(x1,x2)+1;
    int h=DIFF(y1,y2)+1;
    vector<tUnit *> unitsInRange;
    tVector2D selectionDest;
    
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("selectRange: map isn't initialized");
        return;
    }
    if(w<=0||h<=0)
    {
        bug("selectRange: invalid range");
        return;
    }
#endif

    if(!shiftPressed)
        clearSelection();

    hits = pickUnits(x,y,w,h,buf,/*convexShell=*/false);
    if(hasSelectionOrigin)
    {
        selectionDest=pixelToVector2D(x2,y2);
        findUnitsInRange(selectionOrigin,selectionDest,unitsInRange);
    }

    for(i=0; i<hits; i++)
    {
        names=*(ptr++);
        minz=*(ptr++);
        maxz=*(ptr++);
        for(j=0;j<names;j++)
            if(j==0)
            {
                u=unitLookup(*(ptr++));
                if(u==NULL)
                    bug("selectRange: invalid unit ID");
                else if(getAlliance(u->getOwner())<=ALLIANCE_ALLY)
                {
                    found=true;
                    selectUnit(u);
                }
            }
    }
    for(i=0;i<unitsInRange.size();i++)
        if(getAlliance(unitsInRange[i]->getOwner())<=ALLIANCE_ALLY)
        {
            found=true;
            selectUnit(unitsInRange[i]);
        }

    if(found)
        return;

    ptr=buf;
    for(i=0; i<hits; i++)
    {
        names=*(ptr++);
        minz=*(ptr++);
        maxz=*(ptr++);
        for(j=0;j<names;j++)
            if(j==0)
            {
                u=unitLookup(*(ptr++));
                if(u)
                    selectUnit(u);
                else
                    bug("selectRange: invalid unit ID");
            }
    }
    for(i=0;i<unitsInRange.size();i++)
        selectUnit(unitsInRange[i]);
}
void selectRange(int x1, int y1, int x2, int y2, bool shiftPressed)
{
    selectRange(x1,y1,x2,y2,false,zeroVector2D,shiftPressed);
}
void selectRange(int x1, int y1, int x2, int y2, const tVector2D& selectionOrigin, bool shiftPressed)
{
    selectRange(x1,y1,x2,y2,true,selectionOrigin,shiftPressed);
}

/****************************************************************************/
void targetAll(bool shiftPressed, eCommand command)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("targetAll: map isn't initialized");
        return;
    }
    if(command<0 || command>=commandCount)
    {
        bug("targetAll: invalid command");
        return;
    }
#endif
    vector<tUnit *> targets;
    if(!shiftPressed && !commandTable[command].immediate)
        commandSel(COMMAND_STOP);
    for(int i=0;i<units.size();i++)
        if(units[i]->getState()>=STATE_SELECTABLE)
            targets.push_back(units[i]);
    commandSel(command,&targets[0], targets.size());
}
/****************************************************************************/
//user targets command
void targetPoint(int x, int y, bool altPressed, bool ctrlPressed, bool shiftPressed, eCommand command)
{
    int i;
    tUnit *u;
    eParamType paramType;
    tVector3D v;
    vector<tUnit *> targets;
    eAlliance a,b;

#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("targetPoint: map isn't initialized");
        return;
    }
    if(command<0 || command>=commandCount)
    {
        bug("targetPoint: invalid command");
        return;
    }
#endif

    v=pixelToTerrain3D(x,y);

    if(command==COMMAND_NONE)
    {
        u=findUnitAt(x,y,commandTable[command].benevolent);
        if(u)
            paramType=PARAM_UNIT;
        else
            paramType=PARAM_POSITION;
    }
    else
    {
        paramType=commandTable[command].paramType;
        if(paramType==PARAM_UNIT)
        {
            u=findUnitAt(x,y,commandTable[command].benevolent);
            if(u==NULL)
                return;
        }
    }

    if(!shiftPressed && !commandTable[command].immediate)
        commandSel(COMMAND_STOP);

    switch(paramType)
    {
        case PARAM_NONE: //this should not happen
            commandSel(command);
            break;
        case PARAM_POSITION:
        case PARAM_CONSTRUCTION:
            commandSel(command, v);
            break;
        case PARAM_UNIT:
            a=getAlliance(u->getOwner());
            if(a==ALLIANCE_SELF)
                a=ALLIANCE_ALLY;
            if(altPressed)
            {
                for(i=0; i<units.size(); i++)
                    if(units[i]->getState()>=STATE_SELECTABLE &&
                        units[i]->getType()==u->getType() &&
                        canPlayerSee(units[i]))
                    {
                        b=getAlliance(units[i]->getOwner());
                        if(b==ALLIANCE_SELF)
                            b=ALLIANCE_ALLY;
                        if(a==b)
                            targets.push_back(units[i]);
                    }
                commandSel(command, &targets[0], targets.size());
            }
            else if(ctrlPressed)
            {
                for(i=0; i<units.size(); i++)
                    if(units[i]->getState()>=STATE_SELECTABLE &&
                        units[i]->getType()==u->getType() &&
                        canPlayerSee(units[i]) &&
                        isWithinView(units[i]))
                    {
                        b=getAlliance(units[i]->getOwner());
                        if(b==ALLIANCE_SELF)
                            b=ALLIANCE_ALLY;
                        if(a==b)
                            targets.push_back(units[i]);
                    }
                commandSel(command, &targets[0], targets.size());
            }
            else
                commandSel(command, &u, 1);
            break;
    }
}
/****************************************************************************/
//user targets command in the minimap
void targetMinimap(int x, int y, bool shiftPressed, eCommand command)
{
    tUnit *u;
    eParamType paramType;

#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("targetCircle: map isn't initialized");
        return;
    }
    if(command<0 || command>=commandCount)
    {
        bug("targetCircle: invalid command");
        return;
    }
#endif

    float xf=(float)x;
    float yf=(float)y;
    tVector3D v={
        invMinimapMatrix[0]*xf + invMinimapMatrix[4]*yf + invMinimapMatrix[12],
        invMinimapMatrix[1]*xf + invMinimapMatrix[5]*yf + invMinimapMatrix[13],
        0
        };
    if(terrain)
    {
        tTileInfo ti=terrain->getTileInfo(to2D(v),DOMAIN_NONE,0);
        v.z=ti.height;
    }

    if(command==COMMAND_NONE)
    {
        u=findUnitOnMinimap(x,y,commandTable[command].benevolent);
        if(u)
            paramType=PARAM_UNIT;
        else
            paramType=PARAM_POSITION;
    }
    else
    {
        paramType=commandTable[command].paramType;
        if(paramType==PARAM_UNIT)
        {
            u=findUnitOnMinimap(x,y,commandTable[command].benevolent);
            if(u==NULL)
                return;
        }
    }

    if(!shiftPressed && !commandTable[command].immediate)
        commandSel(COMMAND_STOP);

    switch(paramType)
    {
        case PARAM_NONE: //this should not happen
            commandSel(command);
            break;
        case PARAM_POSITION:
        case PARAM_CONSTRUCTION:
            commandSel(command, v);
            break;
        case PARAM_UNIT:
            commandSel(command, &u, 1);
            break;
    }
}
/****************************************************************************/
//user targets command with range
//precondition: paramType==PARAM_UNIT or command==COMMAND_NONE
void static targetRange(int x1, int y1, int x2, int y2, bool hasSelectionOrigin,
    const tVector2D& selectionOrigin, bool shiftPressed, eCommand command)
{
    int hits, names, i, j;
    GLuint buf[PICK_BUFFER_SIZE], *ptr=buf, minz, maxz;
    tUnit *u;
    vector<tUnit *> targets;
    bool found=false;
    int x=MIN(x1,x2);
    int y=MIN(y1,y2);
    int w=DIFF(x1,x2)+1;
    int h=DIFF(y1,y2)+1;
    tVector2D selectionDest;
    vector<tUnit *> unitsInRange;

#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("targetRange: map isn't initialized");
        return;
    }
    if(w<1||h<1)
    {
        bug("targetRange: invalid range");
        return;
    }
#endif

    hits = pickUnits(x,y,w,h,buf,/*convexShell=*/false);
    if(hasSelectionOrigin)
    {
        selectionDest=pixelToVector2D(x2,y2);
        findUnitsInRange(selectionOrigin,selectionDest,unitsInRange);
    }

    if(!shiftPressed && !commandTable[command].immediate)
        commandSel(COMMAND_STOP);

    for(i=0; i<hits; i++)
    {
        names=*(ptr++);
        minz=*(ptr++);
        maxz=*(ptr++);
        for(j=0;j<names;j++)
            if(j==0)
            {
                u=unitLookup(*(ptr++));
                if(u==NULL)
                    bug("targetRange: invalid unit ID");
                else if(getAlliance(u->getOwner())>=ALLIANCE_NEUTRAL)
                {
                    found=true;
                    targets.push_back(u);
                }
            }
    }
    for(i=0;i<unitsInRange.size();i++)
        if(getAlliance(unitsInRange[i]->getOwner())>=ALLIANCE_NEUTRAL)
        {
            found=true;
            targets.push_back(unitsInRange[i]);
        }

    if(found)
    {
        commandSel(command, &targets[0], targets.size());
        return;
    }

    ptr=buf;
    for(i=0; i<hits; i++)
    {
        names=*(ptr++);
        minz=*(ptr++);
        maxz=*(ptr++);
        for(j=0;j<names;j++)
            if(j==0)
            {
                u=unitLookup(*(ptr++));
                if(u)
                    targets.push_back(u);
                else
                    bug("targetRange: invalid unit ID");
            }
    }
    for(i=0;i<unitsInRange.size();i++)
        targets.push_back(unitsInRange[i]);

    commandSel(command, &targets[0], targets.size());
}
void targetRange(int x1, int y1, int x2, int y2, bool shiftPressed, eCommand command)
{
    targetRange(x1,y1,x2,y2,false,zeroVector2D,shiftPressed,command);
}
void targetRange(int x1, int y1, int x2, int y2, const tVector2D& selectionOrigin,
    bool shiftPressed, eCommand command)
{
    targetRange(x1,y1,x2,y2,true,selectionOrigin,shiftPressed,command);
}

/****************************************************************************/
//issues command to all units in current selection
void commandSel(eCommand command)
{
    vector<long> selection;
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("commandSel #1: invalid command");
        return;
    }
#endif
    if(!gameRunning)
        return;

    //Handle interface commands:
    switch(command)
    {
        case COMMAND_DISBAND:
            disbandGroup();
            return;
    }

    for(int i=0; i<selectedUnits.size(); i++)
        if(canControl(selectedUnits[i]))
            selection.push_back(selectedUnits[i]->getID());
    if(selection.size()==0)
        return;

    tCommandMsg message(command,&selection[0],selection.size());
    postMessage(&message);
}
/****************************************************************************/
//issues command to all units in current selection
//v is a 3D vectors; however, the subsequent message only contains the x- and y-components
void commandSel(eCommand command, tVector3D v)
{
    vector<long> selection;
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("commandSel #2: invalid command");
        return;
    }
#endif

    if(!gameRunning)
        return;

    for(int i=0; i<selectedUnits.size(); i++)
        if(canControl(selectedUnits[i]))
            selection.push_back(selectedUnits[i]->getID());
    if(selection.size()==0)
        return;

    flashPoint(v);

    tCommandMsg message(command,&selection[0],selection.size(),to2D(v));
    postMessage(&message);
}
/****************************************************************************/
//issues command to all units in current selection
void commandSel(eCommand command, tUnit *targets[], int targetCount)
{
    int i;
    vector<long> selection;
    vector<long> targetIDs;
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("commandSel #3: invalid command");
        return;
    }
#endif
    if(!gameRunning)
        return;

    for(i=0; i<selectedUnits.size(); i++)
        if(canControl(selectedUnits[i]))
            selection.push_back(selectedUnits[i]->getID());
    if(selection.size()==0)
        return;

    for(i=0; i<targetCount; i++)
    {
        targets[i]->flash();
        targetIDs.push_back(targets[i]->getID());
    }
    if(targetIDs.size()==0)
        return;

    tCommandMsg message(command,&selection[0],selection.size(),&targetIDs[0],targetIDs.size());
    postMessage(&message);
}
/****************************************************************************/
//issues command to all units in current selection
void commandSel(eCommand command, int quantity, bool repeat)
{
    vector<long> selection;
#ifdef DEBUG
    if(command<0 || command>=commandCount)
    {
        bug("commandSel #4: invalid command");
        return;
    }
#endif

    if(!gameRunning)
        return;

    for(int i=0; i<selectedUnits.size(); i++)
        if(canControl(selectedUnits[i]))
            selection.push_back(selectedUnits[i]->getID());
    if(selection.size()==0)
        return;

    tCommandMsg message(command,&selection[0],selection.size(),quantity,repeat);
    postMessage(&message);
}
/****************************************************************************/
//NOTE: Units -must- be in numerical order
tUnit *unitLookup(int ID)
{
    if(units.size()==0) //CAREFUL!  size() is unsigned
        return NULL;
    //binary search
    int a=0;
    int b=units.size()-1;
    int c;
    while(a<=b)
    {
        c=(a+b)/2;
        if(ID>units[c]->getID())
            a=c+1;
        else if(ID<units[c]->getID())
            b=c-1;
        else
        {
            //Ignore units which user cannot select
            if(units[c]->getState()>=STATE_SELECTABLE)
                return units[c];
            else
                return NULL;
        }
    }
    return NULL;
}
/****************************************************************************/
tDeposit *depositLookup(int ID)
{
    int i;
    for(i=0;i<deposits.size();i++)
        if(deposits[i]->ID==ID)
            return deposits[i];
    return NULL;
}
/****************************************************************************/
int getLocalPlayerID()
{
    return localPlayerID;
}
/****************************************************************************/
//get player1's alliance with player2
eAlliance getAlliance(int player1, int player2)
{
#ifdef DEBUG
    if(player1<0 || player1>=MAX_PLAYERS)
    {
        bug("getAlliance: invalid value for player1");
        return ALLIANCE_NEUTRAL;
    }
    if(player2<0 || player2>=MAX_PLAYERS)
    {
        bug("getAlliance: invalid value for player2");
        return ALLIANCE_NEUTRAL;
    }
#endif
    return players[player1].alliances[player2];
}
/****************************************************************************/
//get local player's alliance with player
eAlliance getAlliance(int player)
{
    return getAlliance(localPlayerID, player);
}
/****************************************************************************/
//get player's alliance with local player
eAlliance getReverseAlliance(int player)
{
    return getAlliance(player,localPlayerID);
}
/****************************************************************************/
//Counts the number of visible players
int getPlayerCount()
{
    int count=0;
    for(int i=0;i<MAX_PLAYERS;i++)
        if(players[i].exists && players[i].assignedType!=PLAYER_HIDDEN)
            count++;
    return count;
}
/****************************************************************************/
//The function returns the username of human players if useUsername is true.
const tString& getPlayerCaption(int player, bool useUsername)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("getPlayerCaption: invalid value for player");
        return EMPTY_STRING;
    }
#endif
    if(useUsername && players[player].assignedUser)
        return players[player].assignedUser->name;
    return players[player].caption;
}
/****************************************************************************/
ePlayer getPlayerType(int player)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("getPlayerType: invalid value for player");
        return PLAYER_NONE;
    }
#endif
    return players[player].assignedType;
}
/****************************************************************************/
bool doesPlayerExist(int player)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("doesPlayerExist: invalid value for player");
        return false;
    }
#endif
    return players[player].exists;
}
/****************************************************************************/
bool isPlayerVisible(int player)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("isPlayerVisible: invalid value for player");
        return false;
    }
#endif
    return players[player].exists && players[player].assignedType!=PLAYER_HIDDEN;
}
/****************************************************************************/
//Determines if YOU have paused the game.  If another player pauses game but you
//haven't, the function returns false.
bool isGamePaused()
{
    return localPlayerID>=0 && players[localPlayerID].gamePaused;
}
/****************************************************************************/
//retrieve next open slot (type==PLAYER_OPEN)
int getNextSlot()
{
    for(int i=0; i<MAX_PLAYERS; i++)
        if(players[i].assignedType==PLAYER_OPEN)
            return i;
    return -1;
}
/****************************************************************************/
//determines if player can control unit
bool canControl(int player, tUnit *u)
{
#ifdef DEBUG
    CHECK_UNIT(u)
    if(MEMORY_VIOLATION(u))
        return false;
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("canControl: invalid value for player");
        return false;
    }
#endif
    return getSharedControl(u->getOwner(),player);
    //Any additional variables in this function will affect the apparition's owner.
}
/****************************************************************************/
bool canPlayerSee(int player, tUnit *u)
{
#ifdef DEBUG
    CHECK_UNIT(u)
    if(MEMORY_VIOLATION(u))
        return false;
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("canPlayerSee: invalid value for player");
        return false;
    }
#endif
    if(getSharedVision(u->getOwner(),player))
        return u->isVisibleToAllies();
    if(!u->isVisibleToEnemies())
        return false;
    if(u->isAlwaysVisible())
        return true;
    return isPointVisible(player,u->getPosition());
}
/****************************************************************************/
//determines if local player can control unit
bool canControl(tUnit *u)
{
    return canControl(localPlayerID, u);
}
/****************************************************************************/
//determines if local player can see unit
bool canPlayerSee(tUnit *u)
{
    return canPlayerSee(localPlayerID, u);
}
/****************************************************************************/
//determines if local player can control unit
bool unitExists(tUnit *unit)
{
    for(int i=0; i<units.size(); i++)
        if(units[i]==unit)
            return true;
    return false;
}
/****************************************************************************/
//add missile to missiles vector
tMissile* createMissile(tUnit *source, const tVector3D& origin, tUnit *target, tMissileType *type, tWorldTime startTime)
{
    tMissile *m=NULL;
#ifdef DEBUG
    if(!gameRunning)
    {
        bug("createMissile: game isn't running");
        return NULL;
    }
    if(MEMORY_VIOLATION(source))
        return NULL;
    if(MEMORY_VIOLATION(target))
        return NULL;
    if(MEMORY_VIOLATION(type))
        return NULL;
#endif
    try
    {
        m=mwNew tMissile(source,origin,target,type,startTime);
    }
    catch(...)
    {
        return NULL;
    }
    missiles.push_back(m);
    return m;
}
/****************************************************************************/
//execute a shell command
void parseShellCommand(const tString& commandString) throw(tString)
{
    int i, j, total, resourceID, playerID, targetPlayerID, depositID, x, y;
    float HP, radius, amount, rate;
    tUnit *u;
    tUnitType *ut;
    eAlliance alliance;
    eCommand command;
    tVector2D v;
    int quantity;
    bool repeat,sc,sr,sv;
    vector<long> source, target;
    vector<int> sourcePlayers, targetPlayers, resourceContainer;

#ifdef DEBUG
    if(!gameRunning)
    {
        bug("shell: game isn't running");
        return;
    }
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("shell: invalid localPlayerID");
        return;
    }
#endif

    tStream s;
    s.load(CHARPTR(commandString));
    tString tag=parseString("tag",s);

    if(strPrefix(tag,"plan"))
    {
        playerID=parseShellPlayer("owner", s);
        ut=parseUnitType("unit type", s);
        v=parseShellPos("location", s);
        tUnitCreateMsg message(MSG_UNIT_PLAN, playerID, ut->ID, v);
        postMessage(&message);
    }
    else if(strPrefix(tag,"create"))
    {
        playerID=parseShellPlayer("owner", s);
        ut=parseUnitType("unit type", s);
        v=parseShellPos("location", s);
        tUnitCreateMsg message(MSG_UNIT_CREATE, playerID, ut->ID, v);
        postMessage(&message);
    }
    else if(strPrefix(tag,"destroy")||strPrefix(tag,"kill"))
    {
        parseUnits("units", source, s);
        tUnitMsg message(MSG_UNIT_DESTROY, &source[0], source.size());
        postMessage(&message);
    }
    else if(strPrefix(tag,"setposition"))
    {
        parseUnits("units", source, s);
        v=parseShellPos("location", s);

        //Order the unit to stop first so the unit will become motionless at its new location.
        tCommandMsg message1(COMMAND_STOP,&source[0],source.size());
        postMessage(&message1);
        tUnitVector2DMsg message2(MSG_UNIT_SET_POS, &source[0], source.size(), v);
        postMessage(&message2);
    }
    else if(strPrefix(tag,"setvelocity"))
    {
        parseUnits("units", source, s);
        v=parseVelocity("velocity", s);
        tUnitVector2DMsg message(MSG_UNIT_SET_VELOCITY, &source[0], source.size(), v);
        postMessage(&message);
    }
    else if(strPrefix(tag,"setforward"))
    {
        parseUnits("units", source, s);
        v=parseForward("forward vector", s);
        tUnitVector2DMsg message(MSG_UNIT_SET_FORWARD, &source[0], source.size(), v);
        postMessage(&message);
    }
    else if(strPrefix(tag,"restore"))
    {
        parseUnits("units", source, s);
        tUnitFloatMsg message(MSG_UNIT_SET_HP, &source[0], source.size(), MAX_FLOAT);
        postMessage(&message);
    }
    else if(strPrefix(tag,"sethitpoints") || strPrefix(tag,"sethp"))
    {
        parseUnits("units", source, s);
        HP=parseFloatMin("hitpoints",0, s);
        tUnitFloatMsg message(MSG_UNIT_SET_HP, &source[0], source.size(), HP);
        postMessage(&message);
    }
    else if(strPrefix(tag,"setowner") || strPrefix(tag,"give"))
    {
        parseUnits("units", source, s);
        playerID=parseShellPlayer("new owner", s);
        tUnitLongMsg message(MSG_UNIT_SET_OWNER, &source[0], source.size(), playerID);
        postMessage(&message);
    }
    else if(strPrefix(tag,"setalliance"))
    {
        parsePlayers("source players",sourcePlayers, s);
        parsePlayers("target players",targetPlayers, s);

        alliance=parseAlliance("new alliance", s);

        //Shared control, resources, and vision defaults to true for "ally" and false for "enemy" or "neutral".
        if(alliance==ALLIANCE_ALLY)
            sc=sr=sv=true;
        else
            sc=sr=sv=false;

        if(s.hasOptionalParameter())
        {
            sc=parseBool("shared control", s);
            sr=parseBool("shared resources", s);
            sv=parseBool("shared vision", s);
        }

        for(i=0;i<sourcePlayers.size();i++)
        {
            playerID=sourcePlayers[i];
            for(j=0;j<targetPlayers.size();j++)
            {
                targetPlayerID=targetPlayers[j];
                if(playerID==targetPlayerID) //don't output error message since user may type "setal all all"
                    continue;
                setAlliance(playerID, targetPlayerID, alliance, sc, sr, sv);
            }
        }
    }
    else if(strPrefix(tag,"setresource"))
    {
        parsePlayers("players",sourcePlayers, s);
        parseResources("resources",resourceContainer, s);
        amount=parseFloatMin("new amount",0, s);

        for(i=0;i<sourcePlayers.size();i++)
            for(j=0;j<resourceContainer.size();j++)
            {
                tSetResourceMsg message(sourcePlayers[i], resourceContainer[j], amount);
                postMessage(&message);
            }
    }
    else if(strPrefix(tag,"dumpcommands"))
    {
        u=parseUnit("unit", s);
        u->dumpCommands();
    }
    else if(strPrefix(tag,"dumphash"))
    {
        for(i=0;i<hashSize;i++)
        {
            total=0;
            for(u=unitHash[i];u;u=u->getNextNeighbor())
                total++;
            if(total>0)
                log("Cell %ld contains %d units",i,total);
        }
    }
    else if(strPrefix(tag,"dumpfogofwar")||strPrefix(tag,"dumpfow"))
    {
        playerID=parseShellPlayer("player", s);
        log("Fog of war for player %d",playerID);
        char *buf=mwNew char[fogOfWarWidth+1];
        for(y=fogOfWarHeight-1;y>=0;y--)
        {
            for(x=0;x<fogOfWarWidth;x++)
                buf[x]='0'+players[playerID].fogOfWar[y*fogOfWarWidth+x];
            buf[fogOfWarWidth]='\0';
            log(buf);
        }
        mwDelete[] buf;
    }
    else if(strPrefix(tag,"pause"))
    {
        if(!players[localPlayerID].gamePaused)
            pauseGame();
    }
    else if(strPrefix(tag,"resume"))
    {
        if(players[localPlayerID].gamePaused)
            resumeGame();
    }
    else if(strPrefix(tag,"showstats"))
    {
        showUnitStats=true;
    }
    else if(strPrefix(tag,"hidestats"))
    {
        showUnitStats=false;
    }
    else if(strPrefix(tag,"fullscreen")||strPrefix(tag,"fs"))
    {
        setFullScreen(true);
    }
    else if(strPrefix(tag,"windowedmode")||strPrefix(tag,"wm"))
    {
        setFullScreen(false);
    }
    else if(strPrefix(tag,"setresolution"))
    {
        x=parseIntRange("resolution width",640,1280, s);
        y=parseIntRange("resolution height",480,1024, s);
        setResolution(x,y);
    }
    else if(strPrefix(tag,"startflooding"))
    {
        i=parseIntRange("interval",1,10000, s);
        floodingEnabled=true;
        lastRandomCommand=gameTime;
        floodingInterval=(tGameTime)i;
    }
    else if(strPrefix(tag,"stopflooding"))
    {
        floodingEnabled=false;
    }
    else if(strPrefix(tag,"startsync"))
    {
        syncInterval=parseIntMin("sync interval",1000, s);
        syncEnabled=true;
    }
    else if(strPrefix(tag,"stopsync"))
        syncEnabled=false;
    else if(strPrefix(tag,"createdeposit"))
    {
        resourceID=parseResource("resource", s);
        v=parseShellPos("location", s);
        radius=parseFloatMin("radius",0, s);
        amount=parseFloatMin("amount",0, s);

        tDepositCreateMsg message(resourceID,v,radius,amount);
        postMessage(&message);
    }
    else if(strPrefix(tag,"destroydeposit"))
    {
        depositID=parseIntMin("deposit ID",1, s);
        tDepositDestroyMsg message(depositID);
        postMessage(&message);
    }
    else if(strPrefix(tag,"setdeposit"))
    {
        depositID=parseIntMin("deposit ID",1, s);
        amount=parseFloatMin("amount",0, s);
        tDepositSetMsg message(depositID,amount);
        postMessage(&message);
    }
    else if(strPrefix(tag,"setterraininterval"))
    {
        j=parseInt("terrain interval",s);
        if(j==0)
            resetTerrainInterval();
        else
        {
            for(i=64;i>0;i>>=1)
                if(i==j)
                    break;
            if(i==0)
                throw tString("Please specify a power of 2 between 1 and 64.");
            setTerrainInterval(j);
        }
    }
    else if(strPrefix(tag,"setshademap"))
    {
        i=parseInt("shade map size",s);
        if(i!=0 && i!=256 && i!=512 && i!=1024 && i!=2048)
            throw tString("Shade map size must be 0, 256, 512, 1024, or 2048");
        setShadeMapSize(i);
    }
    else if(strPrefix(tag,"setalphamap"))
    {
        i=parseInt("alpha map size",s);
        if(i!=0 && i!=256 && i!=512 && i!=1024 && i!=2048)
            throw tString("Alpha map size must be 0, 256, 512, 1024, or 2048");
        setAlphaMapSize(i);
    }
    else if(strPrefix(tag,"setwatermap"))
    {
        i=parseInt("water map size",s);
        if(i!=0 && i!=256 && i!=512 && i!=1024 && i!=2048)
            throw tString("Water map size must be 0, 256, 512, 1024, or 2048");
        setWaterMapSize(i);
    }
    else if(strPrefix(tag,"setshadowmap"))
    {
        i=parseInt("shadow map size",s);
        if(i!=0 && i!=256 && i!=512 && i!=1024 && i!=2048)
            throw tString("Shadow map size must be 0, 256, 512, 1024, or 2048");
        setShadowMapSize(i);
    }
    else if(strPrefix(tag,"setpanrate"))
    {
        rate=parseFloatRange("keyboard and mouse pan rate", 0, 100, s);
        setPanRate(rate);
    }
    else if(strPrefix(tag,"setspinrate"))
    {
        rate=parseFloatRange("radians per second", 0, 100, s);
        setSpinRate(rate);
    }
    else if(strPrefix(tag,"setzoomrate"))
    {
        rate=parseFloatRange("exponential constant", 0, 100, s);
        setZoomRate(rate);
    }
    else if(strPrefix(tag,"settiltrate"))
    {
        rate=parseFloatRange("radians per second", 0, 100, s);
        setTiltRate(rate);
    }
    else if(strPrefix(tag,"setmousezoomrate"))
    {
        rate=parseFloatRange("zoom rate", 0, 100, s);
        setMouseZoomRate(rate);
    }
    else if(strPrefix(tag,"refresh"))
    {
        interfaceRefresh();
    }
    else if(strPrefix(tag,"wireframe"))
    {
        terrainWireframe=parseBool("draw terrain as wireframe?",s);
    }
    else if(strPrefix(tag,"testvalue"))
    {
        testValue=parseInt("test value",s);
    }
    else if(strPrefix(tag,"testvalue2") || strPrefix(tag,"test2"))
    {
        testValue2=parseInt("test value 2",s);
    }
    else if(strPrefix(tag,"shockwave"))
    {
        u=parseUnit("unit", s);
        u->spawnShockwave();
    }
    else if(strPrefix(tag,"showtimers"))
        showTimers();
    else if(strPrefix(tag,"hidetimers"))
        hideTimers();
    else if(strPrefix(tag,"saveunits"))
    {
        tString filename=parseString("filename to which to save units",s);
        FILE *fp=fopen(CHARPTR(filename),"w");
        if(fp==NULL)
            throw tString("File not found: ")+filename;

        for(i=0;i<units.size();i++)
        {
            tUnit *u=units[i];
            fprintf(fp,"UNIT %s %s %4.2f %4.2f\n",
                CHARPTR(players[u->getOwner()].name),
                CHARPTR(u->getType()->name),
                u->getPosition().x,
                u->getPosition().y);
        }
        fclose(fp);
    }
    else //the shell command is a unit command
    {
        //Command name cannot contain spaces!
        command=commandLookup(tag,/*exact=*/false);
        if(command==COMMAND_NONE)
            throw tString("Unrecognized command '")+tag+'"';

        parseUnits("source units",source, s);
        switch(commandTable[command].paramType)
        {
            case PARAM_NONE:
            {
                tCommandMsg message1(command,&source[0],source.size());
                postMessage(&message1);
                break;
            }
            case PARAM_UNIT:
            {
                parseUnits("target units",target, s);
                tCommandMsg message2(command,&source[0],source.size(),&target[0],target.size());
                postMessage(&message2);
                break;
            }
            case PARAM_POSITION:
            case PARAM_CONSTRUCTION:
            {
                v=parseShellPos("location", s);
                tCommandMsg message3(command,&source[0],source.size(),v);
                postMessage(&message3);
                break;
            }
            case PARAM_PRODUCTION:
            {
                quantity=parseInt("quantity", s); //quantity can be negative since you can dequeue commands.
                repeat=parseBool("repeat", s);
                tCommandMsg message4(command,&source[0],source.size(),quantity,repeat);
                postMessage(&message4);
                break;
            }
        }
    }
}
/****************************************************************************/
//Determines if a unit or unit explosion is visible in range.
//This function is not sync-safe!  Use it for drawing and selection only.
bool inline isWithinView(tUnit *u)
{
    //This function is internal, therefore I need not verify u
    return isWithinView(u->getModel()->getRadius3D(), u->getPosition3D());
}
bool inline isWithinView(tUnitExplosion *u)
{
    //This function is internal, therefore I need not verify u
    return isWithinView(u->getRadius3D(), u->getPosition3D());
}
bool isWithinView(float radius3D, const tVector3D& p)
{
    float distanceSq=(frustumCenter-p).lengthSquared();
    float maxDistance=frustumRadius+radius3D;
    if(distanceSq>maxDistance*maxDistance)
        return false;

    if(p.x+radius3D<frustumMinX ||
       p.x-radius3D>frustumMaxX ||
       p.y+radius3D<frustumMinY ||
       p.y-radius3D>frustumMaxY ||
       p.z+radius3D<frustumMinZ ||
       p.z-radius3D>frustumMaxZ)
        return false;
        
    //TODO
    //Perform additional tests to determine if the unit is visible
    return true;
}

/***************************************************************************\
pixelToVector2D
pixelToVector3D

Converts a pixel position to a vector in the plane z=0.

Inputs:
    x,y         Pixel position to convert
Ouputs:
    Returns 2D or 3D vector
\***************************************************************************/
tVector3D pixelToVector3D(int x, int y)
{
    float xf=(float)x;
    float yf=(float)y;

    /*
     * [M11 M12 M13 M14][p]     [cx]
     * [M21 M22 M23 M24][q]     [cy]
     * [M31 M32 M33 M34][0]  =  [cz]
     * [M41 M42 M43 M44][1]     [c]
     *
     * [pc]    [m11 m12 m13 m14][x]
     * [qc]    [m21 m22 m23 m24][y]
     * [0]  =  [m31 m32 m33 m34][z]
     * [c]     [m41 m42 m43 m44][1]
     *
     * p = (m11x + m12y + m13z + m14)/c
     * q = (m21x + m22y + m23z + m24)/c
     * 0 = m31x + m32y + m33z + m34
     * c = m41x + m42y + m43z + m44
     */
#ifdef DEBUG
    if(invViewMatrix[10]==0)
    {
        bug("pixelToVector3D: invViewMatrix[10] is zero");
        return zeroVector3D;
    }
#endif
    float z = (-invViewMatrix[2]*xf - invViewMatrix[6]*yf - invViewMatrix[14])/invViewMatrix[10];
    float c = invViewMatrix[3]*xf + invViewMatrix[7]*yf + invViewMatrix[11]*z + invViewMatrix[15];
#ifdef DEBUG
    if(c==0)
    {
        bug("pixelToVector3D: c is zero");
        return zeroVector3D;
    }
#endif
    float p = (invViewMatrix[0]*xf + invViewMatrix[4]*yf + invViewMatrix[8]*z + invViewMatrix[12])/c;
    float q = (invViewMatrix[1]*xf + invViewMatrix[5]*yf + invViewMatrix[9]*z + invViewMatrix[13])/c;

    tVector3D w={p, q, 0};
    return w;
}
tVector2D pixelToVector2D(int x, int y)
{
    return to2D(pixelToVector3D(x,y));
}
tVector3D pixelToVector3DAtElevation(int x, int y, float r)
{
    float xf=(float)x;
    float yf=(float)y;

    if(r==0)
        return pixelToVector3D(x,y);

    /*
     * [M11 M12 M13 M14][p]     [cx]
     * [M21 M22 M23 M24][q]     [cy]
     * [M31 M32 M33 M34][r]  =  [cz]
     * [M41 M42 M43 M44][1]     [c]
     *
     * [pc]    [m11 m12 m13 m14][x]
     * [qc]    [m21 m22 m23 m24][y]
     * [rc]  = [m31 m32 m33 m34][z]
     * [c]     [m41 m42 m43 m44][1]
     *
     * pc = m11x + m12y + m13z + m14
     * qc = m21x + m22y + m23z + m24
     * rc = m31x + m32y + m33z + m34
     * c = m41x + m42y + m43z + m44
     *
     * (m31x + m32y + m33z + m34)/r = m41x + m42y + m43z + m44
     * (m31x + m32y + m34)/r + m33/r*z = m41x + m42y + m44 + m43*z
     * (m31x + m32y + m34)/r - m41x - m42y - m44 = m43*z - m33/r*z
     * (m31x + m32y + m34)/r - m41x - m42y - m44 = (m43 - m33/r)*z
     * z = ((m31x + m32y + m34)/r - m41x - m42y - m44) / (m43 - m33/r)
     */
    float num=(invViewMatrix[2]*xf + invViewMatrix[6]*yf + invViewMatrix[14])/r -
                invViewMatrix[3]*xf - invViewMatrix[7]*yf - invViewMatrix[15];
    float den=invViewMatrix[11] - invViewMatrix[10]/r;
#ifdef DEBUG
    if(den==0)
    {
        bug("pixelToVector3D: den is zero");
        return zeroVector3D;
    }
#endif
    float z=num/den;
    float c = invViewMatrix[3]*xf + invViewMatrix[7]*yf + invViewMatrix[11]*z + invViewMatrix[15];
#ifdef DEBUG
    if(c==0)
    {
        bug("pixelToVector3D: c is zero");
        return zeroVector3D;
    }
#endif
    float p = (invViewMatrix[0]*xf + invViewMatrix[4]*yf + invViewMatrix[8]*z + invViewMatrix[12])/c;
    float q = (invViewMatrix[1]*xf + invViewMatrix[5]*yf + invViewMatrix[9]*z + invViewMatrix[13])/c;

    tVector3D w={p, q, r};
    return w;
}
tVector2D pixelToVector2DAtElevation(int x, int y, float r)
{
    return to2D(pixelToVector3DAtElevation(x,y,r));
}

/***************************************************************************\
vector2DToPixel
vector3DToPixel

Converts a vector to a pixel position.

Inputs:
    v       2D or 3D Vector to convert
    x,y     Variables in which to store pixel position
Outputs:
    Stores pixel position in x and y.
\***************************************************************************/
void vector2DToPixel(const tVector2D& v, int& x, int& y)
{
    float xc=(viewMatrix[0]*v.x+
        viewMatrix[4]*v.y+
        viewMatrix[12]);
    float yc=(viewMatrix[1]*v.x+
        viewMatrix[5]*v.y+
        viewMatrix[13]);
    float c=(viewMatrix[3]*v.x+
        viewMatrix[7]*v.y+
        viewMatrix[15]);

    //If c is zero or less, the point is in front of the near clipping plane
    //and is not visible.  Nevertheless, we may want some information about
    //the point, like its direction from the center of the screen.  Thus,
    //we clamp c at a nominally small value that will not cause an overflow
    //in x or y.
    if(c<0.000001)
        c=0.000001;

    x=(int)(xc/c);
    y=(int)(yc/c);
}
void vector3DToPixel(const tVector3D& v, int& x, int& y)
{
    float xc=(viewMatrix[0]*v.x+
        viewMatrix[4]*v.y+
        viewMatrix[8]*v.z+
        viewMatrix[12]);
    float yc=(viewMatrix[1]*v.x+
        viewMatrix[5]*v.y+
        viewMatrix[9]*v.z+
        viewMatrix[13]);
    float c=(viewMatrix[3]*v.x+
        viewMatrix[7]*v.y+
        viewMatrix[11]*v.z+
        viewMatrix[15]);

    //If c is zero or less, the point is in front of the near clipping plane
    //and is not visible.  Nevertheless, we may want some information about
    //the point, like its direction from the center of the screen.  Thus,
    //we clamp c at a nominally small value that will not cause an overflow
    //in x or y.
    if(c<0.000001)
        c=0.000001;
    float xf=xc/c;
    float yf=yc/c;
    if(fabs(xf)>INT_MAX || fabs(yf)>INT_MAX)
    {
        float scale=INT_MAX/2/MAX(fabs(xf),fabs(yf));
        x=(int)(xf*scale);
        y=(int)(yf*scale);
        return;
    }
    x=(int)xf;
    y=(int)yf;
}
tVector3D pixelToVector3D(int x, int y, float z)
{
    tVector3D v={(float)x,(float)y,z};
    return multiplyMatrix4x4(invViewMatrix,v);
}
tVector2D pixelToVector2D(int x, int y, float z)
{
    return to2D(pixelToVector3D(x,y,z));
}


tVector2D pixelToTerrain2D(int x, int y)
{
    if(terrain)
        return to2D(terrain->pixelToTerrain3D(x,y));
    else
        return pixelToVector2D(x,y);
}
tVector3D pixelToTerrain3D(int x, int y)
{
    if(terrain)
        return terrain->pixelToTerrain3D(x,y);
    else
        return pixelToVector3D(x,y);
}
void terrain2DToPixel(const tVector2D& v, int& x, int& y)
{
    tVector3D v3=to3D(v);
    tTileInfo ti;
    if(terrain)
    {
        ti=terrain->getTileInfo(v,DOMAIN_NONE,0);
        v3.z=ti.height;
    }
    vector3DToPixel(v3,x,y);
}

/****************************************************************************/
bool isValid(const tVector2D& pos, float margin)
//determines if the vector is valid by converting it to a pixel position
{
    return (pos.x >= margin && pos.y >= margin && pos.x <= mapDimensions.x-margin && pos.y <= mapDimensions.y-margin);
}
/****************************************************************************/
void makeValid(tVector2D& pos, float margin)
//ensures that a vector is valid by converting it to a pixel position
{
    if(pos.x<margin)
        pos.x = margin;
    if(pos.x>mapDimensions.x-margin)
        pos.x = mapDimensions.x-margin;
    if(pos.y<margin)
        pos.y = margin;
    if(pos.y>mapDimensions.y-margin)
        pos.y = mapDimensions.y-margin;
}
/****************************************************************************/
tVector2D separation(tUnit *unit, float maxDistance, float cosMaxAngle)
{
    int i,x,y;
    long hashValue;
    tVector2D steering=zeroVector2D, offset;
    float dist,minDist;
    tUnit *u;

#ifdef DEBUG
    if(MEMORY_VIOLATION(unit))
        return zeroVector2D;
#endif

    bool isAirUnit=(unit->getDomain()==DOMAIN_AIR);

    //Determine hash extents:
    int left=HASH_FUNC(unit->getPosition().x-maxDistance);
    int right=HASH_FUNC(unit->getPosition().x+maxDistance);
    clampRange(left,0,hashWidth-1);
    clampRange(right,0,hashWidth-1);

    int bottom=HASH_FUNC(unit->getPosition().y-maxDistance);
    int top=HASH_FUNC(unit->getPosition().y+maxDistance);
    clampRange(bottom,0,hashHeight-1);
    clampRange(top,0,hashHeight-1);

    for(i=0;i<MAX_PLAYERS;i++)
    {
        if(!players[i].exists)
            continue;
        for(x=left;x<=right;x++)
            for(y=bottom;y<=top;y++)
            {
                hashValue=(y*hashWidth+x)*MAX_PLAYERS+i;
                for(u=unitHash[hashValue];u;u=u->getNextNeighbor())
                    if(u->getState()>STATE_DESTROYED && u->repelNearbyUnits() && u!=unit &&
                       u->getTractorBeamParent()!=unit &&
					   unit->getTractorBeamParent()!=u &&
					   ((u->getDomain()==DOMAIN_AIR)==isAirUnit))
                    {
                        offset=unit->getPosition()-u->getPosition();
                        minDist=(u->getModel()->getMaxRadius()+unit->getModel()->getMaxRadius())*1.5;
                        dist=offset.length();
                        if(dist>0 && dist<minDist)
                            steering+=offset*((minDist-dist)/dist);
                    }
            }
    }
    /*for(i=0;i<obstacles.size();i++)
    {
        offset=unit->getPosition()-obstacles[i].position;
        minDist=obstacles[i].radius+unit->getMaxRadius()*2;
        dist=offset.length();
        if(dist>0 && dist<minDist)
            a+=offset*((minDist-dist)/dist);
    }*/

    return steering;
}
/****************************************************************************/
tVector2D collisionAvoidance(tUnit *unit, const tVector2D& v)
{
    int i,delta;
    float ydist,xdist;
    float r,miny=MAX_FLOAT,newminy;
    float a,b,c,d,sqrtd;
    tVector2D offset,newvector,vhat=v.normalize();
    float vlength=v.length();
    bool found;

#ifdef DEBUG
    if(MEMORY_VIOLATION(unit))
        return zeroVector2D;
#endif

    bool isAirUnit=(unit->getDomain()==DOMAIN_AIR);

    if(v.length()<0.001)
        return v;

    while(true)
    {
        found=false;
        //Find the most threatening obstacle:
        for(i=0;i<units.size();i++)
        {
            if( (units[i]->getDomain()==DOMAIN_AIR) != isAirUnit)
                continue;
                
            offset=units[i]->getPosition()-unit->getPosition();
            r=units[i]->getModel()->getMaxRadius()+unit->getModel()->getMaxRadius();

            xdist=vhat.cross(offset);
            ydist=vhat.dot(offset);

            if(xdist<r)
            {
                delta=(int)sqrt(r*r-xdist*xdist);
                if(ydist>0)
                {
                    if(ydist-delta<vlength*3&&ydist<miny)
                    {
                        miny=ydist;

                        //Find the tangent vector on the correct side of the obstacle
                        if(offset.cross(v)<0)
                            r=-r;

                        //Solve the equation ax^2+bx+c=0
                        a=offset.lengthSquared();
                        b=2*offset.y*r;
                        c=r*r-offset.x*offset.x;

                        //If determinant is negative, you are inside obstacle
                        d=b*b-4*a*c;
                        if(d<0)
                            d=0;
                        sqrtd=sqrt(d);

                        newvector.x=(-b+sqrtd)/2/a;
                        if(newvector.x>=1 || newvector.x<=-1)
                            newvector.y=0;
                        else
                        {
                            newvector.y=sqrt(1-newvector.x*newvector.x);
                            if((r+offset.y*newvector.x)/offset.x<0)
                                newvector.y=-newvector.y;
                        }

                        //Find correct solution
                        newminy=offset.dot(newvector)*.99;
                        if(newminy < 0)
                        {
                            newvector.x=(-b-sqrtd)/2/a;
                            if(newvector.x>=1 || newvector.x<=-1)
                                newvector.y=0;
                            else
                            {
                                newvector.y=sqrt(1-newvector.x*newvector.x);
                                if((r+offset.y*newvector.x)/offset.x<0)
                                    newvector.y=-newvector.y;
                            }
                            newminy=offset.dot(newvector)*.99;
                        }
                        found=true;
                    }
                }
            }
        }
        if(!found)
            break;

        vhat=newvector;
        miny=newminy;
    }

    return vhat*vlength;
}
/****************************************************************************/
tUnit *acquireTarget(tUnit *unit, float range, int flags, const tVector3D& origin,
    const tVector3D& direction, float cosTheta, const vector<tUnit *>& vec)
{
    int i;
    float lengthSquared, minDistSquaredA=MAX_FLOAT, minDistSquaredB=MAX_FLOAT;
    tUnit *bestA=NULL, *bestB=NULL;
    bool acquireVulnerableOnly = ((flags&ACQUIRE_VULNERABLE_ONLY)!=0);
    bool acquireRepairableOnly = ((flags&ACQUIRE_REPAIRABLE_ONLY)!=0);
    bool limitAngle = ((flags&ACQUIRE_LIMIT_ANGLE)!=0);
    tVector3D offset;
    int owner=unit->getOwner();

    //Ensure that unit's owner is valid since we use it as an index in tUnitType::repairable.
#ifdef DEBUG
    if(owner<0 || owner>=MAX_PLAYERS)
    {
        bug("acquireTarget: invalid owner");
        return NULL;
    }
#endif

    //If the unit is blind, there's no hope of finding a target.  We check the other
    //condition of CAN_SEE, canPlayerSee, in the loop below.
    if(!unit->canSee())
        return NULL;

    for(i=0;i<vec.size();i++)
    {
        if(acquireVulnerableOnly && !vec[i]->isVulnerable())
            continue;
        if(acquireRepairableOnly && !vec[i]->getType()->repairable[owner])
            continue;
        if(canPlayerSee(owner,vec[i]))
        {
            offset=vec[i]->getCenter()-origin;
            lengthSquared=offset.lengthSquared();
            //Best unit, ignoring angle
            if(lengthSquared<minDistSquaredB)
            {
                minDistSquaredB=lengthSquared;
                bestB=vec[i];
            }
            if(limitAngle && lengthSquared>0)
            {
                offset=offset/sqrt(lengthSquared);
                if(offset.dot(direction) < cosTheta)
                    continue;
            }
            //Best unit, considering angle
            if(lengthSquared<minDistSquaredA)
            {
                minDistSquaredA=lengthSquared;
                bestA=vec[i];
            }
        }
    }
    /*                   Acquire targets in this order:
     *
     *                      <--  Angular Range -->
     *
     *                     \           3           /
     *                      \                     /
     *                       \                   /
     *                        \      .   .      /
     *                         \ '           ' /
     *             4         .  \             /  .
     *                     .     \           /     .
     *                            \    1    /
     *                   '         \       /         '
     *                              \     /
     *                  .     2      \   /            .
     *                                \_/
     *                  .             |_| <- Range -> .
     */
    if(minDistSquaredA<range*range)
        return bestA;
    else if(minDistSquaredB<range*range)
        return bestB;
    else if(bestA)
        return bestA;
    else
        return bestB;
}
/****************************************************************************/
tUnit *acquireTarget(tUnit *unit, float range, float fieldOfView, int flags,
    const tVector3D& origin, const tVector3D& direction, float cosTheta)
{
    int i,x,y;
    long hashValue;
    float lengthSquared, minDistSquaredA=MAX_FLOAT, minDistSquaredB=MAX_FLOAT;
    tUnit *u, *bestA=NULL, *bestB=NULL;
    bool acquireDamagedOnly = ((flags&ACQUIRE_DAMAGED_ONLY)!=0);
    bool acquireAliveOnly = ((flags&ACQUIRE_ALIVE_ONLY)!=0);
    bool acquireVulnerableOnly = ((flags&ACQUIRE_VULNERABLE_ONLY)!=0);
    bool acquireRepairableOnly = ((flags&ACQUIRE_REPAIRABLE_ONLY)!=0);
    bool limitAngle = ((flags&ACQUIRE_LIMIT_ANGLE)!=0);
    tVector3D offset;
    int owner=unit->getOwner();

    //Ensure that unit's owner is valid since we use it as an index in tUnitType::repairable.
#ifdef DEBUG
    if(owner<0 || owner>=MAX_PLAYERS)
    {
        bug("acquireTarget: invalid owner");
        return NULL;
    }
#endif

    //If the unit is blind, there's no hope of finding a target.  We check the other
    //condition of CAN_SEE, canPlayerSee, in the loop below.
    if(!unit->canSee())
        return NULL;

    if(fieldOfView>PURSUE_RADIUS) //Clamp the field of view at PURSUE_RADIUS
        fieldOfView=PURSUE_RADIUS;

    //Determine hash extents:
    tVector2D origin2D=to2D(origin);
    int left=HASH_FUNC(origin2D.x-fieldOfView);
    int right=HASH_FUNC(origin2D.x+fieldOfView);
    clampRange(left,0,hashWidth-1);
    clampRange(right,0,hashWidth-1);

    int bottom=HASH_FUNC(origin2D.y-fieldOfView);
    int top=HASH_FUNC(origin2D.y+fieldOfView);
    clampRange(bottom,0,hashHeight-1);
    clampRange(top,0,hashHeight-1);

    for(i=0;i<MAX_PLAYERS;i++)
    {
        if(!players[i].exists)
            continue;
        if((flags&ACQUIRE_ENEMY_ONLY)!=0 && getAlliance(owner, i) != ALLIANCE_ENEMY)
            continue;
        if((flags&ACQUIRE_ALLY_ONLY)!=0 && getAlliance(owner, i) >= ALLIANCE_NEUTRAL)
            continue;

        for(x=left;x<=right;x++)
            for(y=bottom;y<=top;y++)
            {
                hashValue=(y*hashWidth+x)*MAX_PLAYERS+i;
                for(u=unitHash[hashValue];u;u=u->getNextNeighbor())
                {
                    if(u==unit) //Skip ourselves
                        continue;
                    if(acquireDamagedOnly && u->getHP() == u->getMaxHP())
                        continue;
                    if(acquireAliveOnly && u->getState()!=STATE_ALIVE)
                        continue;
                    if(acquireVulnerableOnly && !u->isVulnerable())
                        continue;
                    if(acquireRepairableOnly && !u->getType()->repairable[owner])
                        continue;
                    if(canPlayerSee(owner,u))
                    {
                        offset=u->getCenter()-origin;
                        lengthSquared=offset.lengthSquared();
                        //Best unit, ignoring angle
                        if(lengthSquared<minDistSquaredB)
                        {
                            minDistSquaredB=lengthSquared;
                            bestB=u;
                        }
                        if(limitAngle && lengthSquared>0)
                        {
                            offset=offset/sqrt(lengthSquared);
                            if(offset.dot(direction) < cosTheta)
                                continue;
                        }
                        //Best unit, considering angle
                        if(lengthSquared<minDistSquaredA)
                        {
                            minDistSquaredA=lengthSquared;
                            bestA=u;
                        }
                    }
                }
            }
    }
    /*                   Acquire targets in this order:
     *
     *                      <--  Angular Range -->
     *
     *                     \           3           /
     *                      \                     /
     *                       \                   /
     *                        \      .   .      /
     *                         \ '           ' /
     *             4         .  \             /  .
     *                     .     \           /     .
     *                            \    1    /
     *                   '         \       /         '
     *                              \     /
     *                  .     2      \   /            .
     *                                \_/
     *                  .             |_| <- Range -> .
     */

    if(minDistSquaredA<range*range)
        return bestA;
    else if(minDistSquaredB<range*range)
        return bestB;
    else if(bestA)
        return bestA;
    else
        return bestB;
}
/****************************************************************************/
void flashPoint(tVector3D v)
{
    tBullsEye *b;
    b = mwNew tBullsEye(animTypeLookup("BullsEye",/*exact=*/true),v);
    if(b)
        bullsEyes.push_back(b);
}
/****************************************************************************/
//This function is internal, therefore I need not validify u
void addNeighbor(tUnit *unit, long hashValue)
{
    unit->setNextNeighbor(unitHash[hashValue]);
    unitHash[hashValue]=unit;
}
/****************************************************************************/
//This function is internal, therefore I need not validify u
void removeNeighbor(tUnit *unit, long hashValue)
{
    tUnit *u;
    if(unitHash[hashValue]==unit)
    {
        unitHash[hashValue]=unit->getNextNeighbor();
    }
    else
    {
        for(u=unitHash[hashValue];u;u=u->getNextNeighbor())
            if(u->getNextNeighbor()==unit)
            {
                u->setNextNeighbor(unit->getNextNeighbor());
                break;
            }
    }
}
/****************************************************************************/
bool doesResourceExist(int resource)
{
#ifdef DEBUG
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("doesResourceExist: invalid resource");
        return false;
    }
#endif
    return resources[resource].exists;
}
/****************************************************************************/
const tString& getResourceCaption(int resource)
{
#ifdef DEBUG
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getResourceCaption: invalid resource");
        return EMPTY_STRING;
    }
#endif
    return resources[resource].caption;
}
/****************************************************************************/
float getResourceAmount(int resource)
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getResourceAmount: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getResourceAmount: invalid resource");
        return 0;
    }
#endif
    //The player may incur a debt if he runs out of resources in the middle of
    //a game interval.  A negative value will confuse the user, so we clamp
    //it at zero.
    return MAX(players[localPlayerID].resources[resource],0);
}
/****************************************************************************/
float getResourceCapacity(int resource)
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getResourceCapacity: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getResourceCapacity: invalid resource");
        return 0;
    }
#endif
    return players[localPlayerID].maxResources[resource];
}
/****************************************************************************/
float getResourceRevenue(int resource)
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getResourceRevenue: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getResourceRevenue: invalid resource");
        return 0;
    }
#endif
    return players[localPlayerID].revenues[resource];
}
/****************************************************************************/
float getResourceCost(int resource)
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getResourceCost: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getResourceCost: invalid resource");
        return 0;
    }
#endif
    return players[localPlayerID].costs[resource];
}
/****************************************************************************/
float getForeignAid(int resource)
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getForeignAid: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getForeignAid: invalid resource");
        return 0;
    }
#endif
    return players[localPlayerID].foreignAid[resource];
}
/****************************************************************************/
void adjustResource(int playerID, int resourceID, float amount)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("adjustResource: invalid playerID");
        return;
    }
    if(resourceID<0||resourceID>=MAX_RESOURCES)
    {
        bug("adjustResource: invalid resourceID");
        return;
    }
#endif
    players[playerID].resources[resourceID]+=amount;

    //This function can incur a debt (for the purpose of conserving resources).
    //However, the function cannot exceed the player's maximum resource storage capacity.
    if(players[playerID].resources[resourceID]>players[playerID].maxResources[resourceID])
        players[playerID].resources[resourceID]=players[playerID].maxResources[resourceID];
    players[playerID].r0[resourceID]=
        players[playerID].resources[resourceID];
}
/****************************************************************************/
void adjustCost(int playerID, int resourceID, float amount)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("adjustCost: invalid playerID");
        return;
    }
    if(resourceID<0||resourceID>=MAX_RESOURCES)
    {
        bug("adjustCost: invalid resourceID");
        return;
    }
#endif
    players[playerID].costs[resourceID]+=amount;
#ifdef DEBUG
    if(players[playerID].costs[resourceID]<0)
    {
        bug("adjustCost: cost < 0");
        players[playerID].costs[resourceID]=0;
    }
#endif
}
/****************************************************************************/
void adjustRevenue(int playerID, int resourceID, float amount)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("adjustRevenue: invalid playerID");
        return;
    }
    if(resourceID<0||resourceID>=MAX_RESOURCES)
    {
        bug("adjustRevenue: invalid resourceID");
        return;
    }
#endif
    players[playerID].revenues[resourceID]+=amount;
#ifdef DEBUG
    if(players[playerID].revenues[resourceID]<0)
    {
        bug("adjustRevenue: revenue < 0");
        players[playerID].revenues[resourceID]=0;
    }
#endif
}
/****************************************************************************/
void adjustCapacity(int playerID, int resourceID, float amount)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("adjustStorage: invalid playerID");
        return;
    }
    if(resourceID<0||resourceID>=MAX_RESOURCES)
    {
        bug("adjustStorage: invalid resourceID");
        return;
    }
#endif
    players[playerID].maxResources[resourceID]+=amount;
#ifdef DEBUG
    if(players[playerID].maxResources[resourceID]<0)
    {
        players[playerID].maxResources[resourceID]=0;
        bug("adjustCapacity: maxResources < 0");
    }
#endif
}
/****************************************************************************/
void clearGroup(int group)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("clearGroup: map isn't initialized");
        return;
    }
    if(group<0)
    {
        bug("clearGroup: group < 0");
        return;
    }
#endif
    for(int i=0;i<units.size();i++)
        if(units[i]->getGroup()==group)
            units[i]->disbandGroup();
}
/****************************************************************************/
void assignToGroup(int group, bool shiftPressed)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("assignToGroup: map isn't initialized");
        return;
    }
    if(group<0)
    {
        bug("assignToGroup: group < 0");
        return;
    }
#endif
    if(!shiftPressed)
        clearGroup(group);
    for(int i=0;i<selectedUnits.size();i++)
        selectedUnits[i]->setGroup(group);
}
/****************************************************************************/
void disbandGroup()
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("disbandGroup: map isn't initialized");
        return;
    }
#endif
    for(int i=0;i<selectedUnits.size();i++)
        selectedUnits[i]->disbandGroup();
}
/****************************************************************************/
void selectGroup(int group, bool shiftPressed)
{
    bool allSelected=true;
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("selectGroup: map isn't initialized");
        return;
    }
    if(group<0)
    {
        bug("selectGroup: group < 0");
        return;
    }
#endif
    if(!shiftPressed)
        clearSelection();
    for(int i=0;i<units.size();i++)
        if(units[i]->getState()>=STATE_SELECTABLE && units[i]->getGroup()==group &&
            !units[i]->isSelected())
        {
            allSelected=false;
            break;
        }
    if(allSelected)
    {
        for(int i=0;i<units.size();i++)
            if(units[i]->getState()>=STATE_SELECTABLE && units[i]->getGroup()==group)
                deselectUnit(units[i]);
    }
    else
    {
        for(int i=0;i<units.size();i++)
            if(units[i]->getState()>=STATE_SELECTABLE && units[i]->getGroup()==group)
                selectUnit(units[i]);
    }
}
/****************************************************************************/
void targetGroup(int group, bool shiftPressed, eCommand command)
{
#ifdef DEBUG
    if(!objectsLoaded)
    {
        bug("targetGroup: map isn't initialized");
        return;
    }
    if(group<0)
    {
        bug("targetGroup: group < 0");
        return;
    }
#endif
    vector<tUnit *> targets;
    if(!shiftPressed && !commandTable[command].immediate)
        commandSel(COMMAND_STOP);
    for(int i=0;i<units.size();i++)
        if(units[i]->getState()>=STATE_SELECTABLE && units[i]->getGroup()==group)
            targets.push_back(units[i]);
    commandSel(command,&targets[0], targets.size());
}
/****************************************************************************/
void setViewDimensions(int _viewWidth, int _viewHeight)
{
#ifdef DEBUG
    if(_viewWidth<=0 || _viewHeight<=0)
    {
        bug("setViewDimensions: viewWidth<=0 or viewHeight<=0");
        return;
    }
#endif
    viewWidth=_viewWidth;
    viewHeight=_viewHeight;
    orientMap();
    //Inform the terrain so that it can adjust the resolution as necessary
    if(terrain)
        terrain->onSetViewDimensions();
}
/****************************************************************************/
void setMinimapDimensions(int _minimapWidth, int _minimapHeight)
{
#ifdef DEBUG
    if(_minimapWidth<=0 || _minimapHeight<=0)
    {
        bug("setMinimapDimensions: minimapWidth<=0 or minimapHeight<=0");
        return;
    }
#endif
    minimapWidth=_minimapWidth;
    minimapHeight=_minimapHeight;
    orientMap();
}
/****************************************************************************/
void spinMap(float _mapRotation)
{
    mapRotation=_mapRotation;
    orientMap();
}
/****************************************************************************/
void zoomMap(float _mapScale)
{
    mapScale=_mapScale;
    orientMap();
    //Inform the terrain so that it can adjust the resolution as necessary
    if(terrain)
        terrain->onZoomMap();
}
/****************************************************************************/
void tiltMap(float _mapTilt)
{
    mapTilt=_mapTilt;
    orientMap();
    //Inform the terrain so that it can adjust the resolution as necessary
    if(terrain)
        terrain->onTiltMap();
}
/****************************************************************************/
void panMap(const tVector2D& _mapOffset)
{
    mapOffset=_mapOffset;
    orientMap();

    sndPositionListener(to3D(mapOffset));
}
/****************************************************************************/
void panMinimap(int x, int y)
{
    tTileInfo ti;
    float xf=(float)x;
    float yf=(float)y;
    tVector3D v={
        invMinimapMatrix[0]*xf + invMinimapMatrix[4]*yf + invMinimapMatrix[12],
        invMinimapMatrix[1]*xf + invMinimapMatrix[5]*yf + invMinimapMatrix[13],
        0
        };
    if(terrain)
    {
        ti=terrain->getTileInfo(to2D(v),DOMAIN_NONE,0);
        v.z=ti.height;
    }
    centerView(v);
}
/****************************************************************************/
tVector3D getViewCenter()
{
    return pixelToTerrain3D(viewWidth/2,viewHeight/2);
}
/****************************************************************************/
void centerView(const tVector3D& v)
{
    /*
     * [1  0     0    0] [cosr -sinr 0 0] [1 0 0 p][X]     [0]
     * [0  cost  sint 0] [sinr cosr  0 0] [0 1 0 q][Y]     [0]
     * [0  -sint cost 0] [0    0     1 0] [0 0 1 0][Z]  =  [?]
     * [0  0     0    1] [0    0     0 1] [0 0 0 1][1]     [?]
     *
     * [1  0     0    0] [cosr -sinr 0 p*cosr-q*sinr][X]     [0]
     * [0  cost  sint 0] [sinr cosr  0 p*sinr+q*cosr][Y]     [0]
     * [0  -sint cost 0] [0    0     1 0            ][Z]  =  [?]
     * [0  0     0    1] [0    0     0 1            ][1]     [?]
     *
     * [cosr       -sinr      0    p*cosr-q*sinr        ][X]     [0]
     * [cost*sinr  cost*cosr  sint cost*(p*sinr+q*cosr) ][Y]     [0]
     * [-sint*sinr -sint*cosr cost -sint*(p*sinr+q*cosr)][Z]  =  [?]
     * [0          0          0    1                    ][1]     [?]
     *
     * [cosr]X + [-sinr]Y + [p*cosr-q*sinr] = 0
     * [cost*sinr]X + [cost*cosr]Y + [sint]Z + [cost*(p*sinr+q*cosr)] = 0
     *
     * [cosr]*p+[-sinr]*q+[cosr*X - sinr*Y]=0
     * [cost*sinr]*p+[cost*cosr]*q+[cost*sinr*X + cost*cosr*Y + sint*Z]=0
     *
     * ap + bq + c = 0
     * dp + eq + f = 0
     */
    float cosr=cos(mapRotation);
    float sinr=sin(mapRotation);
    float cost=cos(mapTilt);
    float sint=sin(mapTilt);
    float p,q;

    float a=cosr;
    float b=-sinr;
    float c=cosr*v.x - sinr*v.y;
    float d=cost*sinr;
    float e=cost*cosr;
    float f=cost*sinr*v.x + cost*cosr*v.y + sint*v.z;

    float den=a*e-b*d;
    if(den!=0)
    {
        p=(b*f-c*e)/den;
        if(b!=0)
            q=(-a*p-c)/b;
        else
            q=(-d*p-f)/e;

        mapOffset.x=-p;
        mapOffset.y=-q;
    }

    orientMap();

    sndPositionListener(to3D(mapOffset));
}
/****************************************************************************/
static void printMatrixProduct(float a[16], float b[16])
{
    int i,j,k;
    float total;
    char buf[1000];
    tBufferIterator ptr(buf,sizeof(buf));

    for(i=0;i<4;i++) //rows of a
    {
        mySprintf(ptr,"\n");
        for(j=0;j<4;j++) //cols of b
        {
            total=0;
            for(k=0;k<4;k++)
                total+=a[i+k*4]*b[j*4+k];
            mySprintf(ptr,"%8.6f ",total);
        }
    }
    log(buf);
}
/****************************************************************************/
void orientMap()
{
#ifdef DEBUG
    if(mapScale<=0)
    {
        bug("orientMap: mapScale<=0");
        return;
    }
    if(viewWidth<=0)
    {
        bug("orientMap: viewWidth<=0");
        return;
    }
    if(viewHeight<=0)
    {
        bug("orientMap: viewHeight<=0");
        return;
    }
    if(minimapWidth<=0)
    {
        bug("orientMap: minimapWidth<=0");
        return;
    }
    if(minimapHeight<=0)
    {
        bug("orientMap: minimapHeight<=0");
        return;
    }
#endif

    int i;
    float center;
    float cosr=cos(mapRotation);
    float sinr=sin(mapRotation);
    float cost=cos(mapTilt);
    float sint=sin(mapTilt);
    float p=-mapOffset.x;
    float q=-mapOffset.y;
    float z=-1/mapScale;
    float VW=(float)viewWidth;
    float VH=(float)viewHeight;
    float MW=(float)minimapWidth;
    float MH=(float)minimapHeight;
    float viewAspectRatio=(float)viewWidth/viewHeight;
    float minimapAspectRatio=(float)minimapWidth/minimapHeight;

    float A=(float)NEAR_PLANE/viewAspectRatio;
    float B=(float)NEAR_PLANE;
    float C=-(float)(FAR_PLANE+NEAR_PLANE)/(FAR_PLANE-NEAR_PLANE);
    float D=-(float)(2*FAR_PLANE*NEAR_PLANE)/(FAR_PLANE-NEAR_PLANE);

    float E=(float)viewAspectRatio/NEAR_PLANE;
    float F=(float)1/NEAR_PLANE;
    float G=-(float)(FAR_PLANE-NEAR_PLANE)/(2*FAR_PLANE*NEAR_PLANE);
    float H=(float)(FAR_PLANE+NEAR_PLANE)/(2*FAR_PLANE*NEAR_PLANE);

    /* PROJECTION MATRIX:
     *
     * [VW/2 0    0 VW/2] [A 0 0  0] [1 0 0 0] [1  0     0    0] [cosr -sinr 0 0] [1 0 0 p]
     * [0    VH/2 0 VH/2] [0 B 0  0] [0 1 0 0] [0  cost  sint 0] [sinr cosr  0 0] [0 1 0 q]
     * [0    0    1 0   ] [0 0 C  D] [0 0 1 z] [0  -sint cost 0] [0    0     1 0] [0 0 1 0]  =
     * [0    0    0 1   ] [0 0 -1 0] [0 0 0 1] [0  0     0    1] [0    0     0 1] [0 0 0 1]
     *
     * [VW/2 0    0 VW/2] [A 0 0  0] [1 0 0 0] [1  0     0    0] [cosr -sinr 0 p*cosr-q*sinr]
     * [0    VH/2 0 VH/2] [0 B 0  0] [0 1 0 0] [0  cost  sint 0] [sinr cosr  0 p*sinr+q*cosr]
     * [0    0    1 0   ] [0 0 C  D] [0 0 1 z] [0  -sint cost 0] [0    0     1 0            ]  =
     * [0    0    0 1   ] [0 0 -1 0] [0 0 0 1] [0  0     0    1] [0    0     0 1            ]
     *
     * [VW/2 0    0 VW/2] [A 0 0  0] [1 0 0 0] [cosr       -sinr      0    p*cosr-q*sinr        ]
     * [0    VH/2 0 VH/2] [0 B 0  0] [0 1 0 0] [cost*sinr  cost*cosr  sint cost*(p*sinr+q*cosr) ]
     * [0    0    1 0   ] [0 0 C  D] [0 0 1 z] [-sint*sinr -sint*cosr cost -sint*(p*sinr+q*cosr)]  =
     * [0    0    0 1   ] [0 0 -1 0] [0 0 0 1] [0          0          0    1                    ]
     *
     * [VW/2 0    0 VW/2] [A 0 0  0] [cosr       -sinr      0    p*cosr-q*sinr          ]
     * [0    VH/2 0 VH/2] [0 B 0  0] [cost*sinr  cost*cosr  sint cost*(p*sinr+q*cosr)   ]
     * [0    0    1 0   ] [0 0 C  D] [-sint*sinr -sint*cosr cost -sint*(p*sinr+q*cosr)+z]  =
     * [0    0    0 1   ] [0 0 -1 0] [0          0          0    1                      ]
     *
     * [VW/2 0    0 VW/2] [A*cosr       -A*sinr      0        A*(p*cosr-q*sinr)            ]
     * [0    VH/2 0 VH/2] [B*cost*sinr  B*cost*cosr  B*sint   B*cost*(p*sinr+q*cosr)       ]
     * [0    0    1 0   ] [-C*sint*sinr -C*sint*cosr C*cost   -C*(sint*(p*sinr+q*cosr)-z)+D]
     * [0    0    0 1   ] [sint*sinr    sint*cosr    -cost    sint*(p*sinr+q*cosr)-z       ]
     */

    float m[16]={
        A*cosr,
        B*cost*sinr,
        -C*sint*sinr,
        sint*sinr,

        -A*sinr,
        B*cost*cosr,
        -C*sint*cosr,
        sint*cosr,

        0,
        B*sint,
        C*cost,
        -cost,

        A*(p*cosr-q*sinr),
        B*cost*(p*sinr+q*cosr),
        -C*(sint*(p*sinr+q*cosr)-z)+D,
        sint*(p*sinr+q*cosr)-z};

    //Column 1
    viewMatrix[0]=VW/2*(m[0]+m[3]);
    viewMatrix[1]=VH/2*(m[1]+m[3]);
    viewMatrix[2]=m[2];
    viewMatrix[3]=m[3];

    //Column 2
    viewMatrix[4]=VW/2*(m[4]+m[7]);
    viewMatrix[5]=VH/2*(m[5]+m[7]);
    viewMatrix[6]=m[6];
    viewMatrix[7]=m[7];

    //Column 3
    viewMatrix[8]=VW/2*(m[8]+m[11]);
    viewMatrix[9]=VH/2*(m[9]+m[11]);
    viewMatrix[10]=m[10];
    viewMatrix[11]=m[11];

    //Column 4
    viewMatrix[12]=VW/2*(m[12]+m[15]);
    viewMatrix[13]=VH/2*(m[13]+m[15]);
    viewMatrix[14]=m[14];
    viewMatrix[15]=m[15];

    /*
     * INVERSE MATRIX:
     *
     * [1 0 0 -p] [cosr  sinr  0    0] [1 0     0     0] [1 0 0 0 ] [E 0 0 0 ] [2/VW 0    0 -1]
     * [0 1 0 -q] [-sinr cosr  0    0] [0 cost  -sint 0] [0 1 0 0 ] [0 F 0 0 ] [0    2/VH 0 -1]
     * [0 0 1 0 ] [0     0     1    0] [0 sint  cost  0] [0 0 1 -z] [0 0 0 -1] [0    0    1 0 ] =
     * [0 0 0 1 ] [0     0     0    1] [0 0     0     1] [0 0 0 1 ] [0 0 G H ] [0    0    0 1 ]
     *
     * [cosr  sinr  0    -p] [1 0     0     0] [1 0 0 0 ] [E 0 0 0 ] [2/VW 0    0 -1]
     * [-sinr cosr  0    -q] [0 cost  -sint 0] [0 1 0 0 ] [0 F 0 0 ] [0    2/VH 0 -1]
     * [0     0     1    0 ] [0 sint  cost  0] [0 0 1 -z] [0 0 0 -1] [0    0    1 0 ] =
     * [0     0     0    1 ] [0 0     0     1] [0 0 0 1 ] [0 0 G H ] [0    0    0 1 ]
     *
     * [cosr  sinr*cost -sinr*sint -p] [1 0 0 0 ] [E 0 0 0 ] [2/VW 0    0 -1]
     * [-sinr cosr*cost -cosr*sint -q] [0 1 0 0 ] [0 F 0 0 ] [0    2/VH 0 -1]
     * [0     sint      cost       0 ] [0 0 1 -z] [0 0 0 -1] [0    0    1 0 ] =
     * [0     0         0          1 ] [0 0 0 1 ] [0 0 G H ] [0    0    0 1 ]
     *
     * [cosr  sinr*cost -sinr*sint sinr*sint*z-p] [E 0 0 0 ] [2/VW 0    0 -1]
     * [-sinr cosr*cost -cosr*sint cosr*sint*z-q] [0 F 0 0 ] [0    2/VH 0 -1]
     * [0     sint      cost       -cost*z      ] [0 0 0 -1] [0    0    1 0 ] =
     * [0     0         0          1            ] [0 0 G H ] [0    0    0 1 ]
     *
     * [cosr*E  sinr*cost*F (sinr*sint*z-p)*G sinr*sint+(sinr*sint*z-p)*H] [2/VW 0    0 -1]
     * [-sinr*E cosr*cost*F (cosr*sint*z-q)*G cosr*sint+(cosr*sint*z-q)*H] [0    2/VH 0 -1]
     * [0       sint*F      -cost*z*G         -cost-cost*z*H             ] [0    0    1 0 ]
     * [0       0           G                 H                          ] [0    0    0 1 ]
     *
     */

    float im[16]={
        cosr*E,
        -sinr*E,
        0,
        0,

        sinr*cost*F,
        cosr*cost*F,
        sint*F,
        0,

        (sinr*sint*z-p)*G,
        (cosr*sint*z-q)*G,
        -cost*z*G,
        G,

        sinr*sint+(sinr*sint*z-p)*H,
        cosr*sint+(cosr*sint*z-q)*H,
        -cost-cost*z*H,
        H};

    //Column 1
    invViewMatrix[0]=im[0]*2/VW;
    invViewMatrix[1]=im[1]*2/VW;
    invViewMatrix[2]=im[2]*2/VW;
    invViewMatrix[3]=im[3]*2/VW;

    //Column 2
    invViewMatrix[4]=im[4]*2/VH;
    invViewMatrix[5]=im[5]*2/VH;
    invViewMatrix[6]=im[6]*2/VH;
    invViewMatrix[7]=im[7]*2/VH;

    //Column 3
    invViewMatrix[8]=im[8];
    invViewMatrix[9]=im[9];
    invViewMatrix[10]=im[10];
    invViewMatrix[11]=im[11];

    //Column 4
    invViewMatrix[12]=-im[0]-im[4]+im[12];
    invViewMatrix[13]=-im[1]-im[5]+im[13];
    invViewMatrix[14]=-im[2]-im[6]+im[14];
    invViewMatrix[15]=-im[3]-im[7]+im[15];

    float minX=MAX_FLOAT, maxX=-MAX_FLOAT;
    float minY=MAX_FLOAT, maxY=-MAX_FLOAT;
    tVector2D points[4]={{0,0},{mapDimensions.x,0},{mapDimensions.x,mapDimensions.y},{0,mapDimensions.y}};
    for(i=0;i<4;i++)
    {
        float x=cosr*points[i].x + -sinr*points[i].y;
        float y=sinr*points[i].x + cosr*points[i].y;
        if(x<minX)  minX=x;
        if(x>maxX)  maxX=x;
        if(y<minY)  minY=y;
        if(y>maxY)  maxY=y;
    }
    float mapWidth=maxX-minX;
    float mapHeight=maxY-minY;
#ifdef DEBUG
    if(mapWidth<=0 || mapHeight<=0)
    {
        bug("orientMap: mapWidth<=0 or mapHeight<=0");
        return;
    }
#endif
    //NOTE: We must preserve the aspect ratio!
    //Case 1: The image is wider than the minimap window.
    //Solution: Make the image taller.
    //Case 2: The image is taller than the minimap window.
    //Solution: Make the image wider.
    if(mapWidth/mapHeight > minimapAspectRatio)
    {
        mapHeight=mapWidth/minimapAspectRatio;
        center=(minY+maxY)/2;
        minY=center-mapHeight/2;
        maxY=center+mapHeight/2;
    }
    else if(mapWidth/mapHeight < minimapAspectRatio)
    {
        mapWidth=minimapAspectRatio*mapHeight;
        center=(minX+maxX)/2;
        minX=center-mapWidth/2;
        maxX=center+mapWidth/2;
    }
    float I=2/(maxX-minX);
    float J=-(maxX+minX)/(maxX-minX);
    float K=2/(maxY-minY);
    float L=-(maxY+minY)/(maxY-minY);
    /*
     * MINIMAP MATRIX:
     *
     * [MW/2 0    0 MW/2] [I 0 0 J] [cosr -sinr 0 0]
     * [0    MH/2 0 MH/2] [0 K 0 L] [sinr cosr  0 0]
     * [0    0    1 0   ] [0 0 0 0] [0    0     1 0]  =
     * [0    0    0 1   ] [0 0 0 1] [0    0     0 1]
     *
     * [MW/2 0    0 MW/2] [I*cosr  -I*sinr 0 J]     [MW/2*I*cosr -MW/2*I*sinr 0 MW/2*(J+1)]
     * [0    MH/2 0 MH/2] [K*sinr  K*cosr  0 L]     [MH/2*K*sinr MH/2*K*cosr  0 MH/2*(L+1)]
     * [0    0    1 0   ] [0       0       0 0]  =  [0           0            0 0         ]
     * [0    0    0 1   ] [0       0       0 1]     [0           0            0 1         ]

     */
    //Column 1
    minimapMatrix[0]=MW/2*I*cosr;
    minimapMatrix[1]=MH/2*K*sinr;
    minimapMatrix[2]=0;
    minimapMatrix[3]=0;

    //Column 2
    minimapMatrix[4]=-MW/2*I*sinr;
    minimapMatrix[5]=MH/2*K*cosr;
    minimapMatrix[6]=0;
    minimapMatrix[7]=0;

    //Column 3
    minimapMatrix[8]=0;
    minimapMatrix[9]=0;
    minimapMatrix[10]=0;
    minimapMatrix[11]=0;

    //Column 4
    minimapMatrix[12]=MW/2*(J+1);
    minimapMatrix[13]=MH/2*(L+1);
    minimapMatrix[14]=0;
    minimapMatrix[15]=1;

    float M=(maxX-minX)/2;
    float N=(maxX+minX)/2;
    float P=(maxY-minY)/2;
    float Q=(maxY+minY)/2;
    /*
     * INVERSE MINIMAP MATRIX:
     *
     * [cosr  sinr 0 0] [M 0 0 N] [2/MW 0    0 -1]
     * [-sinr cosr 0 0] [0 P 0 Q] [0    2/MH 0 -1]
     * [0     0    1 0] [0 0 0 0] [0    0    1 0 ] =
     * [0     0    0 1] [0 0 0 1] [0    0    0 1 ]
     *
     * [cosr*M  sinr*P  0 cosr*N+sinr*Q ] [2/MW 0    0 -1]   [cosr*M*2/MW  sinr*P*2/MH 0 cosr*(N-M)+sinr*(Q-P) ]
     * [-sinr*M cosr*P  0 -sinr*N+cosr*Q] [0    2/MH 0 -1]   [-sinr*M*2/MW cosr*P*2/MH 0 -sinr*(N-M)+cosr*(Q-P)]
     * [0       0       0 0             ] [0    0    1 0 ] = [0            0           0 0                     ]
     * [0       0       0 1             ] [0    0    0 1 ]   [0            0           0 1                     ]
     */
    //Column 1
    invMinimapMatrix[0]=cosr*M*2/MW;
    invMinimapMatrix[1]=-sinr*M*2/MW;
    invMinimapMatrix[2]=0;
    invMinimapMatrix[3]=0;

    //Column 2
    invMinimapMatrix[4]=sinr*P*2/MH;
    invMinimapMatrix[5]=cosr*P*2/MH;
    invMinimapMatrix[6]=0;
    invMinimapMatrix[7]=0;

    //Column 3
    invMinimapMatrix[8]=0;
    invMinimapMatrix[9]=0;
    invMinimapMatrix[10]=0;
    invMinimapMatrix[11]=0;

    //Column 4
    invMinimapMatrix[12]=cosr*(N-M)+sinr*(Q-P);
    invMinimapMatrix[13]=-sinr*(N-M)+cosr*(Q-P);
    invMinimapMatrix[14]=0;
    invMinimapMatrix[15]=1;

    //Set up pan matrix for user-interface.
    //Notice that we need to rotate and scale this matrix in the
    //opposite direction of the viewMatrix.
    panMatrix[0]=cosr/mapScale;  panMatrix[2]=sinr/mapScale;
    panMatrix[1]=-sinr/mapScale; panMatrix[3]=cosr/mapScale;

    //Set up the static frustum variables.
    //I use these variables to clip units outside the view
    int inX[8]={0,viewWidth,0,viewWidth,0,viewWidth,0,viewWidth};
    int inY[8]={0,0,viewHeight,viewHeight,0,0,viewHeight,viewHeight};
    float inE[8]={0,0,0,0,MAX_UNIT_ELEVATION,MAX_UNIT_ELEVATION,MAX_UNIT_ELEVATION,MAX_UNIT_ELEVATION};
    //NOTE: A plane can rise as high as MAX_UNIT_ELEVATION above the ground.  Thus, we add
    //MAX_UNIT_ELEVATION to the highest point of the terrain.
    if(terrain)
    {
        inE[0]=inE[1]=inE[2]=inE[3]=terrain->getMinElevation();
        inE[4]=inE[5]=inE[6]=inE[7]=terrain->getMaxElevation()+MAX_UNIT_ELEVATION;
    }
    tVector3D out[8];
    tVector3D total=zeroVector3D;
    float radiusSq,maxRadiusSq=0;
    frustumMinX=MAX_FLOAT;  frustumMaxX=-MAX_FLOAT;
    frustumMinY=MAX_FLOAT;  frustumMaxY=-MAX_FLOAT;
    frustumMinZ=MAX_FLOAT;  frustumMaxZ=-MAX_FLOAT;
    for(i=0;i<8;i++)
    {
        out[i]=pixelToVector3DAtElevation(inX[i],inY[i],inE[i]);
        if(out[i].x<frustumMinX)    frustumMinX=out[i].x;
        if(out[i].x>frustumMaxX)    frustumMaxX=out[i].x;
        if(out[i].y<frustumMinY)    frustumMinY=out[i].y;
        if(out[i].y>frustumMaxY)    frustumMaxY=out[i].y;
        if(out[i].z<frustumMinZ)    frustumMinZ=out[i].z;
        if(out[i].z>frustumMaxZ)    frustumMaxZ=out[i].z;
        total+=out[i];
    }
    frustumCenter=total/8;
    for(i=0;i<8;i++)
    {
        radiusSq=(out[i]-frustumCenter).lengthSquared();
        if(radiusSq>maxRadiusSq)
            maxRadiusSq=radiusSq;
    }
    frustumRadius=sqrt(maxRadiusSq);

    int cornersX[4]={0,viewWidth,viewWidth,0};
    int cornersY[4]={0,0,viewHeight,viewHeight};
    tVector2D v;

    viewMinX=MAX_FLOAT;
    viewMaxX=0;
    viewMinY=MAX_FLOAT;
    viewMaxY=0;

    if(terrain)
        for(i=0;i<4;i++)
        {
            v=pixelToVector2DAtElevation(cornersX[i],cornersY[i],terrain->getMaxElevation());
            if(v.x<viewMinX)    viewMinX=v.x;
            if(v.x>viewMaxX)    viewMaxX=v.x;
            if(v.y<viewMinY)    viewMinY=v.y;
            if(v.y>viewMaxY)    viewMaxY=v.y;
            v=pixelToVector2DAtElevation(cornersX[i],cornersY[i],terrain->getMinElevation());
            if(v.x<viewMinX)    viewMinX=v.x;
            if(v.x>viewMaxX)    viewMaxX=v.x;
            if(v.y<viewMinY)    viewMinY=v.y;
            if(v.y>viewMaxY)    viewMaxY=v.y;
        }
    else
        for(i=0;i<4;i++)
        {
            v=pixelToVector2D(cornersX[i],cornersY[i]);
            if(v.x<viewMinX)    viewMinX=v.x;
            if(v.x>viewMaxX)    viewMaxX=v.x;
            if(v.y<viewMinY)    viewMinY=v.y;
            if(v.y>viewMaxY)    viewMaxY=v.y;
        }

    //Ensure that these values are within range:
    if(minX<0)                  minX=0;
    if(maxX>mapDimensions.x)    maxX=mapDimensions.x;
    if(minY<0)                  minY=0;
    if(maxY>mapDimensions.y)    maxY=mapDimensions.y;

    if(terrain)
        terrain->onOrientMap();
}
/****************************************************************************/
void getScaleAtPoint(const tVector3D& point, float& projXScale, float& projYScale, float& projZScale)
{
    int x,y,x1,y1,x2,y2,x3,y3;
    float cosr=cos(-mapRotation);
    float sinr=sin(-mapRotation);
    tVector3D p1=point;
    p1.x+=cosr;
    p1.y+=sinr;
    tVector3D p2=point;
    p2.x+=-sinr;
    p2.y+=cosr;
    tVector3D p3=point;
    p3.z+=1;
    vector3DToPixel(point,x,y);
    vector3DToPixel(p1,x1,y1);
    vector3DToPixel(p2,x2,y2);
    vector3DToPixel(p3,x3,y3);
    projXScale=(float)(x1-x);
    projYScale=(float)(y2-y);
    projZScale=(float)(y3-y);
}
/****************************************************************************/
void createUnitExplosion(tUnitExplosion *u)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(u))
        return;
#endif
    unitExplosions.push_back(u);
}
/****************************************************************************/
void createApparition(eCommand command, int x, int y)
{
    tTileInfo t;
    apparitionType=unitTypeLookup(commandTable[command].unitID);
#ifdef DEBUG
    if(apparitionType==NULL)
    {
        bug("createApparition: unit type not found");
        return;
    }
#endif
    updateApparitionOwner();

    if(terrain)
    {
        apparitionPosition=terrain->pixelToTerrain3D(x,y);
        t=terrain->getTileInfo(to2D(apparitionPosition),apparitionType->domain,apparitionType->elevationArg.initialValue);
        apparitionPosition.z=t.height;
        apparitionUp=t.normal;
        apparitionUp.z*=2;
        apparitionUp=apparitionUp.normalize();
        apparitionForward=iHat3D;
        apparitionSide=apparitionUp.cross(apparitionForward);
        apparitionForward=apparitionSide.cross(apparitionUp);
    }
    else
    {
        apparitionPosition=pixelToVector3D(x,y);
        apparitionForward=iHat3D;
        apparitionSide=jHat3D;
        apparitionUp=kHat3D;
    }
}
/****************************************************************************/
void destroyApparition()
{
    apparitionType=NULL;
}
/****************************************************************************/
void drawApparition()
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(apparitionType))
        return;
#endif

    //Apparently, lighting must be enabled during matrix operations...
    glEnable(GL_LIGHTING);
    glPushMatrix();
    glTranslatef(apparitionPosition.x,apparitionPosition.y,apparitionPosition.z);
        //small fudge factor so that flat units and selection circle aren't obscured by terrain
    //Translate this vector with forward3D, side3D, and up3D
    GLfloat orientation[16]={apparitionForward.x,apparitionForward.y,apparitionForward.z,0,
        apparitionSide.x,apparitionSide.y,apparitionSide.z,0,
        apparitionUp.x,apparitionUp.y,apparitionUp.z,0, 0,0,0,1};
    glMultMatrixf(orientation);

    glColor3f(1,1,1);
    apparitionType->modelType->draw(DRAW_TEXTURE|DRAW_MODEL);

    glPopMatrix();
    glDisable(GL_LIGHTING);
}
/****************************************************************************/
void updateApparitionOwner()
{
    for(int i=0; i<selectedUnits.size(); i++)
        if(canControl(selectedUnits[i]))
        {
            apparitionOwner=selectedUnits[i]->getOwner();
            return;
        }
    //If none of the units are capable of constructing the unit, then draw
    //the unit using the local player's color.
    apparitionOwner=localPlayerID;
}
/****************************************************************************/
tWorldTime toWorldTime(tGameTime t)
{
    return worldTimeBias + t*gameSpeed/1000.0;
}
/****************************************************************************/
bool setAlliance(int player1, int player2, eAlliance a,
    bool sharedControl, bool sharedResources, bool sharedVision)
{
#ifdef DEBUG
    if(player1<0 || player1>=MAX_PLAYERS)
    {
        bug("setAlliance: player1 invalid");
        return false;
    }
    if(player2<0 || player2>=MAX_PLAYERS)
    {
        bug("setAlliance: player2 invalid");
        return false;
    }
    if(a<ALLIANCE_SELF || a>ALLIANCE_ENEMY)
    {
        bug("setAlliance: alliance invalid");
        return false;
    }
#endif
    tSetAllianceMsg message(player1, player2, a, sharedControl, sharedResources, sharedVision);
    postMessage(&message);
    return true;
}
/****************************************************************************/
bool setAlliance(int player, eAlliance a,
    bool sharedControl, bool sharedResources, bool sharedVision)
{
    return setAlliance(localPlayerID, player, a,
        sharedControl, sharedResources, sharedVision);
}
/****************************************************************************/
bool getSharedControl(int player1, int player2)
{
#ifdef DEBUG
    if(player1<0 || player1>=MAX_PLAYERS)
    {
        bug("getSharedControl: player1 invalid");
        return false;
    }
    if(player2<0 || player2>=MAX_PLAYERS)
    {
        bug("getSharedControl: player2 invalid");
        return false;
    }
#endif
    return players[player1].sharedControl[player2];
}
/****************************************************************************/
bool getSharedResources(int player1, int player2)
{
#ifdef DEBUG
    if(player1<0 || player1>=MAX_PLAYERS)
    {
        bug("getSharedResources: player1 invalid");
        return false;
    }
    if(player2<0 || player2>=MAX_PLAYERS)
    {
        bug("getSharedResources: player2 invalid");
        return false;
    }
#endif
    return players[player1].sharedResources[player2];
}
/****************************************************************************/
bool getSharedVision(int player1, int player2)
{
#ifdef DEBUG
    if(player1<0 || player1>=MAX_PLAYERS)
    {
        bug("getSharedVision: player1 invalid");
        return false;
    }
    if(player2<0 || player2>=MAX_PLAYERS)
    {
        bug("getSharedVision: player2 invalid");
        return false;
    }
#endif
    return players[player1].sharedVision[player2];
}
/****************************************************************************/
bool getSharedControl(int player)
{
    return getSharedControl(localPlayerID, player);
}
/****************************************************************************/
bool getSharedResources(int player)
{
    return getSharedResources(localPlayerID, player);
}
/****************************************************************************/
bool getSharedVision(int player)
{
    return getSharedVision(localPlayerID, player);
}
/****************************************************************************/
float getNetProfit(int player, int resource)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("getNetProfit: invalid player");
        return 0;
    }
    if(resource<0 || resource>=MAX_RESOURCES)
    {
        bug("getNetProfit: invalid resource");
        return 0;
    }
#endif
    return players[player].revenues[resource]-
        players[player].costs[resource]+
        players[player].foreignAid[resource];
}
/****************************************************************************/
bool isResourceAvailable(int player, int resource)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("isResourceAvailable: invalid player");
        return false;
    }
    if(resource<0 || resource>=MAX_RESOURCES)
    {
        bug("isResourceAvailable: invalid resource");
        return false;
    }
#endif
    return players[player].resourceAvailable[resource];
}
/****************************************************************************/
bool isResourceAvailable(int player, int resource, float amount)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("isResourceAvailable: invalid player");
        return false;
    }
    if(resource<0 || resource>=MAX_RESOURCES)
    {
        bug("isResourceAvailable: invalid resource");
        return false;
    }
#endif
    if(amount<=0)
        return true;
    return (players[player].resources[resource]>=amount);
}
/****************************************************************************/
bool canCollectResource(int player, int resource)
{
#ifdef DEBUG
    if(player<0 || player>=MAX_PLAYERS)
    {
        bug("isResourceFull: invalid player");
        return false;
    }
    if(resource<0 || resource>=MAX_RESOURCES)
    {
        bug("isResourceFull: invalid resource");
        return false;
    }
#endif
    return players[player].resourceCollectable[resource];
}
/****************************************************************************/
//Determine if anyone is sharing resources with the local player
bool canReceiveForeignAid()
{
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("canReceiveForeignAid: invalid localPlayerID");
        return false;
    }
#endif
    for(int i=0;i<MAX_PLAYERS;i++)
        if(players[i].sharedResources[localPlayerID])
            return true;
    return false;
}
/****************************************************************************/
float getCombinedRevenue(int resource)
{
    float total=0;
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getCombinedRevenue: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getCombinedRevenue: invalid resource");
        return 0;
    }
#endif
    for(int i=0;i<MAX_PLAYERS;i++)
        if(i==localPlayerID || players[i].sharedResources[localPlayerID])
            total+=players[i].revenues[resource];

    return total;
}
/****************************************************************************/
float getCombinedCost(int resource)
{
    float total=0;
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getCombinedCost: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getCombinedCost: invalid resource");
        return 0;
    }
#endif
    for(int i=0;i<MAX_PLAYERS;i++)
        if(i==localPlayerID || players[i].sharedResources[localPlayerID])
            total+=players[i].costs[resource];

    return total;
}
/****************************************************************************/
float getCombinedAmount(int resource)
{
    float total=0;
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getCombinedAmount: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getCombinedAmount: invalid resource");
        return 0;
    }
#endif
    for(int i=0;i<MAX_PLAYERS;i++)
        if(i==localPlayerID || players[i].sharedResources[localPlayerID])
            total+=players[i].resources[resource];

    //Clamp the value at zero (see getResourceAmount):
    return MAX(total,0);
}
/****************************************************************************/
float getCombinedCapacity(int resource)
{
    float total=0;
#ifdef DEBUG
    if(localPlayerID<0 || localPlayerID>=MAX_PLAYERS)
    {
        bug("getCombinedCapacity: invalid localPlayerID");
        return 0;
    }
    if(resource<0||resource>=MAX_RESOURCES)
    {
        bug("getCombinedCapacity: invalid resource");
        return 0;
    }
#endif
    for(int i=0;i<MAX_PLAYERS;i++)
        if(i==localPlayerID || players[i].sharedResources[localPlayerID])
            total+=players[i].maxResources[resource];

    return total;
}
/****************************************************************************/
void revealCircle(int playerID, int x, int y, int radius)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("revealCircle: invalid playerID");
        return;
    }
    if(players[playerID].fogOfWar==NULL)
    {
        bug("revealCircle: fogOfWar==NULL");
        return;
    }
#endif
    if(radius<=0)
        return;
    int x1=MAX(x-radius,0);
    int x2=MIN(x+radius,fogOfWarWidth-1);
    int y1=MAX(y-radius,0);
    int y2=MIN(y+radius,fogOfWarHeight-1);
    int i,j,distanceSquared;
    int radiusSquared=radius*radius;
    for(i=x1;i<=x2;i++)
        for(j=y1;j<=y2;j++)
        {
            distanceSquared=(i-x)*(i-x) + (j-y)*(j-y);
            if(distanceSquared<radiusSquared)
                players[playerID].fogOfWar[i+j*fogOfWarWidth]++;
        }
}
/****************************************************************************/
void concealCircle(int playerID, int x, int y, int radius)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("concealCircle: invalid playerID");
        return;
    }
    if(players[playerID].fogOfWar==NULL)
    {
        bug("concealCircle: fogOfWar==NULL");
        return;
    }
#endif
    if(radius<=0)
        return;
    int x1=MAX(x-radius,0);
    int x2=MIN(x+radius,fogOfWarWidth-1);
    int y1=MAX(y-radius,0);
    int y2=MIN(y+radius,fogOfWarHeight-1);
    int i,j,distanceSquared;
    int radiusSquared=radius*radius;
    for(i=x1;i<=x2;i++)
        for(j=y1;j<=y2;j++)
        {
            distanceSquared=(i-x)*(i-x) + (j-y)*(j-y);
            if(distanceSquared<radiusSquared)
                players[playerID].fogOfWar[i+j*fogOfWarWidth]--;
        }
}
/****************************************************************************/
bool isTileVisible(int playerID, int x, int y)
{
#ifdef DEBUG
    if(playerID<0 || playerID>=MAX_PLAYERS)
    {
        bug("isTileVisible: invalid playerID");
        return false;
    }
    if(players[playerID].fogOfWar==NULL)
    {
        bug("isTileVisible: fogOfWar==NULL");
        return false;
    }
    if(x<0 || y<0 || x>=fogOfWarWidth || y>=fogOfWarHeight)
    {
        bug("isTileVisible: invalid coordinates");
        return false;
    }
#endif
    return players[playerID].fogOfWar[x+y*fogOfWarWidth]>0;
}
/****************************************************************************/
bool isPointVisible(int playerID, const tVector2D& v)
{
    return isTileVisible(playerID, (int)v.x, (int)v.y);
}
/****************************************************************************/
tDeposit* createDeposit(int resource, const tVector2D& center, float radius, float amount, tWorldTime t)
{
    tTileInfo ti;
    tVector3D v=to3D(center);
    tDeposit *d;
    if(terrain)
    {
        ti=terrain->getTileInfo(center,DOMAIN_NONE,0);
        v.z=ti.height;
    }
    d=mwNew tDeposit(nextDepositID++,resource,v,radius,amount);
    deposits.push_back(d);
    return d;
}
/****************************************************************************/
void destroyDeposit(tDeposit *d, tWorldTime t)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(d))
        return;
#endif
    d->destroyed=true;
    for(int i=0; i<units.size(); i++)
        units[i]->onDepositDepleted(d, t);
}
/****************************************************************************/
tDeposit *findDepositAt(const tVector2D& p, int resourceID)
{
    //Find the closest deposit with a matching resourceID
    float distance;
    for(int i=0;i<deposits.size();i++)
        if(!deposits[i]->destroyed && deposits[i]->resource==resourceID)
        {
            distance=(p-to2D(deposits[i]->center)).lengthSquared();
            if(distance<deposits[i]->radius*deposits[i]->radius)
                return deposits[i];
        }
    return NULL;
}
/****************************************************************************/
void adjustExtractionRate(tDeposit *d, float amount)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(d))
        return;
#endif
    d->rate+=amount;
#ifdef DEBUG
    if(d->rate<0)
    {
        bug("adjustExtractionRate: rate < 0");
        d->rate=0;
    }
#endif
}
/****************************************************************************/
bool isWithinRange(tDeposit *d, const tVector2D& p)
{
#ifdef DEBUG
    if(MEMORY_VIOLATION(d))
        return false;
#endif
    return (to2D(d->center)-p).lengthSquared()<d->radius*d->radius;
}
/****************************************************************************/
int playerLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    //Check all player names before examining user names to resolve the unfortunate
    //situation where a computer player has the same name as a human player.
    for(i=0;i<MAX_PLAYERS;i++)
        if(STREQUAL(STRTOLOWER(players[i].name),n))
            return i;
    for(i=0;i<MAX_PLAYERS;i++)
        if(players[i].assignedUser && STREQUAL(STRTOLOWER(players[i].assignedUser->name),n))
            return i;
    if(exact)
        return -1;
    for(i=0;i<MAX_PLAYERS;i++) //STRPREFIX is case-insensitive
        if(STRPREFIX(n,players[i].name) || players[i].assignedUser && STRPREFIX(n,players[i].assignedUser->name))
            return i;
    return -1;
}
/****************************************************************************/
int resourceLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0;i<MAX_RESOURCES;i++)
        if(STREQUAL(STRTOLOWER(resources[i].name),n))
            return i;
    if(exact)
        return -1;
    for(i=0;i<MAX_RESOURCES;i++)
        if(STRPREFIX(n,resources[i].name))
            return i;
    return -1;
}
/****************************************************************************/
int armorClassLookup(const tString& name, bool exact)
{
    int i;
    tString n=STRTOLOWER(name);
    for(i=0;i<MAX_ARMOR_CLASSES;i++)
        if(STREQUAL(STRTOLOWER(armorClasses[i].name),n))
            return i;
    if(exact)
        return -1;
    for(i=0;i<MAX_ARMOR_CLASSES;i++)
        if(STRPREFIX(n,armorClasses[i].name))
            return i;
    return -1;
}
/****************************************************************************/
//Returns true if the map and world files haven't changed.
//Returns false if the function calls chooseMap
bool inspectFiles()
{
    bool worldChanged=false;
    bool mapChanged=false;
    if(worldLoaded && !worldFile.exists()) //World file has changed or no longer exists!
        worldChanged=true;
    if(mapLoaded && !mapFile.exists()) //Map file has changed or no longer exists!
        mapChanged=true;
    //chooseMap will generate an onParseError if any errors occur.
    if(mapChanged||worldChanged)
    {
        if(chooseMap(mapFile.getFullName(), worldFile.getFullName(), /*resetPlayers=*/false))
        {
            //Don't display these messages if one of the files was deleted or
            //chooseMap encountered an error.
            if(mapChanged)
                onMapChanged();
            if(worldChanged)
                onWorldChanged();
        }
        else if(gameWaiting) //chooseMap failed for some reason, so abort any waiting games:
        {
            gameWaiting=false;
            onAbortGame();
        }
        return false;
    }
    return true;
}

bool isTerrainIntervalFixed()
{
    if(terrain)
        return terrain->isIntervalFixed();
    else
        return false;
}
int getTerrainInterval()
{
    if(terrain)
        return terrain->getInterval();
    else
        return 0;
}
void setTerrainInterval(int _interval)
{
    if(terrain)
        terrain->setInterval(_interval);
}
void resetTerrainInterval()
{
    if(terrain)
        terrain->resetInterval();
}
void setShadeMapSize(int size)
{
#ifdef DEBUG
    if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
    {
        bug("setShadeMapSize: invalid size");
        return;
    }
#endif
    if(shadeMapSize!=size)
    {
        shadeMapSize=size;
        if(terrain)
            terrain->onSetShadeMapSize(size);
    }
}
void setAlphaMapSize(int size)
{
#ifdef DEBUG
    if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
    {
        bug("setAlphaMapSize: invalid size");
        return;
    }
#endif
    if(alphaMapSize!=size)
    {
        alphaMapSize=size;
        if(terrain)
            terrain->onSetAlphaMapSize(size);
    }
}
void setWaterMapSize(int size)
{
#ifdef DEBUG
    if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
    {
        bug("setWaterMapSize: invalid size");
        return;
    }
#endif
    if(waterMapSize!=size)
    {
        waterMapSize=size;
        if(terrain)
            terrain->onSetWaterMapSize(size);
    }
}
void setShadowMapSize(int size)
{
#ifdef DEBUG
    if(size!=0 && size!=256 && size!=512 && size!=1024 && size!=2048)
    {
        bug("setShadowMapSize: invalid size");
        return;
    }
#endif
    if(shadowMapSize!=size)
    {
        shadowMapSize=size;
        refreshShadowTexture();
    }
}
void refreshShadowTexture()
{
    float borderColor[4]={1,1,1,1};

    if(shadowMapSize>0 && shadowTexture>0)
    {
        glEnable(GL_TEXTURE_2D);
        mglBindTexture(shadowTexture);
        unsigned char *data=mwNew unsigned char[shadowMapSize*shadowMapSize*3];
        memset(data,255,shadowMapSize*shadowMapSize*3);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, shadowMapSize, shadowMapSize, 0,
            GL_RGB, GL_UNSIGNED_BYTE, data);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameterfv(GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, borderColor);
        glDisable(GL_TEXTURE_2D);
        mwDelete[] data;
    }
}
// This is a simple (although fast) method of calculating the shade of any
// point on any plane based on the ambient and diffuse components of each
// light.
float calculateShade(tVector3D& pos, tVector3D& normal)
{
    float shade=ambientLight[0];
    for(int i=0;i<lights.size();i++)
    {
        tLight& l=lights[i];
        tVector3D lightVec={l.position[0],l.position[1],l.position[2]};
        shade+=l.ambient[0];
        if(l.position[3]==0)
            shade+=l.diffuse[0]*normal.dot(lightVec);
        else
            shade+=l.diffuse[0]*normal.dot(lightVec-pos);
    }
    return shade;
}
void addLightsToCacheHeader(tCache& cache)
{
    for(int i=0;i<lights.size();i++)
        cache.addDataToHeader((char*)&lights[i],sizeof(tLight));
    cache.addDataToHeader((char*)ambientLight,sizeof(ambientLight));
}
tLight::tLight() : spotExponent(0), spotCutoff(180), constantAttenuation(1),
    linearAttenuation(0), quadraticAttenuation(0)
{
    const float _ambient[4]={0,0,0,1};
    const float _diffuse[4]={1,1,1,1};
    const float _specular[4]={1,1,1,1};
    const float _position[4]={0,0,1,0};
    const float _spotDirection[3]={0,0,-1};
    int i;

    for(i=0;i<4;i++)
        ambient[i]=_ambient[i];
    for(i=0;i<4;i++)
        diffuse[i]=_diffuse[i];
    for(i=0;i<4;i++)
        specular[i]=_specular[i];
    for(i=0;i<4;i++)
        position[i]=_position[i];
    for(i=0;i<3;i++)
        spotDirection[i]=_spotDirection[i];
}
void refreshLights()
{
    int i;
    for(i=0;i<lights.size();i++)
    {
        tLight& l=lights[i];
        GLenum lightName=getLightName(i);

        glLightfv(lightName,GL_AMBIENT,l.ambient);
        glLightfv(lightName,GL_DIFFUSE,l.diffuse);
        glLightfv(lightName,GL_SPECULAR,l.specular);
        glLightfv(lightName,GL_POSITION,l.position);
        glLightfv(lightName,GL_SPOT_DIRECTION,l.spotDirection);
        glLightf(lightName,GL_SPOT_EXPONENT,l.spotExponent);
        glLightf(lightName,GL_SPOT_CUTOFF,l.spotCutoff);
        glLightf(lightName,GL_CONSTANT_ATTENUATION,l.constantAttenuation);
        glLightf(lightName,GL_LINEAR_ATTENUATION,l.linearAttenuation);
        glLightf(lightName,GL_QUADRATIC_ATTENUATION,l.quadraticAttenuation);
        glEnable(lightName);
    }

    glLightModelfv(GL_LIGHT_MODEL_AMBIENT,ambientLight);
}

/****************************************************************************
                         Global Parsing Functions
 ****************************************************************************/
int parsePlayer(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    int i=playerLookup(str,/*exact=*/false);
    if(i<0)
        throw tString("Unrecognized player '")+str+"'";
    if(!players[i].exists)
        throw tString("This player doesn't exist");
    return i;
}

tVector2D parseWorldPos(const char *fieldName, tStream& s) throw(tString)
{
    tVector2D v=parseVector2D(fieldName, s);
    if(!isValid(v,0))
        throw tString("The world position you have entered is not on the map");
    return v;
}

eAlliance parseAlliance(const char *fieldName, tStream& s) throw(tString)
{
    tString  str=parseString(fieldName, s);
    if(strPrefix(str,"ally"))
        return ALLIANCE_ALLY;
    if(strPrefix(str,"neutral"))
        return ALLIANCE_NEUTRAL;
    if(strPrefix(str,"enemy"))
        return ALLIANCE_ENEMY;

    throw tString("Unrecognized alliance '")+str+"'";
}

int parseResource(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    int i=resourceLookup(str,/*exact=*/false);
    if(i<0)
        throw tString("Unrecognized resource '")+str+"'";
    if(!resources[i].exists)
        throw tString("This resource doesn't exist");
    return i;
}

int parseArmorClass(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName, s);
    int i=armorClassLookup(str,/*exact=*/false);
    if(i<0)
        throw tString("Unrecognized armor class '")+str+"'";
    if(!armorClasses[i].exists)
        throw tString("This armor class doesn't exist");
    return i;
}

void parseResourceBlock(float r[], tStream& s) throw(tString)
{
    int i;
    float amount;
    //Check the array the best we can...
#ifdef DEBUG
    if(MEMORY_VIOLATION(r))
        return;
#endif

    for(int i=0;i<MAX_RESOURCES;i++)
        r[i]=0;

    if(s.beginBlock())
    {
        while(!s.endBlock())
        {
			i=parseResource("resource name",s);
#ifdef DEBUG
			if(i<0 || i>=MAX_RESOURCES)
			{
				bug("parseResources: invalid index");
				return;
			}
#endif
			
			s.matchOptionalToken("=");
            amount=parseFloatMin("resource amount",0,s);
			r[i]=amount;

			s.endStatement();
		}
    }
}

/****************************************************************************
                         Internal Parsing Functions
 ****************************************************************************/
tUnit *parseUnit(const char *fieldName, tStream& s) throw(tString)
{
    long id;
    tUnit *u;
    //If multiple units are selected, we choose a unit at random.
    if(strPrefix(s.peekAhead(),"selection"))
    {
        parseString(fieldName,s); //Discard the token
        if(selectedUnits.size()>0)
            return selectedUnits[0];
        else
            throw tString("You haven't selected any units!");
    }
    id=parseLong(fieldName, s);
    u=unitLookup(id);
    if(u==NULL)
        throw tString("No unit with this ID exists");
    return u;
}

void parseUnits(const char *fieldName, vector<long>& u, tStream& s) throw(tString)
{
    long i, firstUnitID, lastUnitID;
    
    do //stream must contain at least one token
    {
        tString str=parseString("unit ID",s);

        if(strPrefix(str,"selection")) //copy all selected units to container
        {
            for(i=0;i<selectedUnits.size();i++)
                u.push_back(selectedUnits[i]->getID());
        }
        else if(strPrefix(str,"all")) //copy all units to container
        {
            for(i=0;i<units.size();i++)
                if(units[i]->getState()>STATE_DESTROYED)
                    u.push_back(units[i]->getID());
        }
        else if(strPrefix(str,"mine")) //copy all units belonging to local player to container
        {
            for(i=0;i<units.size();i++)
                if(units[i]->getOwner()==localPlayerID && units[i]->getState()>STATE_DESTROYED)
                    u.push_back(units[i]->getID());
        }
        else if(strPrefix(str,"allies")) //copy all allied units to container
        {
            for(i=0;i<units.size();i++)
                if(getAlliance(units[i]->getOwner())<=ALLIANCE_ALLY && units[i]->getState()>STATE_DESTROYED)
                    u.push_back(units[i]->getID());
        }
        else if(strPrefix(str,"enemies")) //copy all enemy units to container
        {
            for(i=0;i<units.size();i++)
                if(getAlliance(units[i]->getOwner())>=ALLIANCE_NEUTRAL && units[i]->getState()>STATE_DESTROYED)
                    u.push_back(units[i]->getID());
        }
		else
		{
			firstUnitID=parseLong("unit id",s);
			if(s.matchOptionalToken("-"))
			{
				lastUnitID=parseLong("unit id",s);
				for(i=firstUnitID;i<=lastUnitID;i++)
					u.push_back(i);
			}
			else
				u.push_back(firstUnitID);
        }
    }
    while(s.matchOptionalToken(","));

    if(u.size()==0)
        throw tString("You must enter at least one unit.");
}

int parseShellPlayer(const char *fieldName, tStream& s) throw(tString)
{
    if(strPrefix(s.peekAhead(),"me") || strPrefix(s.peekAhead(),"self"))
    {
        parseString(fieldName,s); //Discard the token
        return localPlayerID;
    }
    return parsePlayer(fieldName, s);
}

void parsePlayers(const char *fieldName, vector<int>& p, tStream& s) throw(tString)
{
    int i;

    do //stream must contain at least one token
    {
        tString str=parseString("player ID",s);

        i=playerLookup(str,/*exact=*/false);
        if(i>=0)
        {
            if(!players[i].exists)
                throw tString("This player doesn't exist");
            p.push_back(i);
        }
        else if(strPrefix(str,"me") || strPrefix(str,"self")) //copy local player into container
        {
            p.push_back(localPlayerID);
        }
        else if(strPrefix(str,"all")) //copy all players into container
        {
            for(i=0;i<MAX_PLAYERS;i++)
                if(players[i].exists)
                    p.push_back(i);
        }
        else if(strPrefix(str,"allies"))
        {
            for(i=0;i<MAX_PLAYERS;i++)
                if(players[localPlayerID].alliances[i]<=ALLIANCE_ALLY && players[i].exists)
                    p.push_back(i);
        }
        else if(strPrefix(str,"enemies"))
        {
            for(i=0;i<MAX_PLAYERS;i++)
                if(players[localPlayerID].alliances[i]>=ALLIANCE_NEUTRAL && players[i].exists)
                    p.push_back(i);
        }
        else
            throw tString("Unrecognized player '")+str+"'";
    }
    while(s.matchOptionalToken(","));

    if(p.size()==0)
        throw tString("You must enter at least one player.");
}

tVector2D parseShellPos(const char *fieldName, tStream& s) throw(tString)
{
    if(strPrefix(s.peekAhead(),"here"))
    {
        parseString(fieldName,s); //Discard the token
        return to2D(getViewCenter());
    }
    return parseWorldPos(fieldName, s);
}

void parseResources(const char *fieldName, vector<int>& r, tStream& s) throw(tString)
{
    int i;

    do //string must contain at least one token
    {
        tString str=parseString("resource ID",s);

        i=resourceLookup(str,/*exact=*/false);
        if(i>=0)
        {
            if(!resources[i].exists)
                throw tString("This resource doesn't exist");
            r.push_back(i);
        }
        else if(strPrefix(str,"all")) //copy all resources into container
        {
            for(i=0;i<MAX_RESOURCES;i++)
                if(resources[i].exists)
                    r.push_back(i);
        }
        else
            throw tString("Unrecognized resource '")+str+"'";
    }
    while(s.matchOptionalToken(","));

    if(r.size()==0)
        throw tString("You must enter at least one resource.");
}

tVector2D parseVelocity(const char *fieldName, tStream& s) throw(tString)
{
    float angle;
    float length;

    if(strPrefix(s.peekAhead(),"up"))
        angle=M_PI/2-mapRotation;
    else if(strPrefix(s.peekAhead(),"left"))
        angle=M_PI-mapRotation;
    else if(strPrefix(s.peekAhead(),"right"))
        angle=0-mapRotation;
    else if(strPrefix(s.peekAhead(),"down"))
        angle=M_PI*3/2-mapRotation;
    else
        return parseVector2D(fieldName, s);

    parseString(fieldName,s); //Discard the token
    length=parseFloatMin("vector length",0,s);
    tVector2D v={cos(angle)*length,sin(angle)*length};
    return v;
}

tVector2D parseForward(const char *fieldName, tStream& s) throw(tString)
{
    float angle;

    if(strPrefix(s.peekAhead(),"up"))
        angle=M_PI/2-mapRotation;
    else if(strPrefix(s.peekAhead(),"left"))
        angle=M_PI-mapRotation;
    else if(strPrefix(s.peekAhead(),"right"))
        angle=0-mapRotation;
    else if(strPrefix(s.peekAhead(),"down"))
        angle=M_PI*3/2-mapRotation;
    else
        return parseVector2D(fieldName, s);

    parseString(fieldName,s); //Discard the token
    tVector2D v={cos(angle),sin(angle)};
    return v;
}

ePlayer parsePlayerType(const char *fieldName, tStream& s) throw(tString)
{
    tString str=parseString(fieldName,s);
    if(strPrefix(str,"human"))
        return PLAYER_HUMAN;
    else if(strPrefix(str,"computer"))
        return PLAYER_COMPUTER;
    else if(strPrefix(str,"hidden"))
        return PLAYER_HIDDEN;

    throw tString("Unrecognized player type '")+str+"'";
}



/****************************************************************************/
//Player colors
//color assigned to each player
const tRGB playerColors[MAX_PLAYERS]=
{
    {0,0,255},
    {0,255,0},
    {255,0,0},
    {255,255,0},
    {255,0,255},
    {0,255,255},
    {255,128,0},
    {255,255,255}
};

//Alliance colors
//color assigned to each alliance
const tRGB allianceColors[4]=
{
    {0,255,0},    //self
    {255,255,0},  //allied
    {0,0,255},    //neutral
    {255,0,0}     //enemy
};

//Resource colors
//color assigned to each resource in resource bar and on world map
const tRGB resourceColors[MAX_RESOURCES]=
{
    {255,0,255},
    {255,0,0},
    {255,255,0},
    {0,255,255},
    {255,128,0},
    {0,0,255},
    {0,255,0},
    {0,0,0}
};

const char* allianceNames[4]=
{
    "Self",
    "Ally",
    "Neutral",
    "Enemy"
};
